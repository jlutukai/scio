class GeneralResponse {
  String? message;
  String? erroMessage;
  bool? error;

  GeneralResponse({this.message, this.erroMessage, this.error});

  GeneralResponse.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    erroMessage = json['erroMessage'];
    error = json['error'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['erroMessage'] = this.erroMessage;
    data['error'] = this.error;
    return data;
  }
}
