import 'local/localModels.dart';

class PlannerResponse {
  List<PlannerData>? data;
  String? message;
  bool? errors;

  PlannerResponse({this.data, this.message, this.errors});

  PlannerResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <PlannerData>[];
      json['data'].forEach((v) {
        data!.add(new PlannerData.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = this.message;
    data['errors'] = this.errors;
    return data;
  }
}
