class UpdatePlannerResponse {
  int? data;
  String? message;
  String? erroMessage;
  bool? error;

  UpdatePlannerResponse(
      {this.data, this.message, this.erroMessage, this.error});

  UpdatePlannerResponse.fromJson(Map<String, dynamic> json) {
    data = json['data'];
    message = json['message'];
    erroMessage = json['erroMessage'];
    error = json['error'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['data'] = this.data;
    data['message'] = this.message;
    data['erroMessage'] = this.erroMessage;
    data['error'] = this.error;
    return data;
  }
}
