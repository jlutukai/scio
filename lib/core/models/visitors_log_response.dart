import 'local/localModels.dart';

class VisitorsLogResponse {
  List<Visitor>? data;
  String? message;
  bool? errors;

  VisitorsLogResponse({this.data, this.message, this.errors});

  VisitorsLogResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <Visitor>[];
      json['data'].forEach((v) {
        data!.add(new Visitor.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = this.message;
    data['errors'] = this.errors;
    return data;
  }
}
