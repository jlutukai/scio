class ScioUrlResponse {
  ScioUrl? scioUrl;
  String? message;

  ScioUrlResponse({this.scioUrl, this.message});

  ScioUrlResponse.fromJson(Map<String, dynamic> json) {
    scioUrl = json['scio_url'] != null
        ? new ScioUrl.fromJson(json['scio_url'])
        : null;
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.scioUrl != null) {
      data['scio_url'] = this.scioUrl!.toJson();
    }
    data['message'] = this.message;
    return data;
  }
}

class ScioUrl {
  Data? data;

  ScioUrl({this.data});

  ScioUrl.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  String? id;
  String? name;
  String? url;
  String? dropLat;
  String? dropLong;
  String? status;
  String? type;

  Data(
      {this.id,
        this.name,
        this.url,
        this.dropLat,
        this.dropLong,
        this.status,
        this.type});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    name = json['name'];
    url = json['url'];
    dropLat = json['drop_lat'];
    dropLong = json['drop_long'];
    status = json['status'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['name'] = this.name;
    data['url'] = this.url;
    data['drop_lat'] = this.dropLat;
    data['drop_long'] = this.dropLong;
    data['status'] = this.status;
    data['type'] = this.type;
    return data;
  }
}
