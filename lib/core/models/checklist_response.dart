class CheckListResponse {
  List<CheckListData>? data;
  String? message;
  bool? errors;

  CheckListResponse({this.data, this.message, this.errors});

  CheckListResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <CheckListData>[];
      json['data'].forEach((v) {
        data!.add(new CheckListData.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = this.message;
    data['errors'] = this.errors;
    return data;
  }
}

class CheckListData {
  String? id;
  String? plannerId;
  String? name;
  String? questionType;
  String? type;
  String? category;
  String? status;

  CheckListData(
      {this.id,
      this.plannerId,
      this.name,
      this.questionType,
      this.type,
      this.category,
      this.status});

  CheckListData.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    plannerId = json['planner_id'];
    name = json['name'];
    questionType = json['question_type'];
    type = json['type'];
    category = json['category'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['planner_id'] = this.plannerId;
    data['name'] = this.name;
    data['question_type'] = this.questionType;
    data['type'] = this.type;
    data['category'] = this.category;
    data['status'] = this.status;
    return data;
  }
}
