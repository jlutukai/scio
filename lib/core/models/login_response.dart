class LoginResponse {
  LoginData? loginData;
  String? message;

  LoginResponse({this.loginData, this.message});

  LoginResponse.fromJson(Map<String, dynamic> json) {
    loginData = json['login_data'] != null
        ? new LoginData.fromJson(json['login_data'])
        : null;
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.loginData != null) {
      data['login_data'] = this.loginData!.toJson();
    }
    data['message'] = this.message;
    return data;
  }
}

class LoginData {
  Data? data;

  LoginData({this.data});

  LoginData.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  String? status;
  String? name;
  String? id;

  Data({this.status, this.name, this.id});

  Data.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    name = json['name'];
    id = json['Id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['name'] = this.name;
    data['Id'] = this.id;
    return data;
  }
}
