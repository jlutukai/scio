class GetSiteResponse {
  List<SiteData>? data;
  String? message;
  bool? errors;

  GetSiteResponse({this.data, this.message, this.errors});

  GetSiteResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <SiteData>[];
      json['data'].forEach((v) {
        data!.add(new SiteData.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = this.message;
    data['errors'] = this.errors;
    return data;
  }
}

class SiteData {
  String? id;
  String? name;
  String? longitude;
  String? latitude;
  String? status;

  SiteData({this.id, this.name, this.longitude, this.latitude, this.status});

  SiteData.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    name = json['name'];
    longitude = json['longitude'];
    latitude = json['latitude'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['name'] = this.name;
    data['longitude'] = this.longitude;
    data['latitude'] = this.latitude;
    data['status'] = this.status;
    return data;
  }
}
