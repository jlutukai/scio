class GetFormsResponse {
  List<FormData>? data;
  String? message;
  bool? errors;

  GetFormsResponse({this.data, this.message, this.errors});

  GetFormsResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <FormData>[];
      json['data'].forEach((v) {
        data!.add(new FormData.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = this.message;
    data['errors'] = this.errors;
    return data;
  }
}

class FormData {
  String? id;
  String? name;
  String? description;
  List<Items>? items;
  List<CustomItems>? customItems;

  FormData(
      {this.id, this.name, this.description, this.items, this.customItems});

  FormData.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    name = json['name'];
    description = json['description'];
    if (json['items'] != null) {
      items = <Items>[];
      json['items'].forEach((v) {
        items!.add(new Items.fromJson(v));
      });
    }
    if (json['custom_items'] != null) {
      customItems = <CustomItems>[];
      json['custom_items'].forEach((v) {
        customItems!.add(new CustomItems.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['name'] = this.name;
    data['description'] = this.description;
    if (this.items != null) {
      data['items'] = this.items!.map((v) => v.toJson()).toList();
    }
    if (this.customItems != null) {
      data['custom_items'] = this.customItems!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Items {
  String? id;
  String? name;
  String? formId;
  String? type;
  String? status;

  Items({this.id, this.name, this.formId, this.type, this.status});

  Items.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    name = json['name'];
    formId = json['form_id'];
    type = json['type'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['name'] = this.name;
    data['form_id'] = this.formId;
    data['type'] = this.type;
    data['status'] = this.status;
    return data;
  }
}

class CustomItems {
  String? id;
  String? plannerId;
  String? formId;
  String? name;
  String? type;

  CustomItems({this.id, this.plannerId, this.formId, this.name, this.type});

  CustomItems.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    plannerId = json['planner_id'];
    formId = json['form_id'];
    name = json['name'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['planner_id'] = this.plannerId;
    data['form_id'] = this.formId;
    data['name'] = this.name;
    data['type'] = this.type;
    return data;
  }
}
