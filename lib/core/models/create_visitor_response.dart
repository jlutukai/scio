import 'local/localModels.dart';

class CreateVisitorResponse {
  Visitor? data;
  bool? error;
  String? message;

  CreateVisitorResponse({this.data, this.error, this.message});

  CreateVisitorResponse.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Visitor.fromJson(json['data']) : null;
    error = json['error'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    data['error'] = this.error;
    data['message'] = this.message;
    return data;
  }
}
