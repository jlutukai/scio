class PlannerReportResponse {
  List<PlannerReportData>? data;
  String? message;
  bool? errors;

  PlannerReportResponse({this.data, this.message, this.errors});

  PlannerReportResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <PlannerReportData>[];
      json['data'].forEach((v) {
        data!.add(new PlannerReportData.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = this.message;
    data['errors'] = this.errors;
    return data;
  }
}

class PlannerReportData {
  String? id;
  String? questionId;
  String? plannerId;
  String? categoryId;
  String? response;
  String? date;
  String? answeredBy;
  String? defaultCustom;
  String? shiftId;
  String? status;

  PlannerReportData(
      {this.id,
      this.questionId,
      this.plannerId,
      this.categoryId,
      this.response,
      this.date,
      this.answeredBy,
      this.defaultCustom,
      this.shiftId,
      this.status});

  PlannerReportData.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    questionId = json['question_id'];
    plannerId = json['planner_id'];
    categoryId = json['category_id'];
    response = json['response'];
    date = json['date'];
    answeredBy = json['answered_by'];
    defaultCustom = json['default_custom'];
    shiftId = json['shift_id'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['question_id'] = this.questionId;
    data['planner_id'] = this.plannerId;
    data['category_id'] = this.categoryId;
    data['response'] = this.response;
    data['date'] = this.date;
    data['answered_by'] = this.answeredBy;
    data['default_custom'] = this.defaultCustom;
    data['shift_id'] = this.shiftId;
    data['status'] = this.status;
    return data;
  }
}
