import 'package:scio_security/utils/useful.dart';

class TimeSheetResponse {
  List<TimeSheetData>? data;
  String? message;
  bool? errors;

  TimeSheetResponse({this.data, this.message, this.errors});

  TimeSheetResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = <TimeSheetData>[];
      json['data'].forEach((v) {
        data!.add(new TimeSheetData.fromJson(v));
      });
    }
    message = json['message'];
    errors = json['errors'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.map((v) => v.toJson()).toList();
    }
    data['message'] = this.message;
    data['errors'] = this.errors;
    return data;
  }
}

class TimeSheetData {
  String? status;
  String? date;
  String? shiftId;
  String? employeeId;
  String? id;
  String? staffPhone;
  String? name;
  bool? modified;
  String? reason;
  String? imageIn;
  String? imageOut;
  String? biometricId;
  String? biometricIn;
  String? biometricOut;
  String? timeIn;
  String? timeOut;
  String? fileName;
  String? commentIn;
  String? commentOut;
  int syncStatus = syncedStatus;

  TimeSheetData(
      {this.status,
      this.date,
      this.shiftId,
      this.employeeId,
      this.id,
      this.staffPhone,
      this.name,
      this.modified,
      this.reason,
      this.imageIn,
      this.imageOut,
      this.biometricId,
      this.biometricIn,
      this.biometricOut,
      this.timeIn,
      this.timeOut,
      this.commentIn,
      this.commentOut,
      this.fileName,
      required this.syncStatus});

  TimeSheetData.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    date = json['date'];
    shiftId = json['shift_id'];
    employeeId = json['employee_id'];
    id = json['Id'];
    staffPhone = json['staff_phone'];
    name = json['name'];
    reason = json['reason'];
    timeIn = json['time_in'];
    timeOut = json['time_out'];
    commentIn = json['comment_in'];
    commentOut = json['comment_out'];
    fileName = json['file_name'];
  }

  TimeSheetData.fromEntity(Map<String, dynamic> json) {
    id = json['timeSheetId'].toString();
    status = json['status'];
    date = json['date'];
    shiftId = json['shiftId'].toString();
    employeeId = json['employeeId'].toString();
    staffPhone = json['staffPhone'];
    name = json['name'];
    modified = (json['modified'] == 1);
    reason = json['reason'];
    imageIn = json['imageIn'];
    imageOut = json['imageOut'];
    biometricId = json['biometricId'];
    biometricIn = json['biometricIn'];
    biometricOut = json['biometricOut'];
    timeIn = json['timeIn'];
    timeOut = json['timeOut'];
    fileName = json['fileName'];
    commentIn = json['commentIn'];
    commentOut = json['commentOut'];
    syncStatus = json['syncStatus'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['date'] = this.date;
    data['shift_id'] = this.shiftId;
    data['employee_id'] = this.employeeId;
    data['Id'] = this.id;
    data['staff_phone'] = this.staffPhone;
    data['name'] = this.name;
    data['reason'] = this.reason;
    data['time_in'] = this.timeIn;
    data['time_out'] = this.timeOut;
    data['comment_in'] = this.commentIn;
    data['comment_out'] = this.commentOut;
    data['file_name'] = this.fileName;
    return data;
  }

  Map<String, dynamic> toEntity() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['timeSheetId'] = this.id;
    data['status'] = this.status;
    data['date'] = this.date;
    data['shiftId'] = this.shiftId;
    data['employeeId'] = this.employeeId;
    data['staffPhone'] = this.staffPhone;
    data['name'] = this.name;
    data['modified'] = this.modified;
    data['reason'] = this.reason;
    data['imageIn'] = this.imageIn;
    data['imageOut'] = this.imageOut;
    data['biometricId'] = this.biometricId;
    data['biometricIn'] = this.biometricIn;
    data['biometricOut'] = this.biometricOut;
    data['timeIn'] = this.timeIn;
    data['timeOut'] = this.timeOut;
    data['fileName'] = this.fileName;
    data['commentIn'] = this.commentIn;
    data['commentOut'] = this.commentOut;
    data['syncStatus'] = this.syncStatus;
    return data;
  }
}
