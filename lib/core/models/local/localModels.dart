import 'package:hive/hive.dart';

part 'localModels.g.dart';

@HiveType(typeId: 1)
class Visitor {
  @HiveField(1)
  String? id;
  @HiveField(2)
  String? visitorName;
  @HiveField(3)
  String? hostName;
  @HiveField(4)
  String? department;
  @HiveField(5)
  String? designation;
  @HiveField(6)
  String? noVisitors;
  @HiveField(7)
  String? documentNumber;
  @HiveField(8)
  String? documentType;
  @HiveField(9)
  String? plateNo;
  @HiveField(10)
  String? passId;
  @HiveField(11)
  String? visitReason;
  @HiveField(12)
  String? datetimeIn;
  @HiveField(13)
  String? datetimeOut;
  @HiveField(14)
  String? siteId;
  @HiveField(15)
  String? status;
  @HiveField(16)
  List<ItemObj>? items;
  @HiveField(17)
  String? houseNo;
  @HiveField(18)
  String? blockNo;
  @HiveField(19)
  String? vehiclePass;
  @HiveField(20)
  String? companyName;
  @HiveField(21)
  String? officeNo;

  Visitor(
      {this.id,
      this.visitorName,
      this.hostName,
      this.department,
      this.designation,
      this.noVisitors,
      this.documentNumber,
      this.documentType,
      this.plateNo,
      this.passId,
      this.visitReason,
      this.datetimeIn,
      this.datetimeOut,
      this.siteId,
      this.status,
      this.items,
      this.houseNo,
      this.blockNo,
      this.companyName,
      this.vehiclePass,
      this.officeNo});

  Visitor.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    visitorName = json['visitor_name'];
    hostName = json['host_name'];
    department = json['department'];
    designation = json['designation'];
    noVisitors = json['no_visitors'];
    documentNumber = json['documentNumber'];
    documentType = json['document_type'];
    plateNo = json['plateNo'];
    passId = json['pass_id'];
    visitReason = json['visit_reason'];
    datetimeIn = json['datetime_in'];
    datetimeOut = json['datetime_out'];
    siteId = json['site_id'];
    status = json['status'];
    if (json['items'] != null) {
      items = <ItemObj>[];
      json['items'].forEach((v) {
        items!.add(new ItemObj.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['visitor_name'] = this.visitorName;
    data['host_name'] = this.hostName;
    data['department'] = this.department;
    data['designation'] = this.designation;
    data['no_visitors'] = this.noVisitors;
    data['documentNumber'] = this.documentNumber;
    data['document_type'] = this.documentType;
    data['plateNo'] = this.plateNo;
    data['pass_id'] = this.passId;
    data['visit_reason'] = this.visitReason;
    data['datetime_in'] = this.datetimeIn;
    data['datetime_out'] = this.datetimeOut;
    data['site_id'] = this.siteId;
    data['status'] = this.status;
    if (this.items != null) {
      data['items'] = this.items!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

@HiveType(typeId: 2)
class ItemObj {
  @HiveField(0)
  String? id;
  @HiveField(1)
  String? visitorId;
  @HiveField(2)
  String? name;
  @HiveField(3)
  String? serialNo;

  ItemObj({this.id, this.visitorId, this.name, this.serialNo});

  ItemObj.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    visitorId = json['visitor_id'];
    name = json['name'];
    serialNo = json['serialNo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['visitor_id'] = this.visitorId;
    data['name'] = this.name;
    data['serialNo'] = this.serialNo;
    return data;
  }
}

// @HiveType(typeId: 3)
// class TimeSheetData {
//   @HiveField(0)
//   String? status;
//   @HiveField(1)
//   String? date;
//   @HiveField(2)
//   String? shiftId;
//   @HiveField(3)
//   String? employeeId;
//   @HiveField(4)
//   String? id;
//   @HiveField(5)
//   String? staffPhone;
//   @HiveField(6)
//   String? name;
//   @HiveField(7)
//   bool? modified;
//   @HiveField(8)
//   String? reason;
//   @HiveField(9)
//   String? image;
//   @HiveField(10)
//   String? biometricId;
//   @HiveField(11)
//   String? timeIn;
//   @HiveField(12)
//   String? timeOut;
//   @HiveField(13)
//   String? fileName;
//   @HiveField(14)
//   String? commentIn;
//   @HiveField(15)
//   String? commentOut;
//
//   TimeSheetData(
//       {this.status,
//       this.date,
//       this.shiftId,
//       this.employeeId,
//       this.id,
//       this.staffPhone,
//       this.name,
//       this.modified,
//       this.reason,
//       this.image,
//       this.biometricId,
//       this.timeIn,
//       this.timeOut,
//       this.commentIn,
//       this.commentOut,
//       this.fileName});
//
//   TimeSheetData.fromJson(Map<String, dynamic> json) {
//     status = json['status'];
//     date = json['date'];
//     shiftId = json['shift_id'];
//     employeeId = json['employee_id'];
//     id = json['Id'];
//     staffPhone = json['staff_phone'];
//     name = json['name'];
//     reason = json['reason'];
//     timeIn = json['time_in'];
//     timeOut = json['time_out'];
//     commentIn = json['comment_in'];
//     commentOut = json['comment_out'];
//     fileName = json['file_name'];
//   }
//
//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['status'] = this.status;
//     data['date'] = this.date;
//     data['shift_id'] = this.shiftId;
//     data['employee_id'] = this.employeeId;
//     data['Id'] = this.id;
//     data['staff_phone'] = this.staffPhone;
//     data['name'] = this.name;
//     data['reason'] = this.reason;
//     data['time_in'] = this.timeIn;
//     data['time_out'] = this.timeOut;
//     data['comment_in'] = this.commentIn;
//     data['comment_out'] = this.commentOut;
//     data['file_name'] = this.fileName;
//     return data;
//   }
// }
//
@HiveType(typeId: 4)
class TimeSheetDetails {
  @HiveField(0)
  String? timesheetId;
  @HiveField(1)
  String? status;
  @HiveField(2)
  String? timeIn;
  @HiveField(3)
  String? timeOut;
  @HiveField(4)
  String? reason;
  @HiveField(5)
  String? imageIn;
  @HiveField(6)
  String? bioMetric;
  @HiveField(7)
  String? imageOut;
  @HiveField(8)
  String? commentIn;
  @HiveField(9)
  String? commentOut;

  TimeSheetDetails(
      {this.timesheetId,
      this.status,
      this.timeIn,
      this.timeOut,
      this.reason,
      this.imageIn,
      this.bioMetric,
      this.imageOut,
      this.commentOut,
      this.commentIn});

  TimeSheetDetails.fromJson(Map<String, dynamic> json) {
    timesheetId = json['timesheet_id'];
    status = json['status'];
    timeIn = json['time_in'];
    timeOut = json['time_out'];
    reason = json['reason'];
    commentIn = json['comment_in'];
    commentOut = json['comment_out'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['timesheet_id'] = this.timesheetId;
    data['status'] = this.status;
    data['time_in'] = this.timeIn;
    data['time_out'] = this.timeOut;
    data['reason'] = this.reason;
    data['comment_in'] = this.commentIn;
    data['comment_out'] = this.commentOut;
    return data;
  }
}

@HiveType(typeId: 5)
class CheckListResult {
  @HiveField(0)
  String? responseId;
  @HiveField(1)
  String? questionId;
  @HiveField(2)
  String? response;
  @HiveField(3)
  String? date;
  @HiveField(4)
  String? answeredBy;
  @HiveField(5)
  String? defaultCustom;
  @HiveField(6)
  String? shiftId;
  @HiveField(7)
  String? categoryId;
  @HiveField(8)
  String? name;
  @HiveField(9)
  String? questionType;
  @HiveField(10)
  String? fileLocation;
  @HiveField(11)
  String? shiftType;
  @HiveField(12)
  bool? synced;
  @HiveField(13)
  String? status;
  @HiveField(14)
  String? plannerId;

  CheckListResult(
      {this.responseId,
      this.questionId,
      this.response,
      this.date,
      this.answeredBy,
      this.defaultCustom,
      this.shiftId,
      this.categoryId,
      this.name,
      this.questionType,
      this.fileLocation,
      this.shiftType,
      this.synced,
      this.status,
      this.plannerId});

  CheckListResult.fromJson(Map<String, dynamic> json) {
    responseId = json['response_id'];
    questionId = json['question_id'];
    response = json['response'];
    date = json['date'];
    answeredBy = json['answered_by'];
    defaultCustom = json['default_custom'];
    shiftId = json['shift_id'];
    categoryId = json['category_id'];
    name = json['name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['response_id'] = this.responseId;
    data['question_id'] = this.questionId;
    data['response'] = this.response;
    data['date'] = this.date;
    data['answered_by'] = this.answeredBy;
    data['default_custom'] = this.defaultCustom;
    data['shift_id'] = this.shiftId;
    data['category_id'] = this.categoryId;
    return data;
  }
}

@HiveType(typeId: 6)
class PlannerData {
  @HiveField(0)
  String? plannerId;
  @HiveField(1)
  String? plannerName;
  @HiveField(2)
  String? status;
  @HiveField(3)
  String? verificationMethod;
  @HiveField(4)
  List<Shifts>? shifts;

  PlannerData(
      {this.plannerId,
      this.plannerName,
      this.status,
      this.verificationMethod,
      this.shifts});

  PlannerData.fromJson(Map<String, dynamic> json) {
    plannerId = json['planner_id'];
    plannerName = json['planner_name'];
    status = json['status'];
    verificationMethod = json['verification_method'];
    if (json['shifts'] != null) {
      shifts = <Shifts>[];
      json['shifts'].forEach((v) {
        shifts!.add(new Shifts.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['planner_id'] = this.plannerId;
    data['planner_name'] = this.plannerName;
    data['status'] = this.status;
    data['verification_method'] = this.verificationMethod;
    if (this.shifts != null) {
      data['shifts'] = this.shifts!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

@HiveType(typeId: 7)
class Shifts {
  @HiveField(0)
  String? id;
  @HiveField(1)
  String? plannerId;
  @HiveField(2)
  String? shiftName;
  @HiveField(3)
  String? shiftChangeFrom;
  @HiveField(4)
  String? shiftChangeTo;
  @HiveField(5)
  String? shiftColor;
  @HiveField(6)
  String? code;
  @HiveField(7)
  int? duration;

  Shifts(
      {this.id,
      this.plannerId,
      this.shiftName,
      this.shiftChangeFrom,
      this.shiftChangeTo,
      this.shiftColor,
      this.code,
      this.duration});

  Shifts.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    plannerId = json['planner_id'];
    shiftName = json['shift_name'];
    shiftChangeFrom = json['shift_change_from'];
    shiftChangeTo = json['shift_change_to'];
    shiftColor = json['shift_color'];
    code = json['code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['planner_id'] = this.plannerId;
    data['shift_name'] = this.shiftName;
    data['shift_change_from'] = this.shiftChangeFrom;
    data['shift_change_to'] = this.shiftChangeTo;
    data['shift_color'] = this.shiftColor;
    data['code'] = this.code;
    return data;
  }
}

@HiveType(typeId: 8)
class DeviceDetails {
  @HiveField(0)
  String? name;
  @HiveField(1)
  int? xOrigin;
  @HiveField(2)
  int? yOrigin;
  @HiveField(3)
  int? width;
  @HiveField(4)
  int? height;

  DeviceDetails(
      {this.name, this.xOrigin, this.yOrigin, this.width, this.height});
}

@HiveType(typeId: 9)
class UpdateFormData {
  @HiveField(0)
  String? id;
  @HiveField(1)
  String? formId;
  @HiveField(2)
  String? answeredBy;
  @HiveField(3)
  String? plannerId;
  @HiveField(4)
  String? shiftId;
  @HiveField(5)
  String? date;
  @HiveField(6)
  List<FormItems>? items;

  UpdateFormData(
      {this.id,
      this.formId,
      this.answeredBy,
      this.plannerId,
      this.shiftId,
      this.date,
      this.items});

  UpdateFormData.fromJson(Map<String, dynamic> json) {
    id = json['Id'];
    formId = json['form_id'];
    answeredBy = json['answered_by'];
    plannerId = json['planner_id'];
    shiftId = json['shift_id'];
    date = json['date'];
    if (json['items'] != null) {
      items = <FormItems>[];
      json['items'].forEach((v) {
        items!.add(new FormItems.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['form_id'] = this.formId;
    data['answered_by'] = this.answeredBy;
    data['planner_id'] = this.plannerId;
    data['shift_id'] = this.shiftId;
    data['date'] = this.date;
    if (this.items != null) {
      data['items'] = this.items!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

@HiveType(typeId: 10)
class FormItems {
  @HiveField(0)
  String? formItemId;
  @HiveField(1)
  String? response;
  @HiveField(2)
  String? type;
  @HiveField(3)
  String? question;
  @HiveField(4)
  String? responseType;
  @HiveField(5)
  bool? required;
  @HiveField(6)
  bool? isFile;

  FormItems(
      {this.formItemId,
      this.response,
      this.type,
      this.question,
      this.responseType,
      this.required,
      this.isFile});

  FormItems.fromJson(Map<String, dynamic> json) {
    formItemId = json['form_item_id'];
    response = json['response'];
    type = json['type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['form_item_id'] = this.formItemId;
    data['response'] = this.response;
    data['type'] = this.type;
    return data;
  }
}

@HiveType(typeId: 11)
class CreateTaskData {
  @HiveField(0)
  String? assignedBy;
  @HiveField(1)
  String? taskDescription;
  @HiveField(2)
  String? deadline;
  @HiveField(3)
  List<AssignedTo?>? assignTo;
  @HiveField(4)
  String? title;
  @HiveField(5)
  String? taskType;
  @HiveField(6)
  String? taskVisibility;
  @HiveField(7)
  String? priority;
  @HiveField(8)
  String? associatedBy;
  @HiveField(9)
  String? taskTypeName;
  @HiveField(10)
  List<SubTask?>? taskItem;

  CreateTaskData(
      {this.assignedBy,
      this.taskDescription,
      this.deadline,
      this.assignTo,
      this.title,
      this.taskType,
      this.taskVisibility,
      this.priority,
      this.associatedBy,
      this.taskTypeName,
      this.taskItem});
  CreateTaskData.fromJson(Map<String, dynamic> json) {
    assignedBy = json["assigned_by"]?.toString();
    taskDescription = json["task_description"]?.toString();
    deadline = json["deadline"]?.toString();
    if (json["assign_to"] != null) {
      final v = json["assign_to"];
      final arr0 = <AssignedTo>[];
      v.forEach((v) {
        arr0.add(AssignedTo.fromJson(v));
      });
      assignTo = arr0;
    }
    title = json["title"]?.toString();
    taskType = json["task_type"]?.toString();
    taskVisibility = json["task_visibility"]?.toString();
    priority = json["priority"]?.toString();
    associatedBy = json["associated_by"]?.toString();
    if (json["task_item"] != null) {
      final v = json["task_item"];
      final arr0 = <SubTask>[];
      v.forEach((v) {
        arr0.add(SubTask.fromJson(v));
      });
      taskItem = arr0;
    }
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data["assigned_by"] = assignedBy;
    data["task_description"] = taskDescription;
    data["deadline"] = deadline;
    if (assignTo != null) {
      final v = assignTo;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v!.toJson());
      });
      data["assign_to"] = arr0;
    }
    data["title"] = title;
    data["task_type"] = taskType;
    data["task_visibility"] = taskVisibility;
    data["priority"] = priority;
    data["associated_by"] = associatedBy;
    if (taskItem != null) {
      final v = taskItem;
      final arr0 = [];
      v!.forEach((v) {
        arr0.add(v!.toJson());
      });
      data["task_item"] = arr0;
    }
    return data;
  }
}

@HiveType(typeId: 12)
class AssignedTo {
  @HiveField(0)
  String? assignToId;
  @HiveField(1)
  String? type;
  @HiveField(2)
  String? name;

  AssignedTo({this.assignToId, this.type, this.name});
  AssignedTo.fromJson(Map<String, dynamic> json) {
    assignToId = json["assign_to_id"]?.toString();
    type = json["type"]?.toString();
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data["assign_to_id"] = assignToId;
    data["type"] = type;
    return data;
  }
}

@HiveType(typeId: 13)
class SubTask {
  @HiveField(0)
  String? description;

  SubTask({this.description});
  SubTask.fromJson(Map<String, dynamic> json) {
    description = json["description"]?.toString();
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data["description"] = description;
    return data;
  }
}
