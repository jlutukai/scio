class CheckOutVisitorResponse {
  Data? data;
  String? message;
  String? errorMessage;
  bool? error;

  CheckOutVisitorResponse(
      {this.data, this.message, this.errorMessage, this.error});

  CheckOutVisitorResponse.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    message = json['message'];
    errorMessage = json['errorMessage'];
    error = json['error'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    data['message'] = this.message;
    data['errorMessage'] = this.errorMessage;
    data['error'] = this.error;
    return data;
  }
}

class Data {
  String? id;
  String? visitorName;
  String? hostName;
  String? department;
  String? designation;
  String? noVisitors;
  String? documentNumber;
  String? documentType;
  String? plateNo;
  String? passId;
  String? visitReason;
  String? datetimeIn;
  String? datetimeOut;
  String? siteId;
  String? status;
  List<Items>? items;

  Data(
      {this.id,
      this.visitorName,
      this.hostName,
      this.department,
      this.designation,
      this.noVisitors,
      this.documentNumber,
      this.documentType,
      this.plateNo,
      this.passId,
      this.visitReason,
      this.datetimeIn,
      this.datetimeOut,
      this.siteId,
      this.status,
      this.items});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    visitorName = json['visitor_name'];
    hostName = json['host_name'];
    department = json['department'];
    designation = json['designation'];
    noVisitors = json['no_visitors'];
    documentNumber = json['documentNumber'];
    documentType = json['document_type'];
    plateNo = json['plateNo'];
    passId = json['pass_id'];
    visitReason = json['visit_reason'];
    datetimeIn = json['datetime_in'];
    datetimeOut = json['datetime_out'];
    siteId = json['site_id'];
    status = json['status'];
    if (json['items'] != null) {
      items = <Items>[];
      json['items'].forEach((v) {
        items!.add(new Items.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['visitor_name'] = this.visitorName;
    data['host_name'] = this.hostName;
    data['department'] = this.department;
    data['designation'] = this.designation;
    data['no_visitors'] = this.noVisitors;
    data['documentNumber'] = this.documentNumber;
    data['document_type'] = this.documentType;
    data['plateNo'] = this.plateNo;
    data['pass_id'] = this.passId;
    data['visit_reason'] = this.visitReason;
    data['datetime_in'] = this.datetimeIn;
    data['datetime_out'] = this.datetimeOut;
    data['site_id'] = this.siteId;
    data['status'] = this.status;
    if (this.items != null) {
      data['items'] = this.items!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Items {
  String? id;
  String? visitorId;
  String? name;
  String? serialNo;

  Items({this.id, this.visitorId, this.name, this.serialNo});

  Items.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    visitorId = json['visitor_id'];
    name = json['name'];
    serialNo = json['serialNo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['visitor_id'] = this.visitorId;
    data['name'] = this.name;
    data['serialNo'] = this.serialNo;
    return data;
  }
}
