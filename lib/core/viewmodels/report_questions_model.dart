import 'dart:io';

import 'package:hive/hive.dart';
import 'package:scio_security/core/enums/view_state.dart';
import 'package:scio_security/core/models/checklist_response.dart';
import 'package:scio_security/core/models/local/localModels.dart';
import 'package:scio_security/core/services/api.dart';
import 'package:scio_security/core/viewmodels/base_model.dart';

import '../../locator.dart';

class ReportsQuestionsModel extends BaseModel {
  Api? _api = locator<Api>();
  Box checkListAnswers = Hive.box("check_list_answer");
  Box checklist = Hive.box("check_list");
  // List<CheckListData> checklist;
  List<CheckListData>? checklist1;
  List<CheckListData>? checklist2;

  Future<Map<String, dynamic>> getChecklist(
      Map<String, String> data, String? id, Shifts s, String date) async {
    //CheckListResponse
    setState(ViewState.Busy);
    try {
      var r = await _api!.getChecklist(data);
      if (r['success']) {
        var d = CheckListResponse.fromJson(r['response']);
        if (d.data != null) {
          checklist1 = d.data;
          initData(id, s, date);
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<Map<String, dynamic>> getDefaultChecklist(
      String? id, Shifts s, String date) async {
    setState(ViewState.Busy);
    try {
      var r = await _api!.getDefaultChecklist();
      if (r['success']) {
        var d = CheckListResponse.fromJson(r['response']);
        if (d.data != null) {
          checklist2 = d.data;

          initData(id, s, date);
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<Map<String, dynamic>> updateResponse(Map<String, String> data) async {
    // UpdatePlannerResponse
    setState(ViewState.Busy);
    try {
      var r = await _api!.updateResponse(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<Map<String, dynamic>> uploadImage(File image) async {
    setState(ViewState.Busy);
    try {
      var r = await _api!.uploadVerificationImage(image);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<void> initData(String? id, Shifts s, String date) async {
    List<CheckListData> c = [];
    if (checklist1 != null) {
      c.addAll(checklist1!);
    }
    if (checklist2 != null) {
      c.addAll(checklist2!);
    }
    print(checkListAnswers.keys);
    // await checkListAnswers.clear();
    c.forEach((element) {
      print(element.id);
      var e = checkListAnswers
          .get("${s.id}/$date/${element.id}/${element.category}");
      if (e != null) {
        print('updated');
        CheckListResult c = CheckListResult(
            responseId: "${e?.responseId ?? ''}",
            questionId: element.id,
            response: "${e?.response ?? ''}",
            date: "${e?.date ?? ''}",
            answeredBy: "${e?.answeredBy ?? ''}",
            defaultCustom: "${e?.defaultCustom ?? ''}",
            shiftId: "${e?.shiftId ?? ''}",
            categoryId: "${element.category}",
            name: element.name,
            questionType: element.questionType ?? element.type,
            fileLocation: "${e?.fileLocation ?? ''}",
            shiftType: "${s.id}",
            synced: e.synced);
        checkListAnswers.put(
            "${s.id}/$date/${element.id}/${element.category}", c);
      } else {
        print('works');
        CheckListResult c = CheckListResult(
          responseId: '',
          questionId: element.id,
          response: '',
          date: '',
          answeredBy: '',
          defaultCustom: '',
          shiftId: '',
          categoryId: element.category,
          name: element.name,
          questionType: element.questionType ?? element.type,
          fileLocation: '',
          shiftType: "${s.id}",
          synced: true,
        );
        checkListAnswers.put(
            "${s.id}/$date/${element.id}/${element.category}", c);
      }
    });
    await refreshList(id, s);
  }

  Future<void> refreshList(String? id, Shifts s) async {
    await checklist.clear();
    print("here  $s, $id  ${checkListAnswers.values.length}");
    for (var i = 0; i <= checkListAnswers.values.length - 1; i++) {
      var c = checkListAnswers.getAt(i);
      print("@@@@@@@@@@@@@@@@@@@@@@  ${c.shiftType}, ${c.categoryId}");
      if (c.shiftType == s.id && c.categoryId == id) {
        checklist.add(c);
        notifyListeners();
      }
    }
    notifyListeners();
  }
}
