import 'dart:io';

import 'package:scio_security/core/enums/view_state.dart';
import 'package:scio_security/core/services/api.dart';

import '../../locator.dart';
import 'base_model.dart';

class CheckInGuardModel extends BaseModel {
  Api? _api = locator<Api>();

  Future<Map<String, dynamic>> updateStatus(Map<String, String> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api!.updateStatus(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<Map<String, dynamic>> uploadImage(File image) async {
    setState(ViewState.Busy);
    try {
      var r = await _api!.uploadVerificationImage(image);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }
}
