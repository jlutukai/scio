import 'dart:io';

import 'package:scio_security/core/enums/view_state.dart';
import 'package:scio_security/core/services/api.dart';
import 'package:scio_security/core/viewmodels/base_model.dart';

import '../../locator.dart';
import '../models/local/localModels.dart';
import '../models/time_sheet_response.dart';
import '../services/sql_helper.dart';

class CheckOutGuardManualModel extends BaseModel {
  Api? _api = locator<Api>();
  List<TimeSheetData> timeSheet = [];

  Future<Map<String, dynamic>> getTimeSheetRemote(
      Map<String, String> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api!.getTimeSheetEmployees(data);
      if (r['success']) {
        var d = TimeSheetResponse.fromJson(r['response']);
        SQLHelper.saveTimeSheets(d.data ?? []);
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future getTimesheetLocal(String status, String date, Shifts shift) async {
    setState(ViewState.Busy);
    try {
      timeSheet = await SQLHelper.getTimeSheets(
          status, date, date, (int.tryParse(shift.id ?? "0") ?? 0));
      setState(ViewState.Idle);
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<Map<String, dynamic>> updateStatus(Map<String, String> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api!.updateStatus(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<Map<String, dynamic>> uploadImage(File image) async {
    setState(ViewState.Busy);
    try {
      var r = await _api!.uploadVerificationImage(image);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }
}
