import 'package:dio/dio.dart';
import 'package:scio_security/core/enums/view_state.dart';
import 'package:scio_security/core/models/scio_url_response.dart';
import 'package:scio_security/core/services/api.dart';

import '../../locator.dart';
import 'base_model.dart';

class UrlModel extends BaseModel {
  Api? _api = locator<Api>();

  Future<Map<String, dynamic>> veryUrl(
    FormData data,
  ) async {
    setState(ViewState.Busy);
    try {
      var r = await _api!.verifyUrl(data);
      print(r['response']);
      if (r['response'] != null) {
        if (r['success'] != null) {
          if (r['success']) {
            var d = ScioUrlResponse.fromJson(r['response']);
            if (d.scioUrl != null) {
              _api!.saveUrl(d.scioUrl!.data!.url);
            }
          }
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }
}
