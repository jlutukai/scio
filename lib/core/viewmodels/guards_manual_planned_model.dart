import 'dart:io';

import 'package:scio_security/core/enums/view_state.dart';
import 'package:scio_security/core/models/local/localModels.dart';
import 'package:scio_security/core/models/time_sheet_response.dart';
import 'package:scio_security/core/services/api.dart';
import 'package:scio_security/core/services/sql_helper.dart';

import '../../locator.dart';
import 'base_model.dart';

class CheckInGuardManualModel extends BaseModel {
  Api? _api = locator<Api>();
  List<TimeSheetData> timeSheet = [];

  Future<Map<String, dynamic>> getTimeSheetRemote(
      Map<String, String> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api!.getTimeSheetEmployees(data);
      if (r['success']) {
        var d = TimeSheetResponse.fromJson(r['response']);
        SQLHelper.saveTimeSheets(d.data ?? []);
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future getTimesheetLocal(String status, String date, Shifts shift) async {
    setState(ViewState.Busy);
    try {
      timeSheet = await SQLHelper.getTimeSheets(
          status, date, date, (int.tryParse(shift.id ?? "0") ?? 0));
      var off = await SQLHelper.getTimeSheets(
          "Off", date, date, (int.tryParse(shift.id ?? "0") ?? 0));
      timeSheet.addAll(off);
      setState(ViewState.Idle);
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<int> updateTimesheetLocal(TimeSheetData data) async {
    setState(ViewState.Busy);
    try {
      var r = await SQLHelper.updateTimeSheet(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<Map<String, dynamic>> updateStatus(Map<String, String> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api!.updateStatus(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<Map<String, dynamic>> uploadImage(File image) async {
    setState(ViewState.Busy);
    try {
      var r = await _api!.uploadVerificationImage(image);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }
}
