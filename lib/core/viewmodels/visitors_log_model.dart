import 'package:scio_security/core/enums/view_state.dart';
import 'package:scio_security/core/models/local/localModels.dart';
import 'package:scio_security/core/models/visitors_log_response.dart';
import 'package:scio_security/core/services/api.dart';
import 'package:scio_security/core/viewmodels/base_model.dart';
import 'package:scio_security/locator.dart';

class VisitorsLogModel extends BaseModel {
  Api? _api = locator<Api>();
  List<Visitor>? visitors;
  List<Visitor> visitorsIn = [];
  List<Visitor> visitorsOut = [];

  Future<Map<String, dynamic>> createVisitor(Map<String, dynamic> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api!.createVisitor(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<Map<String, dynamic>> getVisitors(Map<String, String> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api!.getVisitors(data);
      if (r['success']) {
        var d = VisitorsLogResponse.fromJson(r['response']);
        setData(d.data);
        visitors = d.data;
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<Map<String, dynamic>> refreshVisitors(Map<String, String> data) async {
    // setState(ViewState.Busy);
    try {
      var r = await _api!.getVisitors(data);
      if (r['success']) {
        var d = VisitorsLogResponse.fromJson(r['response']);
        setData(d.data);
        visitors = d.data;
      }
      notifyListeners();
      // setState(ViewState.Idle);
      return r;
    } catch (e) {
      // setState(ViewState.Idle);
      throw e;
    }
  }

  Future<Map<String, dynamic>> checkOutVisitor(Map<String, String> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api!.checkOutVisitor(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  void setData(List<Visitor>? v) {
    visitorsOut.clear();
    visitorsIn.clear();
    v?.forEach((element) {
      if (element.status == 'Out') {
        visitorsOut.add(element);
        notifyListeners();
      }
    });

    v?.forEach((element) {
      if (element.status == 'In') {
        visitorsIn.add(element);
        notifyListeners();
      }
    });
    print("done ${visitorsIn.length}");
  }
}
