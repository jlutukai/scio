import 'dart:io';

import 'package:hive/hive.dart';
import 'package:scio_security/core/enums/view_state.dart';
import 'package:scio_security/core/models/general_response.dart';
import 'package:scio_security/core/models/image_verification_response.dart';
import 'package:scio_security/core/models/local/localModels.dart';
import 'package:scio_security/core/models/time_sheet_response.dart';
import 'package:scio_security/core/services/api.dart';
import 'package:scio_security/core/viewmodels/base_model.dart';
import 'package:scio_security/utils/useful.dart';

import '../../locator.dart';

class TimeSheetModel extends BaseModel {
  Api? _api = locator<Api>();

  final accountInfo = Hive.box<String>("accountInfo");

  List<TimeSheetData> clockInPrevious = [];
  List<TimeSheetData> clockInCurrent = [];
  List<TimeSheetData> clockInNext = [];

  List<TimeSheetData> onSitePrevious = [];
  List<TimeSheetData> onSiteCurrent = [];
  List<TimeSheetData> onSiteNext = [];

  List<TimeSheetData> clockOutPrevious = [];
  List<TimeSheetData> clockOutCurrent = [];
  List<TimeSheetData> clockOutNext = [];

  List<TimeSheetData> timeSheetData = [];

  Shifts? previous;
  Shifts? current;
  Shifts? next;

  final timeSheet = Hive.box('time_sheet');
  final _unsaved = Hive.box('time_sheet_unsaved');
  late PlannerData p;

  Future<Map<String, dynamic>> getTimeSheetData(
      Map<String, String> data,
      PlannerData planner,
      String? minDate,
      String? maxDate,
      Shifts? previousShift,
      Shifts? currentShift,
      Shifts? nextShift) async {
    setState(ViewState.Busy);
    p = planner;
    savePlanner(planner.plannerId ?? "");
    try {
      var r = await _api!.getTimeSheet(data);
      if (r['success']) {
        var d = TimeSheetResponse.fromJson(r['response']);
        if (_unsaved.isEmpty) {
          await timeSheet.clear();
          d.data!.forEach((element) {
            timeSheet.put(element.id, element);
          });
        } else {
          await sync();
        }
        current = currentShift;
        initialize(
            planner, minDate, maxDate, previousShift, currentShift, nextShift);
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<Map<String, dynamic>> updateStatus(Map<String, String> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api!.updateStatus(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<Map<String, dynamic>> uploadImage(File image) async {
    setState(ViewState.Busy);
    try {
      var r = await _api!.uploadVerificationImage(image);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<Map<String, dynamic>> refreshData(
      Map<String, String> data,
      PlannerData planner,
      String? minDate,
      String? maxDate,
      Shifts? previousShift,
      Shifts? currentShift,
      Shifts? nextShift) async {
    p = planner;
    savePlanner(planner.plannerId ?? "");
    current = currentShift;
    try {
      var r = await _api!.getTimeSheet(data);
      if (r['success']) {
        var d = TimeSheetResponse.fromJson(r['response']);
        await timeSheet.clear();
        d.data!.forEach((element) {
          timeSheet.put(element.id, element);
        });
        initialize(
            planner, minDate, maxDate, previousShift, currentShift, nextShift);
      }
      notifyListeners();
      return r;
    } catch (e) {
      throw e;
    }
  }

  String? getPlannerId() {
    return accountInfo.get("plannerId");
  }

  savePlanner(String id) {
    accountInfo.put("plannerId", id);
  }

  void initialize(PlannerData element, String? minDate, String? maxDate,
      Shifts? previousShift, Shifts? currentShift, Shifts? nextShift) {
    savePlanner(element.plannerId ?? "");
    current = currentShift;
    p = element;
    getPreviousShift(element, minDate, previousShift);
    getNextShift(element, maxDate, nextShift);

    clockInCurrent.clear();
    onSiteCurrent.clear();
    clockOutCurrent.clear();
    timeSheet.values.forEach((e) {
      if (e.date == maxDate) {
        if (e.status == 'Planned') {
          if (currentShift!.id == e.shiftId) {
            clockInCurrent.add(e);
          }
        }
        if (e.status == "In") {
          if (currentShift!.id == e.shiftId) {
            onSiteCurrent.add(e);
          }
        }
        if (e.status == "Awaiting Approval") {
          if (currentShift!.id == e.shiftId) {
            clockOutCurrent.add(e);
          }
        }
      }
    });
    notifyListeners();
  }

  void getNextShift(PlannerData element, String? maxDate, Shifts? nextShift) {
    clockInNext.clear();
    onSiteNext.clear();
    clockOutNext.clear();
    timeSheet.values.forEach((e) {
      if (e.date == maxDate) {
        if (e.status == 'Planned') {
          if (nextShift!.id == e.shiftId) {
            clockInNext.add(e);
          }
        }
        if (e.status == "In") {
          if (nextShift!.id == e.shiftId) {
            onSiteNext.add(e);
          }
        }
        if (e.status == "Awaiting Approval") {
          if (nextShift!.id == e.shiftId) {
            clockOutNext.add(e);
          }
        }
      }
    });
  }

  void getPreviousShift(
      PlannerData element, String? minDate, Shifts? previousShift) {
    clockInPrevious.clear();
    onSitePrevious.clear();
    clockOutPrevious.clear();

    timeSheet.values.forEach((e) {
      if (e.date == minDate) {
        if (e.status == 'Planned') {
          if (previousShift!.id == e.shiftId) {
            clockInPrevious.add(e);
          }
        }
        if (e.status == "In") {
          if (previousShift!.id == e.shiftId) {
            onSitePrevious.add(e);
          }
        }
        if (e.status == "Awaiting Approval") {
          if (previousShift!.id == e.shiftId) {
            clockOutPrevious.add(e);
          }
        }
      }
    });
  }

  Future<void> sync() async {
    for (var i = 0; i <= _unsaved.values.length; i++) {
      if (i <= _unsaved.values.length - 1) {
        await sendDataToDb(_unsaved.getAt(i), i);
      }
    }
  }

  Future<void> sendDataToDb(
      TimeSheetDetails timeSheetDetails, int index) async {
    File? imageIn;
    if (timeSheetDetails.imageIn != null &&
        timeSheetDetails.imageIn!.isNotEmpty)
      imageIn = File(timeSheetDetails.imageIn!);
    File? imageOut;
    if (timeSheetDetails.imageOut != null &&
        timeSheetDetails.imageOut!.isNotEmpty)
      imageOut = File(timeSheetDetails.imageOut!);
    if (imageIn != null) {
      try {
        var res = await uploadImage(imageIn);
        if (res['success']) {
          var resd = ImageVerificationResponse.fromJson(res['response']);
          if (resd.error!) {
            showToast(resd.message!);
          }
        } else {
          /// Todo : error checking
        }
      } catch (e) {
        showToast(e.toString());
      }
    }

    if (imageOut != null) {
      try {
        var res = await uploadImage(imageOut);
        if (res['success']) {
          var resd = ImageVerificationResponse.fromJson(res['response']);
          if (resd.error!) {
            showToast(resd.message!);
          }
        } else {
          /// Todo : error checking
        }
      } catch (e) {
        showToast(e.toString());
      }
    }

    Map<String, String> data = {
      "timesheet_id": "${timeSheetDetails.timesheetId}",
      "status": "${timeSheetDetails.status ?? ''}",
      "time_in": "${timeSheetDetails.timeIn ?? ''}",
      "time_out": "${timeSheetDetails.timeOut ?? ''}",
      "reason": "${timeSheetDetails.reason ?? ''}",
      "comment_in": "${timeSheetDetails.commentIn}",
      "comment_out": "${timeSheetDetails.commentOut}"
    };
    var r = await updateStatus(data);
    if (r['success']) {
      var d = GeneralResponse.fromJson(r['response']);
      if (!d.error!) {
        _unsaved.deleteAt(index);
        print('success');
      } else {
        print(d.erroMessage);
      }
    } else {
      /// Todo : error checking
    }
  }
}
