import 'dart:io';

import 'package:hive/hive.dart';
import 'package:scio_security/core/enums/view_state.dart';
import 'package:scio_security/core/models/general_response.dart';
import 'package:scio_security/core/models/image_verification_response.dart';
import 'package:scio_security/core/models/local/localModels.dart';
import 'package:scio_security/core/models/time_sheet_response.dart';
import 'package:scio_security/core/services/api.dart';
import 'package:scio_security/core/services/sql_helper.dart';
import 'package:scio_security/utils/useful.dart';

import '../../locator.dart';
import 'base_model.dart';

class TimeSheetManualModel extends BaseModel {
  Api? _api = locator<Api>();

  List<TimeSheetData> unSyncedTimeSheet = [];

  // final timeSheet = Hive.box('time_sheet_manual');
  // final timeSheetIn = Hive.box('time_sheet_manual');
  // final _unsaved = Hive.box('time_sheet_unsaved_manual');

  Future<Map<String, dynamic>> getTimeSheetRemote(
      Map<String, String> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api!.getTimeSheetEmployees(data);
      if (r['success']) {
        var d = TimeSheetResponse.fromJson(r['response']);
        SQLHelper.saveTimeSheets(d.data ?? []);
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future getUnSyncedTimesheetLocal() async {
    setState(ViewState.Busy);
    try {
      unSyncedTimeSheet = await SQLHelper.getUnSyncedTimeSheets();
      if (unSyncedTimeSheet.isNotEmpty) {
        sync();
      }
      setState(ViewState.Idle);
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  // Future<Map<String, dynamic>> getTimeSheetData(
  //     Map<String, String> data) async {
  //   setState(ViewState.Busy);
  //   try {
  //     var r = await _api!.getTimeSheetEmployees(data);
  //     if (r['success']) {
  //       var d = TimeSheetResponse.fromJson(r['response']);
  //       print(d.data!.length);
  //       await timeSheet.clear();
  //       d.data!.forEach((element) {
  //         timeSheet.put(element.id, element);
  //       });
  //       if (_unsaved.isNotEmpty) {
  //         showToast("Uploading unsaved data");
  //         await sync();
  //       }
  //       if (d.data != null) {
  //         initialize(d.data);
  //       }
  //     }
  //     setState(ViewState.Idle);
  //     return r;
  //   } catch (e) {
  //     setState(ViewState.Idle);
  //     throw e;
  //   }
  // }

  Future<Map<String, dynamic>> updateStatus(Map<String, String> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api!.updateStatus(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<int> updateTimesheetLocal(TimeSheetData data) async {
    setState(ViewState.Busy);
    try {
      var r = await SQLHelper.updateTimeSheet(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<Map<String, dynamic>> uploadImage(File image) async {
    setState(ViewState.Busy);
    try {
      var r = await _api!.uploadVerificationImage(image);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  sync() async {
    for (var i = 0; i <= unSyncedTimeSheet.length - 1; i++) {
      await sendDataToDb(unSyncedTimeSheet.elementAt(i), i);
    }
  }

  // void initialize([List<TimeSheetData>? data]) {
  //   List<TimeSheetData> timeSheetList =
  //       timeSheet.values.cast<TimeSheetData>().toList();
  //   timeSheetIn.clear();
  //   setState(ViewState.Busy);
  //   clockIn.clear();
  //   onSite.clear();
  //   clockOut.clear();
  //   if (timeSheet.values.isEmpty) {
  //     timeSheet.addAll(data ?? []);
  //   }
  //   print(timeSheetList.length);
  //   // print(data.length);
  //   // timeSheet.values.forEach((element)
  //   for (var i = 0; i < timeSheetList.length; i++) {
  //     TimeSheetData element = timeSheetList.elementAt(i);
  //     print(element.toString() + " " + i.toString());
  //     if (element.status == 'Planned' ||
  //         element.status.toString().contains('off')) {
  //       clockIn.add(element);
  //       notifyListeners();
  //     }
  //     if (element.status == 'In') {
  //       onSite.add(element);
  //       timeSheetIn.add(element);
  //       notifyListeners();
  //     }
  //     if (element.status == 'Awaiting Approval') {
  //       clockOut.add(element);
  //       notifyListeners();
  //     }
  //   }
  //   setState(ViewState.Idle);
  // }

  Future<void> sendDataToDb(TimeSheetData timeSheetDetails, int index) async {
    File? imageIn;
    if (timeSheetDetails.imageIn != null &&
        timeSheetDetails.imageIn!.isNotEmpty)
      imageIn = File(timeSheetDetails.imageIn!);
    File? imageOut;
    if (timeSheetDetails.imageOut != null &&
        timeSheetDetails.imageOut!.isNotEmpty)
      imageOut = File(timeSheetDetails.imageOut!);
    if (imageIn != null) {
      try {
        var res = await uploadImage(imageIn);
        if (res['success']) {
          var resd = ImageVerificationResponse.fromJson(res['response']);
          if (resd.error!) {
            showToast(resd.message!);
          }
        } else {
          /// Todo : error checking
        }
      } catch (e) {
        showToast(e.toString());
      }
    }

    if (imageOut != null) {
      try {
        var res = await uploadImage(imageOut);
        if (res['success']) {
          var resd = ImageVerificationResponse.fromJson(res['response']);
          if (resd.error!) {
            showToast(resd.message!);
          }
        } else {
          /// Todo : error checking
        }
      } catch (e) {
        showToast(e.toString());
      }
    }

    Map<String, String> data = {
      "timesheet_id": "${timeSheetDetails.id}",
      "status": "${timeSheetDetails.status ?? ''}",
      "time_in": "${timeSheetDetails.timeIn ?? ''}",
      "time_out": "${timeSheetDetails.timeOut ?? ''}",
      "reason": "${timeSheetDetails.reason ?? ''}",
      "comment_in": "${timeSheetDetails.commentIn}",
      "comment_out": "${timeSheetDetails.commentOut}"
    };
    var r = await updateStatus(data);
    if (r['success']) {
      var d = GeneralResponse.fromJson(r['response']);
      if (!d.error!) {
        updateTimesheetLocal(timeSheetDetails..syncStatus = syncedStatus);
        print('success');
      } else {
        print(d.erroMessage);
      }
    } else {
      /// Todo : error checking
    }
  }
}
