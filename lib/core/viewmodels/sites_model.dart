import 'package:hive/hive.dart';
import 'package:scio_security/core/enums/view_state.dart';
import 'package:scio_security/core/models/get_sites_response.dart';
import 'package:scio_security/core/services/api.dart';
import 'package:scio_security/core/viewmodels/base_model.dart';

import '../../locator.dart';

class SitesModel extends BaseModel {
  Api? _api = locator<Api>();

  List<SiteData>? sites;
  final accountInfo = Hive.box<String>("accountInfo");

  Future<Map<String, dynamic>> getSites() async {
    setState(ViewState.Busy);
    try {
      var r = await _api!.getSites();
      if (r['success']) {
        var d = GetSiteResponse.fromJson(r['response']);
        sites = d.data;
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  String? getSiteId() {
    return accountInfo.get("siteId");
  }

  saveSiteId(String id) {
    accountInfo.put("siteId", id);
  }
}
