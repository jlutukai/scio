import 'package:dio/dio.dart';
import 'package:scio_security/core/enums/view_state.dart';
import 'package:scio_security/core/services/api.dart';

import '../../locator.dart';
import 'base_model.dart';

class LoginModel extends BaseModel {
  Api? _api = locator<Api>();

  Future<Map<String, dynamic>> login(FormData data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api!.login(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<Map<String, dynamic>> updateToken(Map<String, String> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api!.updateToken(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }
}
