import 'dart:io';

import 'package:hive/hive.dart';
import 'package:scio_security/core/enums/view_state.dart';
import 'package:scio_security/core/models/local/localModels.dart';
import 'package:scio_security/core/models/planner_report_response.dart';
import 'package:scio_security/core/models/report_categories_response.dart';
import 'package:scio_security/core/services/api.dart';
import 'package:scio_security/core/viewmodels/base_model.dart';

import '../../locator.dart';

class DailyReportsModel extends BaseModel {
  Api? _api = locator<Api>();
  List<PlannerReportData>? reportsPrevious;
  List<PlannerReportData>? reportsCurrent;
  Box checkListAnswers = Hive.box("check_list_answer");

  List<CategoryData> categoriesCurrentShift = [];
  List<CategoryData> categoriesPreviousShift = [];

  Future<Map<String, dynamic>> getReports(Map<String, String> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api!.getPlannerReport(data);
      if (r['success']) {
        var d = PlannerReportResponse.fromJson(r['response']);
        initData(d.data!);
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<Map<String, dynamic>> getCategories(
      Shifts currentShift, Shifts previousShift) async {
    // ReportingCategoriesResponse
    setState(ViewState.Busy);
    try {
      var r = await _api!.getCategories();
      if (r['success']) {
        var d = ReportingCategoriesResponse.fromJson(r['response']);
        initializeCategories(d.data!, currentShift, previousShift);
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  void initData(List<PlannerReportData> data) {
    data.forEach((element) {
      CheckListResult c = CheckListResult(
          responseId: "${element.id}",
          questionId: "${element.questionId}",
          response: "${element.response}",
          date: "${element.date}",
          answeredBy: "${element.answeredBy}",
          defaultCustom: "${element.defaultCustom}",
          shiftId: "${element.shiftId}",
          categoryId: "${element.categoryId}",
          name: "",
          questionType: '',
          fileLocation: '',
          shiftType: "${element.shiftId}",
          synced: true,
          status: "${element.status}");
      checkListAnswers.put(
          "${element.shiftId}/${element.date!.substring(8)}/${element.questionId}/${element.categoryId}",
          c);
    });
  }

  Future<Map<String, dynamic>> updateResponse(Map<String, String> data) async {
    //UpdatePlannerResponse
    setState(ViewState.Busy);
    try {
      var r = await _api!.updateResponse(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<Map<String, dynamic>> uploadImage(File image) async {
    setState(ViewState.Busy);
    try {
      var r = await _api!.uploadVerificationImage(image);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  void initializeCategories(
      List<CategoryData> data, Shifts currentShift, Shifts previousShift) {
    categoriesCurrentShift.clear();
    categoriesPreviousShift.clear();
    categoriesPreviousShift.addAll(data);
    categoriesCurrentShift.addAll(data);

    checkListAnswers.values.forEach((element) {
      if (element.status == "Completed" && element.shiftId == currentShift.id) {
        categoriesCurrentShift.forEach((category) {
          CategoryData c = category;
          c.isCompete = true;
          notifyListeners();
        });
      }
      if (element.status == "Completed" &&
          element.shiftId == previousShift.id) {
        categoriesPreviousShift.forEach((category) {
          CategoryData c = category;
          c.isCompete = true;
          notifyListeners();
        });
      }
    });
  }
}
