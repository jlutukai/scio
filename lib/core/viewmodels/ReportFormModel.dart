import 'package:scio_security/core/enums/view_state.dart';
import 'package:scio_security/core/models/get_form_response.dart';
import 'package:scio_security/core/services/api.dart';
import 'package:scio_security/core/viewmodels/base_model.dart';

import '../../locator.dart';

class ReportFormsModel extends BaseModel {
  Api? _api = locator<Api>();
  List<FormData>? forms;

  Future<Map<String, dynamic>> getForms(Map<String, String> data) async {
    setState(ViewState.Busy);
    try {
      var r = await _api!.getReportsForm(data);
      print("#######################");
      print(r);
      if (r['success']) {
        var d = GetFormsResponse.fromJson(r['response']);
        if (!d.errors!) {
          forms = d.data;
        }
      }
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }

  Future<Map<String, dynamic>> updateResponse(Map<String, dynamic> data) async {
    //UpdatePlannerResponse
    setState(ViewState.Busy);
    try {
      var r = await _api!.updateFormResponse(data);
      setState(ViewState.Idle);
      return r;
    } catch (e) {
      setState(ViewState.Idle);
      throw e;
    }
  }
}
