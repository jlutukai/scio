import 'package:flutter/cupertino.dart';

class NavigationService {
  GlobalKey<NavigatorState> _key = GlobalKey<NavigatorState>();

  GlobalKey<NavigatorState> get navigationKey => _key;

  void pop() {
    return _key.currentState!.pop();
  }

  Future<dynamic> goTo(String route, {dynamic arguments}) {
    return _key.currentState!.pushNamed(route, arguments: arguments);
  }
}
