import 'package:flutter/foundation.dart';
import 'package:sqflite/sqflite.dart' as sql;

import '../../utils/useful.dart';
import '../models/time_sheet_response.dart';

class SQLHelper {
  static Future<void> createTables(sql.Database database) async {
    await database.execute("""CREATE TABLE timeSheet(
        timeSheetId INTEGER PRIMARY KEY,
        status TEXT,
        date TEXT,
        shiftId INTEGER,
        employeeId INTEGER,
        staffPhone TEXT, 
        name TEXT, 
        modified INTEGER, 
        reason TEXT, 
        imageIn TEXT, 
        imageOut TEXT, 
        biometricId TEXT, 
        biometricIn TEXT, 
        biometricOut TEXT, 
        timeIn TEXT, 
        timeOut TEXT, 
        fileName TEXT, 
        commentIn TEXT, 
        commentOut TEXT,
        syncStatus INTEGER
      )
      """);
  }

  static Future<sql.Database> db() async {
    return sql.openDatabase(
      'scio.db',
      version: 1,
      onCreate: (sql.Database database, int version) async {
        await createTables(database);
      },
    );
  }

  // save time sheets
  static Future saveTimeSheets(List<TimeSheetData> _timeSheetData) async {
    final db = await SQLHelper.db();
    try {
      db.transaction((txn) async {
        _timeSheetData.forEach((element) async {
          var existing =
              await getOneTimeSheet((int.tryParse(element.id ?? "") ?? 0));
          if (existing == null) {
            final data = element.toEntity();
            await db.insert('timeSheet', data,
                conflictAlgorithm: sql.ConflictAlgorithm.replace);
          }
          if (existing != null && existing.syncStatus == syncedStatus) {
            final data = element.toEntity();
            await db.insert('timeSheet', data,
                conflictAlgorithm: sql.ConflictAlgorithm.replace);
          }
        });
      });
    } catch (e) {
      debugPrint("Saving Error => ${e.toString()}");
      throw e;
    }
  }

  //get one time sheet
  static Future<TimeSheetData?> getOneTimeSheet(int id) async {
    final db = await SQLHelper.db();
    var result = await db.query('timeSheet',
        where: "timeSheetId = ?", whereArgs: [id], limit: 1);
    if (result.isEmpty) {
      return null;
    } else {
      return TimeSheetData.fromEntity(result.first);
    }
  }

  // get all time sheets
  static Future<List<TimeSheetData>> getTimeSheets(
      String status, String startDate, String endDate, int shiftId) async {
    final db = await SQLHelper.db();
    List<TimeSheetData> timeSheets = [];
    var result = await db.query('timeSheet',
        where:
            "status = ? AND shiftId = ? AND date BETWEEN date('$startDate') AND date('$endDate')",
        whereArgs: [status, shiftId],
        orderBy: "timeSheetId");
    if (result.isNotEmpty) {
      timeSheets = <TimeSheetData>[];
      result.forEach((v) {
        timeSheets.add(new TimeSheetData.fromEntity(v));
      });
    }
    return timeSheets;
  }

  // get un synced time sheets
  static Future<List<TimeSheetData>> getUnSyncedTimeSheets() async {
    final db = await SQLHelper.db();
    List<TimeSheetData> timeSheets = [];
    var result = await db.query('timeSheet',
        where: "syncStatus = ?",
        whereArgs: [unSyncedStatus],
        orderBy: "timeSheetId");
    if (result.isNotEmpty) {
      timeSheets = <TimeSheetData>[];
      result.forEach((v) {
        timeSheets.add(new TimeSheetData.fromEntity(v));
      });
    }
    return timeSheets;
  }

  // Update time sheet
  static Future<int> updateTimeSheet(TimeSheetData _timeSheetData) async {
    final db = await SQLHelper.db();
    final data = _timeSheetData.toEntity();
    final result = await db.update('timeSheet', data,
        where: "timeSheetId = ?", whereArgs: [_timeSheetData.id]);
    return result;
  }

  // Delete all
  static Future<void> deleteItem(int id) async {
    final db = await SQLHelper.db();
    try {
      await db.delete("timeSheet");
    } catch (err) {
      debugPrint("Something went wrong when deleting an item: $err");
    }
  }
}
