// import 'package:firebase_messaging/firebase_messaging.dart';
// import 'package:scio_security/utils/useful.dart';
//
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:scio_security/utils/useful.dart';

import '../../main.dart';

class PushNotificationService {
  final FirebaseMessaging _fcm = FirebaseMessaging.instance;
  // final NavigationService navigator = locator<NavigationService>();

  void initialise() {
    // accountInfo = Hive.box<String>("accountInfo");
    FirebaseMessaging.onMessage.listen((RemoteMessage event) {
      print("message recieved");
      print(event.notification!.body);
    });
    FirebaseMessaging.onMessageOpenedApp.listen((message) {
      print('Message clicked!');
    });
    FirebaseMessaging.onBackgroundMessage((message) => messageHandler(message));
  }

  Future<String> getFCMToken() async {
    var t = await _fcm.getToken();
    setTokenId(t!);
    return t;
  }

  // void navigateToPage(Map<String, dynamic> message) {
  //   var data = message['data'];
  //   var r = data['screen'];
  //   var teamId = message['data']['teamID'];
  //
  //   if (r != null) {
  //     print('screeeen data:$r');
  //     if (r == 'connection') {
  //       navigator.goTo(r);
  //     } else if (r == 'comment') {
  //       navigator.goTo(r); // feed_id
  //     } else if (r == 'message') {
  //       navigator.goTo(r, arguments: teamId); // team_id
  //     } else if (r == 'challenge_added') {
  //       navigator.goTo(r, arguments: teamId); // team_id
  //     } else if (r == 'feed_message') {
  //       navigator.goTo(r); // team_id
  //     }
  //   }
  // }
}
