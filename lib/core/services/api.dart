import 'dart:io';

import 'package:dio/dio.dart';
import 'package:dio_http_cache/dio_http_cache.dart';
import 'package:hive/hive.dart';
import 'package:http_parser/http_parser.dart';
import 'package:mime_type/mime_type.dart';
import 'package:scio_security/core/models/login_response.dart';

class Api {
  static const baseUrl = 'https://office.scioerp.net/app_api';
  late Dio _dio;
  String firebaseId = "";
  late Box<String> accountInfo;

  Api() {
    BaseOptions options =
        BaseOptions(receiveTimeout: 50000, connectTimeout: 50000);
    _dio = Dio(options);
    _dio.interceptors.add(LogInterceptor(
        request: true,
        requestBody: true,
        requestHeader: true,
        responseHeader: true,
        responseBody: true));
    _dio.interceptors
        .add(DioCacheManager(CacheConfig(baseUrl: baseUrl)).interceptor);
    _dio.options.headers["content-type"] = "application/json";
    accountInfo = Hive.box<String>("accountInfo");
  }

  String? getToken() {
    return accountInfo.get("sessionToken");
  }

  saveToken(String token) {
    accountInfo.put("sessionToken", token);
  }

  saveUrl(String? url) {
    accountInfo.put("url", "https://$url");
  }

  String? getUrl() {
    return accountInfo.get("url");
  }

  saveUser(String userId, String userName) {
    accountInfo.put("userId", userId);
    accountInfo.put("userName", userName);
  }

  Future<Map<String, dynamic>> verifyUrl(
    FormData data,
  ) async {
    try {
      String endPoint = "$baseUrl/scio_address.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> login(FormData data) async {
    try {
      String endPoint = "${getUrl()}/app_api/login.php";
      Response response = await _dio.post(endPoint, data: data);
      if (response.statusCode == 200) {
        var d = LoginResponse.fromJson(response.data);
        if (d.loginData != null) {
          saveUser(d.loginData!.data!.id ?? "", d.loginData!.data!.name ?? "");
        }
      }
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getSites() async {
    try {
      String endPoint = "${getUrl()}/app_api/get_site.php";
      Response response = await _dio.get(endPoint,
          options: buildCacheOptions(Duration(days: 9), forceRefresh: true));
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> createVisitor(Map<String, dynamic> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/create_visitor.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getVisitors(Map<String, String> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/get_visitor.php";
      Response response = await _dio.get(endPoint,
          queryParameters: data,
          options: buildCacheOptions(Duration(days: 1), forceRefresh: true));
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> checkOutVisitor(Map<String, String> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/check_out_visitor.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getPlanners(Map<String, String> data) async {
    try {
      String endPoint =
          "${getUrl()}/app_api/attendance/get_planner_details.php";
      Response response = await _dio.get(endPoint,
          queryParameters: data,
          options: buildCacheOptions(Duration(days: 1), forceRefresh: true));
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getTimeSheet(Map<String, String> data) async {
    try {
      String endPoint =
          "${getUrl()}/app_api/attendance/get_timesheet_details.php";
      Response response = await _dio.get(endPoint,
          queryParameters: data,
          options: buildCacheOptions(Duration(days: 1), forceRefresh: true));
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getTimeSheetEmployees(
      Map<String, String> data) async {
    try {
      String endPoint =
          "${getUrl()}/app_api/attendance/get_timesheet_employees.php";
      Response response = await _dio.get(endPoint, queryParameters: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> updateStatus(Map<String, String> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/attendance/update_status.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> uploadVerificationImage(File file) async {
    var endpoint = "${getUrl()}/app_api/attendance/upload_picture.php";

    String fileName = file.path.split('/').last;
    String mimeType = mime(fileName)!;
    String mimee = mimeType.split('/')[0];
    String type = mimeType.split('/')[1];
    FormData formData = new FormData.fromMap({
      "image": await MultipartFile.fromFile(file.path,
          filename: fileName, contentType: MediaType(mimee, type)),
    });

    Dio dio = new Dio();
    dio.options.headers["content-type"] = "multipart/form-data";
    // dio.interceptors.add(LoggingInterceptor());

    try {
      Response response = await dio.post(
        endpoint,
        data: formData,
        onSendProgress: (received, total) {
          if (total != -1) {
            print((received / total * 100).toStringAsFixed(0) + "%");
          }
        },
      );
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getPlannerReport(
      Map<String, String> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/report/get_planner_report.php";
      Response response = await _dio.get(endPoint,
          queryParameters: data,
          options: buildCacheOptions(Duration(days: 1), forceRefresh: true));
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getCategories() async {
    try {
      String endPoint = "${getUrl()}/app_api/report/get_checklist_category.php";
      Response response = await _dio.get(endPoint,
          options: buildCacheOptions(Duration(days: 1), forceRefresh: true));
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getChecklist(Map<String, String> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/report/get_planner_checklist.php";
      Response response = await _dio.get(endPoint,
          queryParameters: data,
          options: buildCacheOptions(Duration(days: 10), forceRefresh: true));
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getDefaultChecklist() async {
    try {
      String endPoint = "${getUrl()}/app_api/report/get_default_checklist.php";
      Response response = await _dio.get(endPoint,
          options: buildCacheOptions(Duration(days: 10), forceRefresh: true));
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> updateResponse(Map<String, String> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/report/update_response.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> updateToken(Map<String, String> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/update_token.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> removeToken(Map<String, String> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/logout.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getReportsForm(Map<String, String> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/report/get_form.php";
      Response response = await _dio.get(endPoint,
          queryParameters: data,
          options: buildCacheOptions(Duration(days: 10), forceRefresh: true));
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> updateFormResponse(
      Map<String, dynamic> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/report/update_response.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getTaskTypes() async {
    try {
      String endPoint = "${getUrl()}/app_api/task_new/get_task_type.php";
      Response response = await _dio.get(endPoint,
          options: buildCacheOptions(Duration(days: 10), forceRefresh: true));
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getFunctions() async {
    try {
      String endPoint = "${getUrl()}/app_api/task_new/get_function.php";
      Response response = await _dio.get(endPoint,
          options: buildCacheOptions(Duration(days: 10), forceRefresh: true));
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getStaff() async {
    try {
      String endPoint = "${getUrl()}/app_api/task_new/get_staff.php";
      Response response = await _dio.get(endPoint,
          options: buildCacheOptions(Duration(days: 10), forceRefresh: true));
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> createTask(Map<String, dynamic> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/task_new/create_task.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getTasks(Map<String, dynamic> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/task_new/get_task.php";
      Response response = await _dio.get(
        endPoint, queryParameters: data,
        // options: buildCacheOptions(Duration(days: 10), forceRefresh: true)
      );
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> updateTaskProgress(
      Map<String, String> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/task_new/update_progress.php";
      Response response = await _dio.post(endPoint, queryParameters: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> createTaskComment(
      Map<String, String> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/task_new/create_comment.php";
      Response response = await _dio.post(endPoint, data: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> getTaskComment(Map<String, String> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/task_new/get_comment.php";
      Response response = await _dio.get(endPoint, queryParameters: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  Future<Map<String, dynamic>> editTaskDate(Map<String, String> data) async {
    try {
      String endPoint = "${getUrl()}/app_api/task_new/edit_deadline.php";
      Response response = await _dio.post(endPoint, queryParameters: data);
      return getErrorCode(response);
    } on SocketException {
      final jsonResponse = {
        'success': false,
        'response': "Check your Internet connection"
      };
      return jsonResponse;
    } on FormatException {
      final jsonResponse = {
        'success': false,
        'response': "Bad response format"
      };
      return jsonResponse;
    } on HttpException {
      final jsonResponse = {
        'success': false,
        'response': "Server is unreachable" //Server not responding
      };
      return jsonResponse;
    }
  }

  // _handleError(error) {
  //   String errorDescription = "";
  //   if (error is DioError) {
  //     DioError dioError = error;
  //     switch (dioError.type) {
  //       case DioErrorType.CANCEL:
  //         errorDescription = "Request to API server was cancelled";
  //         break;
  //       case DioErrorType.CONNECT_TIMEOUT:
  //         errorDescription = "Connection timeout with API server";
  //         break;
  //       case DioErrorType.DEFAULT:
  //         errorDescription =
  //             "Connection to API server failed due to internet connection";
  //         break;
  //       case DioErrorType.RECEIVE_TIMEOUT:
  //         errorDescription = "Receive timeout in connection with API server";
  //         break;
  //       case DioErrorType.RESPONSE:
  //         errorDescription = "${dioError.response.data['error']} ";
  //         break;
  //       case DioErrorType.SEND_TIMEOUT:
  //         errorDescription = "Send timeout in connection with API server";
  //         break;
  //     }
  //   } else {
  //     errorDescription = "Unexpected error occurred";
  //   }
  //   return errorDescription;
  // }

  Map<String, dynamic> getErrorCode(Response response) {
    switch (response.statusCode) {
      case 200:
        final result = response.data;
        final jsonResponse = {'success': true, 'response': result};
        return jsonResponse;
      case 201:
        final result = response.data;
        final jsonResponse = {'success': true, 'response': result};
        return jsonResponse;
      case 400:
        final jsonResponse = {
          'success': false,
          'response': "Bad response ${response.statusCode}"
        };
        return jsonResponse;
      case 401:
        final jsonResponse = {
          'success': false,
          'response':
              "UnAuthorized Access, Login to continue ${response.statusCode}"
        };
        return jsonResponse;
      case 404:
        final jsonResponse = {
          'success': false,
          'response': "File Not Found! ${response.statusCode}"
        };
        return jsonResponse;
      case 500:
        final jsonResponse = {
          'success': false,
          'response': "Internal Server Error ${response.statusCode}"
        };
        return jsonResponse;
      case 501:
        final jsonResponse = {
          'success': false,
          'response': "Internal Server Error ${response.statusCode}"
        };
        return jsonResponse;
      case 502:
        final jsonResponse = {
          'success': false,
          'response': "Internal Server error ${response.statusCode}"
        };
        return jsonResponse;
      default:
        final jsonResponse = {
          'success': false,
          'response': "Something Went wrong ${response.statusCode}"
        };
        return jsonResponse;
    }
  }
}

//https://www.getpostman.com/collections/66e34611274b84543b43

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          ((X509Certificate cert, String host, int port) {
        final isValidHost = ["https://${Api().getUrl()}"]
            .contains(host); // <-- allow only hosts in array
        return true;
      });
  }
}
