import 'package:get_it/get_it.dart';
import 'package:scio_security/core/viewmodels/ReportFormModel.dart';
import 'package:scio_security/core/viewmodels/check_out_visitor_model.dart';
import 'package:scio_security/core/viewmodels/home_model.dart';
import 'package:scio_security/core/viewmodels/planner_model.dart';
import 'package:scio_security/core/viewmodels/sites_model.dart';
import 'package:scio_security/core/viewmodels/task_model.dart';
import 'package:scio_security/core/viewmodels/time_sheet_model.dart';
import 'package:scio_security/core/viewmodels/timesheet_manual_model.dart';
import 'package:scio_security/core/viewmodels/visitor_logs_model.dart';
import 'package:scio_security/core/viewmodels/visitors_onsite_model.dart';

import 'core/services/NavigationService.dart';
import 'core/services/api.dart';
import 'core/services/push_notifications_service.dart';
import 'core/viewmodels/check_in_guard_model.dart';
import 'core/viewmodels/check_out_guard_model.dart';
import 'core/viewmodels/daily_reports_model.dart';
import 'core/viewmodels/guards_manual_onsite_model.dart';
import 'core/viewmodels/guards_manual_out_model.dart';
import 'core/viewmodels/guards_manual_planned_model.dart';
import 'core/viewmodels/guards_on_site_model.dart';
import 'core/viewmodels/login_model.dart';
import 'core/viewmodels/report_questions_model.dart';
import 'core/viewmodels/reports_planner_model.dart';
import 'core/viewmodels/url_model.dart';
import 'core/viewmodels/visitors_log_model.dart';

GetIt locator = GetIt.instance;

void setUpLocator() {
  locator.registerLazySingleton(() => Api());
  locator.registerLazySingleton(() => NavigationService());
  locator.registerLazySingleton(() => PushNotificationService());

  locator.registerFactory(() => UrlModel());
  locator.registerFactory(() => LoginModel());
  locator.registerFactory(() => SitesModel());
  locator.registerFactory(() => VisitorsLogModel());
  locator.registerFactory(() => PlannerModel());
  locator.registerFactory(() => ReportsPlannerModel());
  locator.registerFactory(() => TimeSheetModel());
  locator.registerFactory(() => CheckInGuardModel());
  locator.registerFactory(() => CheckOutGuardModel());
  locator.registerFactory(() => GuardsOnSiteModel());
  locator.registerFactory(() => DailyReportsModel());
  locator.registerFactory(() => ReportsQuestionsModel());
  locator.registerFactory(() => HomeModel());
  locator.registerFactory(() => TimeSheetManualModel());
  locator.registerFactory(() => CheckInGuardManualModel());
  locator.registerFactory(() => CheckOutGuardManualModel());
  locator.registerFactory(() => OnSiteManualModel());
  locator.registerFactory(() => VisitorLogsModel());
  locator.registerFactory(() => VisitorOnSiteModel());
  locator.registerFactory(() => CheckOutVisitorModel());
  locator.registerFactory(() => ReportFormsModel());
  locator.registerFactory(() => TaskModel());
}
