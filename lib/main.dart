import 'dart:io';

import 'package:camera/camera.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:hive/hive.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:scio_security/ui/screens/home_page.dart';
import 'package:scio_security/ui/screens/login_page.dart';
import 'package:scio_security/ui/screens/reports_planner.dart';
import 'package:scio_security/ui/screens/settings_page.dart';
import 'package:scio_security/ui/screens/sites_page.dart';
import 'package:scio_security/ui/screens/splash.dart';
import 'package:scio_security/ui/screens/taskManager/task_manager.dart';
import 'package:scio_security/ui/screens/test/camera_page.dart';
import 'package:scio_security/ui/screens/url_page.dart';

import 'core/models/local/localModels.dart';
import 'core/services/NavigationService.dart';
import 'core/services/api.dart';
import 'locator.dart';

late List<CameraDescription> cameras;
final NavigationService navigator = locator<NavigationService>();
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  cameras = await availableCameras();
  Directory document = await getApplicationDocumentsDirectory();

  Hive
    ..init(document.path)
    ..registerAdapter(CheckListResultAdapter())
    ..registerAdapter(TimeSheetDetailsAdapter())
    ..registerAdapter(VisitorAdapter())
    ..registerAdapter(ItemObjAdapter())
    ..registerAdapter(PlannerDataAdapter())
    ..registerAdapter(DeviceDetailsAdapter())
    ..registerAdapter(UpdateFormDataAdapter())
    ..registerAdapter(FormItemsAdapter())
    ..registerAdapter(CreateTaskDataAdapter())
    ..registerAdapter(AssignedToAdapter())
    ..registerAdapter(SubTaskAdapter())
    ..registerAdapter(ShiftsAdapter());
  await Hive.openBox<String>("accountInfo");
  await Hive.openBox('time_sheet');
  await Hive.openBox('device_details');
  await Hive.openBox('planner');
  await Hive.openBox('shift');
  await Hive.openBox('images');
  // await Hive.openBox('time_sheet_unsaved');
  await Hive.openBox('time_sheet_unsaved_manual');
  // await Hive.openBox('time_sheet_manual');
  // await Hive.openBox('time_sheet_in');
  await Hive.openBox('visitors_in');
  await Hive.openBox('visitors_out');
  await Hive.openBox('visitor_in');
  await Hive.openBox('visitor_out');
  await Hive.openBox('check_list_answer');
  await Hive.openBox('form_response');
  await Hive.openBox('form_response_questions');
  await Hive.openBox('check_list');
  await Hive.openBox('created_task_data');
  setUpLocator();
  HttpOverrides.global = new MyHttpOverrides();
  // Workmanager().initialize(callbackDispatcher);
  // Workmanager().registerPeriodicTask("2", "simplePeriodicTask",
  //     frequency: Duration(minutes: 15));

  runApp(MyApp());
}

// void callbackDispatcher() {
//   Workmanager().executeTask((task, inputData) {
//     print("start_work manager @@@@@@@@@@@@@@@@@@@@@@");
//     // initialise the plugin of flutterlocalnotifications.
//     DateFormat dateFormat = new DateFormat.Hm();
//     DateTime now = DateTime.now();
//     DateTime start = dateFormat.parse("18:00");
//     start =
//         new DateTime(now.year, now.month, now.day, start.hour, start.minute);
//     DateTime end = start.add(Duration(hours: 12));
//     print(
//         "$now @@@@@@@@@@@@@@@@@@@@@@ $start @@@@@@@@@@@@@@@@@@@@@@@@@@@ $end @@@@@@@@@@@@@@@@@ ${now.isAfter(start) && now.isBefore(end)}");
//     if (now.isAfter(start) && now.isBefore(end)) {
//     FlutterLocalNotificationsPlugin flip =
//         new FlutterLocalNotificationsPlugin();
//
//     // app_icon needs to be a added as a drawable
//     // resource to the Android head project.
//     var initializationSettingsAndroid =
//         new AndroidInitializationSettings('@mipmap/ic_launcher');
//     var initializationSettingsIOS = new IOSInitializationSettings();
//
//     // initialise settings for both Android and iOS device.
//     var settings = new InitializationSettings(
//         android: initializationSettingsAndroid, iOS: initializationSettingsIOS);
//     flip.initialize(settings, onSelectNotification: onSelectNotification);
//     _showNotificationWithDefaultSound(flip);
//     }
//     print("end_work manager @@@@@@@@@@@@@@@@@@@@@@");
//     return Future.value(true);
//   });
// }

// Future<void> _showNotificationWithDefaultSound(
//     FlutterLocalNotificationsPlugin flip) async {
//   var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
//       'your channel id', 'your channel name', 'your channel description',
//       importance: Importance.max, priority: Priority.high);
//   var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
//
//   var platformChannelSpecifics = new NotificationDetails(
//       android: androidPlatformChannelSpecifics,
//       iOS: iOSPlatformChannelSpecifics);
//   await flip.show(
//       0,
//       'Scio Axs',
//       'Random check time, Open app to perform check in',
//       platformChannelSpecifics,
//       payload: 'Default_Sound');
// }
//
// Future onSelectNotification(String? payload) async {
//   print(
//       "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@   working ########################");
//   navigator.navigationKey.currentState!.push(
//     MaterialPageRoute(
//       builder: (BuildContext context) => RandomRequestPage(),
//     ),
//   );
// }

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      navigatorKey: locator<NavigationService>().navigationKey,
      debugShowCheckedModeBanner: false,
      title: 'Scio axs',
      theme: ThemeData(
        primaryColor: Colors.black,
        brightness: Brightness.light,
      ),
      darkTheme: ThemeData(
        primaryColor: Colors.black,
        brightness: Brightness.dark,
      ),
      themeMode: ThemeMode.dark,
      routes: {
        '/': (context) => SplashScreen(),
        UrlPage.tag: (context) => UrlPage(),
        LoginPage.tag: (context) => LoginPage(),
        HomePage.tag: (context) => HomePage(),
        SitePage.tag: (context) => SitePage(),
        SettingsPage.tag: (context) => SettingsPage(),
        ReportsPlanner.tag: (context) => ReportsPlanner(),
        TaskManagerPage.tag: (context) => TaskManagerPage()
      },
    );
  }
}

Future<void> messageHandler(RemoteMessage? message) async {
  print('background message ${message?.notification?.body}');
}
