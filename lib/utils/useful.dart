import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:hive/hive.dart';
import 'package:intl/intl.dart';
import 'package:scio_security/core/models/local/localModels.dart';
import 'package:scio_security/core/services/api.dart';
import 'package:url_launcher/url_launcher.dart';

Color fromHex(String hexString) {
  final buffer = StringBuffer();
  if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
  buffer.write(hexString.replaceFirst('#', ''));
  return Color(int.parse(buffer.toString(), radix: 16));
}

const String appVersion = "2.0.40";
final df = new DateFormat('yyyy-MM-dd');

const String hillaryPhone = "0722984495";
const String aliciaPhone = "0715219666";
const String chelimoPhone = "0700316962";

const String deepOrange = '#f48100';
const int syncedStatus = 1;
const int unSyncedStatus = 0;
const String yellow = '#f4b600';
const String grey = '#aca793';
const String orange = '#f49600';
const String blinkIdLicense =
    "sRwAAAAeY29tLnNjaW9zZWN1cml0eS5zY2lvX3NlY3VyaXR5sJr1Eu4YbMMcFMoyM96qR/6XrY4fOBpxuMaEkyBeBkXIge67NTCQ19RK3bWLe0Lp45cqtKGB3MP1ZR2lJYf78iitOqS7irhHIq0ePGVHhiAnGYBMg/gQm6d/F2c+rJMqID6fOaPpVp+YzmAfMGnbxDLsn25xP2d5q6swiwG9VFLA0Ey4WsD2PZ/WVPC5MWUV+o1lyo2cwRZdkDFl6hcIkx1jdumPXoYRS3i3dltZvgeqLfM9U0y0ACAyBcpIVN0R4STrRSomQ/OtLcX0qEBNnra6cg7uajFCcjBD7Kiv+o5XM+r8m4UZZiyLX5d3C/VcYPnfEQSY";
const String lorem =
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem unknown printer took a galley of type and scrambled it to make a type specimen book. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem unknown printer took a galley of type and scrambled it to make a type specimen book.";
List<DeviceDetails> getDevices() {
  List<DeviceDetails> devices = [];
  DeviceDetails x3 = DeviceDetails(
      name: "Ulefone-Armor_X3",
      xOrigin: 375,
      yOrigin: 0,
      height: 460,
      width: 100);
  DeviceDetails note = DeviceDetails(
      name: "samsung-SM-N950U",
      xOrigin: 395,
      yOrigin: 0,
      height: 460,
      width: 100);
  DeviceDetails doogee = DeviceDetails(
      name: "DOOGEE-S60Lite",
      xOrigin: 0,
      yOrigin: 345,
      height: 100,
      width: 460);
  DeviceDetails x5 = DeviceDetails(
      name: "Ulefone-Armor X5",
      xOrigin: 0,
      yOrigin: 345,
      height: 100,
      width: 460);
  DeviceDetails xaomi = DeviceDetails(
      name: "Redmi-Redmi Note 8 Pro",
      xOrigin: 0,
      yOrigin: 395,
      height: 100,
      width: 460);
  devices.add(x3);
  devices.add(x5);
  devices.add(note);
  devices.add(doogee);
  devices.add(xaomi);
  return devices;
}

showToast(String msg) {
  Fluttertoast.showToast(msg: msg);
}

Future<bool> checkConnection() async {
  try {
    final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      return true;
    }
    return false;
  } on SocketException catch (_) {
    return false;
  }
}

Future<bool> checkServerConnection() async {
  print("@@@ ${Api().getUrl()}/login.php ");
  try {
    final result = await InternetAddress.lookup(
        '${Api().getUrl()}/app_api/attendance/get_timesheet_employees.php');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      return true;
    }
    return false;
  } on SocketException catch (_) {
    return false;
  }
}

setPlanner(PlannerData? data) {
  Box currentPlanner = Hive.box("planner");
  currentPlanner.put("current_planner", data);
}

PlannerData? getPlanner() {
  Box currentPlanner = Hive.box("planner");
  return currentPlanner.get("current_planner");
}

setShift(Shifts data) {
  Box currentPlanner = Hive.box("planner");
  currentPlanner.put("current_shift", data);
}

Shifts getShift() {
  Box currentPlanner = Hive.box("planner");
  return currentPlanner.get("current_shift");
}

setShiftID(String data) {
  Box<String> currentPlanner = Hive.box("accountInfo");
  currentPlanner.put("shift_id", data);
}

int getShiftId() {
  Box<String> currentPlanner = Hive.box("accountInfo");
  return int.tryParse(currentPlanner.get("shift_id") ?? "0") ?? 0;
}

setShiftDate(String data) {
  Box<String> currentPlanner = Hive.box("accountInfo");
  currentPlanner.put("shift_date", data);
}

String? getShiftDate() {
  Box<String> currentPlanner = Hive.box("accountInfo");
  return currentPlanner.get("shift_date");
}

setUserId(String id) {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  accountInfo.put("userId", id);
}

String? getUserId() {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  return accountInfo.get("userId");
}

setTokenId(String id) {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  accountInfo.put("fcm_token", id);
}

String? getTokenId() {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  return accountInfo.get("fcm_token");
}

setDeviceModel(DeviceDetails d) {
  Box accountInfo = Hive.box("device_details");
  accountInfo.put("device", d);
}

DeviceDetails? getDeviceModel() {
  Box accountInfo = Hive.box("device_details");
  return accountInfo.get("device");
}

bool getCanScanID() {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  return accountInfo.get("can_scan_id") == "true";
}

setRequireScanID(bool can) {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  accountInfo.put("can_scan_id", can.toString());
}

bool getRequireDepart() {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  return accountInfo.get("department") == "true";
}

setRequireDepart(bool can) {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  accountInfo.put("department", can.toString());
}

bool getRequireDesignation() {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  return accountInfo.get("designation") == "true";
}

setRequireDesignation(bool can) {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  accountInfo.put("designation", can.toString());
}

bool getRequirePersonToSee() {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  return accountInfo.get("person_to_see") == "true";
}

setRequirePersonToSee(bool can) {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  accountInfo.put("person_to_see", can.toString());
}

bool getRequireReasonForVisit() {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  return accountInfo.get("reason_for_visit") == "true";
}

setRequireReasonForVisit(bool can) {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  accountInfo.put("reason_for_visit", can.toString());
}

bool getRequireItems() {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  return accountInfo.get("items") == "true";
}

setRequireItems(bool can) {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  accountInfo.put("items", can.toString());
}

bool getRequireCarReg() {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  return accountInfo.get("car_reg") == "true";
}

setRequireCarReg(bool can) {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  accountInfo.put("car_reg", can.toString());
}

bool getRequireVehiclePass() {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  return accountInfo.get("car_pass") == "true";
}

setRequireVehiclePass(bool can) {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  accountInfo.put("car_pass", can.toString());
}

bool getRequirePass() {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  return accountInfo.get("requirePass") == "true";
}

setRequirePass(bool can) {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  accountInfo.put("requirePass", can.toString());
}

bool getRequireHouseNo() {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  return accountInfo.get("house_no") == "true";
}

setRequireHouseNo(bool can) {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  accountInfo.put("house_no", can.toString());
}

bool getRequireBlockNo() {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  return accountInfo.get("block_no") == "true";
}

setRequireBlockNo(bool can) {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  accountInfo.put("block_no", can.toString());
}

bool getRequireOfficeNo() {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  return accountInfo.get("office_no") == "true";
}

setRequireOfficeNo(bool can) {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  accountInfo.put("office_no", can.toString());
}

bool getRequireCompany() {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  return accountInfo.get("company_name") == "true";
}

setRequireCompany(bool can) {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  accountInfo.put("company_name", can.toString());
}

bool getRequireVisitorsLog() {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  return accountInfo.get("visitors_logs") == "true";
}

setRequireVisitorsLog(bool can) {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  accountInfo.put("visitors_logs", can.toString());
}

bool getRequireAttendance() {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  return accountInfo.get("attendance") == "true";
}

setRequireAttendance(bool can) {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  accountInfo.put("attendance", can.toString());
}

bool getRequireReports() {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  return accountInfo.get("reports") == "true";
}

setRequireReports(bool can) {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  accountInfo.put("reports", can.toString());
}

void showErrorDialog(String? r, BuildContext context, String s, [bool? bool]) {
  showModalBottomSheet(
      backgroundColor: Colors.transparent,
      clipBehavior: Clip.antiAlias,
      context: context,
      builder: (context) {
        return SingleChildScrollView(
          child: Container(
            color: Colors.transparent,
            child: Container(
              decoration: BoxDecoration(
                color: Colors.grey[900],
                // color: Theme.of(context).canvasColor,
                borderRadius: BorderRadius.only(
                    topLeft: const Radius.circular(15),
                    topRight: const Radius.circular(15)),
              ),
              child: Padding(
                padding:
                    const EdgeInsets.only(right: 20.0, left: 20.0, top: 20),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.error_outline_rounded,
                          size: 74,
                          color: fromHex(deepOrange),
                        )
                      ],
                    ),
                    ListTile(
                      title: Center(
                        child: Text(
                          r!,
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w800,
                              fontSize: 18),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          s,
                          style:
                              TextStyle(color: fromHex(yellow), fontSize: 14),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Text('Please contact for more help :-'),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                vertical: 8,
                              ),
                              child: GestureDetector(
                                onTap: () {
                                  launch("tel://$hillaryPhone");
                                },
                                child: Row(
                                  children: [
                                    Text('Jesse - '),
                                    Text('$hillaryPhone')
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                vertical: 8,
                              ),
                              child: GestureDetector(
                                onTap: () {
                                  launch("tel://$aliciaPhone");
                                },
                                child: Row(
                                  children: [
                                    Text('Alice - '),
                                    Text('$aliciaPhone')
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                vertical: 8,
                              ),
                              child: GestureDetector(
                                onTap: () {
                                  launch("tel://$chelimoPhone");
                                },
                                child: Row(
                                  children: [
                                    Text('Purity - '),
                                    Text('$chelimoPhone')
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      children: [
                        Expanded(child: Container()),
                        Expanded(
                          child: ListTile(
                            onTap: () async {
                              Navigator.pop(context);
                            },
                            title: Center(
                              child: Text(
                                'Close',
                                style: TextStyle(color: Colors.deepOrange),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      });
}
