import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:scio_security/utils/useful.dart';

class Loader extends StatefulWidget {
  @override
  _LoaderState createState() => _LoaderState();
}

class _LoaderState extends State<Loader> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Column(
        children: [
          Expanded(
            child: Center(
                child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                    height: 200,
                    width: 200,
                    child: Lottie.asset('assets/loading.json')),
                Text('Please wait \n Loading...', style: TextStyle(color: fromHex(deepOrange)),)
              ],
            )),
          )
        ],
      ),
    );
  }
}
