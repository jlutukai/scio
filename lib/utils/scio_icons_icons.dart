/// Flutter icons ScioIcons
/// Copyright (C) 2020 by original authors @ fluttericon.com, fontello.com
/// This font was generated by FlutterIcon.com, which is derived from Fontello.
///
/// To use this font, place it in your fonts/ directory and include the
/// following in your pubspec.yaml
///
/// flutter:
///   fonts:
///    - family:  ScioIcons
///      fonts:
///       - asset: fonts/ScioIcons.ttf
///
/// 
/// * Font Awesome 4, Copyright (C) 2016 by Dave Gandy
///         Author:    Dave Gandy
///         License:   SIL ()
///         Homepage:  http://fortawesome.github.com/Font-Awesome/
/// * MFG Labs, Copyright (C) 2012 by Daniel Bruce
///         Author:    MFG Labs
///         License:   SIL (http://scripts.sil.org/OFL)
///         Homepage:  http://www.mfglabs.com/
/// * Font Awesome 5, Copyright (C) 2016 by Dave Gandy
///         Author:    Dave Gandy
///         License:   SIL (https://github.com/FortAwesome/Font-Awesome/blob/master/LICENSE.txt)
///         Homepage:  http://fortawesome.github.com/Font-Awesome/
///
import 'package:flutter/widgets.dart';

class ScioIcons {
  ScioIcons._();

  static const _kFontFam = 'ScioIcons';
  static const dynamic _kFontPkg = null;

  static const IconData login_1 = IconData(0xe809, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData logout = IconData(0xe80a, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData list_1 = IconData(0xe80b, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData login = IconData(0xf02c, fontFamily: _kFontFam, fontPackage: _kFontPkg);
  static const IconData users = IconData(0xf0c0, fontFamily: _kFontFam, fontPackage: _kFontPkg);
}
