import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:scio_security/ui/screens/test/camera_page.dart';
import 'package:scio_security/utils/useful.dart';

import 'home_page.dart';

class SettingsPage extends StatefulWidget {
  static const tag = 'settings';

  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  Box<String> accountInfo = Hive.box<String>("accountInfo");

  bool requirePass = true;
  bool requireScanId = true;
  bool requireDepartment = true;
  bool requireDesignation = true;
  bool requirePersonToSee = true;
  bool requireReasonForVisit = true;
  bool requireItems = true;
  bool requireCarReg = true;
  bool requireVehiclePass = true;
  bool requireHouseNo = true;
  bool requireBlockNo = true;
  bool requireOfficeNo = true;
  bool requireCompany = true;
  bool requireVisitorsLog = true;
  bool requireAttendance = true;
  bool requireReports = true;

  @override
  void initState() {
    requirePass = getRequirePass();
    requireScanId = getCanScanID();
    requireDepartment = getRequireDepart();
    requireDesignation = getRequireDesignation();
    requirePersonToSee = getRequirePersonToSee();
    requireReasonForVisit = getRequireReasonForVisit();
    requireItems = getRequireItems();
    requireCarReg = getRequireCarReg();
    requireVehiclePass = getRequireVehiclePass();
    requireHouseNo = getRequireHouseNo();
    requireBlockNo = getRequireBlockNo();
    requireOfficeNo = getRequireOfficeNo();
    requireCompany = getRequireCompany();
    requireVisitorsLog = getRequireVisitorsLog();
    requireAttendance = getRequireAttendance();
    requireReports = getRequireReports();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Navigator.pushNamedAndRemoveUntil(
            context, HomePage.tag, (Route<dynamic> route) => false);
        return false;
      },
      child: Stack(
        children: [
          Scaffold(
            backgroundColor: Colors.black,
            appBar: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
              title: Text(
                'Settings',
                style: TextStyle(
                    color: fromHex(deepOrange), fontWeight: FontWeight.bold),
              ),
              centerTitle: true,
              automaticallyImplyLeading: false,
            ),
            body: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    physics: BouncingScrollPhysics(),
                    child: Column(
                      children: [
                        SizedBox(
                          height: 20,
                        ),

                        Dismissible(
                          key: UniqueKey(),
                          direction: DismissDirection.startToEnd,
                          onDismissed: (DismissDirection direction) async {
                            // Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => CameraExampleHome()));
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => CameraExampleHome(
                                      s: "setup",
                                      isSetUp: true,
                                    )));
                            setState(() {});
                          },
                          child: InkWell(
                            onTap: () async {
                              // Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => CameraExampleHome()));
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => CameraExampleHome(
                                        s: "setup",
                                        isSetUp: true,
                                      )));
                            },
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 2.0, horizontal: 15),
                              child: Card(
                                color: Colors.white10.withOpacity(0.07),
                                child: Container(
                                  // color: fromHex("#114259"),
                                  height: 60,
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 1, horizontal: 20),
                                  child: Wrap(
                                    // mainAxisAlignment:
                                    // MainAxisAlignment.spaceBetween,
                                    children: [
                                      Row(
                                        crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                        children: [
                                          Icon(
                                            Icons.camera_alt,
                                            size: 32,
                                            color: fromHex(yellow),
                                          ),
                                          SizedBox(
                                            width: 60,
                                          ),
                                          Text(
                                            "Configure Camera for MRZ",
                                            style: TextStyle(
                                                color: fromHex("#c19d3d"),
                                                fontWeight: FontWeight.bold,
                                                fontSize: 18),
                                          )
                                        ],
                                      ),
                                      Icon(
                                        Icons.arrow_forward_ios_sharp,
                                        color: fromHex(yellow),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),


                        Container(
                          color: Colors.white10.withOpacity(0.07),
                          padding: EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          margin:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                          child: SwitchListTile(
                            activeTrackColor: fromHex(yellow).withOpacity(0.6),
                            activeColor: fromHex(yellow),
                            inactiveThumbColor: fromHex(grey).withOpacity(0.8),
                            inactiveTrackColor: fromHex(grey).withOpacity(0.4),
                            autofocus: true,
                            dense: true,
                            title: Text(
                              "Require Attendance",
                              style: TextStyle(
                                  color: fromHex(yellow),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                            value: requireAttendance,
                            onChanged: (bool value) async {
                              setState(() {
                                requireAttendance = value;
                                setRequireAttendance(value);
                              });
                            },
                          ),
                        ),
                        Container(
                          color: Colors.white10.withOpacity(0.07),
                          padding: EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          margin:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                          child: SwitchListTile(
                            activeTrackColor: fromHex(yellow).withOpacity(0.6),
                            activeColor: fromHex(yellow),
                            inactiveThumbColor: fromHex(grey).withOpacity(0.8),
                            inactiveTrackColor: fromHex(grey).withOpacity(0.4),
                            autofocus: true,
                            dense: true,
                            title: Text(
                              "Require Visitors Log",
                              style: TextStyle(
                                  color: fromHex(yellow),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                            value: requireVisitorsLog,
                            onChanged: (bool value) async {
                              setState(() {
                                requireVisitorsLog = value;
                                setRequireVisitorsLog(value);
                              });
                            },
                          ),
                        ),
                        Container(
                          color: Colors.white10.withOpacity(0.07),
                          padding: EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          margin:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                          child: SwitchListTile(
                            activeTrackColor: fromHex(yellow).withOpacity(0.6),
                            activeColor: fromHex(yellow),
                            inactiveThumbColor: fromHex(grey).withOpacity(0.8),
                            inactiveTrackColor: fromHex(grey).withOpacity(0.4),
                            autofocus: true,
                            dense: true,
                            title: Text(
                              "Require Reports",
                              style: TextStyle(
                                  color: fromHex(yellow),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                            value: requireReports,
                            onChanged: (bool value) async {
                              setState(() {
                                requireReports = value;
                                setRequireReports(value);
                              });
                            },
                          ),
                        ),
                        Container(
                          color: Colors.white10.withOpacity(0.07),
                          padding: EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          margin:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                          child: SwitchListTile(
                            activeTrackColor: fromHex(yellow).withOpacity(0.6),
                            activeColor: fromHex(yellow),
                            inactiveThumbColor: fromHex(grey).withOpacity(0.8),
                            inactiveTrackColor: fromHex(grey).withOpacity(0.4),
                            autofocus: true,
                            dense: true,
                            title: Text(
                              "Require visitor's Pass",
                              style: TextStyle(
                                  color: fromHex(yellow),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                            value: requirePass,
                            onChanged: (bool value) async {
                              if (!value && !requireScanId) {
                                showToast(
                                    "Warning!\n Turn require scan id on first!");
                                return;
                              }
                              setState(() {
                                requirePass = value;
                                setRequirePass(value);
                              });
                            },
                          ),
                        ),
                        getRequirePass()
                            ? Column(
                                children: [
                                  Container(
                                    color: Colors.white10.withOpacity(0.07),
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 10),
                                    margin: EdgeInsets.symmetric(
                                        horizontal: 10, vertical: 2),
                                    child: SwitchListTile(
                                      activeTrackColor:
                                          fromHex(yellow).withOpacity(0.6),
                                      activeColor: fromHex(yellow),
                                      inactiveThumbColor:
                                          fromHex(grey).withOpacity(0.8),
                                      inactiveTrackColor:
                                          fromHex(grey).withOpacity(0.4),
                                      autofocus: true,
                                      dense: true,
                                      title: Text(
                                        "Require Scan ID",
                                        style: TextStyle(
                                            color: fromHex(yellow),
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      value: requireScanId,
                                      onChanged: (bool value) async {
                                        setState(() {
                                          requireScanId = value;
                                          setRequireScanID(value);
                                        });
                                      },
                                    ),
                                  ),
                                ],
                              )
                            : Container(),
                        Container(
                          color: Colors.white10.withOpacity(0.07),
                          padding: EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          margin:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                          child: SwitchListTile(
                            activeTrackColor: fromHex(yellow).withOpacity(0.6),
                            activeColor: fromHex(yellow),
                            inactiveThumbColor: fromHex(grey).withOpacity(0.8),
                            inactiveTrackColor: fromHex(grey).withOpacity(0.4),
                            autofocus: true,
                            dense: true,
                            title: Text(
                              "Require Items",
                              style: TextStyle(
                                  color: fromHex(yellow),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                            value: requireItems,
                            onChanged: (bool value) async {
                              setState(() {
                                requireItems = value;
                                setRequireItems(value);
                              });
                            },
                          ),
                        ),
                        Container(
                          color: Colors.white10.withOpacity(0.07),
                          padding: EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          margin:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                          child: SwitchListTile(
                            activeTrackColor: fromHex(yellow).withOpacity(0.6),
                            activeColor: fromHex(yellow),
                            inactiveThumbColor: fromHex(grey).withOpacity(0.8),
                            inactiveTrackColor: fromHex(grey).withOpacity(0.4),
                            autofocus: true,
                            dense: true,
                            title: Text(
                              "Require Car Reg",
                              style: TextStyle(
                                  color: fromHex(yellow),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                            value: requireCarReg,
                            onChanged: (bool value) async {
                              setState(() {
                                requireCarReg = value;
                                setRequireCarReg(value);
                              });
                            },
                          ),
                        ),
                        Container(
                          color: Colors.white10.withOpacity(0.07),
                          padding: EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          margin:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                          child: SwitchListTile(
                            activeTrackColor: fromHex(yellow).withOpacity(0.6),
                            activeColor: fromHex(yellow),
                            inactiveThumbColor: fromHex(grey).withOpacity(0.8),
                            inactiveTrackColor: fromHex(grey).withOpacity(0.4),
                            autofocus: true,
                            dense: true,
                            title: Text(
                              "Require Vehicle Pass",
                              style: TextStyle(
                                  color: fromHex(yellow),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                            value: requireVehiclePass,
                            onChanged: (bool value) async {
                              setState(() {
                                requireVehiclePass = value;
                                setRequireVehiclePass(value);
                              });
                            },
                          ),
                        ),
                        Container(
                          color: Colors.white10.withOpacity(0.07),
                          padding: EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          margin:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                          child: SwitchListTile(
                            activeTrackColor: fromHex(yellow).withOpacity(0.6),
                            activeColor: fromHex(yellow),
                            inactiveThumbColor: fromHex(grey).withOpacity(0.8),
                            inactiveTrackColor: fromHex(grey).withOpacity(0.4),
                            autofocus: true,
                            dense: true,
                            title: Text(
                              "Require Department",
                              style: TextStyle(
                                  color: fromHex(yellow),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                            value: requireDepartment,
                            onChanged: (bool value) async {
                              setState(() {
                                requireDepartment = value;
                                setRequireDepart(value);
                              });
                            },
                          ),
                        ),
                        Container(
                          color: Colors.white10.withOpacity(0.07),
                          padding: EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          margin:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                          child: SwitchListTile(
                            activeTrackColor: fromHex(yellow).withOpacity(0.6),
                            activeColor: fromHex(yellow),
                            inactiveThumbColor: fromHex(grey).withOpacity(0.8),
                            inactiveTrackColor: fromHex(grey).withOpacity(0.4),
                            autofocus: true,
                            dense: true,
                            title: Text(
                              "Require Company Name",
                              style: TextStyle(
                                  color: fromHex(yellow),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                            value: requireCompany,
                            onChanged: (bool value) async {
                              setState(() {
                                requireCompany = value;
                                setRequireCompany(value);
                              });
                            },
                          ),
                        ),
                        Container(
                          color: Colors.white10.withOpacity(0.07),
                          padding: EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          margin:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                          child: SwitchListTile(
                            activeTrackColor: fromHex(yellow).withOpacity(0.6),
                            activeColor: fromHex(yellow),
                            inactiveThumbColor: fromHex(grey).withOpacity(0.8),
                            inactiveTrackColor: fromHex(grey).withOpacity(0.4),
                            autofocus: true,
                            dense: true,
                            title: Text(
                              "Require Office No.",
                              style: TextStyle(
                                  color: fromHex(yellow),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                            value: requireOfficeNo,
                            onChanged: (bool value) async {
                              setState(() {
                                requireOfficeNo = value;
                                setRequireOfficeNo(value);
                              });
                            },
                          ),
                        ),
                        Container(
                          color: Colors.white10.withOpacity(0.07),
                          padding: EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          margin:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                          child: SwitchListTile(
                            activeTrackColor: fromHex(yellow).withOpacity(0.6),
                            activeColor: fromHex(yellow),
                            inactiveThumbColor: fromHex(grey).withOpacity(0.8),
                            inactiveTrackColor: fromHex(grey).withOpacity(0.4),
                            autofocus: true,
                            dense: true,
                            title: Text(
                              "Require Designation",
                              style: TextStyle(
                                  color: fromHex(yellow),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                            value: requireDesignation,
                            onChanged: (bool value) async {
                              setState(() {
                                requireDesignation = value;
                                setRequireDesignation(value);
                              });
                            },
                          ),
                        ),
                        Container(
                          color: Colors.white10.withOpacity(0.07),
                          padding: EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          margin:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                          child: SwitchListTile(
                            activeTrackColor: fromHex(yellow).withOpacity(0.6),
                            activeColor: fromHex(yellow),
                            inactiveThumbColor: fromHex(grey).withOpacity(0.8),
                            inactiveTrackColor: fromHex(grey).withOpacity(0.4),
                            autofocus: true,
                            dense: true,
                            title: Text(
                              "Require Person To See",
                              style: TextStyle(
                                  color: fromHex(yellow),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                            value: requirePersonToSee,
                            onChanged: (bool value) async {
                              setState(() {
                                requirePersonToSee = value;
                                setRequirePersonToSee(value);
                              });
                            },
                          ),
                        ),
                        Container(
                          color: Colors.white10.withOpacity(0.07),
                          padding: EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          margin:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                          child: SwitchListTile(
                            activeTrackColor: fromHex(yellow).withOpacity(0.6),
                            activeColor: fromHex(yellow),
                            inactiveThumbColor: fromHex(grey).withOpacity(0.8),
                            inactiveTrackColor: fromHex(grey).withOpacity(0.4),
                            autofocus: true,
                            dense: true,
                            title: Text(
                              "Require Reason for Visit",
                              style: TextStyle(
                                  color: fromHex(yellow),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                            value: requireReasonForVisit,
                            onChanged: (bool value) async {
                              setState(() {
                                requireReasonForVisit = value;
                                setRequireReasonForVisit(value);
                              });
                            },
                          ),
                        ),
                        Container(
                          color: Colors.white10.withOpacity(0.07),
                          padding: EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          margin:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                          child: SwitchListTile(
                            activeTrackColor: fromHex(yellow).withOpacity(0.6),
                            activeColor: fromHex(yellow),
                            inactiveThumbColor: fromHex(grey).withOpacity(0.8),
                            inactiveTrackColor: fromHex(grey).withOpacity(0.4),
                            autofocus: true,
                            dense: true,
                            title: Text(
                              "Require House No.",
                              style: TextStyle(
                                  color: fromHex(yellow),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                            value: requireHouseNo,
                            onChanged: (bool value) async {
                              setState(() {
                                requireHouseNo = value;
                                setRequireHouseNo(value);
                              });
                            },
                          ),
                        ),
                        Container(
                          color: Colors.white10.withOpacity(0.07),
                          padding: EdgeInsets.symmetric(
                              horizontal: 10, vertical: 10),
                          margin:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                          child: SwitchListTile(
                            activeTrackColor: fromHex(yellow).withOpacity(0.6),
                            activeColor: fromHex(yellow),
                            inactiveThumbColor: fromHex(grey).withOpacity(0.8),
                            inactiveTrackColor: fromHex(grey).withOpacity(0.4),
                            autofocus: true,
                            dense: true,
                            title: Text(
                              "Require Block No.",
                              style: TextStyle(
                                  color: fromHex(yellow),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                            value: requireBlockNo,
                            onChanged: (bool value) async {
                              setState(() {
                                requireBlockNo = value;
                                setRequireBlockNo(value);
                              });
                            },
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            left: 15,
            top: 27,
            child: FloatingActionButton(
              foregroundColor: Colors.grey,
              backgroundColor: Colors.white10.withOpacity(0.07),
              onPressed: () {
                Navigator.pushNamedAndRemoveUntil(
                    context, HomePage.tag, (Route<dynamic> route) => false);
              },
              mini: true,
              tooltip: "go back home",
              child: Icon(
                Icons.keyboard_backspace_sharp,
                color: fromHex(deepOrange),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
