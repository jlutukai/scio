import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_native_image/flutter_native_image.dart';
import 'package:lottie/lottie.dart';
import 'package:scio_security/core/models/local/localModels.dart';
import 'package:scio_security/utils/useful.dart';

import 'home_page.dart';

class SetUpVSMPage extends StatefulWidget {
  final String path;
  const SetUpVSMPage(this.path);

  @override
  _SetUpVSMPageState createState() => _SetUpVSMPageState();
}

class _SetUpVSMPageState extends State<SetUpVSMPage> {
  late DeviceDetails? deafault;

  final _formKey = GlobalKey<FormState>();
  double posx = 397.0;
  double posy = 0.0;
  String _path = "";

  int _xOrigin = 0;
  int _yOrigin = 345;
  int _height = 100;
  int _width = 460;

  TextEditingController _xOriginTxt = TextEditingController();
  TextEditingController _yOriginTxt = TextEditingController();
  TextEditingController _heightTxt = TextEditingController();
  TextEditingController _widthTxt = TextEditingController();




  @override
  void initState() {
    _path = widget.path;
    super.initState();
  }



  void onTapDown(BuildContext context, TapDownDetails details) {
    print('${details.globalPosition}');
    final RenderBox box = context.findRenderObject() as RenderBox;
    final Offset localOffset = box.globalToLocal(details.globalPosition);
    setState(() {
      posx = localOffset.dx;
      posy = localOffset.dy;
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: GestureDetector(
        onTapDown: (TapDownDetails details) => onTapDown(context, details),
        child: Stack(
          children: <Widget>[
            Center(
              child: Container(
                width: double.maxFinite,
                color: Colors.black,
                child: Image.file(
                  File(_path),
                ),
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Card(
                elevation: 8,
                color: Colors.black,
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Form(
                    key: _formKey,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(

                          children: [
                          Expanded(child: Text("Set up Configuration", style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),),),
                            GestureDetector(
                                onTap: (){
                                  setState(() {
                                    _path = widget.path;
                                    _xOriginTxt.clear();
                                    _yOriginTxt.clear();
                                    _widthTxt.clear();
                                    _heightTxt.clear();

                                  });
                                },
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text("Clear", style: TextStyle(color: Colors.deepOrange),),
                                ))
                        ],),
                        SizedBox(height: 10,),
                        Row(
                          children: [
                            Expanded(child: Container(child: TextFormField(
                              controller: _xOriginTxt,
                              keyboardType: TextInputType.numberWithOptions(decimal: false),
                              textAlignVertical: TextAlignVertical.top,
                              style: TextStyle(color: Colors.white),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return "Please Enter x origin Point";
                                }
                                return null;
                              },
                              onChanged: (value){
                                setState(() {
                                  _xOrigin = int.tryParse(value)?? 0;
                                });
                              },
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: Colors.white10,
                                isDense: true,
                                hintText: "e.g. 0",
                                labelText: "X-Origin",
                                alignLabelWithHint: true,
                                labelStyle: TextStyle(color: fromHex(yellow)),
                                hintStyle: TextStyle(
                                  color: Colors.blueGrey[400],
                                ),
                                border: InputBorder.none,
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: fromHex(yellow)),
                                ),
                              ),
                            ),)),
                            SizedBox(width: 10,),
                            Expanded(child: Container(child: TextFormField(
                              controller: _yOriginTxt,
                              keyboardType: TextInputType.numberWithOptions(decimal: false),

                              textAlignVertical: TextAlignVertical.top,
                              style: TextStyle(color: Colors.white),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return "Please Enter Y-Origin";
                                }
                                return null;
                              },
                              onChanged: (value){
                                setState(() {
                                  _yOrigin = int.tryParse(value)?? 0;
                                });
                              },
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: Colors.white10,
                                isDense: true,
                                hintText: "eg 400",
                                labelText: "Y-Origin",
                                alignLabelWithHint: true,
                                labelStyle: TextStyle(color: fromHex(yellow)),
                                hintStyle: TextStyle(
                                  color: Colors.blueGrey[400],
                                ),
                                border: InputBorder.none,
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: fromHex(yellow)),
                                ),
                              ),
                            ),)),
                          ],
                        ),
                        SizedBox(height: 10,),
                        Row(
                          children: [
                            Expanded(child: Container(
                                child: TextFormField(
                                  controller: _widthTxt,
                                  keyboardType: TextInputType.numberWithOptions(decimal: false),

                                  textAlignVertical: TextAlignVertical.top,
                                  style: TextStyle(color: Colors.white),
                                  validator: (value) {
                                    if (value!.isEmpty) {
                                      return "Please Enter width value";
                                    }
                                    return null;
                                  },
                                  onChanged: (value){
                                    setState(() {
                                      _width = int.tryParse(value)?? 0;
                                    });
                                  },
                                  decoration: InputDecoration(
                                    filled: true,
                                    fillColor: Colors.white10,
                                    isDense: true,
                                    hintText: "eg 460",
                                    labelText: "Width",
                                    alignLabelWithHint: true,
                                    labelStyle: TextStyle(color: fromHex(yellow)),
                                    hintStyle: TextStyle(
                                      color: Colors.blueGrey[400],
                                    ),
                                    border: InputBorder.none,
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: fromHex(yellow)),
                                    ),
                                  ),
                                )
                            )),
                            SizedBox(width: 10,),
                            Expanded(child: Container(
                                child: TextFormField(
                                  controller: _heightTxt,
                                  keyboardType: TextInputType.numberWithOptions(decimal: false),

                                  textAlignVertical: TextAlignVertical.top,
                                  style: TextStyle(color: Colors.white),
                                  validator: (value) {
                                    if (value!.isEmpty) {
                                      return "Please Enter height value";
                                    }
                                    return null;
                                  },
                                  onChanged: (value){
                                    setState(() {
                                      _height = int.tryParse(value)?? 0;
                                    });
                                  },
                                  decoration: InputDecoration(
                                    filled: true,
                                    fillColor: Colors.white10,
                                    isDense: true,
                                    hintText: "eg 100",
                                    labelText: "Height",
                                    alignLabelWithHint: true,
                                    labelStyle: TextStyle(color: fromHex(yellow)),
                                    hintStyle: TextStyle(
                                      color: Colors.blueGrey[400],
                                    ),
                                    border: InputBorder.none,
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide: BorderSide(color: fromHex(yellow)),
                                    ),
                                  ),
                                )
                            )),
                          ],
                        ),
                        SizedBox(height: 10,),
                        Row(children: [
                          Expanded(child:  ElevatedButton(
                            onPressed: () async {
                              if (_formKey.currentState!.validate()) {
                                deafault = DeviceDetails(
                                    name: "Default",
                                    xOrigin: _xOrigin,
                                    yOrigin: _yOrigin,
                                    height: _height,
                                    width: _width);

                                File croppedFile = await FlutterNativeImage.cropImage(
                                    _path,
                                    deafault!.xOrigin!,
                                    deafault!.yOrigin!,
                                    deafault!.width!,
                                    deafault!.height!);

                                setState(() {
                                  _path = croppedFile.path;
                                });
                              }
                            },
                            style: ElevatedButton.styleFrom(
                              primary: fromHex(deepOrange),
                              padding: EdgeInsets.symmetric(
                                horizontal: 50,),
                              textStyle: TextStyle(
                                  fontSize: 30,
                                  fontWeight: FontWeight.bold),
                            ),
                            child: Center(
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 10.0),
                                child: Text(
                                  'Crop',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 18),
                                ),
                              ),
                            ),
                          )),
                          SizedBox(width: 10,),
                          Expanded(
                            child: ElevatedButton(
                              onPressed: () async {
                              if(deafault!=null){
                                setDeviceModel(deafault!);
                                Navigator
                                    .pushNamedAndRemoveUntil(
                                    context,
                                    HomePage.tag,
                                        (Route<dynamic>
                                    route) =>
                                    false);
                              }
                              },
                              style: ElevatedButton.styleFrom(
                                primary: Colors.black,
                                padding: EdgeInsets.symmetric(
                                  horizontal: 50,),
                                textStyle: TextStyle(
                                    fontSize: 30,
                                    fontWeight: FontWeight.bold),
                              ),
                              child: Center(
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 10.0),
                                  child: Text(
                                    'Confirm',
                                    style: TextStyle(
                                        color: fromHex(deepOrange),
                                        fontSize: 18),
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],),
                        SizedBox(
                          height: 20,
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),

          ],
        ),
      ),
    );
  }
}
