import 'dart:io';

import 'package:circular_bottom_navigation/circular_bottom_navigation.dart';
import 'package:circular_bottom_navigation/tab_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:intl/intl.dart';
import 'package:scio_security/core/enums/view_state.dart';
import 'package:scio_security/core/models/general_response.dart';
import 'package:scio_security/core/models/image_verification_response.dart';
import 'package:scio_security/core/models/local/localModels.dart';
import 'package:scio_security/core/models/time_sheet_response.dart';
import 'package:scio_security/core/viewmodels/timesheet_manual_model.dart';
import 'package:scio_security/ui/widgets/clock_in_manual.dart';
import 'package:scio_security/ui/widgets/clock_out_manual.dart';
import 'package:scio_security/ui/widgets/onsite_manual.dart';
import 'package:scio_security/utils/loader.dart';
import 'package:scio_security/utils/scio_icons_icons.dart';
import 'package:scio_security/utils/useful.dart';

import 'base_view.dart';

class GuardsPageManual extends StatefulWidget {
  final String date;
  final Shifts shift;
  GuardsPageManual(this.date, this.shift);
  @override
  _GuardsPageManualState createState() => _GuardsPageManualState();
}

class _GuardsPageManualState extends State<GuardsPageManual> {
  int currentIndex = 0;
  PageController pageController = PageController();
  late CircularBottomNavigationController _navigationController;
  late TimeSheetManualModel _model;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  File? _image;
  String? biometricId;

  // final _timeSheet = Hive.box('time_sheet_unsaved_manual');
  List<TabItem> tabItems = List.of([
    // new TabItem(Icons.home, "Home", fromHex('#ebd774'), labelStyle: TextStyle(color:fromHex('#ebd774'),)),
    new TabItem(ScioIcons.login_1, "Clock In", Colors.white12,
        labelStyle: TextStyle(
          color: fromHex(deepOrange),
        )),
    new TabItem(ScioIcons.users, "On Duty", Colors.white12,
        labelStyle: TextStyle(
          color: fromHex(deepOrange),
        )),
    new TabItem(ScioIcons.logout, "Clock Out", Colors.white12,
        labelStyle: TextStyle(
          color: fromHex(deepOrange),
        )),
  ]);

  @override
  void initState() {
    super.initState();
    setShift(widget.shift);
    setShiftDate(widget.date);
    // _shift = widget.shift;
    // date = widget.date;
    // shiftName =
    //     "${widget.shift.shiftName} ${widget.shift.shiftChangeFrom} - ${widget.shift.shiftChangeTo}";
    _navigationController =
        new CircularBottomNavigationController(currentIndex);
  }

  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<TimeSheetManualModel>(
      onModelReady: (model) async {
        _model = model;
        await model.getUnSyncedTimesheetLocal();

        // Workmanager().registerPeriodicTask("2", "simplePeriodicTask",
        //     frequency: Duration(minutes: 15));
      },
      builder: (context, model, child) => model.state == ViewState.Idle
          ? Stack(
              children: [
                Scaffold(
                  backgroundColor: Colors.black,
                  key: _scaffoldKey,
                  appBar: AppBar(
                    automaticallyImplyLeading: false,
                    centerTitle: true,
                    backgroundColor: Colors.black,
                    title: Text(
                      'Attendance',
                      style: TextStyle(color: fromHex(deepOrange)),
                    ),
                  ),
                  body: Column(
                    children: [
                      Expanded(
                        child: PageView(
                          physics: NeverScrollableScrollPhysics(),
                          controller: pageController,
                          onPageChanged: (index) {
                            setState(() {
                              currentIndex = index;
                            });
                          },
                          children: <Widget>[
                            ClockInManual(),
                            OnSiteManual(),
                            ClockOutManual(),
                          ],
                        ),
                      ),
                    ],
                  ),
                  bottomNavigationBar: CircularBottomNavigation(
                    tabItems,
                    controller: _navigationController,
                    barBackgroundColor: Colors.black,
                    selectedPos: this.currentIndex,
                    selectedIconColor: fromHex(deepOrange),
                    normalIconColor: fromHex(grey),
                    selectedCallback: (int? index) {
                      setState(() {
                        this.currentIndex = index ?? 0;
                      });
                      navigateToScreens(index ?? 0);
                    },
                  ),
                ),
                Positioned(
                  left: 15,
                  top: 27,
                  child: FloatingActionButton(
                    foregroundColor: Colors.grey,
                    backgroundColor: Colors.white10.withOpacity(0.07),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    heroTag: null,
                    mini: true,
                    tooltip: "go back home",
                    child: Icon(
                      Icons.keyboard_backspace_sharp,
                      color: fromHex(yellow),
                    ),
                  ),
                ),
                Positioned(
                  right: 15,
                  top: 27,
                  child: model.unSyncedTimeSheet.isEmpty
                      ? Container()
                      : FloatingActionButton(
                          foregroundColor: Colors.grey,
                          backgroundColor: Colors.white10.withOpacity(0.07),
                          onPressed: () async {
                            bool isConnected = await checkConnection();
                            if (!isConnected) {
                              showToast(
                                  'No connection to DB, Check Internet Connection!');
                              return;
                            }
                            await sync(model.unSyncedTimeSheet);
                          },
                          mini: true,
                          tooltip: "Sync offline data",
                          child: Icon(
                            Icons.sync_rounded,
                            color: fromHex(yellow),
                          ),
                        ),
                ),
              ],
            )
          : Loader(),
    );
  }

  void navigateToScreens(int index) {
    // print('################ @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ ####################### called');
    setState(() {
      currentIndex = index;
      pageController.animateToPage(index,
          duration: Duration(milliseconds: 100), curve: Curves.ease);
    });
  }

  Future<void> sync(List<TimeSheetData> unSyncedTimeSheet) async {
    for (var i = 0; i <= unSyncedTimeSheet.length; i++) {
      if (i <= unSyncedTimeSheet.length - 1) {
        await sendDataToDb(unSyncedTimeSheet.elementAt(i), i);
      }
    }

    bool isConnected = await checkConnection();
    if (isConnected) {
    } else {
      // _model.initialize(); //todo
    }
  }

  Future<void> sendDataToDb(TimeSheetData timeSheetData, int index) async {
    File? imageIn;
    if (timeSheetData.imageIn != null && timeSheetData.imageIn!.isNotEmpty)
      imageIn = File(timeSheetData.imageIn!);
    File? imageOut;
    if (timeSheetData.imageOut != null && timeSheetData.imageOut!.isNotEmpty)
      imageOut = File(timeSheetData.imageOut!);
    if (imageIn != null) {
      try {
        var res = await _model.uploadImage(imageIn);
        if (res['success']) {
          var resd = ImageVerificationResponse.fromJson(res['response']);
          if (resd.error!) {
            showToast(resd.message!);
          }
        } else {
          showErrorDialog(res['response'], context, "upload image");
        }
      } catch (e) {
        showToast(e.toString());
      }
    }

    if (imageOut != null) {
      try {
        var res = await _model.uploadImage(imageOut);
        if (res['success']) {
          var resd = ImageVerificationResponse.fromJson(res['response']);
          if (resd.error!) {
            showToast(resd.message!);
          }
        } else {
          showErrorDialog(res['response'], context, "upload image");
        }
      } catch (e) {
        showToast(e.toString());
      }
    }

    Map<String, String> data = {
      "timesheet_id": "${timeSheetData.id}",
      "status": "${timeSheetData.status ?? ''}",
      "time_in": "${timeSheetData.timeIn ?? ''}",
      "time_out": "${timeSheetData.timeOut ?? ''}",
      "reason": "${timeSheetData.reason ?? ''}",
      "comment_in": "${timeSheetData.commentIn}",
      "comment_out": "${timeSheetData.commentOut}"
    };
    var r = await _model.updateStatus(data);
    if (r['success']) {
      var d = GeneralResponse.fromJson(r['response']);
      if (!d.error!) {
        _model.updateTimesheetLocal(timeSheetData..syncStatus = syncedStatus);
        print('success');
      } else {
        showToast(d.erroMessage!);
      }
    } else {
      showErrorDialog(r['response'], context, "update time sheet details");
    }
  }
}
