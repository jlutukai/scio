import 'package:circular_bottom_navigation/circular_bottom_navigation.dart';
import 'package:circular_bottom_navigation/tab_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:scio_security/core/enums/view_state.dart';
import 'package:scio_security/core/viewmodels/visitors_log_model.dart';
import 'package:scio_security/ui/screens/base_view.dart';
import 'package:scio_security/ui/widgets/check_in_visitor.dart';
import 'package:scio_security/ui/widgets/check_out_visitor.dart';
import 'package:scio_security/ui/widgets/visitor_logs.dart';
import 'package:scio_security/ui/widgets/visitors_on_site.dart';
import 'package:scio_security/utils/scio_icons_icons.dart';
import 'package:scio_security/utils/useful.dart';

class VisitorsPage extends StatefulWidget {
  static const tag = 'visitors';
  final String? id;

  VisitorsPage(this.id);

  @override
  _VisitorsPageState createState() => _VisitorsPageState();
}

class _VisitorsPageState extends State<VisitorsPage> {
  int _indexVisitorsLog = 0;
  PageController? _visitorsPageController;
  late CircularBottomNavigationController _navController;
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  List<TabItem> _tabItems = List.of([
    // new TabItem(Icons.home, "Home", fromHex('#ebd774'), labelStyle: TextStyle(color:fromHex('#ebd774'),)),
    new TabItem(ScioIcons.login_1, "In", Colors.white12,
        labelStyle: TextStyle(
          color: fromHex(deepOrange),
        )),
    new TabItem(ScioIcons.users, "On Site", Colors.white12,
        labelStyle: TextStyle(
          color: fromHex(deepOrange),
        )),
    new TabItem(ScioIcons.list_1, "Logs", Colors.white12,
        labelStyle: TextStyle(
          color: fromHex(deepOrange),
        )),
    new TabItem(ScioIcons.logout, "Out", Colors.white12,
        labelStyle: TextStyle(
          color: fromHex(deepOrange),
        )),
  ]);
  bool? requirePass;

  @override
  void initState() {
    super.initState();
    _visitorsPageController = PageController();
    _navController = new CircularBottomNavigationController(_indexVisitorsLog);
    var r = accountInfo.get("requirePass");
    if (r != null) {
      requirePass = r.toLowerCase() == 'true';
    } else {
      requirePass = true;
    }
  }

  @override
  void dispose() {
    _visitorsPageController!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<VisitorsLogModel>(
      onModelReady: (model) async {
        final DateTime maxDate = DateTime.now();
        final DateTime minDate = DateTime.now().subtract(Duration(days: 1));
        final DateFormat formatter = DateFormat('yyyy-MM-dd HH:mm:ss');
        String max = formatter.format(maxDate);
        String min = formatter.format(minDate);
        Map<String, String> data = {
          'datetime_in_min': "$min",
          'datetime_in_max': "$max",
          'site_id': "${getPlanner()!.plannerId}"
        };
        bool isConnected = await checkConnection();
        if (isConnected) {
          var r = await model.getVisitors(data);
          if (r['success']) {
          } else {
            showErrorDialog(r['response'], context, "get visitors");
          }
        } else {
          showToast("Check Internet connection");
          await model.getVisitors(data);
        }
      },
      builder: (context, model, child) => model.state == ViewState.Idle
          ? Stack(
              children: [
                Scaffold(
                  backgroundColor: Colors.black,
                  body: Column(
                    children: [
                      Expanded(
                        child: PageView(
                          physics: NeverScrollableScrollPhysics(),
                          controller: _visitorsPageController,
                          onPageChanged: (index) {
                            setState(() {
                              _indexVisitorsLog = index;
                            });
                          },
                          children: <Widget>[
                            // GoToHome(),
                            CheckInVisitor(model, requirePass),
                            VisitorOnSite(model),
                            VisitorLogs(model),
                            CheckOutVisitor(model, requirePass)
                            // Container(color: Colors.red,),
                            // Container(color: Colors.green,),
                            // Container(color: Colors.blue,),
                            // Container(color: Colors.red,)
                          ],
                        ),
                      ),
                    ],
                  ),
                  bottomNavigationBar: CircularBottomNavigation(
                    _tabItems,
                    controller: _navController,
                    barBackgroundColor: Colors.black,
                    selectedIconColor: fromHex(deepOrange),
                    normalIconColor: fromHex(grey),
                    selectedCallback: (int? selectedPos) {
                      _navigateToScreens(selectedPos ?? 0);
                    },
                  ),
                ),
                Positioned(
                  left: 15,
                  top: 40,
                  child: FloatingActionButton(
                    foregroundColor: Colors.grey,
                    backgroundColor: Colors.white10.withOpacity(0.07),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    mini: true,
                    tooltip: "go back home",
                    child: Icon(
                      Icons.keyboard_backspace_sharp,
                      color: fromHex(yellow),
                    ),
                  ),
                ),
              ],
            )
          : Scaffold(
              backgroundColor: Colors.black,
              body: Column(
                children: [
                  Expanded(
                    child: Center(
                        child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                            height: 100,
                            width: 100,
                            child: Lottie.asset('assets/test.json')),
                        Text(
                          'Please wait \n Loading...',
                          style: TextStyle(color: fromHex(deepOrange)),
                        )
                      ],
                    )),
                  )
                ],
              ),
            ),
    );
  }

  void _navigateToScreens(int index) {
    setState(() {
      _indexVisitorsLog = index;
      _visitorsPageController!.animateToPage(index,
          duration: Duration(milliseconds: 100), curve: Curves.ease);
    });
  }
}
