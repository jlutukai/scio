import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:scio_security/core/enums/view_state.dart';
import 'package:scio_security/core/models/scio_url_response.dart';
import 'package:scio_security/core/viewmodels/url_model.dart';
import 'package:scio_security/utils/useful.dart';

import 'base_view.dart';
import 'login_page.dart';

class UrlPage extends StatefulWidget {
  static const tag = 'url';
  @override
  _UrlPageState createState() => _UrlPageState();
}

class _UrlPageState extends State<UrlPage> {
  TextEditingController _url = TextEditingController();
  final _formUrlKey = GlobalKey<FormState>();

  @override
  void initState() {
    setRequirePass(true);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<UrlModel>(
      builder: (context, model, child) => Scaffold(
        body: Stack(
          children: [
            // SvgPicture.asset("assets/bg.svg",
            //   fit: BoxFit.fitHeight,
            //   alignment: Alignment.center,
            //   width: MediaQuery.of(context).size.width,
            //   height: MediaQuery.of(context).size.height,
            // ),
            Container(
              decoration: BoxDecoration(
                color: Colors.black,
              ),
              child: Column(
                children: [
                  Container(
                      child: SizedBox(
                    height: 120,
                  )),
                  Expanded(
                    child: SingleChildScrollView(
                      child: Form(
                        key: _formUrlKey,
                        child: Container(
                          height: MediaQuery.of(context).size.height - 120,
                          decoration: BoxDecoration(
                              color: Colors.black,
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(30),
                                  topRight: Radius.circular(30))),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              SizedBox(
                                height: 25,
                              ),
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SvgPicture.asset(
                                    "assets/mainlogo.svg",
                                    width: 150,
                                    height: 200,
                                  ),
                                  // Text("Scio Mobile 2.0", style: TextStyle(color: fromHex("#255B73").withOpacity(0.5), fontSize: 18, fontWeight: FontWeight.bold),),
                                ],
                              ),
                              Flexible(
                                fit: FlexFit.loose,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  mainAxisSize: MainAxisSize.max,
                                  children: [
                                    Container(
                                      margin:
                                          EdgeInsets.symmetric(horizontal: 25),
                                      child: TextFormField(
                                        controller: _url,
                                        keyboardType: TextInputType.url,
                                        style: TextStyle(color: Colors.white),
                                        validator: (value) {
                                          if (value!.isEmpty) {
                                            return "Please Enter url";
                                          }
                                          return null;
                                        },
                                        decoration: InputDecoration(
                                          filled: true,
                                          fillColor: Colors.white10,
                                          isDense: true,
                                          hintText: "example.com",
                                          labelText: "Organization Url",
                                          labelStyle:
                                              TextStyle(color: fromHex(yellow)),
                                          hintStyle: TextStyle(
                                            color: Colors.blueGrey[400],
                                          ),
                                          border: InputBorder.none,
                                          focusedBorder: UnderlineInputBorder(
                                            borderSide: BorderSide(
                                                color: fromHex(yellow)),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      margin: EdgeInsets.symmetric(
                                          horizontal: 25, vertical: 20),
                                      padding: EdgeInsets.only(top: 20),
                                      child: model.state == ViewState.Idle
                                          ? RaisedButton(
                                              elevation: 5.0,
                                              onPressed: () async {
                                                if (_formUrlKey.currentState!
                                                    .validate()) {
                                                  // Map<String, dynamic> data = {
                                                  //   'url': "${_url.text}"
                                                  // };
                                                  FormData data =
                                                      new FormData.fromMap({
                                                    'url': "${_url.text}",
                                                  });
                                                  try {
                                                    var r = await model
                                                        .veryUrl(data);
                                                    bool? isSuccess =
                                                        r['success'];
                                                    print(
                                                        "@@@@@@@@@@@@@@@@@ $isSuccess ${r['success']}");
                                                    if (r['response'] != null &&
                                                        isSuccess != null) {
                                                      if (isSuccess) {
                                                        var d = ScioUrlResponse
                                                            .fromJson(
                                                                r['response']);

                                                        if (d.scioUrl != null) {
                                                          if (d.scioUrl!.data!
                                                                  .status ==
                                                              "Active") {
                                                            showToast(
                                                                d.message!);
                                                            Navigator.pushNamedAndRemoveUntil(
                                                                context,
                                                                LoginPage.tag,
                                                                (Route<dynamic>
                                                                        route) =>
                                                                    false);
                                                          } else {
                                                            showToast(
                                                                "Sorry Account is currently ${d.scioUrl?.data?.status ?? 'In-Active'}");
                                                          }
                                                        } else {
                                                          showToast(d.message ??
                                                              "Failed");
                                                        }
                                                      } else {
                                                        showErrorDialog(
                                                            r['response'],
                                                            context,
                                                            "url verification");
                                                      }
                                                    }
                                                  } catch (e) {
                                                    print(
                                                        "@@@@@@@@@@@@@@@@@ ${e.toString()}");
                                                    showErrorDialog(
                                                        e.toString(),
                                                        context,
                                                        "url verification");
                                                  }
                                                }
                                              },
                                              shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(10.0),
                                              ),
                                              color: fromHex(deepOrange),
                                              child: Center(
                                                child: Padding(
                                                  padding: const EdgeInsets
                                                          .symmetric(
                                                      vertical: 10.0),
                                                  child: Text(
                                                    'Verify',
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 18),
                                                  ),
                                                ),
                                              ),
                                            )
                                          : CupertinoActivityIndicator(),
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
