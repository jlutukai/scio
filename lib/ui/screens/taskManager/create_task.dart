import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_tags/flutter_tags.dart';
import 'package:hive/hive.dart';
import 'package:scio_security/core/enums/view_state.dart';
import 'package:scio_security/core/models/get_function_response.dart';
import 'package:scio_security/core/models/get_staff_task_response.dart';
import 'package:scio_security/core/models/get_tasks_types_response.dart';
import 'package:scio_security/core/models/local/localModels.dart';
import 'package:scio_security/core/viewmodels/task_model.dart';
import 'package:scio_security/ui/screens/taskManager/task_manager.dart';
import 'package:scio_security/utils/loader.dart';
import 'package:scio_security/utils/useful.dart';

import '../base_view.dart';

class CreateTaskPage extends StatefulWidget {
  const CreateTaskPage({Key? key}) : super(key: key);

  @override
  _CreateTaskPageState createState() => _CreateTaskPageState();
}

class _CreateTaskPageState extends State<CreateTaskPage> {
  TextEditingController _taskDescription = TextEditingController();
  TextEditingController _date = TextEditingController();
  TextEditingController _title = TextEditingController();

  String? priority = "";
  String? taskTypeName = "";
  String? taskTypeId = "";
  String? visibility = "";
  String? association = "";
  String? functionName = "";
  String? functionId = "";
  String? staffName = "";
  String? staffId = "";
  bool isTaskSet = false;
  List<String> priorities = [
    'Urgent & Important',
    'Urgent',
    'Important',
    'Wishlist'
  ];
  List<String> visibilityList = ['Private', 'Public'];
  List<String> associations = ['Function', 'Individual'];

  final _formKey = GlobalKey<FormState>();
  late bool hasConnection;
  late CreateTaskData? _taskData;
  final _createTaskData = Hive.box('created_task_data');

  @override
  void initState() {
    super.initState();
    _taskData = _createTaskData.get("current_task", defaultValue: null);
    if (_taskData == null) {
      initializeData();
    } else {
      setData(_taskData!);
    }
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<TaskModel>(
      onModelReady: (model) async {
        await model.getTaskTypes();
        await model.getStaff();
        await model.getFunctions();
      },
      builder: (context, model, child) => model.state == ViewState.Idle
          ? Stack(
              children: [
                Scaffold(
                  backgroundColor: Colors.black,
                  // key: _scaffoldKey,
                  appBar: AppBar(
                    backgroundColor: Colors.black,
                    automaticallyImplyLeading: false,
                    centerTitle: true,
                    title: Text(
                      "Create Task",
                      style: TextStyle(color: fromHex(deepOrange)),
                    ),
                  ),
                  body: Column(
                    children: [
                      Expanded(
                        child: SingleChildScrollView(
                          physics: BouncingScrollPhysics(),
                          child: Column(
                            children: [
                              isTaskSet ? _filledDetails(context) : Container(),
                              _createTaskForm(context, model),
                              SizedBox(
                                height: 30,
                              ),
                            ],
                          ),
                        ),
                      ),
                      _submitBtn(context, model),
                      SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
                Positioned(
                  left: 15,
                  top: 27,
                  child: FloatingActionButton(
                    foregroundColor: Colors.grey,
                    backgroundColor: Colors.white10.withOpacity(0.07),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    mini: true,
                    tooltip: "go back home",
                    child: Icon(
                      Icons.keyboard_backspace_sharp,
                      color: fromHex(yellow),
                    ),
                  ),
                ),
              ],
            )
          : Loader(),
    );
  }

  _createTaskForm(BuildContext context, TaskModel model) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            SizedBox(
              height: 15,
            ),
            Container(
              child: TextFormField(
                controller: _title,
                keyboardType: TextInputType.text,
                textCapitalization: TextCapitalization.words,
                style: TextStyle(color: Colors.white),
                validator: (value) {
                  if (value!.isEmpty) {
                    return "Please Enter task's title";
                  }
                  return null;
                },
                onChanged: (value) {
                  if (_taskData != null) {
                    CreateTaskData t = _taskData!..title = value;
                    isTaskSet = true;
                    _saveTask(t);
                  }
                },
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.white10,
                  isDense: true,
                  hintText: "eg Task A",
                  labelText: "Title",
                  labelStyle: TextStyle(color: fromHex(yellow)),
                  hintStyle: TextStyle(
                    color: Colors.blueGrey[400],
                  ),
                  border: InputBorder.none,
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: fromHex(yellow)),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              child: DropdownSearch<String>(
                mode: Mode.BOTTOM_SHEET,
                showClearButton: false,
                showSearchBox: false,
                // searchBoxDecoration: InputDecoration(
                //     hintText: "Search Priorities",
                //     hintStyle: TextStyle(color: Colors.grey[100]),
                //     prefixIcon: Icon(Icons.search, color: Colors.white),
                //     filled: true,
                //     fillColor: Colors.white10,
                //     border: InputBorder.none,
                //     enabledBorder: InputBorder.none,
                //     focusedBorder: InputBorder.none),
                dropdownSearchDecoration: InputDecoration(
                  filled: true,
                  isDense: true,
                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
                  fillColor: Colors.white10,
                  border: InputBorder.none,
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: fromHex(yellow)),
                  ),
                ),
                items: priorities,
                itemAsString: (p) => "$p",
                autoValidateMode: AutovalidateMode.onUserInteraction,
                validator: (String? u) {
                  if (priority!.isEmpty) {
                    return "Please Select Priority";
                  }
                  return null;
                },
                onChanged: (String? s) async {
                  setState(() {
                    priority = s;
                  });
                  if (_taskData != null) {
                    isTaskSet = true;
                    CreateTaskData t = _taskData!..priority = s;
                    _saveTask(t);
                  }
                },
                dropdownBuilder: _priorityDrop,
                popupBackgroundColor: Colors.grey[90],
                popupItemBuilder: _priorityPopUp,
                dropdownButtonProps: IconButtonProps(
                  icon: Icon(
                    Icons.arrow_drop_down,
                    color: fromHex(yellow),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              child: DropdownSearch<TaskType>(
                mode: Mode.BOTTOM_SHEET,
                showClearButton: false,
                showSearchBox: true,
                emptyBuilder: (context, searchEntry) => Scaffold(
                  body: Column(
                    children: [
                      Expanded(
                        child: Center(
                          child: Text(
                            'No Task Type Found, \n Try with different Key words',
                            textAlign: TextAlign.center,
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                // searchBoxDecoration: InputDecoration(
                //     hintText: "Search Task Types",
                //     hintStyle: TextStyle(color: Colors.grey[100]),
                //     prefixIcon: Icon(Icons.search, color: Colors.white),
                //     filled: true,
                //     fillColor: Colors.white10,
                //     border: InputBorder.none,
                //     enabledBorder: InputBorder.none,
                //     focusedBorder: InputBorder.none),
                dropdownSearchDecoration: InputDecoration(
                  filled: true,
                  isDense: true,
                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
                  fillColor: Colors.white10,
                  border: InputBorder.none,
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: fromHex(yellow)),
                  ),
                ),
                items: model.taskTypes ?? [],
                itemAsString: (p) => "${p?.name}",
                autoValidateMode: AutovalidateMode.onUserInteraction,
                validator: (TaskType? u) {
                  if (taskTypeId!.isEmpty) {
                    return "Please Select Task Type";
                  }
                  return null;
                },
                onChanged: (TaskType? s) async {
                  setState(() {
                    taskTypeId = s?.id ?? "";
                    taskTypeName = s?.name ?? "";
                  });
                  if (_taskData != null) {
                    isTaskSet = true;
                    CreateTaskData t = _taskData!
                      ..taskType = s?.id ?? ""
                      ..taskTypeName = s?.name ?? "";
                    _saveTask(t);
                  }
                },
                dropdownBuilder: _taskTypeDrop,
                popupBackgroundColor: Colors.grey[90],
                popupItemBuilder: _taskTypePopUp,
                dropdownButtonProps: IconButtonProps(
                  icon: Icon(
                    Icons.arrow_drop_down,
                    color: fromHex(yellow),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              child: DropdownSearch<String>(
                mode: Mode.BOTTOM_SHEET,
                showClearButton: false,
                showSearchBox: false,
                // searchBoxDecoration: InputDecoration(
                //     hintText: "Search Visibility",
                //     hintStyle: TextStyle(color: Colors.grey[100]),
                //     prefixIcon: Icon(Icons.search, color: Colors.white),
                //     filled: true,
                //     fillColor: Colors.white10,
                //     border: InputBorder.none,
                //     enabledBorder: InputBorder.none,
                //     focusedBorder: InputBorder.none),
                dropdownSearchDecoration: InputDecoration(
                  filled: true,
                  isDense: true,
                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
                  fillColor: Colors.white10,
                  border: InputBorder.none,
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: fromHex(yellow)),
                  ),
                ),
                items: visibilityList,
                itemAsString: (p) => "$p",
                autoValidateMode: AutovalidateMode.onUserInteraction,
                validator: (String? u) {
                  if (priority!.isEmpty) {
                    return "Please Select Task Visibility";
                  }
                  return null;
                },
                onChanged: (String? s) async {
                  setState(() {
                    visibility = s;
                  });
                  if (_taskData != null) {
                    CreateTaskData t = _taskData!..taskVisibility = s;
                    _saveTask(t);
                  }
                },
                dropdownBuilder: _visibilityDrop,
                popupBackgroundColor: Colors.grey[90],
                popupItemBuilder: _visibilityPopUp,
                dropdownButtonProps: IconButtonProps(
                  icon: Icon(
                    Icons.arrow_drop_down,
                    color: fromHex(yellow),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              child: DropdownSearch<String>(
                mode: Mode.BOTTOM_SHEET,
                showClearButton: false,
                showSearchBox: false,
                // searchBoxDecoration: InputDecoration(
                //     hintText: "Search Assigned to",
                //     hintStyle: TextStyle(color: Colors.grey[100]),
                //     prefixIcon: Icon(Icons.search, color: Colors.white),
                //     filled: true,
                //     fillColor: Colors.white10,
                //     border: InputBorder.none,
                //     enabledBorder: InputBorder.none,
                //     focusedBorder: InputBorder.none),
                dropdownSearchDecoration: InputDecoration(
                  filled: true,
                  isDense: true,
                  contentPadding: EdgeInsets.symmetric(horizontal: 10),
                  fillColor: Colors.white10,
                  border: InputBorder.none,
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: fromHex(yellow)),
                  ),
                ),
                items: associations,
                itemAsString: (p) => "$p",
                autoValidateMode: AutovalidateMode.onUserInteraction,
                validator: (String? u) {
                  if (association!.isEmpty) {
                    return "Please Specify Task assigned to";
                  }
                  return null;
                },
                onChanged: (String? s) async {
                  if (s != association) {
                    setState(() {
                      association = s;
                      functionId = "";
                      functionName = "";
                      staffId = "";
                      staffName = "";
                    });

                    if (_taskData != null) {
                      CreateTaskData t = _taskData!
                        ..associatedBy = s
                        ..assignTo = [];
                      _saveTask(t);
                    }
                  }
                },
                dropdownBuilder: _associationDrop,
                popupBackgroundColor: Colors.grey[90],
                popupItemBuilder: _associationPopUp,
                dropdownButtonProps: IconButtonProps(
                  icon: Icon(
                    Icons.arrow_drop_down,
                    color: fromHex(yellow),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            association == "Individual"
                ? Column(
                    children: [
                      Container(
                        child: DropdownSearch<StaffTask>(
                          mode: Mode.BOTTOM_SHEET,
                          showClearButton: false,
                          showSearchBox: true,
                          emptyBuilder: (context, searchEntry) => Scaffold(
                            body: Column(
                              children: [
                                Expanded(
                                  child: Center(
                                    child: Text(
                                      'No Staff Found, \n Try with different Key words',
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          // searchBoxDecoration: InputDecoration(
                          //     hintText: "Search Staff",
                          //     hintStyle: TextStyle(color: Colors.grey[100]),
                          //     prefixIcon: Icon(Icons.search, color: Colors.white),
                          //     filled: true,
                          //     fillColor: Colors.white10,
                          //     border: InputBorder.none,
                          //     enabledBorder: InputBorder.none,
                          //     focusedBorder: InputBorder.none),
                          dropdownSearchDecoration: InputDecoration(
                            filled: true,
                            isDense: true,
                            contentPadding:
                                EdgeInsets.symmetric(horizontal: 10),
                            fillColor: Colors.white10,
                            border: InputBorder.none,
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: fromHex(yellow)),
                            ),
                          ),
                          items: model.staff ?? [],
                          itemAsString: (p) => "${p?.name}",
                          // autoValidateMode: AutovalidateMode.onUserInteraction,
                          // validator: (StaffTask? u) {
                          //   if (staffId!.isEmpty) {
                          //     return "Please Select staff assigned";
                          //   }
                          //   return null;
                          // },
                          onChanged: (StaffTask? s) async {
                            setState(() {
                              staffId = s?.Id ?? "";
                              staffName = s?.name ?? "";
                            });
                            if (_taskData != null) {
                              if (_taskData!.assignTo != null) {
                                if (_taskData!.assignTo!.isNotEmpty) {
                                  var contain = _taskData!.assignTo!.where(
                                      (element) =>
                                          element!.assignToId! == s!.Id);
                                  print("${contain.toString()}");
                                  if (contain.isEmpty) {
                                    showToast("Added ${s?.name ?? ""}");
                                    AssignedTo a = AssignedTo(
                                        assignToId: s!.Id,
                                        name: s.name,
                                        type: association);
                                    CreateTaskData t = _taskData!
                                      ..assignTo!.add(a);
                                    _saveTask(t);
                                  } else {
                                    print('already added');
                                  }
                                  if (contain.isNotEmpty) {
                                    showToast("Already added ${s?.name ?? ""}");
                                  }
                                } else {
                                  showToast("Added ${s?.name ?? ""}");
                                  AssignedTo a = AssignedTo(
                                      assignToId: s!.Id,
                                      name: s.name,
                                      type: association);
                                  CreateTaskData t = _taskData!
                                    ..assignTo!.add(a);
                                  _saveTask(t);
                                }
                              }
                            }
                          },
                          dropdownBuilder: _staffDrop,
                          popupBackgroundColor: Colors.grey[90],
                          popupItemBuilder: _staffPopUp,
                          dropdownButtonProps: IconButtonProps(
                            icon: Icon(
                              Icons.arrow_drop_down,
                              color: fromHex(yellow),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  )
                : Container(),
            association == "Function"
                ? Column(
                    children: [
                      Container(
                        child: DropdownSearch<FunctionData>(
                          mode: Mode.BOTTOM_SHEET,
                          showClearButton: false,
                          showSearchBox: true,
                          emptyBuilder: (context, searchEntry) => Scaffold(
                            body: Column(
                              children: [
                                Expanded(
                                  child: Center(
                                    child: Text(
                                      'No Functions Found, \n Try with different Key words',
                                      textAlign: TextAlign.center,
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          // searchBoxDecoration: InputDecoration(
                          //     hintText: "Search Functions",
                          //     hintStyle: TextStyle(color: Colors.grey[100]),
                          //     prefixIcon: Icon(Icons.search, color: Colors.white),
                          //     filled: true,
                          //     fillColor: Colors.white10,
                          //     border: InputBorder.none,
                          //     enabledBorder: InputBorder.none,
                          //     focusedBorder: InputBorder.none),
                          dropdownSearchDecoration: InputDecoration(
                            filled: true,
                            isDense: true,
                            contentPadding:
                                EdgeInsets.symmetric(horizontal: 10),
                            fillColor: Colors.white10,
                            border: InputBorder.none,
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: fromHex(yellow)),
                            ),
                          ),
                          items: model.functions ?? [],
                          itemAsString: (p) => "${p?.jobTitle}",
                          // autoValidateMode: AutovalidateMode.onUserInteraction,
                          // validator: (FunctionData? u) {
                          //   if (functionId!.isEmpty) {
                          //     return "Please Select Function";
                          //   }
                          //   return null;
                          // },
                          onChanged: (FunctionData? s) async {
                            setState(() {
                              functionId = s?.id ?? "";
                              functionName = s?.jobTitle ?? "";
                            });
                            if (_taskData != null) {
                              if (_taskData!.assignTo != null) {
                                if (_taskData!.assignTo!.isNotEmpty) {
                                  var contain = _taskData!.assignTo!.where(
                                      (element) =>
                                          element!.assignToId! == s!.id);
                                  print("${contain.toString()}");
                                  if (contain.isEmpty) {
                                    showToast("Added ${s?.jobTitle ?? ""}");
                                    AssignedTo a = AssignedTo(
                                        assignToId: s!.id,
                                        name: s.jobTitle,
                                        type: association);
                                    CreateTaskData t = _taskData!
                                      ..assignTo!.add(a);
                                    _saveTask(t);
                                  } else {
                                    print('already added');
                                  }
                                  if (contain.isNotEmpty) {
                                    showToast(
                                        "Already added ${s?.jobTitle ?? ""}");
                                  }
                                } else {
                                  showToast("Added ${s?.jobTitle ?? ""}");
                                  AssignedTo a = AssignedTo(
                                      assignToId: s!.id,
                                      name: s.jobTitle,
                                      type: association);
                                  CreateTaskData t = _taskData!
                                    ..assignTo!.add(a);
                                  _saveTask(t);
                                }
                              }
                            }
                          },
                          dropdownBuilder: _functionDrop,
                          popupBackgroundColor: Colors.grey[90],
                          popupItemBuilder: _functionPopUp,
                          dropdownButtonProps: IconButtonProps(
                            icon: Icon(
                              Icons.arrow_drop_down,
                              color: fromHex(yellow),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  )
                : Container(),
            Container(
              child: TextFormField(
                controller: _date,
                keyboardType: TextInputType.number,
                style: TextStyle(color: Colors.white),
                validator: (value) {
                  if (value!.isEmpty) {
                    return "Please Enter task's due date";
                  }
                  return null;
                },
                enableInteractiveSelection: false,
                autofocus: false,
                readOnly: true,
                onTap: () {
                  DatePicker.showDatePicker(context,
                      showTitleActions: true,
                      minTime: DateTime.now(),
                      maxTime: DateTime.now().add(Duration(days: 94200)),
                      theme: DatePickerTheme(
                        headerColor: Colors.grey.shade900,
                        backgroundColor: Colors.grey.shade900,
                        itemStyle: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                        cancelStyle: TextStyle(
                          color: Colors.deepOrange,
                          fontSize: 24,
                        ),
                        doneStyle: TextStyle(
                          color: Colors.green.shade700,
                          fontSize: 24,
                        ),
                      ), onChanged: (date) {
                    print('change $date in time zone ' +
                        date.timeZoneOffset.inHours.toString());
                  }, onConfirm: (date) {
                    setState(() {
                      _date.text = df.format(date);
                    });
                    if (_taskData != null) {
                      CreateTaskData t = _taskData!..deadline = df.format(date);
                      _saveTask(t);
                    }
                  }, currentTime: DateTime.now(), locale: LocaleType.en);
                },
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.white10,
                  isDense: true,
                  hintText: "",
                  labelText: "Due Date",
                  labelStyle: TextStyle(color: fromHex(yellow)),
                  hintStyle: TextStyle(
                    color: Colors.blueGrey[400],
                  ),
                  border: InputBorder.none,
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: fromHex(yellow)),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              child: TextFormField(
                controller: _taskDescription,
                keyboardType: TextInputType.text,
                textCapitalization: TextCapitalization.sentences,
                maxLines: 5,
                textAlignVertical: TextAlignVertical.top,
                style: TextStyle(color: Colors.white),
                validator: (value) {
                  if (value!.isEmpty) {
                    return "Please Enter task's description";
                  }
                  return null;
                },
                onChanged: (value) {
                  if (_taskData != null) {
                    CreateTaskData t = _taskData!..taskDescription = value;
                    _saveTask(t);
                  }
                },
                decoration: InputDecoration(
                  filled: true,
                  fillColor: Colors.white10,
                  isDense: true,
                  hintText: "eg Task Description",
                  labelText: "Task Description",
                  labelStyle: TextStyle(color: fromHex(yellow)),
                  hintStyle: TextStyle(
                    color: Colors.blueGrey[400],
                  ),
                  border: InputBorder.none,
                  alignLabelWithHint: true,
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: fromHex(yellow)),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _priorityDrop(BuildContext context, String? item) {
    return Container(
      child: Text(
        priority!.isNotEmpty ? "$priority" : "Select Priority",
        style: TextStyle(
          color: priority!.isEmpty ? fromHex(yellow) : Colors.white,
          fontSize: 17,
        ),
      ),
    );
  }

  Widget _priorityPopUp(BuildContext context, String item, bool isSelected) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
              border: Border.all(color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
      child: ListTile(
        title: Text("$item",
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w100,
              fontSize: 20,
            )),
      ),
    );
  }

  Widget _taskTypeDrop(BuildContext context, TaskType? item) {
    return Container(
      child: Text(
        taskTypeId!.isNotEmpty ? "$taskTypeName" : "Select Task Type",
        style: TextStyle(
          color: taskTypeId!.isEmpty ? fromHex(yellow) : Colors.white,
          fontSize: 17,
        ),
      ),
    );
  }

  Widget _taskTypePopUp(BuildContext context, TaskType item, bool isSelected) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
              border: Border.all(color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
      child: ListTile(
        title: Text("${item.name}",
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w100,
              fontSize: 20,
            )),
      ),
    );
  }

  Widget _visibilityDrop(BuildContext context, String? item) {
    return Container(
      child: Text(
        visibility!.isNotEmpty ? "$visibility" : "Select Task Visibility",
        style: TextStyle(
          color: visibility!.isEmpty ? fromHex(yellow) : Colors.white,
          fontSize: 17,
        ),
      ),
    );
  }

  Widget _visibilityPopUp(BuildContext context, String item, bool isSelected) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
              border: Border.all(color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
      child: ListTile(
        title: Text("$item",
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w100,
              fontSize: 20,
            )),
      ),
    );
  }

  Widget _associationDrop(BuildContext context, String? item) {
    return Container(
      child: Text(
        association!.isNotEmpty ? "$association" : "Specify Task Assigned to",
        style: TextStyle(
          color: association!.isEmpty ? fromHex(yellow) : Colors.white,
          fontSize: 17,
        ),
      ),
    );
  }

  Widget _associationPopUp(BuildContext context, String item, bool isSelected) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
              border: Border.all(color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
      child: ListTile(
        title: Text("$item",
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w100,
              fontSize: 20,
            )),
      ),
    );
  }

  Widget _staffDrop(BuildContext context, StaffTask? item) {
    return Container(
      child: Text(
        "Select Assignee",
        style: TextStyle(
          color: fromHex(yellow),
          fontSize: 17,
        ),
      ),
    );
  }

  Widget _staffPopUp(BuildContext context, StaffTask item, bool isSelected) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
              border: Border.all(color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
      child: ListTile(
        title: Text("${item.name}",
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w100,
              fontSize: 20,
            )),
      ),
    );
  }

  Widget _functionDrop(BuildContext context, FunctionData? item) {
    return Container(
      child: Text(
        "Select Function",
        style: TextStyle(
          color: fromHex(yellow),
          fontSize: 17,
        ),
      ),
    );
  }

  Widget _functionPopUp(
      BuildContext context, FunctionData item, bool isSelected) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
              border: Border.all(color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
      child: ListTile(
        title: Text("${item.jobTitle}",
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.w100,
              fontSize: 20,
            )),
      ),
    );
  }

  void _saveTask(CreateTaskData taskData) {
    _createTaskData.put("current_task", taskData);
    setState(() {
      _taskData = taskData;
    });
  }

  void initializeData() {
    setState(() {
      _taskData = CreateTaskData(
          assignedBy: getUserId(),
          taskDescription: "",
          deadline: "",
          assignTo: [],
          taskItem: [],
          title: "",
          taskType: "",
          taskVisibility: "",
          priority: "",
          associatedBy: "",
          taskTypeName: "");
      _title.text = "";
      priority = '';
      taskTypeId = '';
      taskTypeName = '';
      visibility = '';
      association = '';
      _date.text = '';
      _taskDescription.text = '';
      isTaskSet = false;
      _saveTask(_taskData!);
    });
  }

  _filledDetails(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
      child: Card(
        color: Colors.black,
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 25),
              decoration: BoxDecoration(
                border: Border.all(color: fromHex(yellow).withOpacity(0.3)),
                borderRadius: BorderRadius.circular(15),
              ),
              child: Column(
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Task Details",
                    style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                  Container(
                    padding: EdgeInsets.all(15),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            _taskData!.title!.isEmpty
                                ? Container()
                                : Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "${_taskData!.title}",
                                          style: TextStyle(
                                              fontSize: 15.6,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        ),
                                        Text(
                                          "Task Title",
                                          style: TextStyle(
                                            fontSize: 11,
                                            fontStyle: FontStyle.italic,
                                            color: Colors.white54,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                            _taskData!.priority!.isEmpty
                                ? Container()
                                : Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "${_taskData!.priority}",
                                          style: TextStyle(
                                              fontSize: 15.6,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        ),
                                        Text(
                                          'Task Priority',
                                          style: TextStyle(
                                            fontSize: 11,
                                            fontStyle: FontStyle.italic,
                                            color: Colors.white54,
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                          ],
                        ),
                        Row(
                          children: [
                            _taskData!.taskTypeName!.isEmpty
                                ? Container()
                                : Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          "${_taskData!.taskTypeName}",
                                          style: TextStyle(
                                              fontSize: 15.6,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        ),
                                        Text(
                                          "Task Type",
                                          style: TextStyle(
                                            fontSize: 11,
                                            fontStyle: FontStyle.italic,
                                            color: Colors.white54,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                            _taskData!.taskVisibility!.isEmpty
                                ? Container()
                                : Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          "${_taskData!.taskVisibility}",
                                          style: TextStyle(
                                              fontSize: 15.6,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        ),
                                        Text(
                                          'Task Visibility',
                                          style: TextStyle(
                                            fontSize: 11,
                                            fontStyle: FontStyle.italic,
                                            color: Colors.white54,
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                          ],
                        ),
                        Row(
                          children: [
                            _taskData!.associatedBy!.isEmpty
                                ? Container()
                                : Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          "${_taskData!.associatedBy}",
                                          style: TextStyle(
                                              fontSize: 15.6,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        ),
                                        Text(
                                          "Assigned to",
                                          style: TextStyle(
                                            fontSize: 11,
                                            fontStyle: FontStyle.italic,
                                            color: Colors.white54,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                            _taskData!.deadline!.isEmpty
                                ? Container()
                                : Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          "${_taskData!.deadline}",
                                          style: TextStyle(
                                              fontSize: 15.6,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        ),
                                        Text(
                                          "Due By",
                                          style: TextStyle(
                                            fontSize: 11,
                                            fontStyle: FontStyle.italic,
                                            color: Colors.white54,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                          ],
                        ),
                        _taskData!.assignTo!.isEmpty
                            ? Container()
                            : Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(top: 8.0),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: Tags(
                                            itemCount:
                                                _taskData!.assignTo!.length,
                                            alignment: WrapAlignment.start,
                                            itemBuilder: (int index) {
                                              var type =
                                                  _taskData!.assignTo![index];
                                              return Tooltip(
                                                  message:
                                                      "Associated by : ${type?.type ?? ""}",
                                                  child: ItemTags(
                                                    textStyle: TextStyle(
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.normal,
                                                        color: fromHex(yellow)),
                                                    color: Colors.white10
                                                        .withOpacity(0.07),
                                                    activeColor: Colors.white10
                                                        .withOpacity(0.07),
                                                    singleItem: true,
                                                    index: index,
                                                    title: type?.name ?? "",
                                                    textColor: Colors.white,
                                                    border: Border.all(
                                                        color: Colors.black),
                                                    removeButton:
                                                        ItemTagsRemoveButton(
                                                      color: Colors.deepOrange,
                                                      backgroundColor:
                                                          Colors.transparent,
                                                      onRemoved: () {
                                                        setState(() {
                                                          _taskData!.assignTo!
                                                              .removeAt(index);
                                                        });
                                                        _saveTask(_taskData!);
                                                        return true;
                                                      },
                                                    ), // OR nu
                                                  ));
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding:
                                        const EdgeInsets.only(left: 10, top: 2),
                                    child: Text(
                                      'Assignees',
                                      style: TextStyle(
                                        fontSize: 11,
                                        fontStyle: FontStyle.italic,
                                        color: Colors.white54,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                        Row(
                          children: [
                            _taskData!.taskDescription!.isEmpty
                                ? Container()
                                : Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Text(
                                          "${_taskData!.taskDescription}",
                                          style: TextStyle(
                                              fontSize: 15.6,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white),
                                        ),
                                        Text(
                                          "Task Description",
                                          style: TextStyle(
                                            fontSize: 11,
                                            fontStyle: FontStyle.italic,
                                            color: Colors.white54,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                          ],
                        ),
                        _taskData!.taskItem!.isEmpty
                            ? Container()
                            : Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(
                                    padding: const EdgeInsets.only(top: 8.0),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: Tags(
                                            itemCount:
                                                _taskData!.taskItem!.length,
                                            alignment: WrapAlignment.start,
                                            itemBuilder: (int index) {
                                              var type =
                                                  _taskData!.taskItem![index];
                                              return InkWell(
                                                onLongPress: () {
                                                  _addTaskItemDialog(context,
                                                      type?.description ?? "");
                                                },
                                                child: ItemTags(
                                                  textStyle: TextStyle(
                                                      fontSize: 14,
                                                      fontWeight:
                                                          FontWeight.normal,
                                                      color: fromHex(yellow)),
                                                  color: Colors.white10
                                                      .withOpacity(0.07),
                                                  activeColor: Colors.white10
                                                      .withOpacity(0.07),
                                                  singleItem: true,
                                                  index: index,
                                                  title:
                                                      type?.description ?? "",
                                                  textColor: Colors.white,
                                                  border: Border.all(
                                                      color: Colors.black),
                                                  removeButton:
                                                      ItemTagsRemoveButton(
                                                    color: Colors.deepOrange,
                                                    backgroundColor:
                                                        Colors.transparent,
                                                    onRemoved: () {
                                                      setState(() {
                                                        _taskData!.taskItem!
                                                            .removeAt(index);
                                                      });
                                                      _saveTask(_taskData!);
                                                      return true;
                                                    },
                                                  ), // OR nu
                                                ),
                                              );
                                            },
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Padding(
                                    padding:
                                        const EdgeInsets.only(left: 10, top: 2),
                                    child: Text(
                                      'Assignees',
                                      style: TextStyle(
                                        fontSize: 11,
                                        fontStyle: FontStyle.italic,
                                        color: Colors.white54,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _submitBtn(BuildContext context, TaskModel model) {
    return Column(
      children: [
        Dismissible(
          key: UniqueKey(),
          direction: DismissDirection.startToEnd,
          onDismissed: (DismissDirection direction) async {
            setState(() {});
            _addTaskItemDialog(context, "");
          },
          child: Container(
            margin: EdgeInsets.symmetric(vertical: 5, horizontal: 15),
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
            decoration: BoxDecoration(
              color: Colors.white10.withOpacity(0.07),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Icon(
                  Icons.add,
                  size: 36,
                  color: fromHex(yellow),
                ),
                SizedBox(
                  width: 20,
                ),
                Expanded(
                  child: Text(
                    'Add Subtask',
                    style: TextStyle(
                      color: fromHex(yellow),
                      fontWeight: FontWeight.w100,
                      fontSize: 20,
                    ),
                  ),
                ),
                Icon(
                  Icons.arrow_forward_ios_sharp,
                  color: fromHex(yellow),
                )
              ],
            ),
          ),
        ),
        Dismissible(
          key: UniqueKey(),
          direction: DismissDirection.startToEnd,
          onDismissed: (DismissDirection direction) async {
            setState(() {});
            if (_taskData!.assignTo!.isEmpty) {
              showToast("Please Select Assignee(s)");
              return;
            }
            if (_formKey.currentState!.validate()) {
              if (_taskData!.taskItem!.isEmpty) {
                SubTask sub = SubTask()..description = _title.text;
                CreateTaskData t = _taskData!..taskItem = [sub];
                isTaskSet = true;
                _saveTask(t);
              }

              bool hasConnection = await checkConnection();
              if (hasConnection) {
                print(_taskData!.toJson());
                try {
                  var r = await model.createTask(_taskData!.toJson());
                  if (r['success']) {
                    initializeData();
                    showToast("Success");
                    Navigator.of(context).pushAndRemoveUntil(
                        MaterialPageRoute(
                            builder: (context) => TaskManagerPage()),
                        (Route<dynamic> route) => false);
                  }
                } catch (e) {
                  showToast(e.toString());
                }
              } else {
                // offlineResponses.add(data);
                showToast("Check your internet connection");
                // Navigator.of(context).pop();
              }
            }
            setState(() {});
          },
          child: Container(
            margin: EdgeInsets.symmetric(vertical: 5, horizontal: 15),
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
            decoration: BoxDecoration(
              color: Colors.white10.withOpacity(0.07),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Icon(
                  Icons.done,
                  size: 36,
                  color: fromHex(yellow),
                ),
                SizedBox(
                  width: 20,
                ),
                Expanded(
                  child: Text(
                    'Submit Response',
                    style: TextStyle(
                      color: fromHex(yellow),
                      fontWeight: FontWeight.w100,
                      fontSize: 20,
                    ),
                  ),
                ),
                Icon(
                  Icons.arrow_forward_ios_sharp,
                  color: fromHex(yellow),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }

  void setData(CreateTaskData t) {
    _title.text = t.title ?? "";
    priority = t.priority ?? "";
    taskTypeId = t.taskType ?? "";
    taskTypeName = t.taskTypeName ?? "";
    visibility = t.taskVisibility ?? "";
    association = t.associatedBy ?? "";
    _date.text = t.deadline ?? "";
    _taskDescription.text = t.taskDescription ?? "";
    isTaskSet = true;
  }

  Future<void> _addTaskItemDialog(
      BuildContext context, String description) async {
    List<SubTask?> subtasks = _taskData?.taskItem ?? [];
    TextEditingController _desc = TextEditingController(text: description);
    return await showDialog(
      context: context,
      builder: (context) => Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        elevation: 0.0,
        backgroundColor: Colors.grey[900],
        child: Container(
          color: Colors.transparent,
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
          child: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ListTile(
                  title: Center(
                    child: Text(
                      description.isEmpty ? 'Add Sub Task' : "Edit Sub Task",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.w800),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: TextFormField(
                    controller: _desc,
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.sentences,
                    maxLines: 5,
                    textAlignVertical: TextAlignVertical.top,
                    style: TextStyle(color: Colors.white),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Please Enter task's description";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white10,
                      isDense: true,
                      hintText: "eg Sub Task Description",
                      labelText: "Sub Task Description",
                      labelStyle: TextStyle(color: fromHex(yellow)),
                      hintStyle: TextStyle(
                        color: Colors.blueGrey[400],
                      ),
                      border: InputBorder.none,
                      alignLabelWithHint: true,
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: fromHex(yellow)),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                ListTile(
                  onTap: () async {
                    if (_desc.text.isNotEmpty) {
                      if (_taskData != null) {
                        SubTask sub = SubTask()..description = _desc.text;

                        subtasks.add(sub);

                        CreateTaskData t = _taskData!..taskItem = subtasks;
                        isTaskSet = true;
                        _saveTask(t);
                      }
                    }
                    Navigator.of(context).pop();
                  },
                  title: Center(
                    child: Text(
                      'Submit',
                      style: TextStyle(color: Colors.deepOrange),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
