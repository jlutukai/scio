import 'package:circular_bottom_navigation/circular_bottom_navigation.dart';
import 'package:circular_bottom_navigation/tab_item.dart';
import 'package:flutter/material.dart';
import 'package:scio_security/ui/screens/taskManager/task_list_page.dart';
import 'package:scio_security/ui/screens/taskManager/create_task.dart';
import 'package:scio_security/utils/tasks_icons_icons.dart';
import 'package:scio_security/utils/useful.dart';
import 'package:cupertino_tabbar/cupertino_tabbar.dart' as CupertinoTabBar;

class TaskManagerPage extends StatefulWidget {
  static const tag = 'task_manager';
  const TaskManagerPage({Key? key}) : super(key: key);

  @override
  _TaskManagerPageState createState() => _TaskManagerPageState();
}

class _TaskManagerPageState extends State<TaskManagerPage> {
  int currentIndex = 0;
  PageController pageController = PageController();
  late CircularBottomNavigationController _navigationController;
  List<TabItem> tabItems = List.of([
    // new TabItem(Icons.home, "Home", fromHex('#ebd774'), labelStyle: TextStyle(color:fromHex('#ebd774'),)),
    new TabItem(
      TasksIcons.task_assigned__1_,
      "Open Tasks",
      Colors.white12,
      labelStyle: TextStyle(
        color: fromHex(deepOrange),
      ),
    ),
    new TabItem(
      TasksIcons.task_my__1_,
      "Other",
      Colors.white12,
      labelStyle: TextStyle(
        color: fromHex(deepOrange),
      ),
    ),
    // new TabItem(TasksIcons.task_public, "Completed", Colors.white12,
    //     labelStyle: TextStyle(
    //       color: fromHex(deepOrange),
    //     )),
    new TabItem(
      Icons.checklist_rounded,
      "Public",
      Colors.white12,
      labelStyle: TextStyle(
        color: fromHex(deepOrange),
      ),
    ),
  ]);

  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    _navigationController =
        new CircularBottomNavigationController(currentIndex);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Scaffold(
          backgroundColor: Colors.black,
          // key: _scaffoldKey,
          appBar: AppBar(
            backgroundColor: Colors.black,
            automaticallyImplyLeading: false,
            centerTitle: true,
            title: Text(
              'Task Manager',
              style: TextStyle(color: fromHex(deepOrange)),
            ),
          ),
          body: Column(
            children: [
              Expanded(
                child: PageView(
                  physics: NeverScrollableScrollPhysics(),
                  controller: pageController,
                  onPageChanged: (index) {
                    setState(() {
                      currentIndex = index;
                    });
                  },
                  children: <Widget>[
                    TaskListPage("open"),
                    TaskListPage("other"),
                    // TaskListPage("completed"),
                    TaskListPage("public"),
                  ],
                ),
              ),
              _createTaskBtn(context),
            ],
          ),
          bottomNavigationBar: CircularBottomNavigation(
            tabItems,
            controller: _navigationController,
            barBackgroundColor: Colors.black,
            selectedPos: currentIndex,
            iconsSize: 20,
            selectedIconColor: fromHex(deepOrange),
            normalIconColor: fromHex(grey),
            selectedCallback: (int? index) {
              setState(() {
                this.currentIndex = index ?? 0;
              });
              navigateToScreens(index ?? 0);
            },
          ),
        ),
        Positioned(
          left: 15,
          top: 27,
          child: FloatingActionButton(
            foregroundColor: Colors.grey,
            backgroundColor: Colors.white10.withOpacity(0.07),
            onPressed: () {
              Navigator.pop(context);
            },
            mini: true,
            heroTag: "go_back",
            tooltip: "go back home",
            child: Icon(
              Icons.keyboard_backspace_sharp,
              color: fromHex(yellow),
            ),
          ),
        ),
        // Positioned(
        //   right: 15,
        //   top: 27,
        //   child: _timeSheet.values.isEmpty
        //       ? Container()
        //       : FloatingActionButton(
        //     foregroundColor: Colors.grey,
        //     backgroundColor: Colors.white10.withOpacity(0.07),
        //     onPressed: () async {
        //       bool isConnected = await checkConnection();
        //       if (!isConnected) {
        //         showToast(
        //             'No connection to DB, Check Internet Connection!');
        //         return;
        //       }
        //       await sync();
        //     },
        //     mini: true,
        //     tooltip: "Sync offline data",
        //     child: Icon(
        //       Icons.sync_rounded,
        //       color: fromHex(yellow),
        //     ),
        //   ),
        // ),
      ],
    );
  }

  void navigateToScreens(int index) {
    // print('################ @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ ####################### called');
    setState(() {
      currentIndex = index;
      pageController.animateToPage(index,
          duration: Duration(milliseconds: 100), curve: Curves.ease);
    });
  }

  _taskList(BuildContext context) {
    return Container();
  }

  _createTaskBtn(BuildContext context) {
    return Dismissible(
      key: UniqueKey(),
      direction: DismissDirection.startToEnd,
      onDismissed: (DismissDirection direction) async {
        setState(() {});
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (BuildContext context) => CreateTaskPage(),
          ),
        );
        setState(() {});
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 5, horizontal: 15),
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
        decoration: BoxDecoration(
          color: Colors.white10.withOpacity(0.07),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Icon(
              Icons.add,
              size: 36,
              color: fromHex(yellow),
            ),
            SizedBox(
              width: 20,
            ),
            Expanded(
              child: Text(
                'Create a new task',
                style: TextStyle(
                  color: fromHex(yellow),
                  fontWeight: FontWeight.w100,
                  fontSize: 20,
                ),
              ),
            ),
            Icon(
              Icons.arrow_forward_ios_sharp,
              color: fromHex(yellow),
            )
          ],
        ),
      ),
    );
  }
}
