import 'package:card_swiper/card_swiper.dart';
import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';
import 'package:lottie/lottie.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:scio_security/core/enums/view_state.dart';
import 'package:scio_security/core/models/get_tasks_response.dart';
import 'package:scio_security/core/viewmodels/task_model.dart';
import 'package:scio_security/ui/screens/base_view.dart';
import 'package:scio_security/ui/screens/taskManager/task_detail.dart';
import 'package:scio_security/utils/loader.dart';
import 'package:scio_security/utils/useful.dart';

class TaskListPage extends StatefulWidget {
  final String s;

  const TaskListPage(this.s, {Key? key}) : super(key: key);

  @override
  _TaskListPageState createState() => _TaskListPageState();
}

class _TaskListPageState extends State<TaskListPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return BaseView<TaskModel>(
      onModelReady: (model) async {
        Map<String, String> data = {
          'user_id': "${getUserId()}", // Todo
          // 'user_id': "1247",
          'request_type': "${widget.s}"
        };
        await model.getTasks(data);
      },
      builder: (context, model, child) => model.state == ViewState.Idle
          ? Stack(
              children: [
                Scaffold(
                  backgroundColor: Colors.black,
                  key: _scaffoldKey,
                  body: SingleChildScrollView(
                    physics: BouncingScrollPhysics(),
                    child: model.tasks != null
                        ? model.tasks!.isNotEmpty
                            ? Column(
                                children: [
                                  Container(
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.5,
                                      child: _taskList2(context, model)),
                                  _taskList(context, model),
                                ],
                              )
                            : _isEmptyView()
                        : _isNullView(),
                  ),
                ),
              ],
            )
          : Loader(),
    );
  }

  _taskList2(BuildContext context, TaskModel model) {
    return model.tasks != null
        ? model.tasks!.isNotEmpty
            ? Swiper(
                layout: SwiperLayout.STACK,
                customLayoutOption:
                    CustomLayoutOption(startIndex: -1, stateCount: 3)
                      ..addRotate([-45.0 / 180, 0.0, 45.0 / 180])
                      ..addTranslate([
                        new Offset(-350.0, -20.0),
                        new Offset(0.0, 0.0),
                        new Offset(350.0, -20.0)
                      ]),
                itemWidth: MediaQuery.of(context).size.width - 150,
                itemHeight: MediaQuery.of(context).size.height * 0.35,
                pagination: SwiperPagination(
                    builder: DotSwiperPaginationBuilder(
                        activeColor: fromHex(yellow), color: Colors.grey[900])),
                // loop: false,
                itemBuilder: (context, index) =>
                    _taskListItem1(context, model.tasks![index], model, false),
                itemCount: model.tasks?.length ?? 0)
            : Center(
                child: Text("No ${widget.s} added yet"),
              )
        : Center(
            child: Text("An error occurred while fetching ${widget.s} tasks"),
          );
  }

  _taskList(BuildContext context, TaskModel model) {
    return model.tasks != null
        ? model.tasks!.isNotEmpty
            ? ListView.builder(
                physics: BouncingScrollPhysics(),
                itemCount: model.tasks?.length ?? 0,
                shrinkWrap: true,
                itemBuilder: (context, index) =>
                    _taskListItem(context, model.tasks![index], model, true),
              )
            : Center(
                child: Text("No ${widget.s} added yet"),
              )
        : Center(
            child: Text("An error occurred while fetching ${widget.s} tasks"),
          );
  }

  _taskListItem1(BuildContext context, TaskData task, TaskModel model, bool b) {
    return Container(
      // margin: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      decoration: BoxDecoration(
        color: Colors.grey[900],
        borderRadius: BorderRadius.circular(10),
      ),
      child: Container(
        child: Column(
          children: [
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        child: CircularPercentIndicator(
                          radius: 50.0,
                          lineWidth: 5.0,
                          percent:
                              (double.tryParse(task.progress ?? "0.0") ?? 0.0) /
                                  100,
                          center: new Text("${task.progress} %"),
                          progressColor: Colors.green[600],
                        ),
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Expanded(
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Expanded(
                                  child: Padding(
                                    padding: EdgeInsets.only(left: 1),
                                    child: Text(
                                      "${Jiffy(task.deadline ?? "1970-01-01").format("do MMMM yyyy")}",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Row(
                              children: [
                                Expanded(
                                  child: Text(
                                    "${task.title}",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w800,
                                      color: fromHex(deepOrange),
                                    ),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Text(
                          "${task.taskDescription ?? ''}",
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontSize: 12,
                              fontWeight: FontWeight.w100,
                              color: fromHex(yellow)),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: Wrap(
                              children: [
                                Text(
                                  'Assigned By : ',
                                  style: TextStyle(
                                    fontSize: 13,
                                    fontWeight: FontWeight.w100,
                                    color: fromHex(yellow),
                                  ),
                                ),
                                Text(
                                  '${task.associatedBy}',
                                  style: TextStyle(
                                    fontSize: 13,
                                    fontWeight: FontWeight.bold,
                                    color: fromHex(deepOrange),
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  'on : ',
                                  style: TextStyle(
                                      fontSize: 13,
                                      fontWeight: FontWeight.w100,
                                      color: fromHex(yellow)),
                                ),
                                Text(
                                  '8 Sept',
                                  style: TextStyle(
                                    fontSize: 13,
                                    fontWeight: FontWeight.bold,
                                    color: fromHex(deepOrange),
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: Row(
                              children: [
                                Text(
                                  'And is : ',
                                  style: TextStyle(
                                    fontSize: 13,
                                    fontWeight: FontWeight.w100,
                                    color: fromHex(yellow),
                                  ),
                                ),
                                Text(
                                  "${task.priority}",
                                  style: TextStyle(
                                    fontSize: 13,
                                    fontWeight: FontWeight.bold,
                                    color: fromHex(deepOrange),
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ],
                  )
                ],
              ),
            ),
            SizedBox(
              height: 5,
            ),
            Row(
              children: [
                // widget.s == 'open'
                //     ? IconButton(
                //         icon: Icon(
                //           Icons.edit,
                //           color: Colors.white,
                //         ),
                //         onPressed: () async {
                //           bool hasConnection = await checkConnection();
                //           if (hasConnection) {
                //             _editProgressDialog(context, task, model);
                //           } else {
                //             showToast("Check internet connectivity");
                //           }
                //         },
                //       )
                //     : Container(),
                Expanded(
                  child: Container(),
                ),
                IconButton(
                  onPressed: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (BuildContext context) => TaskDetailPage(task),
                      ),
                    );
                  },
                  icon: Icon(
                    Icons.comment_rounded,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 5,
            ),
          ],
        ),
      ),
    );
  }

  // Future<void> _editProgressDialog(
  //   BuildContext context,
  //   TaskData task,
  //   TaskModel model,
  // ) async {
  //   double _value1 = double.tryParse(task.progress ?? "0.0") ?? 0.0;
  //   return await showDialog(
  //     context: _scaffoldKey.currentContext!,
  //     builder: (context) => model.state == ViewState.Idle
  //         ? Dialog(
  //             shape: RoundedRectangleBorder(
  //               borderRadius: BorderRadius.circular(20),
  //             ),
  //             elevation: 0.0,
  //             backgroundColor: Colors.grey[900],
  //             child: StatefulBuilder(
  //               builder: (context, StateSetter setState) => Container(
  //                 color: Colors.transparent,
  //                 padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
  //                 child: Container(
  //                   child: Column(
  //                     mainAxisSize: MainAxisSize.min,
  //                     mainAxisAlignment: MainAxisAlignment.center,
  //                     children: [
  //                       ListTile(
  //                         title: Center(
  //                           child: Text(
  //                             'Change Progress Value',
  //                             style: TextStyle(color: Colors.white),
  //                           ),
  //                         ),
  //                       ),
  //                       FluidSlider(
  //                         value: _value1,
  //                         onChanged: (double newValue) {
  //                           setState(() {
  //                             _value1 = newValue;
  //                           });
  //                         },
  //                         labelsTextStyle: TextStyle(color: Colors.white),
  //                         sliderColor: fromHex(yellow),
  //                         thumbColor: Colors.black,
  //                         min: 0.0,
  //                         max: 100.0,
  //                       ),
  //                       model.state == ViewState.Idle
  //                           ? ListTile(
  //                               onTap: () async {
  //                                 Map<String, String> data = {
  //                                   'user_id': "${getUserId()}", //Todo
  //                                   // 'user_id': "1247",
  //                                   'task_id': "${task.id}",
  //                                   'task_progress': "${_value1.toInt()}"
  //                                 };
  //                                 if (double.tryParse(task.progress ?? "0.0") ==
  //                                     _value1) {
  //                                   Navigator.of(context).pop();
  //                                 } else {
  //                                   bool hasConnection =
  //                                       await checkConnection();
  //                                   if (hasConnection) {
  //                                     print(data);
  //                                     Navigator.of(context).pop();
  //                                     // _sendData(data, model);
  //                                   } else {
  //                                     // offlineResponses.add(data);
  //                                     showToast(
  //                                         "Check your internet connection");
  //                                     Navigator.of(context).pop();
  //                                   }
  //                                 }
  //                               },
  //                               title: Center(
  //                                 child: Text(
  //                                   'Submit',
  //                                   style: TextStyle(color: Colors.deepOrange),
  //                                 ),
  //                               ),
  //                             )
  //                           : Container(),
  //                     ],
  //                   ),
  //                 ),
  //               ),
  //             ),
  //           )
  //         : Container(),
  //   );
  // }

  Future<void> reload(TaskModel model) async {
    Map<String, String> data = {
      'user_id': "${getUserId()}", // Todo
      // 'user_id': "1247",
      'request_type': "${widget.s}"
    };
    await model.getTasks(data);
  }

  _taskListItem(
      BuildContext context, TaskData task, TaskModel model, bool bool) {
    return Dismissible(
      key: UniqueKey(),
      direction: DismissDirection.startToEnd,
      onDismissed: (DismissDirection direction) async {
        setState(() {});
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (BuildContext context) => TaskDetailPage(task),
          ),
        );
        setState(() {});
      },
      child: Container(
        margin: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        decoration: BoxDecoration(
          color: Colors.white10.withOpacity(0.07),
          borderRadius: BorderRadius.circular(10),
        ),
        child: Container(
          child: Row(
            children: [
              Expanded(
                  child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Text(
                          "${task.title}",
                          style: TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Expanded(
                        child: Text(
                          "${task.status}",
                          textAlign: TextAlign.end,
                          style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.w100,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ))
            ],
          ),
        ),
      ),
    );
  }

  _isNullView() {
    return Container(
      height: MediaQuery.of(context).size.height - 300,
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: [
          Expanded(
            child: Center(
              child: Text("An Error occurred while fetching tasks"),
            ),
          ),
        ],
      ),
    );
  }

  _isEmptyView() {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Column(
        children: [
          Expanded(
            child: Center(
              child: widget.s == 'open'
                  ? Column(
                      children: [
                        Container(
                            height: 200,
                            width: 200,
                            child: Lottie.asset('assets/thumbs_up.json')),
                        Text(
                          'Good Work No Pending Tasks assigned to you',
                          style: TextStyle(color: fromHex(deepOrange)),
                        )
                      ],
                    )
                  : Center(
                      child: Text("No tasks have been added yet..."),
                    ),
            ),
          ),
        ],
      ),
    );
  }

// Future<void> _sendData(Map<String, String> data, TaskModel model) async {
//   try {
//     var r = await model.updateTaskProgress(data);
//     if (r['success']) {
//       await reload(model);
//       showToast("Success");
//     }
//   } catch (e) {
//     showToast(e.toString());
//   }
// }

}
