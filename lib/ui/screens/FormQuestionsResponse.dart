import 'dart:async';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:image_picker/image_picker.dart';
import 'package:lottie/lottie.dart';
import 'package:path_provider/path_provider.dart';
import 'package:scio_security/core/enums/view_state.dart';
import 'package:scio_security/core/models/get_form_response.dart';
import 'package:scio_security/core/models/local/localModels.dart';
import 'package:scio_security/core/viewmodels/ReportFormModel.dart';
import 'package:scio_security/utils/useful.dart';
import 'package:stop_watch_timer/stop_watch_timer.dart';

import 'base_view.dart';

class FormQuestions extends StatefulWidget {
  final FormData formData;
  FormQuestions(this.formData);
  @override
  _FormQuestionsResponseState createState() => _FormQuestionsResponseState();
}

class _FormQuestionsResponseState extends State<FormQuestions> {
  late ReportFormsModel _model;
  List<FormItems> questions = [];

  Box offlineResponses = Hive.box("form_response");
  Box savedResponses = Hive.box("form_response_questions");
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final _picker = ImagePicker();
  final StopWatchTimer _stopWatchTimer = StopWatchTimer();
  UpdateFormData? existingForm;

  @override
  void dispose() async {
    super.dispose();
    await _stopWatchTimer.dispose();
  }

  @override
  void initState() {
    _stopWatchTimer.rawTime.listen((value) {});

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<ReportFormsModel>(
        onModelReady: (model) async {
          _model = model;
          bool isConnected = await checkConnection();
          if (!isConnected) {
            showToast('No connection, Check Internet Connection!');
          }
          questions.clear();

          existingForm = savedResponses.get(
              "${getPlanner()!.plannerId}_${widget.formData.id}_${getShiftId()}");
          if (existingForm == null) {
            widget.formData.items!.forEach((element) {
              FormItems formItem = FormItems()
                ..response = ""
                ..type = "default"
                ..formItemId = element.id
                ..question = element.name
                ..responseType = element.type
                ..required = true;
              questions.add(formItem);
            });
            widget.formData.customItems!.forEach((element) {
              FormItems formItem = FormItems()
                ..response = ""
                ..type = "custom"
                ..formItemId = element.id
                ..responseType = element.type
                ..question = element.name
                ..required = true;
              questions.add(formItem);
            });
            UpdateFormData d = UpdateFormData()
              ..id = ""
              ..plannerId = getPlanner()!.plannerId
              ..answeredBy = getUserId()
              ..formId = widget.formData.id
              ..date = df.format(DateTime.now())
              ..shiftId = getShiftId().toString()
              ..items = questions;
            savedResponses.put(
                "${getPlanner()!.plannerId}_${widget.formData.id}_${getShiftId()}",
                d);
          } else {
            questions.addAll(existingForm!.items!);
          }
          setState(() {});
          if (offlineResponses.values.isNotEmpty) {
            sync();
          }
        },
        builder: (context, model, child) => Stack(
              children: [
                Scaffold(
                  key: _scaffoldKey,
                  backgroundColor: Colors.black,
                  appBar: AppBar(
                    backgroundColor: Colors.transparent,
                    elevation: 0,
                    title: Text(
                      'Forms',
                      style: TextStyle(
                          color: fromHex(deepOrange),
                          fontWeight: FontWeight.bold),
                    ),
                    centerTitle: true,
                    automaticallyImplyLeading: false,
                    actions: [
                      offlineResponses.values.isNotEmpty
                          ? IconButton(
                              onPressed: () async {
                                bool isConnected = await checkConnection();
                                if (!isConnected) {
                                  showToast(
                                      'No connection, Check Internet Connection!');
                                  return;
                                }
                                sync();
                              },
                              icon: Icon(
                                Icons.sync_rounded,
                                color: Colors.white,
                              ))
                          : Container(),
                      existingForm != null
                          ? Padding(
                              padding: const EdgeInsets.only(right: 10),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  InkWell(
                                    onTap: () {
                                      setState(() {
                                        existingForm = null;
                                        savedResponses.delete(
                                            "${getPlanner()!.plannerId}_${widget.formData.id}_${getShiftId()}");
                                      });
                                      setInit();
                                    },
                                    child: Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 10, vertical: 5),
                                        decoration: BoxDecoration(
                                            color: Colors.white10
                                                .withOpacity(0.07),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(16))),
                                        child: Text(
                                          'Clear',
                                          style: TextStyle(
                                              color: Colors.deepOrange),
                                        )),
                                  ),
                                ],
                              ),
                            )
                          : Container()
                    ],
                  ),
                  body: Column(
                    children: [
                      SizedBox(
                        height: 10,
                      ),
                      Expanded(
                        child: model.state == ViewState.Idle
                            ? _questionsList(model, context)
                            : Center(
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Container(
                                        height: 200,
                                        width: 200,
                                        child: Lottie.asset(
                                            'assets/loading.json')),
                                    Text(
                                      'Please wait \n Loading...',
                                      style:
                                          TextStyle(color: fromHex(deepOrange)),
                                    )
                                  ],
                                ),
                              ),
                      ),
                      model.state == ViewState.Idle
                          ? Dismissible(
                              key: UniqueKey(),
                              direction: DismissDirection.startToEnd,
                              onDismissed: (DismissDirection direction) async {
                                bool hasConnection = await checkConnection();
                                bool filled =
                                    await (checkIfAllHaveBeenAnswered()
                                        as FutureOr<bool>);
                                setState(() {});
                                if (!filled) {
                                  showToast("Provide all responses");
                                  return;
                                }

                                UpdateFormData data = UpdateFormData()
                                  ..id = ""
                                  ..plannerId = getPlanner()!.plannerId
                                  ..answeredBy = getUserId()
                                  ..formId = widget.formData.id
                                  ..date = df.format(DateTime.now())
                                  ..shiftId = getShiftId().toString()
                                  ..items = questions;

                                if (hasConnection) {
                                  try {
                                    var r = await model
                                        .updateResponse(data.toJson());
                                    if (r['success']) {
                                      showToast("Success");
                                      Navigator.of(context).pop();
                                    }
                                  } catch (e) {
                                    showToast(e.toString());
                                  }
                                } else {
                                  offlineResponses.add(data);
                                  showToast("Success");
                                  Navigator.of(context).pop();
                                }
                                setState(() {});
                              },
                              child: Container(
                                margin: EdgeInsets.symmetric(
                                    vertical: 5, horizontal: 15),
                                padding: EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 15),
                                decoration: BoxDecoration(
                                  color: Colors.white10.withOpacity(0.07),
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Icon(
                                      Icons.done,
                                      size: 36,
                                      color: fromHex(yellow),
                                    ),
                                    SizedBox(
                                      width: 20,
                                    ),
                                    Expanded(
                                      child: Text(
                                        'Submit Response',
                                        style: TextStyle(
                                          color: fromHex(yellow),
                                          fontWeight: FontWeight.w100,
                                          fontSize: 20,
                                        ),
                                      ),
                                    ),
                                    Icon(
                                      Icons.arrow_forward_ios_sharp,
                                      color: fromHex(yellow),
                                    )
                                  ],
                                ),
                              ),
                            )
                          : Container()
                    ],
                  ),
                ),
                Positioned(
                  left: 15,
                  top: 27,
                  child: FloatingActionButton(
                    foregroundColor: Colors.grey,
                    backgroundColor: Colors.white10.withOpacity(0.07),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    mini: true,
                    tooltip: "go back home",
                    child: Icon(
                      Icons.keyboard_backspace_sharp,
                      color: fromHex(deepOrange),
                    ),
                  ),
                ),
              ],
            ));
  }

  _questionsList(ReportFormsModel model, BuildContext context) {
    return questions != null
        ? questions.isNotEmpty
            ? ListView.builder(
                physics: BouncingScrollPhysics(),
                itemCount: questions.length,
                itemBuilder: (context, index) => Container(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Dismissible(
                      key: UniqueKey(),
                      direction: DismissDirection.startToEnd,
                      onDismissed: (DismissDirection direction) async {
                        _inputDialog(context, questions[index], model);
                        setState(() {});
                      },
                      child: InkWell(
                        onTap: () async {
                          _inputDialog(context, questions[index], model);
                          setState(() {});
                        },
                        child: Container(
                          color: Colors.white10.withOpacity(0.07),
                          margin:
                              EdgeInsets.symmetric(horizontal: 5, vertical: 3),
                          child: Row(
                            children: [
                              Container(
                                color: questions[index].response!.isNotEmpty
                                    ? Colors.green
                                    : Colors.yellow,
                                width: 2,
                                height: 75,
                              ),
                              Expanded(
                                child: Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 10),
                                  child: ExpansionTile(
                                    leading: Text(
                                      "${index + 1}",
                                      style: TextStyle(color: fromHex(yellow)),
                                    ),
                                    title: Text(
                                      "${questions[index].question}",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    children: [
                                      Text(
                                        "${questions[index].response ?? ''}",
                                        style:
                                            TextStyle(color: fromHex(yellow)),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      )),
                ),
              )
            : Center(
                child: Text('Questions Not added yet'),
              )
        : Container();
  }

  void _inputDialog(
      BuildContext context, FormItems checklist, ReportFormsModel model) async {
    print("${checklist.responseType}");
    if (checklist.responseType == null) {
      showToast("Question type is undefined");
      return;
    }
    if (checklist.responseType!.isEmpty) {
      showToast("Question type is empty");
      return;
    }
    if (checklist.responseType!.contains("Yes/No")) {
      String _yes = 'yes';
      String _no = 'no';
      String? _ans = '';
      await showDialog(
        context: _scaffoldKey.currentContext!,
        builder: (context) => Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          elevation: 0.0,
          backgroundColor: Colors.grey[900],
          child: StatefulBuilder(
            builder: (context, StateSetter setState) => Container(
              color: Colors.transparent,
              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              child: Container(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ListTile(
                      title: Text(
                        "${checklist.question}",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.white),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Row(
                            children: [
                              Radio(
                                  value: _no,
                                  groupValue: _ans,
                                  activeColor: fromHex(yellow),
                                  onChanged: (dynamic v) {
                                    setState(() {
                                      _ans = v;
                                    });
                                  }),
                              Text('No',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white)),
                            ],
                          ),
                          Row(
                            children: [
                              Radio(
                                  value: _yes,
                                  activeColor: fromHex(yellow),
                                  // toggleable: true,
                                  groupValue: _ans,
                                  onChanged: (dynamic v) {
                                    setState(() {
                                      _ans = v;
                                    });
                                  }),
                              Text('Yes',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white)),
                            ],
                          ),
                        ],
                      ),
                    ),
                    ListTile(
                      onTap: () async {
                        if (_ans!.isEmpty) {
                          showToast('Please select a response');
                          return;
                        }
                        questions.forEach((element) {
                          if (checklist.formItemId == element.formItemId &&
                              checklist.type == element.type) {
                            setState(() {
                              element.response = "$_ans";
                            });
                          }
                        });

                        setState(() {});
                        setSaved();

                        Navigator.of(context).pop();
                      },
                      title: Center(
                        child: Text(
                          'Submit',
                          style: TextStyle(color: Colors.deepOrange),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      );
    }
    if (checklist.responseType!.contains("Open Text")) {
      //Open Text
      TextEditingController _comment = TextEditingController();
      await showDialog(
          context: context,
          builder: (context) => Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              elevation: 0.0,
              backgroundColor: Colors.grey[900],
              child: Container(
                color: Colors.transparent,
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                child: Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ListTile(
                        title: Text(
                          "${checklist.question}",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                      ),
                      Container(
                        child: TextFormField(
                          controller: _comment,
                          keyboardType: TextInputType.text,
                          textCapitalization: TextCapitalization.sentences,
                          maxLines: 6,
                          textAlignVertical: TextAlignVertical.top,
                          style: TextStyle(color: Colors.white),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return "Please Enter Your Response";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white10,
                            isDense: true,
                            hintText: "Enter your response here..",
                            labelText: "Response",
                            alignLabelWithHint: true,
                            labelStyle: TextStyle(color: fromHex(yellow)),
                            hintStyle: TextStyle(
                              color: Colors.blueGrey[400],
                            ),
                            border: InputBorder.none,
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: fromHex(yellow)),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      ListTile(
                        onTap: () async {
                          if (_comment.text.isEmpty) {
                            showToast("Please fill response");
                            return;
                          }
                          questions.forEach((element) {
                            if (checklist.formItemId == element.formItemId &&
                                checklist.type == element.type) {
                              setState(() {
                                element.response = "${_comment.text}";
                              });
                            }
                          });
                          setSaved();
                          Navigator.of(context).pop();
                        },
                        title: Center(
                          child: Text(
                            'Submit',
                            style: TextStyle(color: Colors.deepOrange),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )));
    }
    if (checklist.responseType!.contains("default")) {
      //Open Text
      TextEditingController _comment = TextEditingController();
      await showDialog(
          context: context,
          builder: (context) => Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              elevation: 0.0,
              backgroundColor: Colors.grey[900],
              child: Container(
                color: Colors.transparent,
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                child: Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ListTile(
                        title: Text(
                          "${checklist.question}",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                      ),
                      Container(
                        child: TextFormField(
                          controller: _comment,
                          keyboardType: TextInputType.text,
                          textCapitalization: TextCapitalization.sentences,
                          maxLines: 6,
                          textAlignVertical: TextAlignVertical.top,
                          style: TextStyle(color: Colors.white),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return "Please Enter Your Response";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white10,
                            isDense: true,
                            hintText: "Enter your response here..",
                            labelText: "Response",
                            alignLabelWithHint: true,
                            labelStyle: TextStyle(color: fromHex(yellow)),
                            hintStyle: TextStyle(
                              color: Colors.blueGrey[400],
                            ),
                            border: InputBorder.none,
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: fromHex(yellow)),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      ListTile(
                        onTap: () async {
                          if (_comment.text.isEmpty) {
                            showToast("Please fill response");
                            return;
                          }
                          questions.forEach((element) {
                            if (checklist.formItemId == element.formItemId &&
                                checklist.type == element.type) {
                              setState(() {
                                element.response = "${_comment.text}";
                              });
                            }
                          });
                          setSaved();
                          Navigator.of(context).pop();
                        },
                        title: Center(
                          child: Text(
                            'Submit',
                            style: TextStyle(color: Colors.deepOrange),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )));
    }
    if (checklist.responseType!.contains("Number")) {
      // Number
      TextEditingController _number = TextEditingController();
      await showDialog(
          context: context,
          builder: (context) => Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              elevation: 0.0,
              backgroundColor: Colors.grey[900],
              child: Container(
                color: Colors.transparent,
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                child: Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ListTile(
                        title: Text(
                          "${checklist.question}",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                      ),
                      Container(
                        child: TextFormField(
                          controller: _number,
                          keyboardType: TextInputType.number,
                          textAlignVertical: TextAlignVertical.top,
                          style: TextStyle(color: Colors.white),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return "Please Enter Your Response";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white10,
                            isDense: true,
                            hintText: "e.g. 1",
                            labelText: "Response",
                            labelStyle: TextStyle(color: fromHex(yellow)),
                            hintStyle: TextStyle(
                              color: Colors.blueGrey[400],
                            ),
                            border: InputBorder.none,
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: fromHex(yellow)),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      ListTile(
                        onTap: () async {
                          if (_number.text.isEmpty) {
                            showToast("Please fill response");
                            return;
                          }
                          questions.forEach((element) {
                            if (checklist.formItemId == element.formItemId &&
                                checklist.type == element.type) {
                              setState(() {
                                element.response = "${_number.text}";
                              });
                            }
                          });
                          setSaved();
                          Navigator.of(context).pop();
                        },
                        title: Center(
                          child: Text(
                            'Submit',
                            style: TextStyle(color: Colors.deepOrange),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )));
    }
    if (checklist.responseType!.contains("Video")) {
      // Video
      File? _video;
      await showDialog(
          context: context,
          builder: (context) => Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              elevation: 0.0,
              backgroundColor: Colors.grey[900],
              child: Container(
                color: Colors.transparent,
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                child: Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ListTile(
                        title: Text(
                          "${checklist.question}",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                      ),
                      Container(),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 40),
                        child: ListTile(
                          onTap: () async {
                            _video = await getVideo(ImageSource.camera);
                            if (_video == null) {
                              showToast("File not found try again!");
                              return;
                            }
                            questions.forEach((element) {
                              if (checklist.formItemId == element.formItemId &&
                                  checklist.type == element.type) {
                                setState(() {
                                  element.response = "${_video!.path}";
                                  element.isFile = true;
                                });
                              }
                            });
                            setSaved();
                            Navigator.of(context).pop();
                          },
                          leading: Icon(
                            Icons.video_call_outlined,
                            color: fromHex(yellow),
                          ),
                          title: Text(
                            'Start Recording',
                            style: TextStyle(color: fromHex(yellow)),
                          ),
                        ),
                      ),
                      Divider(
                        thickness: 1,
                        color: Colors.grey,
                        indent: 40,
                        endIndent: 40,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 40),
                        child: ListTile(
                          onTap: () async {
                            _video = await _getFile(['mp4']);
                            if (_video == null) {
                              showToast("File not found try again!");
                              return;
                            }
                            questions.forEach((element) {
                              if (checklist.formItemId == element.formItemId &&
                                  checklist.type == element.type) {
                                setState(() {
                                  element.response = "${_video!.path}";
                                  element.isFile = true;
                                });
                              }
                            });
                            setSaved();
                            Navigator.of(context).pop();
                          },
                          leading: Icon(
                            Icons.storage,
                            color: fromHex(yellow),
                          ),
                          title: Text(
                            'Pick From Storage',
                            style: TextStyle(color: fromHex(yellow)),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )));
    }
    if (checklist.responseType!.contains("Audio")) {
      //Audio
      File? _audio;
      await showDialog(
          context: context,
          builder: (context) => Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              elevation: 0.0,
              backgroundColor: Colors.grey[900],
              child: Container(
                color: Colors.transparent,
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                child: Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ListTile(
                        title: Text(
                          "${checklist.question}",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                      ),
                      Container(),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 40),
                        child: ListTile(
                          onTap: () async {
                            // bool hasPermissions =
                            // await AudioRecorder.hasPermissions;
                            // if (!hasPermissions) {
                            //   Permission.microphone.request();
                            //   showToast("Allow Audio permissions to proceed!");
                            //   return;
                            // }
                            _audio = await getAudioRecording(context);
                            if (_audio == null) {
                              showToast("File not found try again!");
                              return;
                            }
                            questions.forEach((element) {
                              if (checklist.formItemId == element.formItemId &&
                                  checklist.type == element.type) {
                                setState(() {
                                  element.response = "${_audio!.path}";
                                  element.isFile = true;
                                });
                              }
                            });
                            setSaved();
                            Navigator.of(context).pop();
                          },
                          leading: Icon(
                            Icons.settings_voice_outlined,
                            color: fromHex(yellow),
                          ),
                          title: Text(
                            'Start Recording',
                            style: TextStyle(color: fromHex(yellow)),
                          ),
                        ),
                      ),
                      Divider(
                        thickness: 1,
                        color: Colors.grey,
                        indent: 40,
                        endIndent: 40,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 40),
                        child: ListTile(
                          onTap: () async {
                            _audio = await _getFile(["mp3", "amr", "aac"]);
                            if (_audio == null) {
                              showToast("File not found try again!");
                              return;
                            }
                            questions.forEach((element) {
                              if (checklist.formItemId == element.formItemId &&
                                  checklist.type == element.type) {
                                setState(() {
                                  element.response = "${_audio!.path}";
                                  element.isFile = true;
                                });
                              }
                            });
                            setSaved();
                            Navigator.of(context).pop();
                          },
                          leading: Icon(
                            Icons.storage,
                            color: fromHex(yellow),
                          ),
                          title: Text(
                            'Pick From Storage',
                            style: TextStyle(color: fromHex(yellow)),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )));
    }
    if (checklist.responseType!.contains("Picture")) {
      //Picture
      File? _image;
      await showDialog(
          context: context,
          builder: (context) => Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              elevation: 0.0,
              backgroundColor: Colors.grey[900],
              child: Container(
                color: Colors.transparent,
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                child: Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ListTile(
                        title: Text(
                          "${checklist.question}",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                      ),
                      Container(),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 40),
                        child: ListTile(
                          onTap: () async {
                            _image = await getPicture(ImageSource.camera);
                            if (_image == null) {
                              showToast("File not found try again!");
                              return;
                            }
                            questions.forEach((element) {
                              if (checklist.formItemId == element.formItemId &&
                                  checklist.type == element.type) {
                                setState(() {
                                  element.response = "${_image?.path}";
                                  element.isFile = true;
                                });
                              }
                            });
                            setSaved();
                            Navigator.of(context).pop();
                          },
                          leading: Icon(
                            Icons.video_call_outlined,
                            color: fromHex(yellow),
                          ),
                          title: Text(
                            'Open Camera',
                            style: TextStyle(color: fromHex(yellow)),
                          ),
                        ),
                      ),
                      Divider(
                        thickness: 1,
                        color: Colors.grey,
                        indent: 40,
                        endIndent: 40,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 40),
                        child: ListTile(
                          onTap: () async {
                            _image = await getPicture(ImageSource.gallery);
                            if (_image == null) {
                              showToast("File not found try again!");
                              return;
                            }
                            questions.forEach((element) {
                              if (checklist.formItemId == element.formItemId &&
                                  checklist.type == element.type) {
                                setState(() {
                                  element.response = "${_image?.path}";
                                  element.isFile = true;
                                });
                              }
                            });
                            setSaved();
                            Navigator.of(context).pop();
                          },
                          leading: Icon(
                            Icons.storage,
                            color: fromHex(yellow),
                          ),
                          title: Text(
                            'Pick From Gallery',
                            style: TextStyle(color: fromHex(yellow)),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )));
    }
  }

  Future<File?> getVideo(ImageSource imageSource) async {
    var p = await _picker.pickVideo(source: imageSource);
    if (p != null) {
      File video = File(p.path);
      print(video.path);
      return video;
    } else {
      return null;
    }
  }

  Future<File?> getPicture(ImageSource imageSource) async {
    var p = await _picker.pickImage(source: imageSource, imageQuality: 40);
    if (p != null) {
      File picture = File(p.path);
      return picture;
    } else {
      return null;
    }
  }

  Future<File?> _getFile(List<String> s) async {
    FilePickerResult? result = await FilePicker.platform
        .pickFiles(type: FileType.custom, allowedExtensions: s);

    if (result != null) {
      return File(result.files.single.path!);
    } else {
      return null;
    }
  }

  Future<File?> getAudioRecording(BuildContext context) async {
    File? audioFile;
    int _scanTime = 0;
    _stopWatchTimer.onExecute.add(StopWatchExecute.reset);
    bool isRecording = false;
    var tempDir = await getTemporaryDirectory();
    String path = '${tempDir.path}/${DateTime.now()}.aac';
    await showModalBottomSheet(
        backgroundColor: Colors.transparent,
        clipBehavior: Clip.antiAlias,
        context: _scaffoldKey.currentContext!,
        builder: (context) {
          return StatefulBuilder(
            builder: (context, StateSetter setState) => SingleChildScrollView(
              child: Container(
                color: Colors.transparent,
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.grey[900],
                    // color: Theme.of(context).canvasColor,
                    borderRadius: BorderRadius.only(
                        topLeft: const Radius.circular(15),
                        topRight: const Radius.circular(15)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(right: 20.0, left: 20.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Container(
                          height: 100,
                          width: 100,
                          child: Stack(
                            children: [
                              Lottie.asset('assets/audio.json',
                                  animate: isRecording),
                              Align(
                                alignment: Alignment.center,
                                child: Icon(
                                  Icons.keyboard_voice_rounded,
                                  color: Colors.black,
                                  size: 36,
                                ),
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 0),
                          child: StreamBuilder<int>(
                            stream: _stopWatchTimer.rawTime,
                            initialData: _stopWatchTimer.rawTime.value,
                            builder: (context, snap) {
                              final value = snap.data!;
                              final displayTime = StopWatchTimer.getDisplayTime(
                                  value,
                                  hours: true);
                              return Column(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.all(8),
                                    child: Text(
                                      displayTime,
                                      style: TextStyle(
                                        fontSize: 26,
                                        fontFamily: 'Helvetica',
                                        color: fromHex(yellow),
                                      ),
                                    ),
                                  ),
                                  // Padding(
                                  //   padding: const EdgeInsets.all(8),
                                  //   child: Text(
                                  //     value.toString(),
                                  //     style: const TextStyle(
                                  //         fontSize: 16,
                                  //         fontFamily: 'Helvetica',
                                  //         fontWeight: FontWeight.w400),
                                  //   ),
                                  // ),
                                ],
                              );
                            },
                          ),
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: ListTile(
                                onTap: () async {
                                  print(path);
                                  // if (isRecording) {
                                  //   Recording recording =
                                  //       await AudioRecorder.stop();
                                  //   print(
                                  //       "Path : ${recording.path},  Format : ${recording.audioOutputFormat},  Duration : ${recording.duration},  Extension : ${recording.extension},");
                                  //   _stopWatchTimer.onExecute
                                  //       .add(StopWatchExecute.stop);
                                  //   setState(() {
                                  //     audioFile = File(recording.path);
                                  //     isRecording = false;
                                  //   });
                                  // } else {
                                  //   await AudioRecorder.start(
                                  //       path: "$path",
                                  //       audioOutputFormat:
                                  //           AudioOutputFormat.AAC);
                                  //   isRecording =
                                  //       await AudioRecorder.isRecording;
                                  //   _stopWatchTimer.onExecute
                                  //       .add(StopWatchExecute.start);
                                  //   setState(() {});
                                  // }
                                },
                                title: Center(
                                  child: Text(
                                    isRecording ? 'Pause' : 'Start',
                                    style: TextStyle(
                                        color: fromHex(deepOrange),
                                        fontSize: 18),
                                  ),
                                ),
                              ),
                            ),
                            isRecording
                                ? Expanded(
                                    child: ListTile(
                                    onTap: () async {
                                      if (isRecording) {
                                        // Recording recording =
                                        //     await AudioRecorder.stop();
                                        // print(
                                        //     "Path : ${recording.path},  Format : ${recording.audioOutputFormat},  Duration : ${recording.duration},  Extension : ${recording.extension},");
                                        // _stopWatchTimer.onExecute
                                        //     .add(StopWatchExecute.stop);
                                        // setState(() {
                                        //   audioFile = File(recording.path);
                                        //   isRecording = false;
                                        // });
                                      }
                                    },
                                    title: Center(
                                      child: Text(
                                        isRecording ? 'Stop' : '',
                                        style: TextStyle(
                                            color: fromHex(deepOrange),
                                            fontSize: 18),
                                      ),
                                    ),
                                  ))
                                : Container(),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        });
    return audioFile;
  }

  Future<bool?> checkIfAllHaveBeenAnswered() async {
    bool? isFilled;
    for (var i = 0; i <= questions.length - 1; i++) {
      FormItems element = questions[i];
      if (element.required! && element.response!.isNotEmpty) {
        isFilled = true;
      } else {
        isFilled = false;
        break;
      }
    }
    return isFilled;
  }

  Future<void> sync() async {
    for (var i = 0; i <= offlineResponses.values.length; i++) {
      UpdateFormData item = offlineResponses.getAt(i);
      await sendDataToDb(item, i);
    }
  }

  Future<void> sendDataToDb(UpdateFormData item, int i) async {
    try {
      var r = await _model.updateResponse(item.toJson());
      if (r['success']) {
        offlineResponses.deleteAt(i);
      }
    } catch (e) {
      showToast(e.toString());
    }
  }

  void setSaved() {
    if (existingForm != null) {
      UpdateFormData? d = existingForm!..items = questions;
      savedResponses.put(
          "${getPlanner()!.plannerId}_${widget.formData.id}_${getShiftId()}",
          d);
    } else {
      setInit();
      UpdateFormData? d = existingForm!..items = questions;
      savedResponses.put(
          "${getPlanner()!.plannerId}_${widget.formData.id}_${getShiftId()}",
          d);
    }
    setState(() {});
  }

  void setInit() {
    questions.clear();
    widget.formData.items!.forEach((element) {
      FormItems formItem = FormItems()
        ..response = ""
        ..type = "default"
        ..formItemId = element.id
        ..question = element.name
        ..responseType = element.type
        ..required = true;
      questions.add(formItem);
    });
    widget.formData.customItems!.forEach((element) {
      FormItems formItem = FormItems()
        ..response = ""
        ..type = "custom"
        ..formItemId = element.id
        ..responseType = element.type
        ..question = element.name
        ..required = true;
      questions.add(formItem);
    });
    UpdateFormData d = UpdateFormData()
      ..id = ""
      ..plannerId = getPlanner()!.plannerId
      ..answeredBy = getUserId()
      ..formId = widget.formData.id
      ..date = df.format(DateTime.now())
      ..shiftId = getShiftId().toString()
      ..items = questions;
    setState(() {
      existingForm = d;
    });
    savedResponses.put(
        "${getPlanner()!.plannerId}_${widget.formData.id}_${getShiftId()}", d);
  }

  // Future<void> _setAnswer(
  //     String ans, ReportFormsModel model, FormItems checklist,
  //     [File file]) async {
  //   String filePath = '';
  //   final f = new DateFormat('yyyy-MM-dd');
  //   DateTime now = DateTime.now();
  //   bool isConnected = await checkConnection();
  //   if (isConnected) {
  //     Map<String, String> data = {
  //       "response_id": "${checklist.responseId}",
  //       "question_id": "${checklist.questionId}",
  //       "response": "${ans ?? ''}",
  //       "date": "${f.format(now)}",
  //       "answered_by": "${getUserId()}",
  //       "default_custom": "custom",
  //       "shift_id": "${widget.shift.id}",
  //       "category_id": "${checklist.categoryId}",
  //       "planner_id": "${widget.plannerId}"
  //     };
  //     print(data);
  //     try {
  //       var r = await model.updateResponse(data); // UpdatePlannerResponse
  //       if (r['success']) {
  //         var d = UpdatePlannerResponse.fromJson(r['response']);
  //         if (!d.error) {
  //           if (file != null) {
  //             String fileName = file.path.split('/').last;
  //             String mimeType = mime(fileName);
  //             String mimee = mimeType.split('/')[0];
  //             String type = mimeType.split('/')[1];
  //             File newFile = File(file.path);
  //             String dir = path.dirname(newFile.path);
  //             String newPath = path.join(dir, "${d.data}.$type");
  //             File finalFile = newFile.renameSync(newPath);
  //             try {
  //               var res = await model.uploadImage(finalFile);
  //               if (res['success']) {
  //                 var resd =
  //                     ImageVerificationResponse.fromJson(res['response']);
  //                 if (resd.error) {
  //                   showToast(resd.message);
  //                 }
  //               } else {
  //                 showErrorDialog(r['response'], context, "upload image");
  //               }
  //             } catch (e) {
  //               showToast(e.toString());
  //             }
  //             filePath = finalFile.path;
  //           }
  //           CheckListResult c = CheckListResult(
  //             responseId: "${d.data}",
  //             questionId: "${checklist.questionId}",
  //             response: "$ans",
  //             date: "${f.format(now)}",
  //             answeredBy: "${getUserId()}",
  //             defaultCustom: '',
  //             shiftId: "${widget.shift.id}",
  //             categoryId: "${checklist.categoryId}",
  //             name: "${checklist.name}",
  //             questionType: "${checklist.questionType}",
  //             fileLocation: filePath,
  //             shiftType: "${widget.shift.id}",
  //             synced: true,
  //           );
  //
  //           checkListAnswers.put(
  //               "${widget.shift.id}/${widget.date}/${checklist.questionId}/${checklist.categoryId}",
  //               c);
  //           await model.refreshList("${widget.category.id}", widget.shift);
  //         } else {
  //           showToast("${d.erroMessage}");
  //         }
  //       } else {}
  //     } catch (e) {}
  //   } else {
  //     CheckListResult c = CheckListResult(
  //       responseId: "${checklist.responseId}",
  //       questionId: "${checklist.questionId}",
  //       response: "$ans",
  //       date: "${f.format(now)}",
  //       answeredBy: "${getUserId()}",
  //       defaultCustom: '',
  //       shiftId: "${widget.shift.id}",
  //       categoryId: "${checklist.categoryId}",
  //       name: "${checklist.name}",
  //       questionType: "${checklist.questionType}",
  //       fileLocation: filePath,
  //       shiftType: "${widget.shift.id}",
  //       synced: false,
  //     );
  //
  //     checkListAnswers.put(
  //         "${widget.shift.id}/${widget.date}/${checklist.questionId}/${checklist.categoryId}",
  //         c);
  //     await model.refreshList("${widget.category.id}", widget.shift);
  //     // checkListAnswers.put(
  //     //     "${checklist.id}_${f.format(now)}_${widget.shift.id}", c);
  //   }
  // }
}
