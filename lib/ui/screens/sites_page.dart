import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:scio_security/core/enums/view_state.dart';
import 'package:scio_security/core/viewmodels/sites_model.dart';
import 'package:scio_security/ui/screens/base_view.dart';
import 'package:scio_security/ui/screens/visitors_page.dart';
import 'package:scio_security/utils/loader.dart';
import 'package:scio_security/utils/useful.dart';

class SitePage extends StatefulWidget {
  static const tag = 'sites';
  @override
  _SitePageState createState() => _SitePageState();
}

class _SitePageState extends State<SitePage> {
  final visitorsIn = Hive.box('visitors_in');
  final visitorsOut = Hive.box('visitors_out');
  @override
  Widget build(BuildContext context) {
    return BaseView<SitesModel>(
      onModelReady: (model) async {
        var r = await model.getSites();
        if (r['success']) {
        } else {
          showErrorDialog(r['response'], context, "get sites");
        }
      },
      builder: (context, model, child) => model.state == ViewState.Idle
          ? Stack(
              children: [
                Scaffold(
                  backgroundColor: Colors.black,
                  appBar: AppBar(
                    backgroundColor: Colors.transparent,
                    elevation: 0,
                    title: Text(
                      'Select Site',
                      style: TextStyle(
                          color: fromHex(deepOrange),
                          fontWeight: FontWeight.bold),
                    ),
                    centerTitle: true,
                    automaticallyImplyLeading: false,
                  ),
                  body: Column(
                    children: [
                      Expanded(
                        child: model.sites != null
                            ? model.sites!.isNotEmpty
                                ? ListView.builder(
                                    itemCount: model.sites!.length,
                                    itemBuilder: (context, index) {
                                      return Dismissible(
                                          key: UniqueKey(),
                                          direction:
                                              DismissDirection.startToEnd,
                                          onDismissed: (DismissDirection
                                              direction) async {
                                            setState(() {});
                                            if (visitorsIn.isNotEmpty ||
                                                visitorsOut.isNotEmpty) {
                                              if (model.sites![index].id ==
                                                  model.getSiteId()) {
                                                Navigator.of(context).push(
                                                    MaterialPageRoute(
                                                        builder: (BuildContext
                                                                context) =>
                                                            VisitorsPage(model
                                                                .sites![index]
                                                                .id)));
                                              }
                                            } else {
                                              Navigator.of(context).push(
                                                  MaterialPageRoute(
                                                      builder: (BuildContext
                                                              context) =>
                                                          VisitorsPage(model
                                                              .sites![index]
                                                              .id)));
                                            }
                                          },
                                          child: InkWell(
                                            onTap: () async {
                                              if (visitorsIn.isNotEmpty ||
                                                  visitorsOut.isNotEmpty) {
                                                if (model.sites![index].id ==
                                                    model.getSiteId()) {
                                                  Navigator.of(context).push(
                                                      MaterialPageRoute(
                                                          builder: (BuildContext
                                                                  context) =>
                                                              VisitorsPage(model
                                                                  .sites![index]
                                                                  .id)));
                                                }
                                              } else {
                                                Navigator.of(context).push(
                                                    MaterialPageRoute(
                                                        builder: (BuildContext
                                                                context) =>
                                                            VisitorsPage(model
                                                                .sites![index]
                                                                .id)));
                                              }
                                            },
                                            child: Container(
                                              color: Colors.white10
                                                  .withOpacity(0.07),
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: 10, vertical: 10),
                                              margin: EdgeInsets.symmetric(
                                                  horizontal: 5, vertical: 3),
                                              child: ListTile(
                                                leading: Text(
                                                  "${index + 1}",
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                ),
                                                title: Text(
                                                  "${model.sites![index].name}",
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                ),
                                              ),
                                            ),
                                          ));
                                    })
                                : Center(
                                    child: Text(
                                      'No Sites Found!',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  )
                            : Container(),
                      )
                    ],
                  ),
                ),
                Positioned(
                  left: 15,
                  top: 27,
                  child: FloatingActionButton(
                    foregroundColor: Colors.grey,
                    backgroundColor: Colors.white10.withOpacity(0.07),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    mini: true,
                    tooltip: "go back home",
                    child: Icon(
                      Icons.keyboard_backspace_sharp,
                      color: fromHex(deepOrange),
                    ),
                  ),
                ),
              ],
            )
          : Loader(),
    );
  }
}
