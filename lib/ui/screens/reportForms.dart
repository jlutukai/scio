import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:scio_security/core/enums/view_state.dart';
import 'package:scio_security/core/viewmodels/ReportFormModel.dart';
import 'package:scio_security/ui/screens/FormQuestionsResponse.dart';
import 'package:scio_security/utils/useful.dart';

import 'base_view.dart';

class ReportForms extends StatefulWidget {
  @override
  _ReportFormsState createState() => _ReportFormsState();
}

class _ReportFormsState extends State<ReportForms> {
  ReportFormsModel? _model;
  @override
  Widget build(BuildContext context) {
    return BaseView<ReportFormsModel>(
        onModelReady: (model) async {
          _model = model;

          Map<String, String> data = {
            'planner_id': "${getPlanner()!.plannerId}"
          };
          model.getForms(data);
          print(data);
        },
        builder: (context, model, child) => Stack(
              children: [
                Scaffold(
                  backgroundColor: Colors.black,
                  appBar: AppBar(
                    backgroundColor: Colors.transparent,
                    elevation: 0,
                    title: Text(
                      'Forms',
                      style: TextStyle(
                          color: fromHex(deepOrange),
                          fontWeight: FontWeight.bold),
                    ),
                    centerTitle: true,
                    automaticallyImplyLeading: false,
                  ),
                  body: Column(
                    children: [
                      SizedBox(
                        height: 10,
                      ),
                      Expanded(
                        child: model.state == ViewState.Idle
                            ? _formsList(model, context)
                            : Center(
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Container(
                                        height: 200,
                                        width: 200,
                                        child: Lottie.asset(
                                            'assets/loading.json')),
                                    Text(
                                      'Please wait \n Loading...',
                                      style:
                                          TextStyle(color: fromHex(deepOrange)),
                                    )
                                  ],
                                ),
                              ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  left: 15,
                  top: 27,
                  child: FloatingActionButton(
                    foregroundColor: Colors.grey,
                    backgroundColor: Colors.white10.withOpacity(0.07),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    mini: true,
                    tooltip: "go back home",
                    child: Icon(
                      Icons.keyboard_backspace_sharp,
                      color: fromHex(deepOrange),
                    ),
                  ),
                ),
              ],
            ));
  }

  _formsList(ReportFormsModel model, BuildContext context) {
    return model.forms != null
        ? model.forms!.isNotEmpty
            ? ListView.builder(
                physics: BouncingScrollPhysics(),
                itemCount: model.forms!.length,
                itemBuilder: (context, index) => InkWell(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (BuildContext context) =>
                            FormQuestions(model.forms![index])));
                  },
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                    decoration: BoxDecoration(
                      color: Colors.white10.withOpacity(0.07),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Container(
                      child: Row(
                        children: [
                          Expanded(
                              child: Column(
                            children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(left: 10),
                                    child: Text(
                                      "${model.forms![index].name}",
                                      style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.only(left: 20),
                                    child: Text(
                                      "${model.forms![index].description ?? ''}",
                                      style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w100,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ))
                        ],
                      ),
                    ),
                  ),
                ),
              )
            : Center(
                child: Text('No Forms Added Yet'),
              )
        : Container();
  }
}
