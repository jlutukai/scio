import 'dart:io';

import 'package:cupertino_tabbar/cupertino_tabbar.dart' as CupertinoTabBar;
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:lottie/lottie.dart';
import 'package:scio_security/core/enums/view_state.dart';
import 'package:scio_security/core/models/check_list_answer_response.dart';
import 'package:scio_security/core/models/image_verification_response.dart';
import 'package:scio_security/core/models/local/localModels.dart';
import 'package:scio_security/core/models/report_categories_response.dart';
import 'package:scio_security/core/viewmodels/daily_reports_model.dart';
import 'package:scio_security/ui/screens/reports_questions_page.dart';
import 'package:scio_security/utils/useful.dart';

import 'base_view.dart';

class DailyReportsPage extends StatefulWidget {
  /// Removed
  final PlannerData p;
  final String minDate;
  final String maxDate;
  final Shifts previousShift;
  final Shifts currentShift;
  final Shifts nextShift;
  DailyReportsPage(this.p, this.minDate, this.maxDate, this.previousShift,
      this.currentShift, this.nextShift);

  @override
  _DailyReportsPageState createState() => _DailyReportsPageState();
}

class _DailyReportsPageState extends State<DailyReportsPage> {
  late DailyReportsModel _model;
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  Box checkListAnswers = Hive.box("check_list_answer");
  int cupertinoTabBarIValue = 1;
  int cupertinoTabBarIValueGetter() => cupertinoTabBarIValue;
  PageController? _pageControlle;

  @override
  void initState() {
    _pageControlle = PageController(initialPage: 1);
    super.initState();
  }

  @override
  void dispose() {
    _pageControlle!.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<DailyReportsModel>(
        onModelReady: (model) async {
          _model = model;

          Map<String, String> data = {
            'min_date': "${widget.minDate}",
            'max_date': "${widget.maxDate}",
            'planner_id': "${widget.p.plannerId}",
          };
          print(data);

          bool isConnected = await checkConnection();
          if (isConnected) {
 await _syncData();
            var r1 = await model.getReports(data);
            print("#################### $r1 #############################");
            if (r1['success']) {
            } else {
              showErrorDialog(r1['response'], context, "get reports");
            }
            var r2 = await model.getCategories(
                widget.currentShift, widget.previousShift);
            print("#################### $r2 #############################");
            if (r2['success']) {
            } else {
              showErrorDialog(r2['response'], context, "get categories");
            }
          } else {
            var r1 = await model.getReports(data);
            var r2 = await model.getCategories(
                widget.currentShift, widget.previousShift);
          }
        },
        builder: (context, model, child) => Stack(
              children: [
                Scaffold(
                  backgroundColor: Colors.black,
                  appBar: AppBar(
                    backgroundColor: Colors.transparent,
                    elevation: 0,
                    title: Text(
                      'Daily Reports',
                      style: TextStyle(
                          color: fromHex(deepOrange),
                          fontWeight: FontWeight.bold),
                    ),
                    centerTitle: true,
                    automaticallyImplyLeading: false,
                  ),
                  body: Column(
                    children: [
                      CupertinoTabBar.CupertinoTabBar(
                        Colors.black,
                        fromHex(yellow),
                        [
                          Text(
                            "Previous",
                            style: TextStyle(
                              color: cupertinoTabBarIValue == 0
                                  ? Colors.black
                                  : Colors.white,
                              fontSize: 18.75,
                              fontWeight: FontWeight.w400,
                              fontFamily: "SFProRounded",
                            ),
                            textAlign: TextAlign.center,
                          ),
                          Text(
                            "Current Shift",
                            style: TextStyle(
                              color: cupertinoTabBarIValue == 1
                                  ? Colors.black
                                  : Colors.white,
                              fontSize: 18.75,
                              fontWeight: FontWeight.w400,
                              fontFamily: "SFProRounded",
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ],
                        cupertinoTabBarIValueGetter,
                        (int index) {
                          setState(() {
                            cupertinoTabBarIValue = index;
                            _pageControlle!.animateToPage(index,
                                duration: Duration(milliseconds: 1),
                                curve: Curves.ease);
                          });
                        },
                        useSeparators: false,
                        // allowScrollable: true,
                      ),
                      Container(
                        constraints: const BoxConstraints.expand(height: 20.0),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Expanded(
                        child: model.state == ViewState.Idle
                            ? PageView(
                                physics: NeverScrollableScrollPhysics(),
                                controller: _pageControlle,
                                onPageChanged: (index) {},
                                children: <Widget>[
                                  _previous(model),
                                  _current(model),
                                ],
                              )
                            : Center(
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Container(
                                        height: 200,
                                        width: 200,
                                        child: Lottie.asset(
                                            'assets/loading.json')),
                                    Text(
                                      'Please wait \n Loading...',
                                      style:
                                          TextStyle(color: fromHex(deepOrange)),
                                    )
                                  ],
                                ),
                              ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  left: 15,
                  top: 27,
                  child: FloatingActionButton(
                    foregroundColor: Colors.grey,
                    backgroundColor: Colors.white10.withOpacity(0.07),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    mini: true,
                    tooltip: "go back home",
                    child: Icon(
                      Icons.keyboard_backspace_sharp,
                      color: fromHex(deepOrange),
                    ),
                  ),
                ),
              ],
            ));
  }

  _categoryListItem(
      CategoryData category, BuildContext context, Shifts shift, String date) {
    return Dismissible(
      key: UniqueKey(),
      direction: DismissDirection.startToEnd,
      onDismissed: (DismissDirection direction) {
        setState(() {});
        Navigator.of(context).push(MaterialPageRoute(
            builder: (BuildContext context) => ReportQuestions(
                  widget.p.plannerId,
                  category,
                  shift,
                  date,
                )));
      },
      child: InkWell(
        onTap: () {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (BuildContext context) => ReportQuestions(
                    widget.p.plannerId,
                    category,
                    shift,
                    date,
                  )));
        },
        onLongPress: () {},
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5),
          child: Card(
            color: Colors.white10.withOpacity(0.07),
            child: Container(
              // color: fromHex("#c19d3d"),
              height: 60,
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Icon(
                        Icons.article,
                        color: fromHex(yellow),
                      ),
                      Text(
                        "  ${category.name}",
                        style: TextStyle(
                            color: fromHex(yellow),
                            fontWeight: FontWeight.bold,
                            fontSize: 14),
                      )
                    ],
                  ),
                  Icon(
                    Icons.arrow_forward_ios_sharp,
                    color: fromHex(yellow),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  _previous(DailyReportsModel model) {
    return ListView.builder(
      itemCount: model.categoriesPreviousShift.length,
      physics: BouncingScrollPhysics(),
      itemBuilder: (context, index) => _categoryListItem(
        model.categoriesPreviousShift[index],
        context,
        widget.previousShift,
        widget.minDate.substring(8),
      ),
    );
  }

  _current(DailyReportsModel model) {
    return ListView.builder(
      itemCount: model.categoriesCurrentShift.length,
      physics: BouncingScrollPhysics(),
      itemBuilder: (context, index) => _categoryListItem(
        model.categoriesCurrentShift[index],
        context,
        widget.currentShift,
        widget.maxDate.substring(8),
      ),
    );
  }

  Future<void> _syncData() async {
    if (checkListAnswers.isNotEmpty) {
      for (var i = 0; i <= checkListAnswers.values.length - 1; i++) {
        var c = checkListAnswers.getAt(i);
        await sendDataToDb(c, i);
      }
    }
  }

  sendDataToDb(CheckListResult c, int i) async {
    Map<String, String> data = {
      "response_id": "${c.responseId}",
      "question_id": "${c.questionId}",
      "response": "${c.response ?? ''}",
      "date": "${c.date}",
      "answered_by": "${c.answeredBy}",
      "default_custom": "custom",
      "shift_id": "${c.shiftId}",
      "category_id": "${c.categoryId}",
      "planner_id": "${c.plannerId}"
    };
    try {
      var r = await _model.updateResponse(data); // UpdatePlannerResponse
      if (r['success']) {
        var d = UpdatePlannerResponse.fromJson(r['response']);
        if (!d.error!) {
          if (c.fileLocation!.isNotEmpty) {
            File finalFile = File(c.fileLocation!);
            try {
              var res = await _model.uploadImage(finalFile);
              if (res['success']) {
                var resd = ImageVerificationResponse.fromJson(res['response']);
                if (resd.error!) {
                  showToast(resd.message!);
                }
              } else {
                showErrorDialog(res['response'], context, "upload image");
              }
            } catch (e) {
              showToast(e.toString());
            }
          }
          checkListAnswers.deleteAt(i);
        } else {
          showToast("${d.erroMessage}");
        }
      } else {}
    } catch (e) {}
  }
}
