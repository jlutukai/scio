import 'dart:async';
import 'dart:io';

// import 'package:audio_recorder/audio_recorder.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:mime_type/mime_type.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:scio_security/core/enums/view_state.dart';
import 'package:scio_security/core/models/check_list_answer_response.dart';
import 'package:scio_security/core/models/image_verification_response.dart';
import 'package:scio_security/core/models/local/localModels.dart';
import 'package:scio_security/core/models/report_categories_response.dart';
import 'package:scio_security/core/viewmodels/report_questions_model.dart';
import 'package:scio_security/utils/loader.dart';
import 'package:scio_security/utils/useful.dart';
import 'package:stop_watch_timer/stop_watch_timer.dart';

import 'base_view.dart';

class ReportQuestions extends StatefulWidget {
  final String? plannerId;
  final CategoryData category;
  final Shifts shift;
  final String date;
  ReportQuestions(
    this.plannerId,
    this.category,
    this.shift,
    this.date,
  );

  @override
  _ReportQuestionsState createState() => _ReportQuestionsState();
}

class _ReportQuestionsState extends State<ReportQuestions> {
  ReportsQuestionsModel? _model;
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  Box checkListAnswers = Hive.box("check_list_answer");
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final _picker = ImagePicker();
  final StopWatchTimer _stopWatchTimer = StopWatchTimer();

  @override
  void initState() {
    super.initState();
    _stopWatchTimer.rawTime.listen((value) {});
    print(
        "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ ${widget.shift.id}   ${widget.date}");
  }

  @override
  void dispose() async {
    super.dispose();
    await _stopWatchTimer.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<ReportsQuestionsModel>(
      onModelReady: (model) async {
        _model = model;
        Map<String, String> data = {
          'planner_id': "${getPlanner()!.plannerId}",
        };
        var r1 = await model.getChecklist(
            data, widget.category.id, widget.shift, widget.date);
        if (r1['success']) {
        } else {
          showErrorDialog(r1['response'], context, "get checklist");
        }
        var r2 = await model.getDefaultChecklist(
            widget.category.id, widget.shift, widget.date);
        if (r2['success']) {
        } else {
          showErrorDialog(r2['response'], context, "get default checklist");
        }
        bool isConnected = await checkConnection();
        if (isConnected) {
        } else {
          showToast('Please Check Internet Connection');
        }
      },
      builder: (context, model, child) => model.state == ViewState.Idle
          ? Stack(
              children: [
                Scaffold(
                  key: _scaffoldKey,
                  backgroundColor: Colors.black,
                  appBar: AppBar(
                    backgroundColor: Colors.transparent,
                    elevation: 0,
                    title: Text(
                      "${widget.category.name}",
                      style: TextStyle(
                          color: fromHex(deepOrange),
                          fontWeight: FontWeight.bold),
                    ),
                    centerTitle: true,
                    automaticallyImplyLeading: false,
                  ),
                  body: SingleChildScrollView(
                    physics: BouncingScrollPhysics(),
                    child: Column(
                      children: [
                        model.checklist != null
                            ? model.checklist.isNotEmpty
                                ? ListView.builder(
                                    shrinkWrap: true,
                                    physics: BouncingScrollPhysics(),
                                    itemCount: model.checklist.length,
                                    itemBuilder: (context, index) {
                                      String answered =
                                          model.checklist.getAt(index).response;
                                      return Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 10),
                                        child: Dismissible(
                                            key: UniqueKey(),
                                            direction:
                                                DismissDirection.startToEnd,
                                            onDismissed: (DismissDirection
                                                direction) async {
                                              setState(() {});
                                              _inputDialog(
                                                  context,
                                                  model.checklist.getAt(index),
                                                  model);
                                            },
                                            child: InkWell(
                                              onTap: () async {
                                                _inputDialog(
                                                    context,
                                                    model.checklist
                                                        .getAt(index),
                                                    model);
                                              },
                                              child: Container(
                                                color: Colors.white10
                                                    .withOpacity(0.07),
                                                margin: EdgeInsets.symmetric(
                                                    horizontal: 5, vertical: 3),
                                                child: Row(
                                                  children: [
                                                    Container(
                                                      color: answered.isNotEmpty
                                                          ? Colors.green
                                                          : Colors.yellow,
                                                      width: 2,
                                                      height: 75,
                                                    ),
                                                    Expanded(
                                                      child: Container(
                                                        padding: EdgeInsets
                                                            .symmetric(
                                                                horizontal: 10,
                                                                vertical: 10),
                                                        child: ExpansionTile(
                                                          leading: Text(
                                                            "${index + 1}",
                                                            style: TextStyle(
                                                                color: fromHex(
                                                                    yellow)),
                                                          ),
                                                          title: Text(
                                                            "${model.checklist.getAt(index).name}",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white),
                                                          ),
                                                          children: [
                                                            Text(
                                                              "${model.checklist.getAt(index).response ?? ''}",
                                                              style: TextStyle(
                                                                  color: fromHex(
                                                                      yellow)),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            )),
                                      );
                                    })
                                : Container(
                                    height: MediaQuery.of(context).size.height,
                                    child: Center(
                                      child: Text(
                                        'No records Found!',
                                        style: TextStyle(color: Colors.white),
                                      ),
                                    ))
                            : Container()
                      ],
                    ),
                  ),
                ),
                Positioned(
                  left: 15,
                  top: 27,
                  child: FloatingActionButton(
                    foregroundColor: Colors.grey,
                    backgroundColor: Colors.white10.withOpacity(0.07),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    mini: true,
                    tooltip: "go back",
                    child: Icon(
                      Icons.keyboard_backspace_sharp,
                      color: fromHex(deepOrange),
                    ),
                  ),
                ),
              ],
            )
          : Loader(),
    );
  }

  void _inputDialog(BuildContext context, CheckListResult checklist,
      ReportsQuestionsModel model) async {
    print("${checklist.questionType}");
    if (checklist.questionType == null) {
      showToast("Question type is undefined");
      return;
    }
    if (checklist.questionType!.isEmpty) {
      showToast("Question type is empty");
      return;
    }
    if (checklist.questionType!.contains("Yes/No")) {
      String _yes = 'yes';
      String _no = 'no';
      String? _ans = '';
      await showDialog(
        context: _scaffoldKey.currentContext!,
        builder: (context) => Dialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          elevation: 0.0,
          backgroundColor: Colors.grey[900],
          child: StatefulBuilder(
            builder: (context, StateSetter setState) => Container(
              color: Colors.transparent,
              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              child: Container(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ListTile(
                      title: Text(
                        "${checklist.name}",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, color: Colors.white),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Row(
                            children: [
                              Radio(
                                  value: _no,
                                  groupValue: _ans,
                                  activeColor: fromHex(yellow),
                                  onChanged: (dynamic v) {
                                    setState(() {
                                      _ans = v;
                                    });
                                  }),
                              Text('No',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white)),
                            ],
                          ),
                          Row(
                            children: [
                              Radio(
                                  value: _yes,
                                  activeColor: fromHex(yellow),
                                  // toggleable: true,
                                  groupValue: _ans,
                                  onChanged: (dynamic v) {
                                    setState(() {
                                      _ans = v;
                                    });
                                  }),
                              Text('Yes',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white)),
                            ],
                          ),
                        ],
                      ),
                    ),
                    ListTile(
                      onTap: () async {
                        if (_ans!.isEmpty) {
                          showToast('Please select a response');
                          return;
                        }
                        _setAnswer(_ans, model, checklist);
                        Navigator.of(context).pop();
                      },
                      title: Center(
                        child: Text(
                          'Submit',
                          style: TextStyle(color: Colors.deepOrange),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      );
    }
    if (checklist.questionType!.contains("Open Text")) {
      //Open Text
      TextEditingController _comment = TextEditingController();
      await showDialog(
          context: context,
          builder: (context) => Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              elevation: 0.0,
              backgroundColor: Colors.grey[900],
              child: Container(
                color: Colors.transparent,
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                child: Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ListTile(
                        title: Text(
                          "${checklist.name}",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                      ),
                      Container(
                        child: TextFormField(
                          controller: _comment,
                          keyboardType: TextInputType.text,
                          textCapitalization: TextCapitalization.sentences,
                          maxLines: 6,
                          textAlignVertical: TextAlignVertical.top,
                          style: TextStyle(color: Colors.white),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return "Please Enter Your Response";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white10,
                            isDense: true,
                            hintText: "Enter your response here..",
                            labelText: "Response",
                            alignLabelWithHint: true,
                            labelStyle: TextStyle(color: fromHex(yellow)),
                            hintStyle: TextStyle(
                              color: Colors.blueGrey[400],
                            ),
                            border: InputBorder.none,
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: fromHex(yellow)),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      ListTile(
                        onTap: () async {
                          if (_comment.text.isEmpty) {
                            showToast("Please fill response");
                            return;
                          }
                          _setAnswer(_comment.text, model, checklist);
                          Navigator.of(context).pop();
                        },
                        title: Center(
                          child: Text(
                            'Submit',
                            style: TextStyle(color: Colors.deepOrange),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )));
    }
    if (checklist.questionType!.contains("Number")) {
      // Number
      TextEditingController _number = TextEditingController();
      await showDialog(
          context: context,
          builder: (context) => Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              elevation: 0.0,
              backgroundColor: Colors.grey[900],
              child: Container(
                color: Colors.transparent,
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                child: Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ListTile(
                        title: Text(
                          "${checklist.name}",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                      ),
                      Container(
                        child: TextFormField(
                          controller: _number,
                          keyboardType: TextInputType.number,
                          textAlignVertical: TextAlignVertical.top,
                          style: TextStyle(color: Colors.white),
                          validator: (value) {
                            if (value!.isEmpty) {
                              return "Please Enter Your Response";
                            }
                            return null;
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white10,
                            isDense: true,
                            hintText: "e.g. 1",
                            labelText: "Response",
                            labelStyle: TextStyle(color: fromHex(yellow)),
                            hintStyle: TextStyle(
                              color: Colors.blueGrey[400],
                            ),
                            border: InputBorder.none,
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: fromHex(yellow)),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      ListTile(
                        onTap: () async {
                          if (_number.text.isEmpty) {
                            showToast("Please fill response");
                            return;
                          }
                          _setAnswer(_number.text, model, checklist);
                          Navigator.of(context).pop();
                        },
                        title: Center(
                          child: Text(
                            'Submit',
                            style: TextStyle(color: Colors.deepOrange),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )));
    }
    if (checklist.questionType!.contains("Video")) {
      // Video
      File? _video;
      await showDialog(
          context: context,
          builder: (context) => Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              elevation: 0.0,
              backgroundColor: Colors.grey[900],
              child: Container(
                color: Colors.transparent,
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                child: Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ListTile(
                        title: Text(
                          "${checklist.name}",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                      ),
                      Container(),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 40),
                        child: ListTile(
                          onTap: () async {
                            _video = await getVideo(ImageSource.camera);
                            if (_video == null) {
                              showToast("File not found try again!");
                              return;
                            }
                            _setAnswer("video", model, checklist, _video);
                            Navigator.of(context).pop();
                          },
                          leading: Icon(
                            Icons.video_call_outlined,
                            color: fromHex(yellow),
                          ),
                          title: Text(
                            'Start Recording',
                            style: TextStyle(color: fromHex(yellow)),
                          ),
                        ),
                      ),
                      Divider(
                        thickness: 1,
                        color: Colors.grey,
                        indent: 40,
                        endIndent: 40,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 40),
                        child: ListTile(
                          onTap: () async {
                            _video = await _getFile(['mp4']);
                            if (_video == null) {
                              showToast("File not found try again!");
                              return;
                            }
                            _setAnswer("video", model, checklist, _video);
                            Navigator.of(context).pop();
                          },
                          leading: Icon(
                            Icons.storage,
                            color: fromHex(yellow),
                          ),
                          title: Text(
                            'Pick From Storage',
                            style: TextStyle(color: fromHex(yellow)),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )));
    }
    if (checklist.questionType!.contains("Audio")) {
      //Audio
      File? _audio;
      await showDialog(
          context: context,
          builder: (context) => Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              elevation: 0.0,
              backgroundColor: Colors.grey[900],
              child: Container(
                color: Colors.transparent,
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                child: Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ListTile(
                        title: Text(
                          "${checklist.name}",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                      ),
                      Container(),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 40),
                        child: ListTile(
                          onTap: () async {
                            bool hasPermissions = false;
                            // await AudioRecorder.hasPermissions;
                            if (!hasPermissions) {
                              Permission.microphone.request();
                              showToast("Allow Audio permissions to proceed!");
                              return;
                            }
                            _audio = await getAudioRecording(context);
                            if (_audio == null) {
                              showToast("File not found try again!");
                              return;
                            }
                            _setAnswer("audio", model, checklist, _audio);
                            Navigator.of(context).pop();
                          },
                          leading: Icon(
                            Icons.settings_voice_outlined,
                            color: fromHex(yellow),
                          ),
                          title: Text(
                            'Start Recording',
                            style: TextStyle(color: fromHex(yellow)),
                          ),
                        ),
                      ),
                      Divider(
                        thickness: 1,
                        color: Colors.grey,
                        indent: 40,
                        endIndent: 40,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 40),
                        child: ListTile(
                          onTap: () async {
                            _audio = await _getFile(["mp3", "amr", "aac"]);
                            if (_audio == null) {
                              showToast("File not found try again!");
                              return;
                            }
                            _setAnswer("audio", model, checklist, _audio);
                            Navigator.of(context).pop();
                          },
                          leading: Icon(
                            Icons.storage,
                            color: fromHex(yellow),
                          ),
                          title: Text(
                            'Pick From Storage',
                            style: TextStyle(color: fromHex(yellow)),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )));
    }
    if (checklist.questionType!.contains("Picture")) {
      //Picture
      File? _image;
      await showDialog(
          context: context,
          builder: (context) => Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              elevation: 0.0,
              backgroundColor: Colors.grey[900],
              child: Container(
                color: Colors.transparent,
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                child: Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      ListTile(
                        title: Text(
                          "${checklist.name}",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, color: Colors.white),
                        ),
                      ),
                      Container(),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 40),
                        child: ListTile(
                          onTap: () async {
                            _image = await getPicture(ImageSource.camera);
                            if (_image == null) {
                              showToast("File not found try again!");
                              return;
                            }
                            _setAnswer("image", model, checklist, _image);
                            Navigator.of(context).pop();
                          },
                          leading: Icon(
                            Icons.video_call_outlined,
                            color: fromHex(yellow),
                          ),
                          title: Text(
                            'Open Camera',
                            style: TextStyle(color: fromHex(yellow)),
                          ),
                        ),
                      ),
                      Divider(
                        thickness: 1,
                        color: Colors.grey,
                        indent: 40,
                        endIndent: 40,
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 40),
                        child: ListTile(
                          onTap: () async {
                            _image = await getPicture(ImageSource.gallery);
                            if (_image == null) {
                              showToast("File not found try again!");
                              return;
                            }
                            _setAnswer("image", model, checklist, _image);
                            Navigator.of(context).pop();
                          },
                          leading: Icon(
                            Icons.storage,
                            color: fromHex(yellow),
                          ),
                          title: Text(
                            'Pick From Gallery',
                            style: TextStyle(color: fromHex(yellow)),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )));
    }
  }

  Future<File?> getVideo(ImageSource imageSource) async {
    var p = await _picker.pickVideo(source: imageSource);
    if (p != null) {
      File video = File(p.path);
      // String dir = path.dirname(video.path);
      // String newPath = path.join(dir, "in_$employeeId.mp4");
      // File photo = video.renameSync(newPath);
      print(video.path);
      return video;
    } else {
      return null;
    }
  }

  Future<File?> getPicture(ImageSource imageSource) async {
    var p = await _picker.pickImage(source: imageSource, imageQuality: 40);
    if (p != null) {
      File picture = File(p.path);
      // String dir = path.dirname(video.path);
      // String newPath = path.join(dir, "in_$employeeId.mp4");
      // File photo = video.renameSync(newPath);
      return picture;
    } else {
      return null;
    }
  }

  Future<File?> _getFile(List<String> s) async {
    FilePickerResult? result = await FilePicker.platform
        .pickFiles(type: FileType.custom, allowedExtensions: s);

    if (result != null) {
      return File(result.files.single.path!);
    } else {
      return null;
    }
  }

  Future<File?> getAudioRecording(BuildContext context) async {
    File? audioFile;
    int _scanTime = 0;
    _stopWatchTimer.onExecute.add(StopWatchExecute.reset);
    bool isRecording = false;
    var tempDir = await getTemporaryDirectory();
    String path = '${tempDir.path}/${DateTime.now()}.aac';
    await showModalBottomSheet(
        backgroundColor: Colors.transparent,
        clipBehavior: Clip.antiAlias,
        context: _scaffoldKey.currentContext!,
        builder: (context) {
          return StatefulBuilder(
            builder: (context, StateSetter setState) => SingleChildScrollView(
              child: Container(
                color: Colors.transparent,
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.grey[900],
                    // color: Theme.of(context).canvasColor,
                    borderRadius: BorderRadius.only(
                        topLeft: const Radius.circular(15),
                        topRight: const Radius.circular(15)),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(right: 20.0, left: 20.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Container(
                          height: 100,
                          width: 100,
                          child: Stack(
                            children: [
                              Lottie.asset('assets/audio.json',
                                  animate: isRecording),
                              Align(
                                alignment: Alignment.center,
                                child: Icon(
                                  Icons.keyboard_voice_rounded,
                                  color: Colors.black,
                                  size: 36,
                                ),
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 0),
                          child: StreamBuilder<int>(
                            stream: _stopWatchTimer.rawTime,
                            initialData: _stopWatchTimer.rawTime.value,
                            builder: (context, snap) {
                              final value = snap.data!;
                              final displayTime = StopWatchTimer.getDisplayTime(
                                  value,
                                  hours: true);
                              return Column(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.all(8),
                                    child: Text(
                                      displayTime,
                                      style: TextStyle(
                                        fontSize: 26,
                                        fontFamily: 'Helvetica',
                                        color: fromHex(yellow),
                                      ),
                                    ),
                                  ),
                                  // Padding(
                                  //   padding: const EdgeInsets.all(8),
                                  //   child: Text(
                                  //     value.toString(),
                                  //     style: const TextStyle(
                                  //         fontSize: 16,
                                  //         fontFamily: 'Helvetica',
                                  //         fontWeight: FontWeight.w400),
                                  //   ),
                                  // ),
                                ],
                              );
                            },
                          ),
                        ),
                        Row(
                          children: [
                            Expanded(
                              child: ListTile(
                                onTap: () async {
                                  print(path);
                                  // if (isRecording) {
                                  //   Recording recording =
                                  //       await AudioRecorder.stop();
                                  //   print(
                                  //       "Path : ${recording.path},  Format : ${recording.audioOutputFormat},  Duration : ${recording.duration},  Extension : ${recording.extension},");
                                  //   _stopWatchTimer.onExecute
                                  //       .add(StopWatchExecute.stop);
                                  //   setState(() {
                                  //     audioFile = File(recording.path);
                                  //     isRecording = false;
                                  //   });
                                  // } else {
                                  //   await AudioRecorder.start(
                                  //       path: "$path",
                                  //       audioOutputFormat:
                                  //           AudioOutputFormat.AAC);
                                  //   isRecording =
                                  //       await AudioRecorder.isRecording;
                                  //   _stopWatchTimer.onExecute
                                  //       .add(StopWatchExecute.start);
                                  //   setState(() {});
                                  // }
                                },
                                title: Center(
                                  child: Text(
                                    isRecording ? 'Pause' : 'Start',
                                    style: TextStyle(
                                        color: fromHex(deepOrange),
                                        fontSize: 18),
                                  ),
                                ),
                              ),
                            ),
                            isRecording
                                ? Expanded(
                                    child: ListTile(
                                    onTap: () async {
                                      if (isRecording) {
                                        // Recording recording =
                                        //     await AudioRecorder.stop();
                                        // print(
                                        //     "Path : ${recording.path},  Format : ${recording.audioOutputFormat},  Duration : ${recording.duration},  Extension : ${recording.extension},");
                                        // _stopWatchTimer.onExecute
                                        //     .add(StopWatchExecute.stop);
                                        // setState(() {
                                        //   audioFile = File(recording.path);
                                        //   isRecording = false;
                                        // });
                                      }
                                    },
                                    title: Center(
                                      child: Text(
                                        isRecording ? 'Stop' : '',
                                        style: TextStyle(
                                            color: fromHex(deepOrange),
                                            fontSize: 18),
                                      ),
                                    ),
                                  ))
                                : Container(),
                          ],
                        ),
                        SizedBox(
                          height: 20,
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
        });
    return audioFile;
  }

  Future<void> _setAnswer(
      String? ans, ReportsQuestionsModel model, CheckListResult checklist,
      [File? file]) async {
    String filePath = '';
    final f = new DateFormat('yyyy-MM-dd');
    DateTime now = DateTime.now();
    bool isConnected = await checkConnection();
    if (isConnected) {
      Map<String, String> data = {
        "response_id": "${checklist.responseId}",
        "question_id": "${checklist.questionId}",
        "response": "${ans ?? ''}",
        "date": "${f.format(now)}",
        "answered_by": "${getUserId()}",
        "default_custom": "custom",
        "shift_id": "${widget.shift.id}",
        "category_id": "${checklist.categoryId}",
        "planner_id": "${widget.plannerId}"
      };
      print(data);
      try {
        var r = await model.updateResponse(data); // UpdatePlannerResponse
        if (r['success']) {
          var d = UpdatePlannerResponse.fromJson(r['response']);
          if (!d.error!) {
            if (file != null) {
              String fileName = file.path.split('/').last;
              String mimeType = mime(fileName)!;
              String mimee = mimeType.split('/')[0];
              String type = mimeType.split('/')[1];
              File newFile = File(file.path);
              String dir = path.dirname(newFile.path);
              String newPath = path.join(dir, "${d.data}.$type");
              File finalFile = newFile.renameSync(newPath);
              try {
                var res = await model.uploadImage(finalFile);
                if (res['success']) {
                  var resd =
                      ImageVerificationResponse.fromJson(res['response']);
                  if (resd.error!) {
                    showToast(resd.message!);
                  }
                } else {
                  showErrorDialog(r['response'], context, "upload image");
                }
              } catch (e) {
                showToast(e.toString());
              }
              filePath = finalFile.path;
            }
            CheckListResult c = CheckListResult(
              responseId: "${d.data}",
              questionId: "${checklist.questionId}",
              response: "$ans",
              date: "${f.format(now)}",
              answeredBy: "${getUserId()}",
              defaultCustom: '',
              shiftId: "${widget.shift.id}",
              categoryId: "${checklist.categoryId}",
              name: "${checklist.name}",
              questionType: "${checklist.questionType}",
              fileLocation: filePath,
              shiftType: "${widget.shift.id}",
              synced: true,
            );

            checkListAnswers.put(
                "${widget.shift.id}/${widget.date}/${checklist.questionId}/${checklist.categoryId}",
                c);
            await model.refreshList("${widget.category.id}", widget.shift);
          } else {
            showToast("${d.erroMessage}");
          }
        } else {}
      } catch (e) {}
    } else {
      CheckListResult c = CheckListResult(
        responseId: "${checklist.responseId}",
        questionId: "${checklist.questionId}",
        response: "$ans",
        date: "${f.format(now)}",
        answeredBy: "${getUserId()}",
        defaultCustom: '',
        shiftId: "${widget.shift.id}",
        categoryId: "${checklist.categoryId}",
        name: "${checklist.name}",
        questionType: "${checklist.questionType}",
        fileLocation: filePath,
        shiftType: "${widget.shift.id}",
        synced: false,
      );

      checkListAnswers.put(
          "${widget.shift.id}/${widget.date}/${checklist.questionId}/${checklist.categoryId}",
          c);
      await model.refreshList("${widget.category.id}", widget.shift);
      // checkListAnswers.put(
      //     "${checklist.id}_${f.format(now)}_${widget.shift.id}", c);
    }
  }
}
