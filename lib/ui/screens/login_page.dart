import 'package:cached_network_image/cached_network_image.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:scio_security/core/enums/view_state.dart';
import 'package:scio_security/core/models/general_response.dart';
import 'package:scio_security/core/models/login_response.dart';
import 'package:scio_security/core/services/api.dart';
import 'package:scio_security/core/services/push_notifications_service.dart';
import 'package:scio_security/core/viewmodels/login_model.dart';
import 'package:scio_security/ui/screens/base_view.dart';
import 'package:scio_security/ui/screens/home_page.dart';
import 'package:scio_security/utils/useful.dart';

import '../../locator.dart';

class LoginPage extends StatefulWidget {
  static const tag = 'login';
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool _isHidden = true;
  String pass = "";
  TextEditingController _userName = TextEditingController();
  TextEditingController _password = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  PushNotificationService fcm = locator<PushNotificationService>();

  @override
  void initState() {
    fcm.getFCMToken();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<LoginModel>(
      builder: (context, model, child) => Scaffold(
        resizeToAvoidBottomInset: true,
        backgroundColor: Colors.black,
        body: Stack(
          children: [
            SingleChildScrollView(
              child: Container(
                height: MediaQuery.of(context).size.height,
                child: Column(
                  children: [
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          CachedNetworkImage(
                            imageUrl:
                                "${Api().getUrl()}/CUSTOM/Logo/logo_app.png",
                          )
                        ],
                      ),
                    ),
                    Expanded(
                        child: Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 25),
                            child: TextFormField(
                              controller: _userName,
                              keyboardType: TextInputType.text,
                              style: TextStyle(color: Colors.white),
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return "Please Enter user name";
                                }
                                return null;
                              },
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: Colors.white10,
                                isDense: true,
                                hintText: "John Doe",
                                labelText: "Username",
                                labelStyle: TextStyle(color: fromHex(yellow)),
                                hintStyle: TextStyle(
                                  color: Colors.blueGrey[400],
                                ),
                                border: InputBorder.none,
                                focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                      BorderSide(color: fromHex(yellow)),
                                ),
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: 25),
                            padding: EdgeInsets.only(top: 15),
                            child: TextFormField(
                              controller: _password,
                              keyboardType: TextInputType.visiblePassword,
                              obscureText: _isHidden,
                              style: TextStyle(color: Colors.white),
                              onChanged: (val) {
                                pass = val;
                                setState(() {});
                              },
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return "Please your password";
                                }
                                return null;
                              },
                              decoration: InputDecoration(
                                filled: true,
                                fillColor: Colors.white10,
                                isDense: true,
                                labelText: "Password",
                                labelStyle: TextStyle(color: fromHex(yellow)),
                                hintText: "Your password",
                                hintStyle: TextStyle(
                                  color: Colors.blueGrey[400],
                                ),
                                border: InputBorder.none,
                                focusedBorder: UnderlineInputBorder(
                                  borderSide:
                                      BorderSide(color: fromHex(yellow)),
                                ),
                                suffixIcon: pass.isNotEmpty
                                    ? IconButton(
                                        onPressed: _toggleVisibility,
                                        icon: _isHidden
                                            ? Icon(
                                                Icons.visibility_off,
                                                color: Colors.grey[400],
                                              )
                                            : Icon(
                                                Icons.visibility,
                                                color: fromHex(yellow),
                                              ),
                                      )
                                    : null,
                              ),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(
                                horizontal: 25, vertical: 20),
                            padding: EdgeInsets.only(top: 20),
                            child: model.state == ViewState.Idle
                                ? ElevatedButton(
                                    onPressed: () async {
                                      await fcm.getFCMToken();
                                      if (_formKey.currentState!.validate()) {
                                        FormData data = new FormData.fromMap({
                                          'username': "${_userName.text}",
                                          'password': "${_password.text}",
                                        });
                                        try {
                                          var r = await model.login(data);
                                          if (r['response'] != null &&
                                              r['success'] != null) {
                                            if (r['success']) {
                                              var d = LoginResponse.fromJson(
                                                  r['response']);
                                              if (d.loginData != null) {
                                                if (d.loginData!.data!.status ==
                                                    "Active") {
                                                  Map<String, String> data1 = {
                                                    'token_id':
                                                        "${getTokenId()}",
                                                    'user_id':
                                                        "${d.loginData!.data!.id}",
                                                  };
                                                  print(data1);
                                                  // try {
                                                  //   var res = await model
                                                  //       .updateToken(data1);
                                                  //   if (res['success']) {
                                                  //     var response =
                                                  //         GeneralResponse
                                                  //             .fromJson(res[
                                                  //                 'response']);
                                                  //     if (!response.error!) {
                                                  //       showToast(d.message!);
                                                  Navigator
                                                      .pushNamedAndRemoveUntil(
                                                          context,
                                                          HomePage.tag,
                                                          (Route<dynamic>
                                                                  route) =>
                                                              false);
                                                  //     } else {
                                                  //       showToast(
                                                  //           "${response.erroMessage ?? ""}");
                                                  //     }
                                                  //   } else {
                                                  //     showErrorDialog(
                                                  //         res['response'],
                                                  //         context,
                                                  //         "token");
                                                  //   }
                                                  // } catch (exe) {
                                                  //   showErrorDialog(
                                                  //       exe.toString(),
                                                  //       context,
                                                  //       "token");
                                                  // }
                                                } else {
                                                  showToast(
                                                      "Sorry Account is currently ${d.loginData?.data?.status ?? 'In-Active'}");
                                                }
                                              } else {
                                                showToast(d.message ?? '');
                                              }
                                            } else {
                                              showErrorDialog(r['response'],
                                                  context, "login", true);
                                            }
                                          }
                                        } catch (e) {
                                          showErrorDialog(
                                              e.toString(), context, "login");
                                        }
                                      }
                                    },
                                    style: ElevatedButton.styleFrom(
                                      primary: fromHex(deepOrange),
                                      padding: EdgeInsets.symmetric(
                                        horizontal: 50,
                                      ),
                                      textStyle: TextStyle(
                                          fontSize: 30,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    child: Center(
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 10.0),
                                        child: Text(
                                          'Login',
                                          style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 18),
                                        ),
                                      ),
                                    ),
                                  )
                                : Center(
                                    child: CupertinoActivityIndicator(),
                                  ),
                          ),
                        ],
                      ),
                    )),
                    SvgPicture.asset(
                      "assets/footer.svg",
                      width: MediaQuery.of(context).size.width,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _toggleVisibility() {
    setState(() {
      _isHidden = !_isHidden;
    });
  }
}
