import 'dart:async';
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hive/hive.dart';
import 'package:scio_security/core/services/push_notifications_service.dart';
import 'package:scio_security/ui/screens/home_page.dart';
import 'package:scio_security/ui/screens/login_page.dart';
import 'package:scio_security/ui/screens/url_page.dart';
import 'package:scio_security/utils/useful.dart';

import '../../locator.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  PushNotificationService fcm = locator<PushNotificationService>();
  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  Map<String, dynamic>? _deviceData = <String, dynamic>{};

  @override
  void initState() {
    fcm.initialise();
    print(fcm.getFCMToken());
    initPlatformState();
    var url = accountInfo.get('url');
    var name = accountInfo.get('userId');
    if (url != null) {
      if (url.isNotEmpty) {
        if (name != null && name.isNotEmpty) {
          Timer(
              Duration(seconds: 5),
              () => Navigator.pushNamedAndRemoveUntil(
                  context, HomePage.tag, (Route<dynamic> route) => false));
        } else {
          Timer(
              Duration(seconds: 5),
              () => Navigator.pushNamedAndRemoveUntil(
                  context, LoginPage.tag, (Route<dynamic> route) => false));
        }
      }
      if (url.isEmpty) {
        Timer(
            Duration(seconds: 5),
            () => Navigator.pushNamedAndRemoveUntil(
                context, UrlPage.tag, (Route<dynamic> route) => false));
      }
    }
    if (url == null) {
      Timer(
          Duration(seconds: 5),
          () => Navigator.pushNamedAndRemoveUntil(
              context, UrlPage.tag, (Route<dynamic> route) => false));
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Stack(
        children: [
          Align(
            alignment: Alignment.center,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  'Welcome',
                  style: TextStyle(color: Colors.grey, fontSize: 36),
                ),
                SizedBox(
                  height: 30,
                ),
                SvgPicture.asset(
                  "assets/mainlogo.svg",
                  fit: BoxFit.fitHeight,
                  alignment: Alignment.center,
                  width: 150,
                  height: 200,
                ),
                Text(
                  "$appVersion",
                  style: TextStyle(
                      fontSize: 11,
                      fontWeight: FontWeight.w100,
                      color: fromHex(deepOrange),
                      fontStyle: FontStyle.italic),
                )
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: SvgPicture.asset(
              "assets/footer.svg",
              width: MediaQuery.of(context).size.width,
            ),
          )
        ],
      ),
    );
  }

  Future<void> initPlatformState() async {
    Map<String, dynamic>? deviceData;

    try {
      if (Platform.isAndroid) {
        deviceData = _readAndroidBuildData(await deviceInfoPlugin.androidInfo);
      } else if (Platform.isIOS) {
        deviceData = _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
      }
    } on PlatformException {
      deviceData = <String, dynamic>{
        'Error:': 'Failed to get platform version.'
      };
    }

    if (!mounted) return;

    setState(() {
      _deviceData = deviceData;
    });
  }

  Map<String, dynamic> _readAndroidBuildData(AndroidDeviceInfo build) {
    print("${build.brand}-${build.model}");
    getDevices().forEach((element) {
      if (element.name == "${build.brand}-${build.model}") {
        setDeviceModel(element);
      }
    });

    return <String, dynamic>{
      'version.securityPatch': build.version.securityPatch,
      'version.sdkInt': build.version.sdkInt,
      'version.release': build.version.release,
      'version.previewSdkInt': build.version.previewSdkInt,
      'version.incremental': build.version.incremental,
      'version.codename': build.version.codename,
      'version.baseOS': build.version.baseOS,
      'board': build.board,
      'bootloader': build.bootloader,
      'brand': build.brand,
      'device': build.device,
      'display': build.display,
      'fingerprint': build.fingerprint,
      'hardware': build.hardware,
      'host': build.host,
      'id': build.id,
      'manufacturer': build.manufacturer,
      'model': build.model,
      'product': build.product,
      'supported32BitAbis': build.supported32BitAbis,
      'supported64BitAbis': build.supported64BitAbis,
      'supportedAbis': build.supportedAbis,
      'tags': build.tags,
      'type': build.type,
      'isPhysicalDevice': build.isPhysicalDevice,
      'androidId': build.androidId,
      'systemFeatures': build.systemFeatures,
    };
  }

  Map<String, dynamic> _readIosDeviceInfo(IosDeviceInfo data) {
    return <String, dynamic>{
      'name': data.name,
      'systemName': data.systemName,
      'systemVersion': data.systemVersion,
      'model': data.model,
      'localizedModel': data.localizedModel,
      'identifierForVendor': data.identifierForVendor,
      'isPhysicalDevice': data.isPhysicalDevice,
      'utsname.sysname:': data.utsname.sysname,
      'utsname.nodename:': data.utsname.nodename,
      'utsname.release:': data.utsname.release,
      'utsname.version:': data.utsname.version,
      'utsname.machine:': data.utsname.machine,
    };
  }
}
