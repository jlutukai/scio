import 'package:cached_network_image/cached_network_image.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hive/hive.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:scio_security/core/enums/view_state.dart';
import 'package:scio_security/core/models/general_response.dart';
import 'package:scio_security/core/models/local/localModels.dart';
import 'package:scio_security/core/services/api.dart';
import 'package:scio_security/core/viewmodels/home_model.dart';
import 'package:scio_security/ui/screens/login_page.dart';
import 'package:scio_security/ui/screens/reportForms.dart';
import 'package:scio_security/ui/screens/settings_page.dart';
import 'package:scio_security/ui/screens/taskManager/task_manager.dart';
import 'package:scio_security/ui/screens/test/camera_page.dart';
import 'package:scio_security/ui/screens/visitors_page.dart';
import 'package:scio_security/utils/useful.dart';

import 'base_view.dart';
import 'guards_page_manual.dart';

class HomePage extends StatefulWidget {
  static const tag = 'home';
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  final visitorBox = Hive.box('visitor_in');
  Box currentPlanner = Hive.box("planner");
  List<PlannerData> planners = [];
  final GlobalKey<ScaffoldState> _scaffoldKey12 = GlobalKey<ScaffoldState>();
  Shifts? theShift;
  String? date;

  @override
  Widget build(BuildContext context) {
    return BaseView<HomeModel>(
      onModelReady: (model) async {
        var id = accountInfo.get('userId');
        Map<String, String> data = {
          'supervisor_id': "$id",
        };
        print(data);
        bool isConnected = await checkConnection();
        if (isConnected) {
          var r = await model.getPlanners(data);
          if (r['success']) {
          } else {
            showErrorDialog(r['response'], context, "get planners");
          }
        } else {
          showToast("No internet connection");
          await model.getPlanners(data);
        }
        if (model.planners != null) {
          if (model.planners!.isNotEmpty && model.planners!.length == 1) {
            setPlanner(model.planners!.first);
            model.initialize(model.planners!.first);
          }
        }
      },
      builder: (context, model, child) => Scaffold(
        backgroundColor: Colors.black,
        key: _scaffoldKey12,
        resizeToAvoidBottomInset: true,
        // resizeToAvoidBottomPadding: true,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          automaticallyImplyLeading: false,
          actions: [
            Padding(
              padding: const EdgeInsets.only(right: 10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: () async {
                      // bool isConnected = await checkConnection();
                      // if (!isConnected) {
                      //   showToast("Check Internet Connection, to proceed");
                      // }
                      // Map<String, String> data1 = {
                      //   'token_id': "${getTokenId()}",
                      // };
                      // try {
                      // var res = await model.deleteToken(data1);
                      // if (re//s['success']) {
                      //   GeneralResponse r =
                      //       GeneralResponse.fromJson(res['response']);
                      //   if (!r.error!) {
                      accountInfo.delete('userName');
                      accountInfo.delete('userId');
                      currentPlanner.delete('current_planner');
                      visitorBox.delete('visitor');
                      Navigator.pushNamedAndRemoveUntil(context, LoginPage.tag,
                          (Route<dynamic> route) => false);
                      // } else {
                      //   showToast(r.erroMessage!);
                      // }
                      // }
                      // } catch (e) {
                      //   showErrorDialog(e.toString(), context, "logout");
                      // }
                    },
                    child: Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                        decoration: BoxDecoration(
                            borderRadius:
                                BorderRadius.all(Radius.circular(16))),
                        child: Row(
                          children: [
                            Text(
                              'Log Out',
                              style: TextStyle(color: fromHex(yellow)),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Icon(
                              Icons.exit_to_app_outlined,
                              color: fromHex(yellow),
                            ),
                          ],
                        )),
                  ),
                ],
              ),
            ),
          ],
        ),
        body: model.state == ViewState.Idle
            ? Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 15),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'Welcome, ',
                              style: TextStyle(
                                  color: fromHex(yellow),
                                  fontWeight: FontWeight.w100,
                                  fontSize: 20),
                            ),
                            Text(
                              "${accountInfo.get('userName') ?? ''}",
                              style: TextStyle(
                                  color: fromHex(deepOrange),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 14),
                            )
                          ],
                        ),
                        SizedBox(
                          height: 5,
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Text(
                              'Current Assignment, ',
                              style: TextStyle(
                                  color: fromHex(yellow),
                                  fontWeight: FontWeight.w100,
                                  fontSize: 14),
                            ),
                            model.planners != null
                                ? model.planners!.isNotEmpty
                                    ? model.planners!.length > 1
                                        ? Expanded(
                                            child: DropdownSearch<PlannerData>(
                                              mode: Mode.BOTTOM_SHEET,
                                              showSearchBox: true,
                                              // searchBoxDecoration:
                                              //     InputDecoration(
                                              //   hintText: "Search Planner",
                                              //   hintStyle: TextStyle(
                                              //       color: Colors.grey[100]),
                                              //   prefixIcon: Icon(Icons.search,
                                              //       color: Colors.grey[100]),
                                              //   filled: true,
                                              //   fillColor: Colors.white10,
                                              // ),
                                              dropdownSearchDecoration:
                                                  InputDecoration(
                                                      filled: true,
                                                      fillColor:
                                                          Colors.transparent,
                                                      contentPadding:
                                                          EdgeInsets.zero,
                                                      border: InputBorder.none,
                                                      enabledBorder:
                                                          InputBorder.none),
                                              items: model.planners!,
                                              itemAsString: (p) =>
                                                  "${p?.plannerName ?? "Undefined"}",
                                              autoValidateMode: AutovalidateMode
                                                  .onUserInteraction,
                                              validator: (PlannerData? u) => u ==
                                                      null
                                                  ? "planner name field is required "
                                                  : null,
                                              onChanged: (PlannerData? data) {
                                                setPlanner(data);
                                                model.initialize(data!);
                                              },
                                              dropdownBuilder:
                                                  _customDropDownExample,
                                              popupBackgroundColor:
                                                  Colors.grey.shade900,
                                              popupItemBuilder:
                                                  _customPopupItemBuilderExample,
                                              dropdownButtonProps:
                                                  IconButtonProps(
                                                      icon: Icon(
                                                Icons.arrow_drop_down_sharp,
                                                color: fromHex(yellow),
                                              )),
                                            ),
                                          )
                                        : Container(
                                            child: Text(
                                            "  ${model.planners![0].plannerName ?? "undefined"}",
                                            style: TextStyle(
                                                color: fromHex(deepOrange),
                                                fontWeight: FontWeight.bold),
                                          ))
                                    : Container()
                                : Container(),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Expanded(
                    child: Container(
                        child: CachedNetworkImage(
                      imageUrl: "${Api().getUrl()}/CUSTOM/Logo/logo_app.png",
                    )),
                  ),
                  Container(
                    // padding: const EdgeInsets.symmetric(vertical:25.0,),
                    decoration: BoxDecoration(
                      // color: Colors.white10.withOpacity(0.07),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30)),
                    ),
                    child: SingleChildScrollView(
                      physics: BouncingScrollPhysics(),
                      child: Column(
                        children: [
                          Dismissible(
                            key: UniqueKey(),
                            direction: DismissDirection.startToEnd,
                            onDismissed: (DismissDirection direction) async {
                              setState(() {});
                              PlannerData? p = getPlanner();
                              if (p == null) {
                                showToast("please choose assignment!");
                                return;
                              }
                              if (p.shifts!.isEmpty) {
                                showToast("No shifts found! Contact Admin");
                                return;
                              }
                              setState(() {
                                date = null;
                                theShift = null;
                              });
                              await showShiftsDialog(p, context);

                              if (date == null) {
                                showToast("Please choose date");
                                return;
                              }
                              if (date == 'None') {
                                showToast("Please choose date");
                                return;
                              }
                              if (theShift == null) {
                                showToast("Please choose shift");
                                return;
                              }

                              setShiftID(theShift!.id ?? '');

                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (_) =>
                                      GuardsPageManual(date!, theShift!)));

                              // if (getPlanner() != null) {
                              //   model.initialize(getPlanner());
                              // }
                              // if (model.minDate != null &&
                              //     model.maxDate != null &&
                              //     model.previousShift != null &&
                              //     model.nextShift != null) {
                              //   Navigator.of(context).push(MaterialPageRoute(
                              //       builder: (BuildContext context) =>
                              //           GuardsPage(
                              //               p,
                              //               model.minDate,
                              //               model.maxDate,
                              //               model.previousShift,
                              //               model.currentShift,
                              //               model.nextShift)));
                              // }
                            },
                            child: InkWell(
                              onTap: () async {
                                PlannerData? p = getPlanner();
                                if (p == null) {
                                  showToast("please choose assignment!");
                                  return;
                                }
                                if (p.shifts!.isEmpty) {
                                  showToast("No shifts found! Contact Admin");
                                  return;
                                }
                                setState(() {
                                  date = null;
                                  theShift = null;
                                });
                                await showShiftsDialog(p, context);
                                if (date == null) {
                                  showToast("Please choose date");
                                  return;
                                }
                                if (date == 'None') {
                                  showToast("Please choose date");
                                  return;
                                }
                                if (theShift == null) {
                                  showToast("Please choose shift");
                                  return;
                                }
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        GuardsPageManual(date!, theShift!)));
                                // if (getPlanner() != null) {
                                //   model.initialize(getPlanner());
                                // }
                                // if (model.minDate != null &&
                                //     model.maxDate != null &&
                                //     model.previousShift != null &&
                                //     model.currentShift != null &&
                                //     model.nextShift != null) {
                                //   Navigator.of(context).push(MaterialPageRoute(
                                //       builder: (BuildContext context) =>
                                //           GuardsPage(
                                //               p,
                                //               model.minDate,
                                //               model.maxDate,
                                //               model.previousShift,
                                //               model.currentShift,
                                //               model.nextShift)));
                                // }
                              },
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 5),
                                child: Card(
                                  color: Colors.white10.withOpacity(0.07),
                                  child: Container(
                                    height: 60,
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 10, horizontal: 20),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            SvgPicture.asset(
                                              "assets/attendance.svg",
                                              width: 36,
                                              height: 36,
                                              color: fromHex(yellow),
                                            ),
                                            SizedBox(
                                              width: 60,
                                            ),
                                            Text(
                                              "Attendance",
                                              style: TextStyle(
                                                  color: fromHex(yellow),
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 18),
                                            )
                                          ],
                                        ),
                                        Icon(
                                          Icons.arrow_forward_ios_sharp,
                                          color: fromHex(yellow),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Dismissible(
                            key: UniqueKey(),
                            direction: DismissDirection.startToEnd,
                            onDismissed: (DismissDirection direction) async {
                              setState(() {});
                              PlannerData? p = getPlanner();
                              if (p == null) {
                                showToast("please choose assignment!");
                                return;
                              }
                              if (p.shifts!.isEmpty) {
                                showToast("No shifts found! Contact Admin");
                                return;
                              }
                              setState(() {
                                date = null;
                                theShift = null;
                              });
                              await showShiftsDialog(p, context);
                              if (date == null) {
                                showToast("Please choose date");
                                return;
                              }
                              if (date == 'None') {
                                showToast("Please choose date");
                                return;
                              }
                              if (theShift == null) {
                                showToast("Please choose shift");
                                return;
                              }
                              setShiftID(theShift!.id ?? "");
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      ReportForms()));
                            },
                            child: InkWell(
                              onTap: () async {
                                PlannerData? p = getPlanner();
                                if (p == null) {
                                  showToast("please choose assignment!");
                                  return;
                                }
                                if (p.shifts!.isEmpty) {
                                  showToast("No shifts found! Contact Admin");
                                  return;
                                }
                                setState(() {
                                  date = null;
                                  theShift = null;
                                });
                                await showShiftsDialog(p, context);
                                if (date == null) {
                                  showToast("Please choose date");
                                  return;
                                }
                                if (date == 'None') {
                                  showToast("Please choose date");
                                  return;
                                }
                                if (theShift == null) {
                                  showToast("Please choose shift");
                                  return;
                                }
                                setShiftID(theShift!.id ?? "");
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        ReportForms()));
                              },
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 5),
                                child: Card(
                                  color: Colors.white10.withOpacity(0.07),
                                  child: Container(
                                    // color: fromHex("#c19d3d"),
                                    height: 60,
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 10, horizontal: 20),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            SvgPicture.asset(
                                              "assets/handover.svg",
                                              width: 36,
                                              height: 36,
                                              color: fromHex(yellow),
                                            ),
                                            SizedBox(
                                              width: 60,
                                            ),
                                            Text(
                                              "Reports",
                                              style: TextStyle(
                                                  color: fromHex(yellow),
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 18),
                                            )
                                          ],
                                        ),
                                        Icon(
                                          Icons.arrow_forward_ios_sharp,
                                          color: fromHex(yellow),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Dismissible(
                            key: UniqueKey(),
                            direction: DismissDirection.startToEnd,
                            onDismissed: (DismissDirection direction) async {
                              setState(() {});
                              // PlannerData? p = getPlanner();
                              // if (p == null) {
                              //   showToast("please choose assignment!");
                              //   return;
                              // }
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      TaskManagerPage()));
                            },
                            child: InkWell(
                              onTap: () async {
                                // PlannerData? p = getPlanner();
                                // if (p == null) {
                                //   showToast("please choose assignment!");
                                //   return;
                                // }
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        TaskManagerPage()));
                              },
                              child: Padding(
                                padding:
                                    const EdgeInsets.symmetric(horizontal: 5),
                                child: Card(
                                  color: Colors.white10.withOpacity(0.07),
                                  child: Container(
                                    // color: fromHex("#c19d3d"),
                                    height: 60,
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 10, horizontal: 20),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            SvgPicture.asset(
                                              "assets/handover.svg",
                                              width: 36,
                                              height: 36,
                                              color: fromHex(yellow),
                                            ),
                                            SizedBox(
                                              width: 60,
                                            ),
                                            Text(
                                              "Task Manager",
                                              style: TextStyle(
                                                  color: fromHex(yellow),
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 18),
                                            )
                                          ],
                                        ),
                                        Icon(
                                          Icons.arrow_forward_ios_sharp,
                                          color: fromHex(yellow),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                        ],
                      ),
                    ),
                  ),
                  SvgPicture.asset(
                    "assets/footer.svg",
                    width: MediaQuery.of(context).size.width,
                  ),
                ],
              )
            : Column(
                children: [
                  Expanded(
                      child: Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                            height: 200,
                            width: 200,
                            child: Lottie.asset('assets/loading.json')),
                        Text(
                          'Please wait \n Loading...',
                          style: TextStyle(color: fromHex(deepOrange)),
                        )
                      ],
                    ),
                  ))
                ],
              ),
      ),
    );
  }

  getShifts(HomeModel model, PlannerData planner) {
    model.initialize(planner);
  }

  Widget _customDropDownExample(BuildContext context, PlannerData? item) {
    return Container(
      child: (item?.plannerName == null)
          ? Text(
              getPlanner() == null
                  ? "No assignment selected"
                  : "${getPlanner()!.plannerName}",
              style: TextStyle(color: fromHex(deepOrange)),
            )
          : Text(
              "${item!.plannerName ?? "undefined"}",
              style: TextStyle(
                  color: fromHex(deepOrange), fontWeight: FontWeight.bold),
            ),
    );
  }

  Widget _customPopupItemBuilderExample(
      BuildContext context, PlannerData item, bool isSelected) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 8),
      decoration: !isSelected
          ? null
          : BoxDecoration(
              border: Border.all(color: Theme.of(context).primaryColor),
              borderRadius: BorderRadius.circular(5),
              color: Colors.white,
            ),
      child: ListTile(
        title: Text(item.plannerName ?? 'undefined',
            style: TextStyle(color: fromHex(deepOrange))),
      ),
    );
  }

  Future<void> showShiftsDialog(PlannerData p, BuildContext context) async {
    final f = new DateFormat('yyyy-MM-dd');

    List<String> _reasons = ['None', 'Yesterday', 'Today', 'Tomorrow'];
    String? reason = 'None';

    List<String?> _shifts = [];
    String? shift = 'None';
    _shifts.add(shift);
    p.shifts!.forEach((element) {
      _shifts.add(
          "${element.shiftName} ( ${element.shiftChangeFrom}-${element.shiftChangeTo} )");
    });
    await showDialog(
        context: _scaffoldKey12.currentContext!,
        builder: (context) => Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            elevation: 0.0,
            backgroundColor: Colors.grey[900],
            child: StatefulBuilder(builder: (context, StateSetter setState) {
              return Container(
                color: Colors.transparent,
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                child: Container(
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 15,
                        ),
                        Row(
                          children: [
                            Container(
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 10.0, right: 10.0),
                                child: Text(
                                  'Choose Date',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 11,
                                      fontStyle: FontStyle.italic),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Container(
                          decoration: BoxDecoration(
                              color: Colors.white10,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5.0))),
                          child: DropdownButtonHideUnderline(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  top: 8.0, bottom: 8.0, left: 5.0),
                              child: ButtonTheme(
                                alignedDropdown: true,
                                child: DropdownButton(
                                  hint: Text('Specify Date'),
                                  isExpanded: true,
                                  isDense: true,
                                  value: reason,
                                  dropdownColor: Colors.grey[800],
                                  style: TextStyle(
                                    color: fromHex(yellow),
                                  ),
                                  onChanged: (String? val) {
                                    DateTime now = DateTime.now();

                                    if (val == 'Yesterday') {
                                      setState(() {
                                        DateTime y =
                                            now.subtract(Duration(days: 1));
                                        date = f.format(y);
                                      });
                                    }
                                    if (val == 'Today') {
                                      setState(() {
                                        date = f.format(now);
                                      });
                                    }
                                    if (val == 'Tomorrow') {
                                      setState(() {
                                        DateTime t = now.add(Duration(days: 1));
                                        date = f.format(t);
                                      });
                                    }
                                    print(date);
                                    setState(() {
                                      reason = val;
                                    });
                                  },
                                  items: _reasons.map<DropdownMenuItem<String>>(
                                      (String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Row(
                          children: [
                            Container(
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 10.0, right: 10.0),
                                child: Text(
                                  'Choose Shift',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 11,
                                      fontStyle: FontStyle.italic),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Container(
                          decoration: BoxDecoration(
                              color: Colors.white10,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5.0))),
                          child: DropdownButtonHideUnderline(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  top: 8.0, bottom: 8.0, left: 5.0),
                              child: ButtonTheme(
                                alignedDropdown: true,
                                child: DropdownButton(
                                  hint: Text('Specify shift'),
                                  isExpanded: true,
                                  isDense: true,
                                  value: shift,
                                  dropdownColor: Colors.grey[800],
                                  style: TextStyle(
                                    color: fromHex(yellow),
                                  ),
                                  onChanged: (String? val) {
                                    setState(() {
                                      shift = val;

                                      p.shifts!.forEach((element) {
                                        if ("${element.shiftName} ( ${element.shiftChangeFrom}-${element.shiftChangeTo} )" ==
                                            val) {
                                          theShift = element;
                                        }
                                      });
                                    });
                                    print(theShift!.id);
                                  },
                                  items: _shifts.map<DropdownMenuItem<String>>(
                                      (String? value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value!),
                                    );
                                  }).toList(),
                                ),
                              ),
                            ),
                          ),
                        ),
                        ListTile(
                          onTap: () {
                            if (date == null) {
                              showToast("Please choose date");
                              return;
                            }
                            if (date == 'None') {
                              showToast("Please choose date");
                              return;
                            }
                            if (theShift == null) {
                              showToast("Please choose shift");
                              return;
                            }
                            print("$date  ${theShift!.id}");

                            Navigator.pop(context);
                          },
                          title: Center(
                            child: Text(
                              'Submit',
                              style: TextStyle(color: Colors.deepOrange),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            })));
  }

  void goToNext(String date, Shifts theShift) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (BuildContext context) => GuardsPageManual(date, theShift)));
  }
}
