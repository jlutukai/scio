import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hive/hive.dart';
import 'package:scio_security/core/enums/view_state.dart';
import 'package:scio_security/core/viewmodels/reports_planner_model.dart';
import 'package:scio_security/utils/loader.dart';
import 'package:scio_security/utils/useful.dart';

import 'base_view.dart';

class ReportsPlanner extends StatefulWidget {
  static const tag = 'reportsPlanner';
  @override
  _ReportsPlannerState createState() => _ReportsPlannerState();
}

class _ReportsPlannerState extends State<ReportsPlanner> {
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  ReportsPlannerModel? _model;

  @override
  Widget build(BuildContext context) {
    return BaseView<ReportsPlannerModel>(
      onModelReady: (model) async {
        _model = model;
        var id = accountInfo.get('userId');
        Map<String, String> data = {
          'supervisor_id': "$id",
        };
        print(data);
        bool isConnected = await checkConnection();
        if (isConnected) {
          var r = await model.getPlanners(data);
          if (r['success']) {
          } else {
            showErrorDialog(r['response'], context, "get planners");
          }
        } else {
          showToast('Please Check Internet Connection');
        }
      },
      builder: (context, model, child) => model.state == ViewState.Idle
          ? Stack(
              children: [
                Scaffold(
                  backgroundColor: Colors.black,
                  appBar: AppBar(
                    backgroundColor: Colors.transparent,
                    elevation: 0,
                    title: Text(
                      'Choose Planner',
                      style: TextStyle(
                          color: fromHex(deepOrange),
                          fontWeight: FontWeight.bold),
                    ),
                    centerTitle: true,
                    automaticallyImplyLeading: false,
                  ),
                  body: Column(
                    children: [
                      Expanded(
                        child: model.planners != null
                            ? model.planners!.isNotEmpty
                                ? ListView.builder(
                                    itemCount: model.planners!.length,
                                    itemBuilder: (context, index) {
                                      return Container(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 10),
                                        child: Dismissible(
                                            key: UniqueKey(),
                                            direction:
                                                DismissDirection.startToEnd,
                                            onDismissed: (DismissDirection
                                                direction) async {
                                              setState(() {});
                                              await model.initialize(
                                                  model.planners![index]);
                                            },
                                            child: InkWell(
                                              onTap: () async {
                                                await model.initialize(
                                                    model.planners![index]);
                                              },
                                              child: Container(
                                                color: Colors.white10
                                                    .withOpacity(0.07),
                                                padding: EdgeInsets.symmetric(
                                                    horizontal: 10,
                                                    vertical: 10),
                                                margin: EdgeInsets.symmetric(
                                                    horizontal: 5, vertical: 3),
                                                child: ListTile(
                                                  trailing: SvgPicture.asset(
                                                    "assets/planner.svg",
                                                    width: 36,
                                                    height: 36,
                                                    color: fromHex(yellow),
                                                  ),
                                                  leading: Text(
                                                    "${index + 1}",
                                                    style: TextStyle(
                                                        color: fromHex(yellow)),
                                                  ),
                                                  title: Text(
                                                    "${model.planners![index].plannerName}",
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  ),
                                                ),
                                              ),
                                            )),
                                      );
                                    })
                                : Center(
                                    child: Text(
                                      'No Planners Found!',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  )
                            : Container(),
                      )
                    ],
                  ),
                ),
                Positioned(
                  left: 15,
                  top: 27,
                  child: FloatingActionButton(
                    foregroundColor: Colors.grey,
                    backgroundColor: Colors.white10.withOpacity(0.07),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    mini: true,
                    tooltip: "go back home",
                    child: Icon(
                      Icons.keyboard_backspace_sharp,
                      color: fromHex(deepOrange),
                    ),
                  ),
                ),
              ],
            )
          : Loader(),
    );
  }
}
