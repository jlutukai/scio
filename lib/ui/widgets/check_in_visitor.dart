import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_tags/flutter_tags.dart';
import 'package:hive/hive.dart';
import 'package:intl/intl.dart';
import 'package:permission_handler/permission_handler.dart';
// import 'package:qrscan/qrscan.dart' as scanner;
import 'package:scio_security/core/models/create_visitor_response.dart';
import 'package:scio_security/core/models/local/localModels.dart';
import 'package:scio_security/core/viewmodels/visitors_log_model.dart';
import 'package:scio_security/ui/screens/test/camera_page.dart';
import 'package:scio_security/ui/widgets/qr_code_scanner.dart';
import 'package:scio_security/utils/useful.dart';

class CheckInVisitor extends StatefulWidget {
  final VisitorsLogModel model;
  final bool? requirePass;

  CheckInVisitor(this.model, this.requirePass);

  @override
  _CheckInVisitorState createState() => _CheckInVisitorState();
}

class _CheckInVisitorState extends State<CheckInVisitor> {
  Visitor? visitor;
  String passId = '';
  List<ItemObj> _items = [];
  final visitors = Hive.box('visitors_in');
  final visitorBox = Hive.box('visitor_in');

  // Box<String> accountInfo = Hive.box<String>("accountInfo");
  List<Visitor> _visitors = [];

  void initState() {
    if (visitors.isNotEmpty) {
      _visitors.addAll(visitors.values as Iterable<Visitor>);
    }
    _visitors.addAll(widget.model.visitorsIn);
    var _v = visitorBox.get('visitor');
    if (_v != null) {
      visitor = _v;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print("${getRequirePass()}    ${getCanScanID()}");
    return Container(
      margin: EdgeInsets.only(top: 15),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          title: Text(
            'Check In Visitor',
            style: TextStyle(
                color: fromHex(deepOrange), fontWeight: FontWeight.bold),
          ),
          centerTitle: true,
          automaticallyImplyLeading: false,
          actions: [
            visitor != null
                ? Padding(
                    padding: const EdgeInsets.only(right: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        InkWell(
                          onTap: () {
                            setState(() {
                              visitor = null;
                              visitorBox.delete('visitor');
                            });
                          },
                          child: Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: 10, vertical: 5),
                              decoration: BoxDecoration(
                                  color: Colors.white10.withOpacity(0.07),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(16))),
                              child: Text(
                                'Clear',
                                style: TextStyle(color: Colors.deepOrange),
                              )),
                        ),
                      ],
                    ),
                  )
                : Container()
          ],
        ),
        body: Column(
          children: [
            Expanded(
              child: SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: Column(
                  children: [
                    visitor != null ? _visitorObject() : Container(),
                    Dismissible(
                      key: UniqueKey(),
                      direction: DismissDirection.startToEnd,
                      onDismissed: (DismissDirection direction) {
                        _hostDialog(context);
                        setState(() {});
                      },
                      child: InkWell(
                        onTap: () {
                          _hostDialog(context);
                        },
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 2.0, horizontal: 15),
                          child: Card(
                            color: Colors.white10.withOpacity(0.07),
                            child: Container(
                              // color: fromHex("#114259"),
                              height: 60,
                              padding: const EdgeInsets.symmetric(
                                  vertical: 10, horizontal: 20),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.account_circle_outlined,
                                        size: 32,
                                        color: fromHex(yellow),
                                      ),
                                      SizedBox(
                                        width: 60,
                                      ),
                                      Text(
                                        "Person to visit",
                                        style: TextStyle(
                                            color: fromHex(yellow),
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18),
                                      )
                                    ],
                                  ),
                                  Icon(
                                    Icons.arrow_forward_ios_sharp,
                                    color: fromHex(yellow),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    getRequireItems()
                        ? Dismissible(
                            key: UniqueKey(),
                            direction: DismissDirection.startToEnd,
                            onDismissed: (DismissDirection direction) {
                              _itemsDialog(context);
                              setState(() {});
                            },
                            child: InkWell(
                              onTap: () {
                                _itemsDialog(context);
                              },
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 2.0, horizontal: 15),
                                child: Card(
                                  color: Colors.white10.withOpacity(0.07),
                                  child: Container(
                                    // color: fromHex("#114259"),
                                    height: 60,
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 10, horizontal: 20),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Icon(
                                              Icons.view_comfortable,
                                              size: 32,
                                              color: fromHex(yellow),
                                            ),
                                            SizedBox(
                                              width: 60,
                                            ),
                                            Text(
                                              "Items",
                                              style: TextStyle(
                                                  color: fromHex("#c19d3d"),
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 18),
                                            )
                                          ],
                                        ),
                                        Icon(
                                          Icons.arrow_forward_ios_sharp,
                                          color: fromHex(yellow),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          )
                        : Container(),
                    getRequireCarReg()
                        ? Dismissible(
                            key: UniqueKey(),
                            direction: DismissDirection.startToEnd,
                            onDismissed: (DismissDirection direction) async {
                              // Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => CameraExampleHome()));
                              var status = await Permission.storage.request();
                              if (status.isGranted) {
                                getImage(context, "plate");
                              }
                              setState(() {});
                            },
                            child: InkWell(
                              onTap: () async {
                                // Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => CameraExampleHome()));
                                var status = await Permission.storage.request();
                                if (status.isGranted) {
                                  getImage(context, "plate");
                                }
                              },
                              onLongPress: () {
                                _carPlateDialog(context);
                              },
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 2.0, horizontal: 15),
                                child: Card(
                                  color: Colors.white10.withOpacity(0.07),
                                  child: Container(
                                    // color: fromHex("#114259"),
                                    height: 60,
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 1, horizontal: 20),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Icon(
                                              Icons.directions_car,
                                              size: 32,
                                              color: fromHex(yellow),
                                            ),
                                            SizedBox(
                                              width: 60,
                                            ),
                                            Text(
                                              "Car Plate No.",
                                              style: TextStyle(
                                                  color: fromHex("#c19d3d"),
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 18),
                                            )
                                          ],
                                        ),
                                        Icon(
                                          Icons.arrow_forward_ios_sharp,
                                          color: fromHex(yellow),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          )
                        : Container(),
                    getCanScanID()
                        ? Dismissible(
                            key: UniqueKey(),
                            // direction: DismissDirection.startToEnd,
                            onDismissed: (DismissDirection direction) async {
                              if (direction == DismissDirection.startToEnd) {
                                var status = await Permission.storage.request();
                                if (status.isGranted) {
                                  getImage(context, "ID");
                                  // scanDocument();
                                }
                                setState(() {});
                              } else {
                                var status = await Permission.storage.request();
                                if (status.isGranted) {
                                  getImage(context, "Passport");
                                  // scanDocument();
                                }
                                setState(() {});
                              }
                            },
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 2.0, horizontal: 15),
                              child: Card(
                                color: Colors.white10.withOpacity(0.07),
                                child: Container(
                                  // color: fromHex("#114259"),
                                  height: 60,
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 20),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        child: InkWell(
                                          onTap: () async {
                                            var status = await Permission
                                                .storage
                                                .request();
                                            if (status.isGranted) {
                                              getImage(context, "Passport");
                                              // scanDocument();
                                            } //passport
                                          },
                                          onLongPress: () {
                                            _passportDialog(context);
                                          },
                                          child: Padding(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 12.0),
                                            child: Row(
                                              children: [
                                                Icon(
                                                  Icons.arrow_back_ios_sharp,
                                                  color: fromHex(yellow),
                                                ),
                                                Expanded(
                                                  child: Center(
                                                    child: Text(
                                                      "Passport",
                                                      style: TextStyle(
                                                          color:
                                                              fromHex(yellow),
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 18),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      Icon(
                                        Icons.camera_alt_outlined,
                                        size: 32,
                                        color: fromHex(yellow),
                                      ),
                                      Expanded(
                                        child: InkWell(
                                          onTap: () async {
                                            var status = await Permission
                                                .storage
                                                .request();
                                            if (status.isGranted) {
                                              getImage(context, "ID");
                                              // scanDocument();
                                            } // ID
                                          },
                                          onLongPress: () {
                                            _idDialog(context);
                                          },
                                          child: Padding(
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 12.0),
                                            child: Row(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                              children: [
                                                Expanded(
                                                  child: Center(
                                                    child: Text(
                                                      "ID",
                                                      style: TextStyle(
                                                          color:
                                                              fromHex(yellow),
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontSize: 18),
                                                    ),
                                                  ),
                                                ),
                                                Icon(
                                                  Icons.arrow_forward_ios_sharp,
                                                  color: fromHex(yellow),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          )
                        : Container(),
                    getRequireVehiclePass()
                        ? Dismissible(
                            key: UniqueKey(),
                            direction: DismissDirection.startToEnd,
                            onDismissed: (DismissDirection direction) async {
                              setState(() {});

                              var status = await Permission.camera.request();

                              if (status.isGranted) {
                                /// Todo : Scanner
                                final p = await Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => const QRCodeScanner(),
                                ));
                                // String? p = await scanner.scan();
                                setVehiclePassId(p);
                              }
                              // Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => TestPage(getPassId:getPassId)));
                            },
                            child: InkWell(
                              onTap: () async {
                                var status = await Permission.camera.request();

                                if (status.isGranted) {
                                  /// Todo : Scanner
                                  final p = await Navigator.of(context).push(MaterialPageRoute(
                                                                     builder: (context) => const QRCodeScanner(),
                                     ));
                                  // String? p = await scanner.scan();
                                  getPassId(p);
                                }
                              },
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 2.0, horizontal: 15),
                                child: Card(
                                  color: Colors.white10.withOpacity(0.07),
                                  child: Container(
                                    // color: fromHex("#114259"),
                                    height: 60,
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 10, horizontal: 20),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Icon(
                                              Icons.qr_code_sharp,
                                              size: 32,
                                              color: fromHex(yellow),
                                            ),
                                            SizedBox(
                                              width: 60,
                                            ),
                                            Text(
                                              "Vehicle Pass",
                                              style: TextStyle(
                                                  color: fromHex(yellow),
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 18),
                                            )
                                          ],
                                        ),
                                        Icon(
                                          Icons.arrow_forward_ios_sharp,
                                          color: fromHex(yellow),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          )
                        : Container(),
                    getRequirePass()
                        ? Dismissible(
                            key: UniqueKey(),
                            direction: DismissDirection.startToEnd,
                            onDismissed: (DismissDirection direction) async {
                              setState(() {});
                              if (visitor == null) {
                                showToast("Please fill above details first");
                                return;
                              }
                              if (visitor!.visitorName!.isEmpty &&
                                  getCanScanID()) {
                                showToast("Please specify visitor's name");
                                return;
                              }
                              if (visitor!.documentNumber!.isEmpty &&
                                  getCanScanID()) {
                                showToast(
                                    "Please specify visitor's ID/Passport Number");
                                return;
                              }
                              var status = await Permission.camera.request();

                              if (status.isGranted) {
                                /// Todo : Scanner
                                final p = await Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => const QRCodeScanner(),
                                ));

                                // String? p = await scanner.scan();
                                getPassId(p);
                              }
                              // Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => TestPage(getPassId:getPassId)));
                            },
                            child: InkWell(
                              onTap: () async {
                                if (visitor == null) {
                                  showToast("Please fill above details first");
                                  return;
                                }
                                if (visitor!.visitorName!.isEmpty &&
                                    getCanScanID()) {
                                  showToast("Please specify visitor's name");
                                  return;
                                }
                                if (visitor!.documentNumber!.isEmpty &&
                                    getCanScanID()) {
                                  showToast(
                                      "Please specify visitor's ID/Passport Number");
                                  return;
                                }
                                // Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => TestPage(getPassId:getPassId)));
                                var status = await Permission.camera.request();

                                if (status.isGranted) {
                                  /// Todo : Scanner
                                  final p = await Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => const QRCodeScanner(),
                                  ));
                                  // String? p = await scanner.scan();
                                  getPassId(p);
                                }
                              },
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 2.0, horizontal: 15),
                                child: Card(
                                  color: Colors.white10.withOpacity(0.07),
                                  child: Container(
                                    // color: fromHex("#114259"),
                                    height: 60,
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 10, horizontal: 20),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Icon(
                                              Icons.qr_code_sharp,
                                              size: 32,
                                              color: fromHex(yellow),
                                            ),
                                            SizedBox(
                                              width: 60,
                                            ),
                                            Text(
                                              "Visitor's Pass",
                                              style: TextStyle(
                                                  color: fromHex(yellow),
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 18),
                                            )
                                          ],
                                        ),
                                        Icon(
                                          Icons.arrow_forward_ios_sharp,
                                          color: fromHex(yellow),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          )
                        : Dismissible(
                            key: UniqueKey(),
                            direction: DismissDirection.startToEnd,
                            onDismissed: (DismissDirection direction) {
                              setState(() {});
                              if (visitor == null) {
                                showToast("Please fill above details first");
                                return;
                              }

                              if (visitor!.visitorName!.isEmpty) {
                                showToast("Please specify visitor's name");
                                return;
                              }
                              if (visitor!.documentNumber!.isEmpty) {
                                showToast(
                                    "Please specify visitor's ID/Passport Number");
                                return;
                              }
                              getPassId('');
                            },
                            child: InkWell(
                              onTap: () {
                                if (visitor == null) {
                                  showToast("Please fill above details first");
                                  return;
                                }
                                if (visitor!.hostName!.isEmpty) {
                                  showToast("Please specify host's name");
                                  return;
                                }
                                if (visitor!.visitorName!.isEmpty) {
                                  showToast("Please specify visitor's name");
                                  return;
                                }
                                if (visitor!.documentNumber!.isEmpty) {
                                  showToast(
                                      "Please specify visitor's ID/Passport Number");
                                  return;
                                }
                                getPassId('');
                              },
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 2.0, horizontal: 15),
                                child: Card(
                                  color: Colors.white10.withOpacity(0.07),
                                  child: Container(
                                    // color: fromHex("#114259"),
                                    height: 60,
                                    padding: const EdgeInsets.symmetric(
                                        vertical: 10, horizontal: 20),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Icon(
                                              Icons.done_outline_rounded,
                                              size: 32,
                                              color: fromHex(yellow),
                                            ),
                                            SizedBox(
                                              width: 60,
                                            ),
                                            Text(
                                              "Confirm Check In",
                                              style: TextStyle(
                                                  color: fromHex(yellow),
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 18),
                                            )
                                          ],
                                        ),
                                        Icon(
                                          Icons.arrow_forward_ios_sharp,
                                          color: fromHex(yellow),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  // Future<void> scanDocument() async {
  //   String license;
  //   Recognizer recognizer = BlinkIdCombinedRecognizer();
  //   OverlaySettings settings = BlinkIdOverlaySettings();
  //
  //   // set your license
  //   if (Theme.of(context).platform == TargetPlatform.iOS) {
  //     license = "";
  //   } else if (Theme.of(context).platform == TargetPlatform.android) {
  //     license = blinkIdLicense;
  //   }
  //
  //   try {
  //     // perform scan and gather results
  //     scanResults = await MicroblinkScanner.scanWithCamera(
  //         RecognizerCollection([recognizer]), settings, license);
  //   } on PlatformException {
  //     // handle exception
  //   }
  //   print(jsonEncode(scanResults));
  // }

  Future getImage(BuildContext context, String s) async {
    DeviceDetails? d = getDeviceModel();
    if (d == null) {
      showToast("Unable to scan, Please contact admin");
      return;
    }
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => CameraExampleHome(
                  s: s,
                  setCarPlate: setCarPlate,
                  setVisitorsDetails: setVisitorsDetails,
              isSetUp: false,
                )));
  }

  scanAgain(BuildContext context, String s) {
    getImage(context, s);
  }

  setCarPlate(String noPlate) {
    Visitor _visitor = Visitor(
        visitorName: visitor?.visitorName ?? '',
        hostName: visitor?.hostName ?? '',
        noVisitors: visitor?.noVisitors ?? '',
        department: visitor?.department ?? '',
        designation: visitor?.designation ?? '',
        visitReason: visitor?.visitReason ?? '',
        documentNumber: visitor?.documentNumber ?? '',
        plateNo: "${noPlate.trim()}",
        passId: visitor?.passId ?? '',
        items: visitor?.items ?? [],
        companyName: visitor?.companyName ?? '',
        officeNo: visitor?.officeNo ?? '',
        houseNo: visitor?.houseNo ?? '',
        blockNo: visitor?.blockNo ?? '',
        vehiclePass: visitor?.vehiclePass ?? '');
    setVisitor(_visitor);
  }

  setVisitorsDetails(String id, String name, String type) {
    Visitor _visitor = Visitor(
        visitorName: "${name.trim()}",
        hostName: visitor?.hostName ?? '',
        noVisitors: visitor?.noVisitors ?? '',
        department: visitor?.department ?? '',
        designation: visitor?.designation ?? '',
        visitReason: visitor?.visitReason ?? '',
        documentType: type,
        documentNumber: "${id.trim()}",
        plateNo: visitor?.plateNo ?? '',
        passId: visitor?.passId ?? '',
        items: visitor?.items ?? [],
        companyName: visitor?.companyName ?? '',
        officeNo: visitor?.officeNo ?? '',
        houseNo: visitor?.houseNo ?? '',
        blockNo: visitor?.blockNo ?? '',
        vehiclePass: visitor?.vehiclePass ?? '');
    setVisitor(_visitor);
  }

  _hostDialog(BuildContext context) async {
    TextEditingController _hostsName = TextEditingController();
    TextEditingController _numberOfVisitors = TextEditingController();
    TextEditingController _department = TextEditingController();
    TextEditingController _designation = TextEditingController();
    TextEditingController _reason = TextEditingController();
    TextEditingController _companyName = TextEditingController();
    TextEditingController _houseNo = TextEditingController();
    TextEditingController _blockNo = TextEditingController();
    TextEditingController _officeNo = TextEditingController();
    if (visitor != null) {
      _hostsName.text = visitor!.hostName!;
      _numberOfVisitors.text = visitor!.noVisitors!;
      _department.text = visitor!.department!;
      _designation.text = visitor!.designation!;
      _reason.text = visitor!.visitReason!;
      _companyName.text = visitor!.companyName!;
      _houseNo.text = visitor!.houseNo!;
      _blockNo.text = visitor!.blockNo!;
      _officeNo.text = visitor!.officeNo!;
    }
    await showDialog(
      context: context,
      builder: (context) => Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        elevation: 0.0,
        backgroundColor: Colors.grey[900],
        child: Container(
          color: Colors.transparent,
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
          child: Container(
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 15,
                  ),
                  getRequireCompany()
                      ? Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: TextFormField(
                            controller: _companyName,
                            keyboardType: TextInputType.text,
                            textCapitalization: TextCapitalization.words,
                            style: TextStyle(color: Colors.white),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "Please Enter Company Name";
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white10,
                              isDense: true,
                              hintText: "eg Company A",
                              labelText: "Company Name",
                              labelStyle: TextStyle(color: fromHex(yellow)),
                              hintStyle: TextStyle(
                                color: Colors.blueGrey[400],
                              ),
                              border: InputBorder.none,
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: fromHex(yellow)),
                              ),
                            ),
                          ),
                        )
                      : Container(),
                  getRequireOfficeNo()
                      ? Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: TextFormField(
                            controller: _officeNo,
                            keyboardType: TextInputType.text,
                            textCapitalization: TextCapitalization.characters,
                            style: TextStyle(color: Colors.white),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "Please Enter Office No.";
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white10,
                              isDense: true,
                              hintText: "eg 1",
                              labelText: "Office No.",
                              labelStyle: TextStyle(color: fromHex(yellow)),
                              hintStyle: TextStyle(
                                color: Colors.blueGrey[400],
                              ),
                              border: InputBorder.none,
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: fromHex(yellow)),
                              ),
                            ),
                          ),
                        )
                      : Container(),
                  getRequireDepart()
                      ? Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: TextFormField(
                            controller: _department,
                            keyboardType: TextInputType.text,
                            textCapitalization: TextCapitalization.words,
                            style: TextStyle(color: Colors.white),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "Please Enter Department";
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white10,
                              isDense: true,
                              hintText: "eg Customer Care",
                              labelText: "Department to Visit",
                              labelStyle: TextStyle(color: fromHex(yellow)),
                              hintStyle: TextStyle(
                                color: Colors.blueGrey[400],
                              ),
                              border: InputBorder.none,
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: fromHex(yellow)),
                              ),
                            ),
                          ),
                        )
                      : Container(),
                  getRequireDesignation()
                      ? Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: TextFormField(
                            controller: _designation,
                            keyboardType: TextInputType.text,
                            textCapitalization: TextCapitalization.words,
                            style: TextStyle(color: Colors.white),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "Please Enter Designation";
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white10,
                              isDense: true,
                              hintText: "eg Customer Care Manager",
                              labelText: "Designation",
                              labelStyle: TextStyle(color: fromHex(yellow)),
                              hintStyle: TextStyle(
                                color: Colors.blueGrey[400],
                              ),
                              border: InputBorder.none,
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: fromHex(yellow)),
                              ),
                            ),
                          ),
                        )
                      : Container(),
                  getRequirePersonToSee()
                      ? Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: TextFormField(
                            controller: _hostsName,
                            keyboardType: TextInputType.text,
                            textCapitalization: TextCapitalization.words,
                            style: TextStyle(color: Colors.white),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "Please Enter person to see";
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white10,
                              isDense: true,
                              hintText: "eg John Doe",
                              labelText: "Person to See",
                              labelStyle: TextStyle(color: fromHex(yellow)),
                              hintStyle: TextStyle(
                                color: Colors.blueGrey[400],
                              ),
                              border: InputBorder.none,
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: fromHex(yellow)),
                              ),
                            ),
                          ),
                        )
                      : Container(),
                  getRequireHouseNo()
                      ? Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: TextFormField(
                            controller: _houseNo,
                            keyboardType: TextInputType.text,
                            textCapitalization: TextCapitalization.characters,
                            style: TextStyle(color: Colors.white),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "Please Enter House No.";
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white10,
                              isDense: true,
                              hintText: "eg 1",
                              labelText: "House No.",
                              labelStyle: TextStyle(color: fromHex(yellow)),
                              hintStyle: TextStyle(
                                color: Colors.blueGrey[400],
                              ),
                              border: InputBorder.none,
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: fromHex(yellow)),
                              ),
                            ),
                          ),
                        )
                      : Container(),
                  getRequireBlockNo()
                      ? Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: TextFormField(
                            controller: _blockNo,
                            keyboardType: TextInputType.text,
                            textCapitalization: TextCapitalization.characters,
                            style: TextStyle(color: Colors.white),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "Please Enter Block ";
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white10,
                              isDense: true,
                              hintText: "eg 1",
                              labelText: "Block",
                              labelStyle: TextStyle(color: fromHex(yellow)),
                              hintStyle: TextStyle(
                                color: Colors.blueGrey[400],
                              ),
                              border: InputBorder.none,
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: fromHex(yellow)),
                              ),
                            ),
                          ),
                        )
                      : Container(),
                  getRequireReasonForVisit()
                      ? Container(
                          margin: EdgeInsets.only(bottom: 10),
                          child: TextFormField(
                            controller: _reason,
                            keyboardType: TextInputType.text,
                            textCapitalization: TextCapitalization.words,
                            style: TextStyle(color: Colors.white),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "Please Enter reason for visit";
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white10,
                              isDense: true,
                              hintText: "eg Official",
                              labelText: "Reason for visit",
                              labelStyle: TextStyle(color: fromHex(yellow)),
                              hintStyle: TextStyle(
                                color: Colors.blueGrey[400],
                              ),
                              border: InputBorder.none,
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: fromHex(yellow)),
                              ),
                            ),
                          ),
                        )
                      : Container(),
                  Container(
                    child: TextFormField(
                      controller: _numberOfVisitors,
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
                      ],
                      style: TextStyle(color: Colors.white),
                      validator: (value) {
                        if (value!.isEmpty) {
                          return "Please Enter number of visitors";
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: Colors.white10,
                        isDense: true,
                        hintText: "eg 1",
                        labelText: "No. Of Visitors",
                        labelStyle: TextStyle(color: fromHex(yellow)),
                        hintStyle: TextStyle(
                          color: Colors.blueGrey[400],
                        ),
                        border: InputBorder.none,
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: fromHex(yellow)),
                        ),
                      ),
                    ),
                  ),
                  ListTile(
                    onTap: () async {
                      String noOfV = "1";
                      if (_numberOfVisitors.text.isNotEmpty) {
                        noOfV = _numberOfVisitors.text;
                      }
                      Visitor _visitor = Visitor(
                        visitorName: visitor?.visitorName ?? '',
                        hostName: "${_hostsName.text}",
                        department: "${_department.text}",
                        designation: "${_designation.text}",
                        noVisitors: "$noOfV",
                        visitReason: "${_reason.text}",
                        documentNumber: visitor?.documentNumber ?? '',
                        documentType: visitor?.documentType ?? '',
                        plateNo: visitor?.plateNo ?? '',
                        passId: visitor?.passId ?? '',
                        items: visitor?.items ?? [],
                        companyName: "${_companyName.text}",
                        officeNo: "${_officeNo.text}",
                        blockNo: "${_blockNo.text}",
                        houseNo: "${_houseNo.text}",
                        vehiclePass: visitor?.vehiclePass ?? '',
                      );
                      setVisitor(_visitor);
                      Navigator.pop(context);
                    },
                    title: Center(
                      child: Text(
                        'Save Details',
                        style: TextStyle(color: Colors.deepOrange),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _itemsDialog(BuildContext context) async {
    List<String> itemTypes = ['None', 'Devices', 'Others'];
    String? itemType = "None";
    TextEditingController _items = TextEditingController();
    TextEditingController _serialNo = TextEditingController();
    List<ItemObj>? items = [];
    if (visitor != null) {
      items = visitor!.items;
    }
    await showDialog(
      context: context,
      builder: (context) => Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        elevation: 0.0,
        backgroundColor: Colors.grey[900],
        child: Container(
            color: Colors.transparent,
            padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                StatefulBuilder(builder: (context, StateSetter setState) {
                  return Container(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 15,
                        ),
                        Row(
                          children: [
                            Container(
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 10.0, right: 10.0),
                                child: Text(
                                  'Specify item type',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 11,
                                      fontStyle: FontStyle.italic),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Container(
                          decoration: BoxDecoration(
                              color: Colors.white10,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5.0))),
                          child: DropdownButtonHideUnderline(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  top: 8.0, bottom: 8.0, left: 5.0),
                              child: ButtonTheme(
                                alignedDropdown: true,
                                child: DropdownButton(
                                  hint: Text('Specify item type'),
                                  isExpanded: true,
                                  isDense: true,
                                  value: itemType,
                                  dropdownColor: Colors.grey[800],
                                  style: TextStyle(
                                    color: fromHex(yellow),
                                  ),
                                  onChanged: (String? val) {
                                    setState(() {
                                      itemType = val;
                                    });
                                  },
                                  items: itemTypes
                                      .map<DropdownMenuItem<String>>(
                                          (String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Container(
                          child: TextFormField(
                            controller: _items,
                            keyboardType: TextInputType.text,
                            textCapitalization: TextCapitalization.words,
                            style: TextStyle(color: Colors.white),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "Please Enter Item";
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white10,
                              isDense: true,
                              hintText: "eg Laptop",
                              labelText: "Specify item",
                              labelStyle: TextStyle(color: fromHex(yellow)),
                              hintStyle: TextStyle(
                                color: Colors.blueGrey[400],
                              ),
                              border: InputBorder.none,
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: fromHex(yellow)),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        itemType == 'Devices'
                            ? Container(
                                child: TextFormField(
                                  controller: _serialNo,
                                  keyboardType: TextInputType.text,
                                  textCapitalization:
                                      TextCapitalization.characters,
                                  style: TextStyle(color: Colors.white),
                                  validator: (value) {
                                    if (value!.isEmpty) {
                                      return "Please Enter Item's Serial No";
                                    }
                                    return null;
                                  },
                                  decoration: InputDecoration(
                                    filled: true,
                                    fillColor: Colors.white10,
                                    isDense: true,
                                    hintText: "eg SN0000001",
                                    labelText: "Specify item's Serial No.",
                                    labelStyle:
                                        TextStyle(color: fromHex(yellow)),
                                    hintStyle: TextStyle(
                                      color: Colors.blueGrey[400],
                                    ),
                                    border: InputBorder.none,
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide:
                                          BorderSide(color: fromHex(yellow)),
                                    ),
                                  ),
                                ),
                              )
                            : Container(),
                      ],
                    ),
                  );
                }),
                ListTile(
                  onTap: () async {
                    if (_items.text.isNotEmpty) {
                      var i = ItemObj(
                          name: "${_items.text}",
                          serialNo: "${_serialNo.text}");
                      items!.add(i);
                    }
                    if (items!.isNotEmpty) {
                      Visitor _visitor = Visitor(
                          visitorName: visitor?.visitorName ?? '',
                          hostName: visitor?.hostName ?? '',
                          noVisitors: visitor?.noVisitors ?? '',
                          department: visitor?.department ?? '',
                          designation: visitor?.designation ?? '',
                          visitReason: visitor?.visitReason ?? '',
                          documentNumber: visitor?.documentNumber ?? '',
                          documentType: visitor?.documentType ?? '',
                          plateNo: visitor?.plateNo ?? '',
                          passId: visitor?.passId ?? '',
                          items: items,
                          companyName: visitor?.companyName ?? '',
                          officeNo: visitor?.officeNo ?? '',
                          houseNo: visitor?.houseNo ?? '',
                          blockNo: visitor?.blockNo ?? '',
                          vehiclePass: visitor?.vehiclePass ?? '');
                      setVisitor(_visitor);

                      Navigator.pop(context);
                    }
                    if (items.isEmpty) {
                      Navigator.pop(context);
                    }
                  },
                  title: Center(
                    child: Text(
                      'Add Item',
                      style: TextStyle(color: Colors.deepOrange),
                    ),
                  ),
                ),
              ],
            )),
      ),
    );
  }

  void _carPlateDialog(BuildContext context) async {
    TextEditingController _noPlate = TextEditingController();
    if (visitor != null) {
      _noPlate.text = visitor!.plateNo!;
    }
    await showDialog(
      context: context,
      builder: (context) => Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        elevation: 0.0,
        backgroundColor: Colors.grey[900],
        child: Container(
          color: Colors.transparent,
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
          child: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 15,
                ),
                Container(
                  child: TextFormField(
                    controller: _noPlate,
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.characters,
                    style: TextStyle(color: Colors.white),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Please Enter car Plate";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white10,
                      isDense: true,
                      hintText: "eg K** 000A",
                      labelText: "Car Plate No.",
                      labelStyle: TextStyle(color: fromHex(yellow)),
                      hintStyle: TextStyle(
                        color: Colors.blueGrey[400],
                      ),
                      border: InputBorder.none,
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: fromHex(yellow)),
                      ),
                    ),
                  ),
                ),
                ListTile(
                  onTap: () async {
                    Visitor _visitor = Visitor(
                        visitorName: visitor?.visitorName ?? '',
                        hostName: visitor?.hostName ?? '',
                        noVisitors: visitor?.noVisitors ?? '',
                        department: visitor?.department ?? '',
                        designation: visitor?.designation ?? '',
                        visitReason: visitor?.visitReason ?? '',
                        documentType: visitor?.documentType ?? '',
                        documentNumber: visitor?.documentNumber ?? '',
                        plateNo: "${_noPlate.text}",
                        passId: visitor?.passId ?? '',
                        items: visitor?.items ?? [],
                        companyName: visitor?.companyName ?? '',
                        officeNo: visitor?.officeNo ?? '',
                        houseNo: visitor?.houseNo ?? '',
                        blockNo: visitor?.blockNo ?? '',
                        vehiclePass: visitor?.vehiclePass ?? '');
                    setVisitor(_visitor);
                    Navigator.pop(context);
                  },
                  title: Center(
                    child: Text(
                      'Save Details',
                      style: TextStyle(color: Colors.deepOrange),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _passportDialog(BuildContext context) async {
    TextEditingController _name = TextEditingController();
    TextEditingController _docId = TextEditingController();

    if (visitor != null) {
      _name.text = visitor!.visitorName!;
      _docId.text = visitor!.documentNumber!;
    }
    await showDialog(
      context: context,
      builder: (context) => Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        elevation: 0.0,
        backgroundColor: Colors.grey[900],
        child: Container(
          color: Colors.transparent,
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
          child: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 15,
                ),
                Container(
                  child: TextFormField(
                    controller: _name,
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.words,
                    style: TextStyle(color: Colors.white),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Please Enter visitor's name";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white10,
                      isDense: true,
                      hintText: "eg John Doe",
                      labelText: "visitor's name",
                      labelStyle: TextStyle(color: fromHex(yellow)),
                      hintStyle: TextStyle(
                        color: Colors.blueGrey[400],
                      ),
                      border: InputBorder.none,
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: fromHex(yellow)),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: TextFormField(
                    controller: _docId,
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.characters,
                    style: TextStyle(color: Colors.white),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Please Enter visitor's Passport no";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white10,
                      isDense: true,
                      hintText: "eg A*****8",
                      labelText: "visitor's Passport Number",
                      labelStyle: TextStyle(color: fromHex(yellow)),
                      hintStyle: TextStyle(
                        color: Colors.blueGrey[400],
                      ),
                      border: InputBorder.none,
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: fromHex(yellow)),
                      ),
                    ),
                  ),
                ),
                ListTile(
                  onTap: () async {
                    Visitor _visitor = Visitor(
                        visitorName: "${_name.text}",
                        hostName: visitor?.hostName ?? '',
                        noVisitors: visitor?.noVisitors ?? '',
                        department: visitor?.department ?? '',
                        designation: visitor?.designation ?? '',
                        visitReason: visitor?.visitReason ?? '',
                        documentType: 'Passport',
                        documentNumber: "${_docId.text}",
                        plateNo: visitor?.plateNo ?? '',
                        passId: visitor?.passId ?? '',
                        items: visitor?.items ?? [],
                        companyName: visitor?.companyName ?? '',
                        officeNo: visitor?.officeNo ?? '',
                        houseNo: visitor?.houseNo ?? '',
                        blockNo: visitor?.blockNo ?? '',
                        vehiclePass: visitor?.vehiclePass ?? '');
                    setVisitor(_visitor);
                    Navigator.pop(context);
                  },
                  title: Center(
                    child: Text(
                      'Save Details',
                      style: TextStyle(color: Colors.deepOrange),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _idDialog(BuildContext context) async {
    TextEditingController _name = TextEditingController();
    TextEditingController _docId = TextEditingController();

    if (visitor != null) {
      _name.text = visitor!.visitorName!;
      _docId.text = visitor!.documentNumber!;
    }
    await showDialog(
      context: context,
      builder: (context) => Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        elevation: 0.0,
        backgroundColor: Colors.grey[900],
        child: Container(
          color: Colors.transparent,
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
          child: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 15,
                ),
                Container(
                  child: TextFormField(
                    controller: _name,
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.words,
                    style: TextStyle(color: Colors.white),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Please Enter visitor's name";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white10,
                      isDense: true,
                      hintText: "eg John Doe",
                      labelText: "visitor's name",
                      labelStyle: TextStyle(color: fromHex(yellow)),
                      hintStyle: TextStyle(
                        color: Colors.blueGrey[400],
                      ),
                      border: InputBorder.none,
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: fromHex(yellow)),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: TextFormField(
                    controller: _docId,
                    keyboardType: TextInputType.number,
                    style: TextStyle(color: Colors.white),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Please Enter visitor's ID no";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white10,
                      isDense: true,
                      hintText: "eg 23*****8",
                      labelText: "visitor's ID Number",
                      labelStyle: TextStyle(color: fromHex(yellow)),
                      hintStyle: TextStyle(
                        color: Colors.blueGrey[400],
                      ),
                      border: InputBorder.none,
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: fromHex(yellow)),
                      ),
                    ),
                  ),
                ),
                ListTile(
                  onTap: () async {
                    Visitor _visitor = Visitor(
                        visitorName: "${_name.text}",
                        hostName: visitor?.hostName ?? '',
                        noVisitors: visitor?.noVisitors ?? '',
                        department: visitor?.department ?? '',
                        designation: visitor?.designation ?? '',
                        visitReason: visitor?.visitReason ?? '',
                        documentType: 'National ID',
                        documentNumber: "${_docId.text}",
                        plateNo: visitor?.plateNo ?? '',
                        passId: visitor?.passId ?? '',
                        items: visitor?.items ?? [],
                        companyName: visitor?.companyName ?? '',
                        officeNo: visitor?.officeNo ?? '',
                        houseNo: visitor?.houseNo ?? '',
                        blockNo: visitor?.blockNo ?? '',
                        vehiclePass: visitor?.vehiclePass ?? '');
                    setVisitor(_visitor);
                    Navigator.pop(context);
                  },
                  title: Center(
                    child: Text(
                      'Save Details',
                      style: TextStyle(color: Colors.deepOrange),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> getPassId(String? passId) async {
    bool alreadyExists = false;

    if (getRequirePass() && passId!.isEmpty) {
      showToast("Pass Id is empty");
      return;
    }
    if (passId!.isNotEmpty) {
      _visitors.forEach((element) {
        alreadyExists = element.passId == passId;
      });
      if (alreadyExists) {
        showToast("Pass ID is already in use!!!");
        return;
      }
    }
    final DateTime now = DateTime.now();
    final DateFormat formatter = DateFormat('yyyy-MM-dd HH:mm:ss');
    String formatted = formatter.format(now);
    print("Current time $formatted");
    Visitor _visitor = Visitor(
        visitorName: visitor?.visitorName ?? '',
        hostName: visitor?.hostName ?? 'Unspecified',
        noVisitors: visitor?.noVisitors ?? '1',
        department: visitor?.department ?? 'Unspecified',
        designation: visitor?.designation ?? 'Unspecified',
        visitReason: visitor?.visitReason ?? 'Unknown',
        documentNumber: visitor?.documentNumber ?? '',
        documentType: visitor?.documentType ?? '',
        plateNo: visitor?.plateNo ?? '',
        passId: passId,
        items: visitor?.items ?? [],
        datetimeIn: formatted,
        datetimeOut: visitor?.datetimeOut ?? '',
        companyName: visitor?.companyName ?? '',
        officeNo: visitor?.officeNo ?? '',
        houseNo: visitor?.houseNo ?? '',
        blockNo: visitor?.blockNo ?? '',
        vehiclePass: visitor?.vehiclePass ?? '');

    setState(() {
      visitor = _visitor;
    });

    bool isConnected = await checkConnection();
    if (isConnected) {
      Map<String, dynamic> data = {
        'visitor_name': "${_visitor.visitorName}",
        'host_name': "${_visitor.hostName}",
        'department': "${_visitor.department}",
        'designation': "${_visitor.designation}",
        'no_visitors': "${_visitor.noVisitors}",
        'documentNumber': "${_visitor.documentNumber}",
        'document_type': "${_visitor.documentType}",
        'plateNo': "${_visitor.plateNo}",
        'pass_id': "${_visitor.passId}",
        'visit_reason': "${_visitor.visitReason}",
        'datetime_in': "${_visitor.datetimeIn}",
        'datetime_out': null,
        'item': _visitor.items,
        'site_id': "${getPlanner()!.plannerId}",
        'status': "In",
      };
      try {
        var r = await widget.model.createVisitor(data);
        if (r['success']) {
          var d = CreateVisitorResponse.fromJson(r['response']);
          if (!d.error!) {
            showToast(
                "${visitor!.visitorName} has been successfully checked in");
            visitor = null;
            visitorBox.delete('visitor');
            refreshData();
          } else {
            showToast(d.message ?? "Failed");
          }
        } else {
          showErrorDialog(r['response'], context, "Visitor check in");
        }
      } catch (e) {
        showToast(e.toString());
      }
    }
    if (!isConnected) {
      final visitors = Hive.box('visitors_in');
      visitors.add(visitor);
      showToast("${visitor!.visitorName} has been successfully checked in");
      setState(() {
        visitor = null;
        visitorBox.delete('visitor');
      });
    }
  }

  Future<void> refreshData() async {
    final DateTime maxDate = DateTime.now();
    final DateTime minDate = DateTime.now().subtract(Duration(days: 1));
    final DateFormat formatter = DateFormat('yyyy-MM-dd HH:mm:ss');
    String max = formatter.format(maxDate);
    String min = formatter.format(minDate);
    Map<String, String> data = {
      'datetime_in_min': "$min",
      'datetime_in_max': "$max",
      'site_id': "${getPlanner()!.plannerId}"
    };
    var r = await widget.model.refreshVisitors(data);
    if (r['success']) {
    } else {
      showErrorDialog(r['response'], context, "get visitors");
    }
    _visitors.clear();
    if (visitors.isNotEmpty) {
      _visitors.addAll(visitors.values as Iterable<Visitor>);
    }
    _visitors.addAll(widget.model.visitorsIn);
    setState(() {});
  }

  void setVisitor(Visitor v) {
    visitorBox.put('visitor', v);
    setState(() {
      visitor = v;
    });
  }

  _visitorObject() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
      child: Card(
        color: Colors.black,
        child: Column(
          children: [
            Stack(
              children: [
                Container(
                  margin: EdgeInsets.only(bottom: 25),
                  decoration: BoxDecoration(
                    border: Border.all(color: fromHex(yellow).withOpacity(0.3)),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Visitor's Info",
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                      Container(
                        padding: EdgeInsets.all(15),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                visitor!.hostName!.isEmpty
                                    ? Container()
                                    : Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "${visitor!.hostName}",
                                              style: TextStyle(
                                                  fontSize: 15.6,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white),
                                            ),
                                            Text(
                                              "Host",
                                              style: TextStyle(
                                                fontSize: 11,
                                                fontStyle: FontStyle.italic,
                                                color: Colors.white54,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                visitor!.noVisitors!.isEmpty
                                    ? Container()
                                    : Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "${visitor!.noVisitors}",
                                              style: TextStyle(
                                                  fontSize: 15.6,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white),
                                            ),
                                            Text(
                                              'No. of Visitors',
                                              style: TextStyle(
                                                fontSize: 11,
                                                fontStyle: FontStyle.italic,
                                                color: Colors.white54,
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                              ],
                            ),
                            Row(
                              children: [
                                visitor!.department!.isEmpty
                                    ? Container()
                                    : Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Text(
                                              "${visitor!.department}",
                                              style: TextStyle(
                                                  fontSize: 15.6,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white),
                                            ),
                                            Text(
                                              "Department",
                                              style: TextStyle(
                                                fontSize: 11,
                                                fontStyle: FontStyle.italic,
                                                color: Colors.white54,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                visitor!.designation!.isEmpty
                                    ? Container()
                                    : Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Text(
                                              "${visitor!.designation}",
                                              style: TextStyle(
                                                  fontSize: 15.6,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white),
                                            ),
                                            Text(
                                              'Designation',
                                              style: TextStyle(
                                                fontSize: 11,
                                                fontStyle: FontStyle.italic,
                                                color: Colors.white54,
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                              ],
                            ),
                            Row(
                              children: [
                                visitor!.companyName!.isEmpty
                                    ? Container()
                                    : Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Text(
                                              "${visitor!.companyName}",
                                              style: TextStyle(
                                                  fontSize: 15.6,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white),
                                            ),
                                            Text(
                                              "Company",
                                              style: TextStyle(
                                                fontSize: 11,
                                                fontStyle: FontStyle.italic,
                                                color: Colors.white54,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                visitor!.officeNo!.isEmpty
                                    ? Container()
                                    : Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Text(
                                              "${visitor!.officeNo}",
                                              style: TextStyle(
                                                  fontSize: 15.6,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white),
                                            ),
                                            Text(
                                              'Office No.',
                                              style: TextStyle(
                                                fontSize: 11,
                                                fontStyle: FontStyle.italic,
                                                color: Colors.white54,
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                              ],
                            ),
                            Row(
                              children: [
                                visitor!.houseNo!.isEmpty
                                    ? Container()
                                    : Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Text(
                                              "${visitor!.houseNo}",
                                              style: TextStyle(
                                                  fontSize: 15.6,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white),
                                            ),
                                            Text(
                                              "House No.",
                                              style: TextStyle(
                                                fontSize: 11,
                                                fontStyle: FontStyle.italic,
                                                color: Colors.white54,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                visitor!.blockNo!.isEmpty
                                    ? Container()
                                    : Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Text(
                                              "${visitor!.blockNo}",
                                              style: TextStyle(
                                                  fontSize: 15.6,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white),
                                            ),
                                            Text(
                                              'Block No.',
                                              style: TextStyle(
                                                fontSize: 11,
                                                fontStyle: FontStyle.italic,
                                                color: Colors.white54,
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                              ],
                            ),
                            Row(
                              children: [
                                visitor!.visitReason!.isEmpty
                                    ? Container()
                                    : Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Text(
                                              "${visitor!.visitReason}",
                                              style: TextStyle(
                                                  fontSize: 15.6,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white),
                                            ),
                                            Text(
                                              'reason',
                                              style: TextStyle(
                                                fontSize: 11,
                                                fontStyle: FontStyle.italic,
                                                color: Colors.white54,
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                              ],
                            ),
                            Row(
                              children: [
                                visitor!.visitorName!.isEmpty
                                    ? Container()
                                    : Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 10),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "${visitor!.visitorName!.trim()}",
                                                style: TextStyle(
                                                    fontSize: 15.6,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.white),
                                              ),
                                              Text(
                                                'Name',
                                                style: TextStyle(
                                                  fontSize: 11,
                                                  fontStyle: FontStyle.italic,
                                                  color: Colors.white54,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                visitor!.documentNumber!.isEmpty
                                    ? Container()
                                    : Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 10),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "${visitor!.documentNumber!.trim()}",
                                                style: TextStyle(
                                                    fontSize: 15.6,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.white),
                                              ),
                                              Text(
                                                'ID/Passport',
                                                style: TextStyle(
                                                  fontSize: 11,
                                                  fontStyle: FontStyle.italic,
                                                  color: Colors.white54,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                              ],
                            ),
                            Row(
                              children: [
                                visitor!.plateNo!.isEmpty
                                    ? Container()
                                    : Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "${visitor!.plateNo!.trim()}",
                                              style: TextStyle(
                                                  fontSize: 15.6,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white),
                                            ),
                                            Text(
                                              'Plate No',
                                              style: TextStyle(
                                                fontSize: 11,
                                                fontStyle: FontStyle.italic,
                                                color: Colors.white54,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                visitor!.passId!.isEmpty
                                    ? Container()
                                    : Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "${visitor!.passId}",
                                              style: TextStyle(
                                                  fontSize: 15.6,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white),
                                            ),
                                            Text(
                                              'Pass ID',
                                              style: TextStyle(
                                                fontSize: 11,
                                                fontStyle: FontStyle.italic,
                                                color: Colors.white54,
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                              ],
                            ),
                            Row(
                              children: [
                                visitor!.vehiclePass!.isEmpty
                                    ? Container()
                                    : Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "${visitor!.vehiclePass}",
                                              style: TextStyle(
                                                  fontSize: 15.6,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white),
                                            ),
                                            Text(
                                              'Vehicle Pass',
                                              style: TextStyle(
                                                fontSize: 11,
                                                fontStyle: FontStyle.italic,
                                                color: Colors.white54,
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                              ],
                            ),
                            visitor!.items!.isEmpty
                                ? Container()
                                : Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(top: 8.0),
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: Tags(
                                                itemCount:
                                                    visitor!.items!.length,
                                                alignment: WrapAlignment.start,
                                                itemBuilder: (int index) {
                                                  var type =
                                                      visitor!.items![index];
                                                  return Tooltip(
                                                      message:
                                                          "S/N: ${type.serialNo}",
                                                      child: ItemTags(
                                                        textStyle: TextStyle(
                                                            fontSize: 14,
                                                            fontWeight:
                                                                FontWeight
                                                                    .normal,
                                                            color: fromHex(
                                                                yellow)),
                                                        color: Colors.white10
                                                            .withOpacity(0.07),
                                                        activeColor: Colors
                                                            .white10
                                                            .withOpacity(0.07),
                                                        singleItem: true,
                                                        index: index,
                                                        title: type.name!,
                                                        textColor: Colors.white,
                                                        border: Border.all(
                                                            color:
                                                                Colors.black),
                                                        removeButton:
                                                            ItemTagsRemoveButton(
                                                          color:
                                                              Colors.deepOrange,
                                                          backgroundColor:
                                                              Colors
                                                                  .transparent,
                                                          onRemoved: () {
                                                            setState(() {
                                                              visitor!.items!
                                                                  .removeAt(
                                                                      index);
                                                            });
                                                            return true;
                                                          },
                                                        ), // OR nu
                                                      ));
                                                },
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 10, top: 2),
                                        child: Text(
                                          'Items',
                                          style: TextStyle(
                                            fontSize: 11,
                                            fontStyle: FontStyle.italic,
                                            color: Colors.white54,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                    bottom: 6,
                    left: 10,
                    right: 10,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          height: 40,
                          width: 200,
                          decoration: BoxDecoration(
                            color: fromHex(deepOrange),
                            borderRadius: BorderRadius.circular(15),
                          ),
                          child: Row(
                            children: [
                              Expanded(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                        "${visitor?.datetimeIn?.substring(10).trim() ?? ''}"),
                                    Text(
                                      'IN',
                                      style: TextStyle(fontSize: 11),
                                    )
                                  ],
                                ),
                              ),
                              Icon(
                                Icons.access_time,
                                color: Colors.black,
                              ),
                              Expanded(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text("${visitor?.datetimeOut ?? '_'}"),
                                    Text(
                                      'OUT',
                                      style: TextStyle(fontSize: 11),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ))
              ],
            ),
          ],
        ),
      ),
    );
  }

  void setVehiclePassId(String? p) {
    bool exists = false;
    if (getRequireVehiclePass() && p!.isEmpty) {
      showToast("Id is empty, try again");
      return;
    }
    if (p!.isNotEmpty) {
      _visitors.forEach((element) {
        exists = element.vehiclePass == p;
      });
    }
    if (exists) {
      showToast("Pass ID is already IN!!!");
      return;
    }

    Visitor v = Visitor(
        visitorName: visitor?.visitorName ?? '',
        hostName: visitor?.hostName ?? 'Unspecified',
        noVisitors: visitor?.noVisitors ?? '1',
        department: visitor?.department ?? 'Unspecified',
        designation: visitor?.designation ?? 'Unspecified',
        visitReason: visitor?.visitReason ?? 'Unknown',
        documentNumber: visitor?.documentNumber ?? '',
        documentType: visitor?.documentType ?? '',
        plateNo: visitor?.plateNo ?? '',
        passId: passId,
        items: visitor?.items ?? [],
        datetimeOut: visitor?.datetimeOut ?? '',
        companyName: visitor?.companyName ?? '',
        officeNo: visitor?.officeNo ?? '',
        houseNo: visitor?.houseNo ?? '',
        blockNo: visitor?.blockNo ?? '',
        vehiclePass: p);
    setVisitor(v);
  }
}
