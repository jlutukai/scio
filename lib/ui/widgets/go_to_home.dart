import 'package:flutter/material.dart';

class GoToHome extends StatefulWidget {
  @override
  _GoToHomeState createState() => _GoToHomeState();
}

class _GoToHomeState extends State<GoToHome> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Column(
        children: [
          Expanded(child: Center(child: Text('Go back to menu'),))
        ],
      ),
    );
  }
}
