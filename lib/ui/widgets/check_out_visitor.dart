import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_tags/flutter_tags.dart';
import 'package:hive/hive.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:permission_handler/permission_handler.dart';
// import 'package:qrscan/qrscan.dart' as scanner;
import 'package:scio_security/core/enums/view_state.dart';
import 'package:scio_security/core/models/check_out_visitor_response.dart';
import 'package:scio_security/core/models/create_visitor_response.dart';
import 'package:scio_security/core/models/local/localModels.dart';
import 'package:scio_security/core/viewmodels/check_out_visitor_model.dart';
import 'package:scio_security/core/viewmodels/visitors_log_model.dart';
import 'package:scio_security/ui/screens/base_view.dart';
import 'package:scio_security/ui/screens/test/camera_page.dart';
import 'package:scio_security/ui/widgets/visitors_log_non_conformance.dart';
import 'package:scio_security/utils/useful.dart';

class CheckOutVisitor extends StatefulWidget {
  final VisitorsLogModel model;
  final bool? requirePass;

  CheckOutVisitor(this.model, this.requirePass);

  @override
  _CheckOutVisitorState createState() => _CheckOutVisitorState();
}

class _CheckOutVisitorState extends State<CheckOutVisitor> {
  final visitorsIn = Hive.box('visitors_in');
  final visitorsOut = Hive.box('visitors_out');
  Box<String> accountInfo = Hive.box<String>("accountInfo");
  List<Visitor> _visitors = [];
  Visitor? visitor;
  String passId = '';
  int index = 0;

  void getVisitor(String? passId) {
    if (accountInfo.get("requirePass") == 'true') {
      if (visitorsIn.isNotEmpty) {
        for (var i = 0; i <= visitorsIn.values.length; i++) {
          if (visitorsIn.getAt(i).passId == passId ||
              visitorsIn.getAt(i).vehiclePass == passId) {
            print(
                '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ offline');
            setState(() {
              visitor = visitorsIn.getAt(i);
              index = i;
            });
            break;
          }
        }
      } else {
        widget.model.visitorsIn.forEach((element) {
          if (element.passId == passId || element.vehiclePass == passId) {
            setState(() {
              visitor = element;
            });
          }
        });
        if (visitor == null) {
          showToast("Pass ID Not Found");
        }
      }
    } else {
      if (visitorsIn.isNotEmpty) {
        for (var i = 0; i <= visitorsIn.values.length; i++) {
          if (visitorsIn.getAt(i).documentNumber == passId) {
            setState(() {
              visitor = visitorsIn.getAt(i);
              index = i;
            });
            break;
          }
        }
      }
      widget.model.visitorsIn.forEach((element) {
        if (element.documentNumber == passId) {
          setState(() {
            visitor = element;
          });
        }
      });
      if (visitor == null) {
        showToast("Not Found");
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<CheckOutVisitorModel>(
      onModelReady: (model) async {
        await initData(model);
      },
      builder: (context, model, child) => Container(
        margin: EdgeInsets.only(top: 15),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.transparent,
            title: Text(
              'Check Out Visitor',
              style: TextStyle(
                  color: fromHex(deepOrange), fontWeight: FontWeight.bold),
            ),
            centerTitle: true,
            automaticallyImplyLeading: false,
          ),
          body: model.state == ViewState.Idle
              ? SingleChildScrollView(
                  child: Column(
                    children: [
                      visitor != null ? _visitorDetails() : Container(),
                      accountInfo.get("requirePass") == 'true'
                          ? Dismissible(
                              key: UniqueKey(),
                              direction: DismissDirection.startToEnd,
                              onDismissed: (DismissDirection direction) async {
                                setState(() {});
                                // Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => TestPage(getPassId:getVisitor)));
                                var status = await Permission.camera.request();
                                if (status.isGranted) {
                                  /// Todo : Scanner
                                  // String? p = await scanner.scan();
                                  // getVisitor(p);
                                }
                              },
                              child: InkWell(
                                onTap: () async {
                                  // Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => TestPage(getPassId:getVisitor)));
                                  var status =
                                      await Permission.camera.request();
                                  if (status.isGranted) {
                                    /// Todo : Scanner
                                    // String? p = await scanner.scan();
                                    // getVisitor(p);
                                  }
                                },
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      vertical: 8.0, horizontal: 15),
                                  child: Card(
                                    color: Colors.white10.withOpacity(0.07),
                                    child: Container(
                                      // color: fromHex("#114259"),
                                      height: 60,
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 10, horizontal: 20),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            children: [
                                              Icon(
                                                Icons.qr_code_sharp,
                                                size: 32,
                                                color: fromHex(yellow),
                                              ),
                                              SizedBox(
                                                width: 60,
                                              ),
                                              Text(
                                                "Visitor's Pass",
                                                style: TextStyle(
                                                    color: fromHex(yellow),
                                                    fontWeight: FontWeight.bold,
                                                    fontSize: 18),
                                              )
                                            ],
                                          ),
                                          Icon(
                                            Icons.arrow_forward_ios_sharp,
                                            color: fromHex(yellow),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            )
                          : Dismissible(
                              key: UniqueKey(),
                              // direction: DismissDirection.startToEnd,
                              onDismissed: (DismissDirection direction) {
                                if (direction == DismissDirection.startToEnd) {
                                  getImage(context, "ID");
                                  setState(() {});
                                } else {
                                  getImage(context, "Passport");
                                  setState(() {});
                                }
                              },
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    vertical: 8.0, horizontal: 15),
                                child: Card(
                                  color: Colors.white10.withOpacity(0.07),
                                  child: Container(
                                    // color: fromHex("#114259"),
                                    height: 60,
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 20),
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: InkWell(
                                            onTap: () {
                                              getImage(context,
                                                  "Passport"); //passport
                                            },
                                            onLongPress: () {
                                              _passportDialog(context);
                                            },
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      vertical: 12.0),
                                              child: Row(
                                                children: [
                                                  Icon(
                                                    Icons.arrow_back_ios_sharp,
                                                    color: fromHex(yellow),
                                                  ),
                                                  Expanded(
                                                    child: Center(
                                                      child: Text(
                                                        "Passport",
                                                        style: TextStyle(
                                                            color:
                                                                fromHex(yellow),
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: 18),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                        Icon(
                                          Icons.camera_alt_outlined,
                                          size: 32,
                                          color: fromHex(yellow),
                                        ),
                                        Expanded(
                                          child: InkWell(
                                            onTap: () {
                                              getImage(context, "ID"); // ID
                                            },
                                            onLongPress: () {
                                              _idDialog(context);
                                            },
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      vertical: 12.0),
                                              child: Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                                children: [
                                                  Expanded(
                                                    child: Center(
                                                      child: Text(
                                                        "ID",
                                                        style: TextStyle(
                                                            color:
                                                                fromHex(yellow),
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontSize: 18),
                                                      ),
                                                    ),
                                                  ),
                                                  Icon(
                                                    Icons
                                                        .arrow_forward_ios_sharp,
                                                    color: fromHex(yellow),
                                                  )
                                                ],
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ),
                      Dismissible(
                        key: UniqueKey(),
                        direction: DismissDirection.startToEnd,
                        onDismissed: (DismissDirection direction) {
                          setState(() {});
                          if (visitor == null) {
                            showToast(accountInfo.get("requirePass") == 'true'
                                ? "Scan visitor's pass first"
                                : "Scan Passport / ID first");
                            return;
                          }
                          checkOutVisitor(model);
                        },
                        child: InkWell(
                          onTap: () {
                            if (visitor == null) {
                              showToast(accountInfo.get("requirePass") == 'true'
                                  ? "Scan visitor's pass first"
                                  : "Scan Passport / ID first");
                              return;
                            }
                            checkOutVisitor(model);
                          },
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8.0, horizontal: 15),
                            child: Card(
                              color: Colors.white10.withOpacity(0.07),
                              child: Container(
                                // color: fromHex("#114259"),
                                height: 60,
                                padding: const EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 20),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Icon(
                                          Icons.done_outline_rounded,
                                          size: 32,
                                          color: fromHex(yellow),
                                        ),
                                        SizedBox(
                                          width: 60,
                                        ),
                                        Text(
                                          "Confirm Checkout",
                                          style: TextStyle(
                                              color: fromHex(yellow),
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18),
                                        )
                                      ],
                                    ),
                                    Icon(
                                      Icons.arrow_forward_ios_sharp,
                                      color: fromHex(yellow),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      Dismissible(
                        key: UniqueKey(),
                        direction: DismissDirection.startToEnd,
                        onDismissed: (DismissDirection direction) {
                          setState(() {});
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => VisitorsLogNonConf()));
                        },
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        VisitorsLogNonConf()));
                          },
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 8.0, horizontal: 15),
                            child: Card(
                              color: Colors.white10.withOpacity(0.07),
                              child: Container(
                                // color: fromHex("#114259"),
                                height: 60,
                                padding: const EdgeInsets.symmetric(
                                    vertical: 10, horizontal: 20),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Icon(
                                          Icons.close_outlined,
                                          size: 32,
                                          color: fromHex(yellow),
                                        ),
                                        SizedBox(
                                          width: 60,
                                        ),
                                        Text(
                                          "Non Conformance",
                                          style: TextStyle(
                                              color: fromHex(yellow),
                                              fontWeight: FontWeight.bold,
                                              fontSize: 18),
                                        )
                                      ],
                                    ),
                                    Icon(
                                      Icons.arrow_forward_ios_sharp,
                                      color: fromHex(yellow),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              : Column(
                  children: [
                    Expanded(
                      child: Center(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                                height: 200,
                                width: 200,
                                child: Lottie.asset('assets/loading.json')),
                            Text(
                              'Please wait \n Loading...',
                              style: TextStyle(color: fromHex(deepOrange)),
                            )
                          ],
                        ),
                      ),
                    )
                  ],
                ),
        ),
      ),
    );
  }

  Future getImage(BuildContext context, String s) async {
    DeviceDetails? d = getDeviceModel();
    if (d == null) {
      showToast("Unable to scan, Please contact admin");
      return;
    }
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => CameraExampleHome(
                s: s, setVisitorsDetails: setVisitorsDetails, isSetUp: false,)));
  }

  setVisitorsDetails(String id, String name, String type) {
    getVisitor(id);
  }

  void _passportDialog(BuildContext context) async {
    TextEditingController _docId = TextEditingController();
    await showDialog(
      context: context,
      builder: (context) => Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        elevation: 0.0,
        backgroundColor: Colors.grey[900],
        child: Container(
          color: Colors.transparent,
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
          child: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 15,
                ),
                Container(
                  child: TextFormField(
                    controller: _docId,
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.characters,
                    style: TextStyle(color: Colors.white),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Please Enter visitor's Passport no";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white10,
                      isDense: true,
                      hintText: "eg A*****8",
                      labelText: "visitor's Passport Number",
                      labelStyle: TextStyle(color: fromHex(yellow)),
                      hintStyle: TextStyle(
                        color: Colors.blueGrey[400],
                      ),
                      border: InputBorder.none,
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: fromHex(yellow)),
                      ),
                    ),
                  ),
                ),
                ListTile(
                  onTap: () async {
                    if (_docId.text.isNotEmpty) {
                      getVisitor(_docId.text.trim());
                    }
                    Navigator.pop(context);
                  },
                  title: Center(
                    child: Text(
                      'Search Visitor',
                      style: TextStyle(color: Colors.deepOrange),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _idDialog(BuildContext context) async {
    TextEditingController _docId = TextEditingController();

    await showDialog(
      context: context,
      builder: (context) => Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        elevation: 0.0,
        backgroundColor: Colors.grey[900],
        child: Container(
          color: Colors.transparent,
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
          child: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 15,
                ),
                Container(
                  child: TextFormField(
                    controller: _docId,
                    keyboardType: TextInputType.number,
                    style: TextStyle(color: Colors.white),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Please Enter visitor's ID no";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white10,
                      isDense: true,
                      hintText: "eg 23*****8",
                      labelText: "visitor's ID Number",
                      labelStyle: TextStyle(color: fromHex(yellow)),
                      hintStyle: TextStyle(
                        color: Colors.blueGrey[400],
                      ),
                      border: InputBorder.none,
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: fromHex(yellow)),
                      ),
                    ),
                  ),
                ),
                ListTile(
                  onTap: () async {
                    if (_docId.text.isNotEmpty) {
                      getVisitor(_docId.text.trim());
                    }
                    Navigator.pop(context);
                  },
                  title: Center(
                    child: Text(
                      'Search Visitor',
                      style: TextStyle(color: Colors.deepOrange),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> checkOutVisitor(CheckOutVisitorModel model) async {
    final DateTime now = DateTime.now();
    final DateFormat formatter = DateFormat('yyyy-MM-dd HH:mm:ss');
    String formatted = formatter.format(now);
    Visitor _visitor = Visitor(
        id: visitor?.id ?? '',
        visitorName: visitor?.visitorName ?? '',
        hostName: visitor?.hostName ?? '',
        noVisitors: visitor?.noVisitors ?? '1',
        department: visitor?.department ?? '',
        designation: visitor?.designation ?? '',
        visitReason: visitor?.visitReason ?? '',
        documentType: visitor?.documentType ?? '',
        documentNumber: visitor?.documentNumber ?? '',
        plateNo: visitor?.plateNo ?? '',
        passId: visitor?.passId ?? '',
        items: visitor?.items ?? [],
        datetimeIn: visitor?.datetimeIn ?? '',
        datetimeOut: formatted);
    setState(() {
      visitor = _visitor;
    });
    bool isConnected = await checkConnection(); //check internet connection
    if (isConnected) {
      if (_visitor.id == null || _visitor.id!.isEmpty) {
        // checks if visitor is not already created online
        var b = jsonEncode(_visitor.items!.map((e) => e.toJson()).toList());
        Map<String, dynamic> data = {
          'visitor_name': "${_visitor.visitorName}",
          'host_name': "${_visitor.hostName}",
          'department': "${_visitor.department}",
          'designation': "${_visitor.designation}",
          'no_visitors': "${_visitor.noVisitors}",
          'documentNumber': "${_visitor.documentNumber}",
          'document_type': "${_visitor.documentType}",
          'plateNo': "${_visitor.plateNo}",
          'pass_id': "${_visitor.passId}",
          'visit_reason': "${_visitor.visitReason}",
          'datetime_in': "${_visitor.datetimeIn}",
          'datetime_out': "${_visitor.datetimeOut}",
          'item': _visitor.items,
          'site_id': "${getPlanner()!.plannerId}",
          'status': "Out",
        };
        print(data);
        try {
          var r = await model.createVisitor(data);
          if (r['success']) {
            var d = CreateVisitorResponse.fromJson(r['response']);
            if (!d.error!) {
              showToast(
                  "${visitor!.visitorName} has been successfully checked out");
              visitorsIn.deleteAt(index);
              setState(() {
                visitor = null;
              });
              refreshData();
            } else {
              showToast(d.message ?? "Failed");
            }
          } else {
            showErrorDialog(r['response'], context, "visitor check out");
          }
        } catch (e) {
          // showToast(e.toString());
        }
      }
      if (_visitor.id != null || _visitor.id!.isNotEmpty) {
        // updates visitor's time out and status
        Map<String, String> data = {
          'visitor_id': "${_visitor.id}",
          'datetime_out': "${_visitor.datetimeOut}",
        };
        print(
            '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ online $data');
        try {
          var r = await model.checkOutVisitor(data);
          if (r['success']) {
            var d = CheckOutVisitorResponse.fromJson(r['response']);
            if (!d.error!) {
              showToast(
                  "${_visitor.visitorName} has been successfully checked out");
              setState(() {
                visitor = null;
              });
              refreshData();
            } else {
              showToast(d.message ?? "Failed");
            }
          }
        } catch (e) {
          // showToast(e.toString());
        }
      }
    }

    if (!isConnected) {
      // if no internet connection
      if (_visitor.id == null || _visitor.id!.isEmpty) {
        visitorsOut.add(_visitor);
        visitorsIn.deleteAt(index);
        showToast("${_visitor.visitorName} has been successfully checked out");
        setState(() {
          visitor = null;
          index = 0;
        });
      }
      if (_visitor.id != null || _visitor.id!.isNotEmpty) {
        visitorsOut.add(_visitor);
        showToast("${_visitor.visitorName} has been successfully checked out");
        setState(() {
          visitor = null;
          index = 0;
        });
      }
    }
  }

  Future<void> refreshData() async {
    final DateTime maxDate = DateTime.now();
    final DateTime minDate = DateTime.now().subtract(Duration(days: 1));
    final DateFormat formatter = DateFormat('yyyy-MM-dd HH:mm:ss');
    String max = formatter.format(maxDate);
    String min = formatter.format(minDate);
    Map<String, String> data = {
      'datetime_in_min': "$min",
      'datetime_in_max': "$max",
      'site_id': "${getPlanner()!.plannerId}"
    };
    var r = await widget.model.refreshVisitors(data);
    if (r['success']) {
    } else {
      showErrorDialog(r['response'], context, "get visitors");
    }
  }

  Future<void> initData(CheckOutVisitorModel model) async {
    final DateTime maxDate = DateTime.now();
    final DateTime minDate = DateTime.now().subtract(Duration(days: 1));
    final DateFormat formatter = DateFormat('yyyy-MM-dd HH:mm:ss');
    String max = formatter.format(maxDate);
    String min = formatter.format(minDate);
    Map<String, String> data = {
      'datetime_in_min': "$min",
      'datetime_in_max': "$max",
      'site_id': "${getPlanner()!.plannerId}"
    };
    bool isConnected = await checkConnection();
    if (isConnected) {
      var r = await model.refreshVisitors(data);
      if (r['success']) {
      } else {
        showErrorDialog(r['response'], context, "get visitors");
      }
    }
  }

  _visitorDetails() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
      child: Card(
        color: Colors.black,
        child: Column(
          children: [
            Stack(
              children: [
                Container(
                  margin: EdgeInsets.only(bottom: 25),
                  decoration: BoxDecoration(
                    border: Border.all(color: fromHex(yellow).withOpacity(0.3)),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        "Visitor's Info",
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                      ),
                      Container(
                        padding: EdgeInsets.all(15),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                visitor!.hostName!.isEmpty
                                    ? Container()
                                    : Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "${visitor!.hostName}",
                                              style: TextStyle(
                                                  fontSize: 15.6,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white),
                                            ),
                                            Text(
                                              "Host",
                                              style: TextStyle(
                                                fontSize: 11,
                                                fontStyle: FontStyle.italic,
                                                color: Colors.white54,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                visitor!.noVisitors!.isEmpty
                                    ? Container()
                                    : Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "${visitor!.noVisitors}",
                                              style: TextStyle(
                                                  fontSize: 15.6,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white),
                                            ),
                                            Text(
                                              'No. of Visitors',
                                              style: TextStyle(
                                                fontSize: 11,
                                                fontStyle: FontStyle.italic,
                                                color: Colors.white54,
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                              ],
                            ),
                            Row(
                              children: [
                                visitor!.companyName == null
                                    ? Container()
                                    : visitor!.companyName!.isEmpty
                                        ? Container()
                                        : Expanded(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                Text(
                                                  "${visitor!.companyName}",
                                                  style: TextStyle(
                                                      fontSize: 15.6,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.white),
                                                ),
                                                Text(
                                                  "Company",
                                                  style: TextStyle(
                                                    fontSize: 11,
                                                    fontStyle: FontStyle.italic,
                                                    color: Colors.white54,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                visitor!.officeNo == null
                                    ? Container()
                                    : visitor!.officeNo!.isEmpty
                                        ? Container()
                                        : Expanded(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                Text(
                                                  "${visitor!.officeNo}",
                                                  style: TextStyle(
                                                      fontSize: 15.6,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.white),
                                                ),
                                                Text(
                                                  'Office No.',
                                                  style: TextStyle(
                                                    fontSize: 11,
                                                    fontStyle: FontStyle.italic,
                                                    color: Colors.white54,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          )
                              ],
                            ),
                            Row(
                              children: [
                                visitor!.houseNo == null
                                    ? Container()
                                    : visitor!.houseNo!.isEmpty
                                        ? Container()
                                        : Expanded(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                Text(
                                                  "${visitor!.houseNo}",
                                                  style: TextStyle(
                                                      fontSize: 15.6,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.white),
                                                ),
                                                Text(
                                                  "House No.",
                                                  style: TextStyle(
                                                    fontSize: 11,
                                                    fontStyle: FontStyle.italic,
                                                    color: Colors.white54,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                visitor!.blockNo == null
                                    ? Container()
                                    : visitor!.blockNo!.isEmpty
                                        ? Container()
                                        : Expanded(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                Text(
                                                  "${visitor!.blockNo}",
                                                  style: TextStyle(
                                                      fontSize: 15.6,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.white),
                                                ),
                                                Text(
                                                  'Block No.',
                                                  style: TextStyle(
                                                    fontSize: 11,
                                                    fontStyle: FontStyle.italic,
                                                    color: Colors.white54,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          )
                              ],
                            ),
                            Row(
                              children: [
                                visitor!.department!.isEmpty
                                    ? Container()
                                    : Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Text(
                                              "${visitor!.department}",
                                              style: TextStyle(
                                                  fontSize: 15.6,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white),
                                            ),
                                            Text(
                                              "Department",
                                              style: TextStyle(
                                                fontSize: 11,
                                                fontStyle: FontStyle.italic,
                                                color: Colors.white54,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                visitor!.designation!.isEmpty
                                    ? Container()
                                    : Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Text(
                                              "${visitor!.designation}",
                                              style: TextStyle(
                                                  fontSize: 15.6,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white),
                                            ),
                                            Text(
                                              'Designation',
                                              style: TextStyle(
                                                fontSize: 11,
                                                fontStyle: FontStyle.italic,
                                                color: Colors.white54,
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                              ],
                            ),
                            Row(
                              children: [
                                visitor!.visitReason!.isEmpty
                                    ? Container()
                                    : Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            SizedBox(
                                              height: 10,
                                            ),
                                            Text(
                                              "${visitor!.visitReason}",
                                              style: TextStyle(
                                                  fontSize: 15.6,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white),
                                            ),
                                            Text(
                                              'reason',
                                              style: TextStyle(
                                                fontSize: 11,
                                                fontStyle: FontStyle.italic,
                                                color: Colors.white54,
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                              ],
                            ),
                            Row(
                              children: [
                                visitor!.visitorName!.isEmpty
                                    ? Container()
                                    : Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 10),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "${visitor!.visitorName!.trim()}",
                                                style: TextStyle(
                                                    fontSize: 15.6,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.white),
                                              ),
                                              Text(
                                                'Name',
                                                style: TextStyle(
                                                  fontSize: 11,
                                                  fontStyle: FontStyle.italic,
                                                  color: Colors.white54,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                visitor!.documentNumber!.isEmpty
                                    ? Container()
                                    : Expanded(
                                        child: Padding(
                                          padding: const EdgeInsets.symmetric(
                                              vertical: 10),
                                          child: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                "${visitor!.documentNumber!.trim()}",
                                                style: TextStyle(
                                                    fontSize: 15.6,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.white),
                                              ),
                                              Text(
                                                'ID/Passport',
                                                style: TextStyle(
                                                  fontSize: 11,
                                                  fontStyle: FontStyle.italic,
                                                  color: Colors.white54,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                              ],
                            ),
                            Row(
                              children: [
                                visitor!.plateNo!.isEmpty
                                    ? Container()
                                    : Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "${visitor!.plateNo!.trim()}",
                                              style: TextStyle(
                                                  fontSize: 15.6,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white),
                                            ),
                                            Text(
                                              'Plate No',
                                              style: TextStyle(
                                                fontSize: 11,
                                                fontStyle: FontStyle.italic,
                                                color: Colors.white54,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                visitor!.passId!.isEmpty
                                    ? Container()
                                    : Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              "${visitor!.passId}",
                                              style: TextStyle(
                                                  fontSize: 15.6,
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.white),
                                            ),
                                            Text(
                                              'Pass ID',
                                              style: TextStyle(
                                                fontSize: 11,
                                                fontStyle: FontStyle.italic,
                                                color: Colors.white54,
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                              ],
                            ),
                            Row(
                              children: [
                                visitor!.vehiclePass == null
                                    ? Container()
                                    : visitor!.vehiclePass!.isEmpty
                                        ? Container()
                                        : Expanded(
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  "${visitor!.vehiclePass}",
                                                  style: TextStyle(
                                                      fontSize: 15.6,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.white),
                                                ),
                                                Text(
                                                  'Vehicle Pass',
                                                  style: TextStyle(
                                                    fontSize: 11,
                                                    fontStyle: FontStyle.italic,
                                                    color: Colors.white54,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          )
                              ],
                            ),
                            visitor!.items!.isEmpty
                                ? Container()
                                : Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(top: 8.0),
                                        child: Row(
                                          children: [
                                            Expanded(
                                              child: Tags(
                                                itemCount:
                                                    visitor!.items!.length,
                                                alignment: WrapAlignment.start,
                                                itemBuilder: (int index) {
                                                  var type =
                                                      visitor!.items![index];
                                                  return Tooltip(
                                                      message:
                                                          "S/N: ${type.serialNo}",
                                                      child: ItemTags(
                                                        textStyle: TextStyle(
                                                            fontSize: 14,
                                                            fontWeight:
                                                                FontWeight
                                                                    .normal,
                                                            color: fromHex(
                                                                yellow)),
                                                        color: Colors.white10
                                                            .withOpacity(0.07),
                                                        activeColor: Colors
                                                            .white10
                                                            .withOpacity(0.07),
                                                        singleItem: true,
                                                        index: index,
                                                        title: type.name!,
                                                        textColor: Colors.white,
                                                        border: Border.all(
                                                            color:
                                                                Colors.black),
                                                        removeButton:
                                                            ItemTagsRemoveButton(
                                                          color:
                                                              Colors.deepOrange,
                                                          backgroundColor:
                                                              Colors
                                                                  .transparent,
                                                          onRemoved: () {
                                                            setState(() {
                                                              visitor!.items!
                                                                  .removeAt(
                                                                      index);
                                                            });
                                                            return true;
                                                          },
                                                        ), // OR nu
                                                      ));
                                                },
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            left: 10, top: 2),
                                        child: Text(
                                          'Items',
                                          style: TextStyle(
                                            fontSize: 11,
                                            fontStyle: FontStyle.italic,
                                            color: Colors.white54,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                    bottom: 6,
                    left: 10,
                    right: 10,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          height: 40,
                          width: 200,
                          decoration: BoxDecoration(
                            color: fromHex(deepOrange),
                            borderRadius: BorderRadius.circular(15),
                          ),
                          child: Row(
                            children: [
                              Expanded(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text(
                                        "${visitor?.datetimeIn?.substring(10).trim() ?? ''}"),
                                    Text(
                                      'IN',
                                      style: TextStyle(fontSize: 11),
                                    )
                                  ],
                                ),
                              ),
                              Icon(
                                Icons.access_time,
                                color: Colors.black,
                              ),
                              Expanded(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Text("${visitor?.datetimeOut ?? '_'}"),
                                    Text(
                                      'OUT',
                                      style: TextStyle(fontSize: 11),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ))
              ],
            ),
          ],
        ),
      ),
    );
  }
}
