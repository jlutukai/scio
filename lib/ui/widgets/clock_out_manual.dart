import 'dart:async';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:path/path.dart' as path;
import 'package:scio_security/core/enums/view_state.dart';
import 'package:scio_security/core/models/local/localModels.dart';
import 'package:scio_security/core/services/api.dart';
import 'package:scio_security/core/viewmodels/guards_manual_out_model.dart';
import 'package:scio_security/core/viewmodels/timesheet_manual_model.dart';
import 'package:scio_security/ui/screens/base_view.dart';
import 'package:scio_security/utils/useful.dart';

import '../../core/models/time_sheet_response.dart';
import 'guard_info.dart';

class ClockOutManual extends StatefulWidget {
  ClockOutManual();

  @override
  _ClockOutManualState createState() => _ClockOutManualState();
}

class _ClockOutManualState extends State<ClockOutManual> {
  final GlobalKey<ScaffoldState> _scaffoldKey3 = GlobalKey<ScaffoldState>();
  final String status = "Awaiting Approval";

  String? date;
  String? shiftName;
  Shifts? _shift;

  @override
  void initState() {
    super.initState();
    _shift = getShift();
    date = getShiftDate();
    shiftName =
        "${_shift?.shiftName} ${_shift?.shiftChangeFrom} - ${_shift?.shiftChangeTo}";
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<CheckOutGuardManualModel>(
      onModelReady: (model) async {
        Map<String, String> data = {
          'date': "$date",
          'shift_id': "${_shift?.id ?? 0}",
          'planner_id': "${getPlanner()!.plannerId}",
        };
        bool isConnected = await checkConnection();
        if (isConnected) {
          var r = await model.getTimeSheetRemote(data);
          await getTimeSheetsLocal(model);
          if (r['success']) {
            var d = TimeSheetResponse.fromJson(r['response']);
            if (d.data != null) {
            } else {
              if (d.data!.isEmpty) {
                showToast("");
              }
            }
          } else {
            showErrorDialog(r['response'], context, "get time sheet details");
          }
        }
        await getTimeSheetsLocal(model);
      },
      builder: (context, model, child) => Scaffold(
        backgroundColor: Colors.black,
        body: Column(
          children: [
            model.state == ViewState.Idle
                ? Padding(
                    padding: const EdgeInsets.only(left: 80, bottom: 20),
                    child: GestureDetector(
                      onTap: () {
                        showShiftsDialog(getPlanner()!, context, model);
                      },
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Text(
                                    'Date',
                                    style: TextStyle(
                                        color: fromHex(yellow),
                                        fontWeight: FontWeight.normal,
                                        fontSize: 16),
                                  ),
                                  Text(
                                    "  $date",
                                    style: TextStyle(
                                        color: fromHex(deepOrange),
                                        fontWeight: FontWeight.bold,
                                        fontSize: 14),
                                  )
                                ],
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Text(
                                    'Shift',
                                    style: TextStyle(
                                        color: fromHex(yellow),
                                        fontWeight: FontWeight.normal,
                                        fontSize: 16),
                                  ),
                                  Text(
                                    "  $shiftName",
                                    style: TextStyle(
                                        color: fromHex(deepOrange),
                                        fontWeight: FontWeight.bold,
                                        fontSize: 14),
                                  )
                                ],
                              ),
                            ],
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Icon(
                            Icons.calendar_today_outlined,
                            color: fromHex(yellow),
                          )
                        ],
                      ),
                    ),
                  )
                : Container(),
            Expanded(
                child: model.state == ViewState.Idle
                    ? _detailsList(model, context, model.timeSheet)
                    : Center(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                                height: 200,
                                width: 200,
                                child: Lottie.asset('assets/loading.json')),
                            Text(
                              'Please wait \n Loading...',
                              style: TextStyle(color: fromHex(deepOrange)),
                            )
                          ],
                        ),
                      )),
          ],
        ),
      ),
    );
  }

  Future<void> showShiftsDialog(PlannerData p, BuildContext context,
      CheckOutGuardManualModel model) async {
    final f = new DateFormat('yyyy-MM-dd');

    List<String> _reasons = ['None', 'Yesterday', 'Today', 'Tomorrow'];
    String? reason = 'None';
    String? d;
    Shifts? s;

    List<String?> _shifts = [];
    String? shift = 'None';
    _shifts.add(shift);
    p.shifts!.forEach((element) {
      _shifts.add(
          "${element.shiftName} ( ${element.shiftChangeFrom}-${element.shiftChangeTo} )");
    });
    await showDialog(
        context: _scaffoldKey3.currentContext!,
        builder: (context) => Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            elevation: 0.0,
            backgroundColor: Colors.grey[900],
            child: StatefulBuilder(builder: (context, StateSetter setState) {
              return Container(
                color: Colors.transparent,
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                child: Container(
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 15,
                        ),
                        Row(
                          children: [
                            Container(
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 10.0, right: 10.0),
                                child: Text(
                                  'Choose Date',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 11,
                                      fontStyle: FontStyle.italic),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Container(
                          decoration: BoxDecoration(
                              color: Colors.white10,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5.0))),
                          child: DropdownButtonHideUnderline(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  top: 8.0, bottom: 8.0, left: 5.0),
                              child: ButtonTheme(
                                alignedDropdown: true,
                                child: DropdownButton(
                                  hint: Text('Specify Date'),
                                  isExpanded: true,
                                  isDense: true,
                                  value: reason,
                                  dropdownColor: Colors.grey[800],
                                  style: TextStyle(
                                    color: fromHex(yellow),
                                  ),
                                  onChanged: (String? val) {
                                    DateTime now = DateTime.now();

                                    if (val == 'Yesterday') {
                                      setState(() {
                                        DateTime y =
                                            now.subtract(Duration(days: 1));
                                        d = f.format(y);
                                      });
                                    }
                                    if (val == 'Today') {
                                      setState(() {
                                        d = f.format(now);
                                      });
                                    }
                                    if (val == 'Tomorrow') {
                                      setState(() {
                                        DateTime t = now.add(Duration(days: 1));
                                        d = f.format(t);
                                      });
                                    }
                                    print(d);
                                    setState(() {
                                      reason = val;
                                    });
                                  },
                                  items: _reasons.map<DropdownMenuItem<String>>(
                                      (String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Row(
                          children: [
                            Container(
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 10.0, right: 10.0),
                                child: Text(
                                  'Choose Shift',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 11,
                                      fontStyle: FontStyle.italic),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Container(
                          decoration: BoxDecoration(
                              color: Colors.white10,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5.0))),
                          child: DropdownButtonHideUnderline(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  top: 8.0, bottom: 8.0, left: 5.0),
                              child: ButtonTheme(
                                alignedDropdown: true,
                                child: DropdownButton(
                                  hint: Text('Specify shift'),
                                  isExpanded: true,
                                  isDense: true,
                                  value: shift,
                                  dropdownColor: Colors.grey[800],
                                  style: TextStyle(
                                    color: fromHex(yellow),
                                  ),
                                  onChanged: (String? val) {
                                    setState(() {
                                      shift = val;

                                      p.shifts!.forEach((element) {
                                        if ("${element.shiftName} ( ${element.shiftChangeFrom}-${element.shiftChangeTo} )" ==
                                            val) {
                                          s = element;
                                        }
                                      });
                                    });
                                    print(s!.id);
                                  },
                                  items: _shifts.map<DropdownMenuItem<String>>(
                                      (String? value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value!),
                                    );
                                  }).toList(),
                                ),
                              ),
                            ),
                          ),
                        ),
                        ListTile(
                          onTap: () async {
                            if (d == null) {
                              showToast("Please choose date");
                              return;
                            }
                            if (d == 'None') {
                              showToast("Please choose date");
                              return;
                            }
                            if (s == null) {
                              showToast("Please choose shift");
                              return;
                            }
                            setState(() {
                              _shift = s!;
                              shiftName =
                                  "${s!.shiftName} ${s!.shiftChangeFrom} - ${s!.shiftChangeTo}";
                              date = "$d";
                            });
                            Map<String, String> data = {
                              'date': "$d",
                              'shift_id': "${s!.id}",
                              'planner_id': "${getPlanner()!.plannerId}",
                            };
                            setShiftID(s!.id ?? "0");
                            setShift(s!);
                            setShiftDate(d!);
                            Navigator.pop(context);
                            bool isConnected = await checkConnection();
                            if (isConnected) {
                              var r = await model.getTimeSheetRemote(data);
                              await getTimeSheetsLocal(model);
                              if (r['success']) {
                              } else {
                                showErrorDialog(r['response'], context,
                                    "get time sheet details");
                              }
                            } else {
                              await getTimeSheetsLocal(model);
                            }
                          },
                          title: Center(
                            child: Text(
                              'Submit',
                              style: TextStyle(color: Colors.deepOrange),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            })));
  }

  _detailsList(CheckOutGuardManualModel model, BuildContext context,
      List<TimeSheetData> timeSheetData) {
    return timeSheetData.isNotEmpty
        ? ListView.builder(
            physics: BouncingScrollPhysics(),
            itemCount: timeSheetData.length,
            itemBuilder: (context, index) => InkWell(
                  onLongPress: () async {
                    File? img =
                        await _getImageProfile(timeSheetData[index].employeeId);
                  },
                  onTap: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) =>
                          CustomDialog(timeSheetData[index]),
                    );
                  },
                  child: Dismissible(
                    key: UniqueKey(),
                    onDismissed: (DismissDirection direction) {
                      showErrorDialog("Error", context, "Already Checked Out");
                      setState(() {});
                    },
                    child: Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 3, horizontal: 15),
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.white10.withOpacity(0.07),
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 0, right: 10),
                              height: 100,
                              width: 10,
                              decoration: BoxDecoration(
                                  color: timeSheetData[index].reason == null ||
                                          timeSheetData[index]
                                              .reason!
                                              .isEmpty ||
                                          timeSheetData[index]
                                                  .reason!
                                                  .toLowerCase() ==
                                              "null"
                                      ? Colors.green[800]
                                      : Colors.deepOrange[900],
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(10),
                                      bottomLeft: Radius.circular(10))),
                            ),
                            CircleAvatar(
                              radius: 35,
                              backgroundImage: CachedNetworkImageProvider(
                                  timeSheetData[index].fileName != null
                                      ? timeSheetData[index]
                                              .fileName!
                                              .isNotEmpty
                                          ? "${Api().getUrl()}/CUSTOM/Employees/profile_pictures/${timeSheetData[index].fileName}"
                                          : noImage
                                      : noImage),
                            ),
                            Expanded(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Text(
                                    "${timeSheetData[index].name}",
                                    style: TextStyle(
                                        color: fromHex(yellow),
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      Column(
                                        children: [
                                          Text(
                                            "${timeSheetData[index].timeIn}",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 12),
                                          ),
                                          SizedBox(
                                            height: 3,
                                          ),
                                          Text(
                                            "Time In",
                                            style: TextStyle(
                                                color: fromHex(deepOrange),
                                                fontSize: 11),
                                          ),
                                        ],
                                      ),
                                      Column(
                                        children: [
                                          Text(
                                            "${timeSheetData[index].timeOut}",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 12),
                                          ),
                                          SizedBox(
                                            height: 3,
                                          ),
                                          Text(
                                            "Time Out",
                                            style: TextStyle(
                                                color: fromHex(deepOrange),
                                                fontSize: 11),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                ],
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ))
        : Center(
            child: Text(
              'No employees checked out yet',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                  fontWeight: FontWeight.bold),
            ),
          );
  }

  Future<File?> _getImageProfile(String? employeeId) async {
    var p = await ImagePicker()
        .pickImage(source: ImageSource.camera, imageQuality: 10);
    if (p != null) {
      File croppedFile = await (ImageCropper()
          .cropImage(sourcePath: p.path, aspectRatioPresets: [
        CropAspectRatioPreset.square
      ], uiSettings: [
        AndroidUiSettings(
            toolbarTitle: 'Crop Image',
            toolbarColor: fromHex(yellow),
            toolbarWidgetColor: Colors.black,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: true),
        IOSUiSettings(
          minimumAspectRatio: 1.0,
        )
      ]) as FutureOr<File>);
      File picture = File(croppedFile.path);
      String dir = path.dirname(picture.path);
      String newPath = path.join(dir, "$employeeId.jpg");
      File photo = picture.renameSync(newPath);
      return photo;
    } else {
      return null;
    }
  }

  Future getTimeSheetsLocal(CheckOutGuardManualModel model) async {
    if (_shift != null && date != null) {
      try {
        await model.getTimesheetLocal(status, date!, _shift!);
      } catch (e) {
        print(e.toString());
      }
    }
  }
}
