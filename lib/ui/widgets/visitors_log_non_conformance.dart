import 'package:flutter/material.dart';
import 'package:flutter_tags/flutter_tags.dart';
import 'package:hive/hive.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:scio_security/core/enums/view_state.dart';
import 'package:scio_security/core/models/local/localModels.dart';
import 'package:scio_security/core/viewmodels/visitors_log_model.dart';
import 'package:scio_security/ui/screens/base_view.dart';
import 'package:scio_security/utils/useful.dart';

class VisitorsLogNonConf extends StatefulWidget {
  @override
  _VisitorsLogNonConfState createState() => _VisitorsLogNonConfState();
}

class _VisitorsLogNonConfState extends State<VisitorsLogNonConf> {
  final visitorsIn = Hive.box('visitors_in');
  final visitorsOut = Hive.box('visitors_out');
  List<Visitor> _visitors = [];
  late VisitorsLogModel _model;
  Visitor? visitor;
  String passId = '';
  int index = 0;

  void getVisitor(String passId) {
    if (visitorsIn.isNotEmpty) {
      for (var i = 0; i <= visitorsIn.values.length; i++) {
        if (visitorsIn.getAt(i).documentNumber == passId) {
          setState(() {
            visitor = visitorsIn.getAt(i);
            index = i;
          });
          break;
        }
      }
    }
    _model.visitorsIn.forEach((element) {
      if (element.documentNumber == passId) {
        setState(() {
          visitor = element;
        });
      }
    });
    if (visitor == null) {
      showToast("Not Found");
    }
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<VisitorsLogModel>(
      onModelReady: (model) async {
        _model = model;
        final DateTime maxDate = DateTime.now();
        final DateTime minDate = DateTime.now().subtract(Duration(days: 1));
        final DateFormat formatter = DateFormat('yyyy-MM-dd HH:mm:ss');
        String max = formatter.format(maxDate);
        String min = formatter.format(minDate);
        Map<String, String> data = {
          'datetime_in_min': "$min",
          'datetime_in_max': "$max",
          'site_id': "${getPlanner()!.plannerId}"
        };
        print(data);
        bool isConnected = await checkConnection();
        if (isConnected) {
          var r = await model.getVisitors(data);
          if (r['success']) {
          } else {
            showErrorDialog(r['response'], context, "get visitors");
          }
        }
      },
      builder: (context, model, child) => model.state == ViewState.Idle
          ? Stack(
              children: [
                Scaffold(
                  backgroundColor: Colors.black,
                  appBar: AppBar(
                    backgroundColor: Colors.transparent,
                    elevation: 0,
                    title: Text(
                      'Non Conformance',
                      style: TextStyle(
                          color: fromHex(deepOrange),
                          fontWeight: FontWeight.bold),
                    ),
                    centerTitle: true,
                    automaticallyImplyLeading: false,
                  ),
                  body: Column(
                    children: [
                      visitor != null
                          ? Padding(
                              padding: EdgeInsets.symmetric(
                                  vertical: 10, horizontal: 15),
                              child: Card(
                                color: Colors.black,
                                child: Column(
                                  children: [
                                    Stack(
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(bottom: 25),
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                                color: fromHex(yellow)
                                                    .withOpacity(0.3)),
                                            borderRadius:
                                                BorderRadius.circular(15),
                                          ),
                                          child: Column(
                                            children: [
                                              SizedBox(
                                                height: 10,
                                              ),
                                              Text(
                                                "Visitor's Info",
                                                style: TextStyle(
                                                    fontSize: 18,
                                                    fontWeight: FontWeight.bold,
                                                    color: Colors.white),
                                              ),
                                              Container(
                                                padding: EdgeInsets.all(15),
                                                child: Column(
                                                  children: [
                                                    Row(
                                                      children: [
                                                        visitor!.hostName!
                                                                .isEmpty
                                                            ? Container()
                                                            : Expanded(
                                                                child: Column(
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .start,
                                                                  children: [
                                                                    Text(
                                                                      "${visitor!.hostName}",
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                              15.6,
                                                                          fontWeight: FontWeight
                                                                              .bold,
                                                                          color:
                                                                              Colors.white),
                                                                    ),
                                                                    Text(
                                                                      "Host",
                                                                      style:
                                                                          TextStyle(
                                                                        fontSize:
                                                                            11,
                                                                        fontStyle:
                                                                            FontStyle.italic,
                                                                        color: Colors
                                                                            .white54,
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                        visitor!.noVisitors!
                                                                .isEmpty
                                                            ? Container()
                                                            : Expanded(
                                                                child: Column(
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .start,
                                                                  children: [
                                                                    Text(
                                                                      "${visitor!.noVisitors}",
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                              15.6,
                                                                          fontWeight: FontWeight
                                                                              .bold,
                                                                          color:
                                                                              Colors.white),
                                                                    ),
                                                                    Text(
                                                                      'No. of Visitors',
                                                                      style:
                                                                          TextStyle(
                                                                        fontSize:
                                                                            11,
                                                                        fontStyle:
                                                                            FontStyle.italic,
                                                                        color: Colors
                                                                            .white54,
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              )
                                                      ],
                                                    ),
                                                    Row(
                                                      children: [
                                                        visitor!.department!
                                                                .isEmpty
                                                            ? Container()
                                                            : Expanded(
                                                                child: Column(
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .start,
                                                                  children: [
                                                                    SizedBox(
                                                                      height:
                                                                          10,
                                                                    ),
                                                                    Text(
                                                                      "${visitor!.department}",
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                              15.6,
                                                                          fontWeight: FontWeight
                                                                              .bold,
                                                                          color:
                                                                              Colors.white),
                                                                    ),
                                                                    Text(
                                                                      "Department",
                                                                      style:
                                                                          TextStyle(
                                                                        fontSize:
                                                                            11,
                                                                        fontStyle:
                                                                            FontStyle.italic,
                                                                        color: Colors
                                                                            .white54,
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                        visitor!.designation!
                                                                .isEmpty
                                                            ? Container()
                                                            : Expanded(
                                                                child: Column(
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .start,
                                                                  children: [
                                                                    SizedBox(
                                                                      height:
                                                                          10,
                                                                    ),
                                                                    Text(
                                                                      "${visitor!.designation}",
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                              15.6,
                                                                          fontWeight: FontWeight
                                                                              .bold,
                                                                          color:
                                                                              Colors.white),
                                                                    ),
                                                                    Text(
                                                                      'Designation',
                                                                      style:
                                                                          TextStyle(
                                                                        fontSize:
                                                                            11,
                                                                        fontStyle:
                                                                            FontStyle.italic,
                                                                        color: Colors
                                                                            .white54,
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              )
                                                      ],
                                                    ),
                                                    Row(
                                                      children: [
                                                        visitor!.visitReason!
                                                                .isEmpty
                                                            ? Container()
                                                            : Expanded(
                                                                child: Column(
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .start,
                                                                  children: [
                                                                    SizedBox(
                                                                      height:
                                                                          10,
                                                                    ),
                                                                    Text(
                                                                      "${visitor!.visitReason}",
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                              15.6,
                                                                          fontWeight: FontWeight
                                                                              .bold,
                                                                          color:
                                                                              Colors.white),
                                                                    ),
                                                                    Text(
                                                                      'reason',
                                                                      style:
                                                                          TextStyle(
                                                                        fontSize:
                                                                            11,
                                                                        fontStyle:
                                                                            FontStyle.italic,
                                                                        color: Colors
                                                                            .white54,
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              )
                                                      ],
                                                    ),
                                                    Row(
                                                      children: [
                                                        visitor!.visitorName!
                                                                .isEmpty
                                                            ? Container()
                                                            : Expanded(
                                                                child: Padding(
                                                                  padding: const EdgeInsets
                                                                          .symmetric(
                                                                      vertical:
                                                                          10),
                                                                  child: Column(
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .start,
                                                                    children: [
                                                                      Text(
                                                                        "${visitor!.visitorName!.trim()}",
                                                                        style: TextStyle(
                                                                            fontSize:
                                                                                15.6,
                                                                            fontWeight:
                                                                                FontWeight.bold,
                                                                            color: Colors.white),
                                                                      ),
                                                                      Text(
                                                                        'Name',
                                                                        style:
                                                                            TextStyle(
                                                                          fontSize:
                                                                              11,
                                                                          fontStyle:
                                                                              FontStyle.italic,
                                                                          color:
                                                                              Colors.white54,
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                              ),
                                                        visitor!.documentNumber!
                                                                .isEmpty
                                                            ? Container()
                                                            : Expanded(
                                                                child: Padding(
                                                                  padding: const EdgeInsets
                                                                          .symmetric(
                                                                      vertical:
                                                                          10),
                                                                  child: Column(
                                                                    crossAxisAlignment:
                                                                        CrossAxisAlignment
                                                                            .start,
                                                                    children: [
                                                                      Text(
                                                                        "${visitor!.documentNumber!.trim()}",
                                                                        style: TextStyle(
                                                                            fontSize:
                                                                                15.6,
                                                                            fontWeight:
                                                                                FontWeight.bold,
                                                                            color: Colors.white),
                                                                      ),
                                                                      Text(
                                                                        'ID/Passport',
                                                                        style:
                                                                            TextStyle(
                                                                          fontSize:
                                                                              11,
                                                                          fontStyle:
                                                                              FontStyle.italic,
                                                                          color:
                                                                              Colors.white54,
                                                                        ),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                ),
                                                              )
                                                      ],
                                                    ),
                                                    Row(
                                                      children: [
                                                        visitor!.plateNo!
                                                                .isEmpty
                                                            ? Container()
                                                            : Expanded(
                                                                child: Column(
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .start,
                                                                  children: [
                                                                    Text(
                                                                      "${visitor!.plateNo!.trim()}",
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                              15.6,
                                                                          fontWeight: FontWeight
                                                                              .bold,
                                                                          color:
                                                                              Colors.white),
                                                                    ),
                                                                    Text(
                                                                      'Plate No',
                                                                      style:
                                                                          TextStyle(
                                                                        fontSize:
                                                                            11,
                                                                        fontStyle:
                                                                            FontStyle.italic,
                                                                        color: Colors
                                                                            .white54,
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                        visitor!.passId!.isEmpty
                                                            ? Container()
                                                            : Expanded(
                                                                child: Column(
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .start,
                                                                  children: [
                                                                    Text(
                                                                      "${visitor!.passId}",
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                              15.6,
                                                                          fontWeight: FontWeight
                                                                              .bold,
                                                                          color:
                                                                              Colors.white),
                                                                    ),
                                                                    Text(
                                                                      'Pass ID',
                                                                      style:
                                                                          TextStyle(
                                                                        fontSize:
                                                                            11,
                                                                        fontStyle:
                                                                            FontStyle.italic,
                                                                        color: Colors
                                                                            .white54,
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              )
                                                      ],
                                                    ),
                                                    visitor!.items!.isEmpty
                                                        ? Container()
                                                        : Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            children: [
                                                              Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .only(
                                                                        top:
                                                                            8.0),
                                                                child: Row(
                                                                  children: [
                                                                    Expanded(
                                                                      child:
                                                                          Tags(
                                                                        itemCount: visitor!
                                                                            .items!
                                                                            .length,
                                                                        alignment:
                                                                            WrapAlignment.start,
                                                                        itemBuilder:
                                                                            (int
                                                                                index) {
                                                                          var type =
                                                                              visitor!.items![index];
                                                                          return Tooltip(
                                                                              message: "S/N: ${type.serialNo}",
                                                                              child: ItemTags(
                                                                                textStyle: TextStyle(fontSize: 14, fontWeight: FontWeight.normal, color: fromHex(yellow)),
                                                                                color: Colors.white10.withOpacity(0.07),
                                                                                activeColor: Colors.white10.withOpacity(0.07),
                                                                                singleItem: true,
                                                                                index: index,
                                                                                title: type.name!,
                                                                                textColor: Colors.white,
                                                                                border: Border.all(color: Colors.black),
                                                                                removeButton: ItemTagsRemoveButton(
                                                                                  color: Colors.deepOrange,
                                                                                  backgroundColor: Colors.transparent,
                                                                                  onRemoved: () {
                                                                                    setState(() {
                                                                                      visitor!.items!.removeAt(index);
                                                                                    });
                                                                                    return true;
                                                                                  },
                                                                                ), // OR nu
                                                                              ));
                                                                        },
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              ),
                                                              Padding(
                                                                padding:
                                                                    const EdgeInsets
                                                                            .only(
                                                                        left:
                                                                            10,
                                                                        top: 2),
                                                                child: Text(
                                                                  'Items',
                                                                  style:
                                                                      TextStyle(
                                                                    fontSize:
                                                                        11,
                                                                    fontStyle:
                                                                        FontStyle
                                                                            .italic,
                                                                    color: Colors
                                                                        .white54,
                                                                  ),
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                        Positioned(
                                            bottom: 6,
                                            left: 10,
                                            right: 10,
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Container(
                                                  height: 40,
                                                  width: 200,
                                                  decoration: BoxDecoration(
                                                    color: fromHex(deepOrange),
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            15),
                                                  ),
                                                  child: Row(
                                                    children: [
                                                      Expanded(
                                                        child: Column(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          children: [
                                                            Text(
                                                                "${visitor?.datetimeIn?.substring(10).trim() ?? ''}"),
                                                            Text(
                                                              'IN',
                                                              style: TextStyle(
                                                                  fontSize: 11),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                      Icon(
                                                        Icons.access_time,
                                                        color: Colors.black,
                                                      ),
                                                      Expanded(
                                                        child: Column(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .center,
                                                          children: [
                                                            Text(
                                                                "${visitor?.datetimeOut ?? '_'}"),
                                                            Text(
                                                              'OUT',
                                                              style: TextStyle(
                                                                  fontSize: 11),
                                                            )
                                                          ],
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ))
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            )
                          : Container(),
                      Dismissible(
                        key: UniqueKey(),
                        // direction: DismissDirection.startToEnd,
                        onDismissed: (DismissDirection direction) {
                          if (direction == DismissDirection.startToEnd) {
                            getImage(context, "ID");
                            setState(() {});
                          } else {
                            getImage(context, "Passport");
                            setState(() {});
                          }
                        },
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              vertical: 8.0, horizontal: 15),
                          child: Card(
                            color: Colors.white10.withOpacity(0.07),
                            child: Container(
                              // color: fromHex("#114259"),
                              height: 60,
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 20),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: InkWell(
                                      onTap: () {
                                        getImage(
                                            context, "Passport"); //passport
                                      },
                                      onLongPress: () {
                                        _passportDialog(context);
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 12.0),
                                        child: Row(
                                          children: [
                                            Icon(
                                              Icons.arrow_back_ios_sharp,
                                              color: fromHex(yellow),
                                            ),
                                            Expanded(
                                              child: Center(
                                                child: Text(
                                                  "Passport",
                                                  style: TextStyle(
                                                      color: fromHex(yellow),
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 18),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                  Icon(
                                    Icons.camera_alt_outlined,
                                    size: 32,
                                    color: fromHex(yellow),
                                  ),
                                  Expanded(
                                    child: InkWell(
                                      onTap: () {
                                        getImage(context, "ID"); // ID
                                      },
                                      onLongPress: () {
                                        _idDialog(context);
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 12.0),
                                        child: Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          // mainAxisAlignment: MainAxisAlignment.spaceAround,
                                          children: [
                                            Expanded(
                                              child: Center(
                                                child: Text(
                                                  "ID",
                                                  style: TextStyle(
                                                      color: fromHex(yellow),
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 18),
                                                ),
                                              ),
                                            ),
                                            Icon(
                                              Icons.arrow_forward_ios_sharp,
                                              color: fromHex(yellow),
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                  left: 15,
                  top: 27,
                  child: FloatingActionButton(
                    foregroundColor: Colors.grey,
                    backgroundColor: Colors.white10.withOpacity(0.07),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    mini: true,
                    tooltip: "go back home",
                    child: Icon(
                      Icons.keyboard_backspace_sharp,
                      color: fromHex(deepOrange),
                    ),
                  ),
                ),
              ],
            )
          : Scaffold(
              backgroundColor: Colors.black,
              body: Column(
                children: [
                  Expanded(
                    child: Center(
                        child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                            height: 100,
                            width: 100,
                            child: Lottie.asset('assets/test.json')),
                        Text(
                          'Please wait \n Loading...',
                          style: TextStyle(color: fromHex(deepOrange)),
                        )
                      ],
                    )),
                  )
                ],
              ),
            ),
    );
  }

  Future getImage(BuildContext context, String s) async {
    // var pickedImage = await ImagePicker().getImage(source: ImageSource.camera);

    // if (pickedImage != null)
    // Navigator.push(
    //     context,
    //     MaterialPageRoute(
    //         builder: (context) => DetailScreen(
    //             path: pickedImage.path,
    //             s: s,
    //             setVisitorsDetails: setVisitorsDetails)));
  }

  setVisitorsDetails(String id, String name, String type) {
    getVisitor(id);
  }

  void _passportDialog(BuildContext context) async {
    TextEditingController _docId = TextEditingController();
    await showDialog(
      context: context,
      builder: (context) => Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        elevation: 0.0,
        backgroundColor: Colors.grey[900],
        child: Container(
          color: Colors.transparent,
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
          child: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 15,
                ),
                Container(
                  child: TextFormField(
                    controller: _docId,
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.characters,
                    style: TextStyle(color: Colors.white),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Please Enter visitor's Passport no";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white10,
                      isDense: true,
                      hintText: "eg A*****8",
                      labelText: "visitor's Passport Number",
                      labelStyle: TextStyle(color: fromHex(yellow)),
                      hintStyle: TextStyle(
                        color: Colors.blueGrey[400],
                      ),
                      border: InputBorder.none,
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: fromHex(yellow)),
                      ),
                    ),
                  ),
                ),
                ListTile(
                  onTap: () async {
                    if (_docId.text.isNotEmpty) {
                      getVisitor(_docId.text.trim());
                    }
                    Navigator.pop(context);
                  },
                  title: Center(
                    child: Text(
                      'Search Visitor',
                      style: TextStyle(color: Colors.deepOrange),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _idDialog(BuildContext context) async {
    TextEditingController _docId = TextEditingController();

    await showDialog(
      context: context,
      builder: (context) => Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        elevation: 0.0,
        backgroundColor: Colors.grey[900],
        child: Container(
          color: Colors.transparent,
          padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
          child: Container(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 15,
                ),
                Container(
                  child: TextFormField(
                    controller: _docId,
                    keyboardType: TextInputType.number,
                    style: TextStyle(color: Colors.white),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Please Enter visitor's ID no";
                      }
                      return null;
                    },
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white10,
                      isDense: true,
                      hintText: "eg 23*****8",
                      labelText: "visitor's ID Number",
                      labelStyle: TextStyle(color: fromHex(yellow)),
                      hintStyle: TextStyle(
                        color: Colors.blueGrey[400],
                      ),
                      border: InputBorder.none,
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: fromHex(yellow)),
                      ),
                    ),
                  ),
                ),
                ListTile(
                  onTap: () async {
                    if (_docId.text.isNotEmpty) {
                      getVisitor(_docId.text.trim());
                    }
                    Navigator.pop(context);
                  },
                  title: Center(
                    child: Text(
                      'Search Visitor',
                      style: TextStyle(color: Colors.deepOrange),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
