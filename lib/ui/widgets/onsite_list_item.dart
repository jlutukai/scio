import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tags/flutter_tags.dart';
import 'package:scio_security/core/models/local/localModels.dart';
import 'package:scio_security/utils/useful.dart';

class OnsiteListItem extends StatefulWidget {
  final Visitor visitor;
  OnsiteListItem(this.visitor);

  @override
  _OnsiteListItemState createState() => _OnsiteListItemState();
}

class _OnsiteListItemState extends State<OnsiteListItem> {
  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.white10.withOpacity(0.07),
      margin: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
      child: Row(
        children: [
          Container(
            width: 10,
            height: 60,
            color: fromHex(yellow),
          ),
          Expanded(
            child: ExpansionTile(
              // backgroundColor: Colors.white10.withOpacity(0.07),
              title: Text(
                "${widget.visitor.visitorName}",
                style: TextStyle(color: fromHex(yellow)),
              ),
              children: [
                Column(
                  children: [
                    Column(
                      children: [
                        Stack(
                          children: [
                            Container(
                              margin: EdgeInsets.only(
                                bottom: 35,
                                right: 10,
                              ),
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: fromHex(yellow).withOpacity(0.3)),
                                borderRadius: BorderRadius.circular(15),
                              ),
                              child: Column(
                                children: [
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    "Visitor's Info",
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white),
                                  ),
                                  Container(
                                    padding: EdgeInsets.all(15),
                                    child: Column(
                                      children: [
                                        Row(
                                          children: [
                                            Expanded(
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    "${widget.visitor.hostName ?? ''}",
                                                    style: TextStyle(
                                                        fontSize: 15.6,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color: Colors.white),
                                                  ),
                                                  Text(
                                                    "Host",
                                                    style: TextStyle(
                                                      fontSize: 11,
                                                      fontStyle:
                                                          FontStyle.italic,
                                                      color: Colors.white54,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Expanded(
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    "${widget.visitor.noVisitors ?? ''}",
                                                    style: TextStyle(
                                                        fontSize: 15.6,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color: Colors.white),
                                                  ),
                                                  Text(
                                                    'No. of Visitors',
                                                    style: TextStyle(
                                                      fontSize: 11,
                                                      fontStyle:
                                                          FontStyle.italic,
                                                      color: Colors.white54,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                        Row(
                                          children: [
                                            Expanded(
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  SizedBox(
                                                    height: 10,
                                                  ),
                                                  Text(
                                                    "${widget.visitor.department ?? ''}",
                                                    style: TextStyle(
                                                        fontSize: 15.6,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color: Colors.white),
                                                  ),
                                                  Text(
                                                    "Department",
                                                    style: TextStyle(
                                                      fontSize: 11,
                                                      fontStyle:
                                                          FontStyle.italic,
                                                      color: Colors.white54,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Expanded(
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  SizedBox(
                                                    height: 10,
                                                  ),
                                                  Text(
                                                    "${widget.visitor.designation ?? ''}",
                                                    style: TextStyle(
                                                        fontSize: 15.6,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color: Colors.white),
                                                  ),
                                                  Text(
                                                    'Designation',
                                                    style: TextStyle(
                                                      fontSize: 11,
                                                      fontStyle:
                                                          FontStyle.italic,
                                                      color: Colors.white54,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                        Row(
                                          children: [
                                            widget.visitor.companyName == null
                                                ? Container()
                                                : widget.visitor.companyName!
                                                        .isEmpty
                                                    ? Container()
                                                    : Expanded(
                                                        child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            SizedBox(
                                                              height: 10,
                                                            ),
                                                            Text(
                                                              "${widget.visitor.companyName}",
                                                              style: TextStyle(
                                                                  fontSize:
                                                                      15.6,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  color: Colors
                                                                      .white),
                                                            ),
                                                            Text(
                                                              "Company",
                                                              style: TextStyle(
                                                                fontSize: 11,
                                                                fontStyle:
                                                                    FontStyle
                                                                        .italic,
                                                                color: Colors
                                                                    .white54,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                            widget.visitor.officeNo == null
                                                ? Container()
                                                : widget.visitor.officeNo!
                                                        .isEmpty
                                                    ? Container()
                                                    : Expanded(
                                                        child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            SizedBox(
                                                              height: 10,
                                                            ),
                                                            Text(
                                                              "${widget.visitor.officeNo}",
                                                              style: TextStyle(
                                                                  fontSize:
                                                                      15.6,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  color: Colors
                                                                      .white),
                                                            ),
                                                            Text(
                                                              'Office No.',
                                                              style: TextStyle(
                                                                fontSize: 11,
                                                                fontStyle:
                                                                    FontStyle
                                                                        .italic,
                                                                color: Colors
                                                                    .white54,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      )
                                          ],
                                        ),
                                        Row(
                                          children: [
                                            widget.visitor.houseNo == null
                                                ? Container()
                                                : widget.visitor.houseNo!
                                                        .isEmpty
                                                    ? Container()
                                                    : Expanded(
                                                        child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            SizedBox(
                                                              height: 10,
                                                            ),
                                                            Text(
                                                              "${widget.visitor.houseNo}",
                                                              style: TextStyle(
                                                                  fontSize:
                                                                      15.6,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  color: Colors
                                                                      .white),
                                                            ),
                                                            Text(
                                                              "House No.",
                                                              style: TextStyle(
                                                                fontSize: 11,
                                                                fontStyle:
                                                                    FontStyle
                                                                        .italic,
                                                                color: Colors
                                                                    .white54,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                            widget.visitor.blockNo == null
                                                ? Container()
                                                : widget.visitor.blockNo!
                                                        .isEmpty
                                                    ? Container()
                                                    : Expanded(
                                                        child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            SizedBox(
                                                              height: 10,
                                                            ),
                                                            Text(
                                                              "${widget.visitor.blockNo}",
                                                              style: TextStyle(
                                                                  fontSize:
                                                                      15.6,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  color: Colors
                                                                      .white),
                                                            ),
                                                            Text(
                                                              'Block No.',
                                                              style: TextStyle(
                                                                fontSize: 11,
                                                                fontStyle:
                                                                    FontStyle
                                                                        .italic,
                                                                color: Colors
                                                                    .white54,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      )
                                          ],
                                        ),
                                        Row(
                                          children: [
                                            Expanded(
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  SizedBox(
                                                    height: 10,
                                                  ),
                                                  Text(
                                                    "${widget.visitor.visitReason ?? ''}",
                                                    style: TextStyle(
                                                        fontSize: 15.6,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color: Colors.white),
                                                  ),
                                                  Text(
                                                    'reason',
                                                    style: TextStyle(
                                                      fontSize: 11,
                                                      fontStyle:
                                                          FontStyle.italic,
                                                      color: Colors.white54,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                        Row(
                                          children: [
                                            Expanded(
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        vertical: 10),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      "${widget.visitor.visitorName?.trim() ?? ''}",
                                                      style: TextStyle(
                                                          fontSize: 15.6,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color: Colors.white),
                                                    ),
                                                    Text(
                                                      'Name',
                                                      style: TextStyle(
                                                        fontSize: 11,
                                                        fontStyle:
                                                            FontStyle.italic,
                                                        color: Colors.white54,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                            Expanded(
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.symmetric(
                                                        vertical: 10),
                                                child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  children: [
                                                    Text(
                                                      "${widget.visitor.documentNumber?.trim() ?? ''}",
                                                      style: TextStyle(
                                                          fontSize: 15.6,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color: Colors.white),
                                                    ),
                                                    Text(
                                                      'ID/Passport',
                                                      style: TextStyle(
                                                        fontSize: 11,
                                                        fontStyle:
                                                            FontStyle.italic,
                                                        color: Colors.white54,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            )
                                          ],
                                        ),
                                        Row(
                                          children: [
                                            Expanded(
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    "${widget.visitor.plateNo?.trim() ?? ''}",
                                                    style: TextStyle(
                                                        fontSize: 15.6,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color: Colors.white),
                                                  ),
                                                  Text(
                                                    'Plate No',
                                                    style: TextStyle(
                                                      fontSize: 11,
                                                      fontStyle:
                                                          FontStyle.italic,
                                                      color: Colors.white54,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Expanded(
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    "${widget.visitor.passId ?? ''}",
                                                    style: TextStyle(
                                                        fontSize: 15.6,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color: Colors.white),
                                                  ),
                                                  Text(
                                                    'Pass ID',
                                                    style: TextStyle(
                                                      fontSize: 11,
                                                      fontStyle:
                                                          FontStyle.italic,
                                                      color: Colors.white54,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            )
                                          ],
                                        ),
                                        Row(
                                          children: [
                                            widget.visitor.vehiclePass == null
                                                ? Container()
                                                : widget.visitor.vehiclePass!
                                                        .isEmpty
                                                    ? Container()
                                                    : Expanded(
                                                        child: Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          children: [
                                                            Text(
                                                              "${widget.visitor.vehiclePass}",
                                                              style: TextStyle(
                                                                  fontSize:
                                                                      15.6,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                  color: Colors
                                                                      .white),
                                                            ),
                                                            Text(
                                                              'Vehicle Pass',
                                                              style: TextStyle(
                                                                fontSize: 11,
                                                                fontStyle:
                                                                    FontStyle
                                                                        .italic,
                                                                color: Colors
                                                                    .white54,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      )
                                          ],
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 8.0),
                                              child: Row(
                                                children: [
                                                  Expanded(
                                                    child: Tags(
                                                      itemCount: widget.visitor
                                                              .items?.length ??
                                                          0,
                                                      alignment:
                                                          WrapAlignment.start,
                                                      itemBuilder: (int index) {
                                                        var type = widget
                                                            .visitor
                                                            .items?[index];
                                                        return Tooltip(
                                                            message:
                                                                "S/N: ${type?.serialNo ?? ''}",
                                                            child: ItemTags(
                                                              textStyle: TextStyle(
                                                                  fontSize: 14,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .normal,
                                                                  color: fromHex(
                                                                      yellow)),
                                                              color: Colors
                                                                  .white10
                                                                  .withOpacity(
                                                                      0.07),
                                                              activeColor: Colors
                                                                  .white10
                                                                  .withOpacity(
                                                                      0.07),
                                                              singleItem: true,
                                                              index: index,
                                                              title:
                                                                  type?.name ??
                                                                      '',
                                                              textColor:
                                                                  Colors.white,
                                                            ));
                                                      },
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 10, top: 2),
                                              child: Text(
                                                'Items',
                                                style: TextStyle(
                                                  fontSize: 11,
                                                  fontStyle: FontStyle.italic,
                                                  color: Colors.white54,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Positioned(
                                bottom: 6,
                                left: 10,
                                right: 10,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                      height: 40,
                                      width: 200,
                                      decoration: BoxDecoration(
                                        color: fromHex(deepOrange),
                                        borderRadius: BorderRadius.circular(15),
                                      ),
                                      child: Row(
                                        children: [
                                          Expanded(
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Text(
                                                    "${widget.visitor.datetimeIn?.substring(10).trim() ?? ''}"),
                                                Text(
                                                  'IN',
                                                  style:
                                                      TextStyle(fontSize: 11),
                                                )
                                              ],
                                            ),
                                          ),
                                          Icon(
                                            Icons.access_time,
                                            color: Colors.black,
                                          ),
                                          Expanded(
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                Text(
                                                    "${widget.visitor.datetimeOut ?? ''}"),
                                                Text(
                                                  'OUT',
                                                  style:
                                                      TextStyle(fontSize: 11),
                                                )
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ))
                          ],
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
