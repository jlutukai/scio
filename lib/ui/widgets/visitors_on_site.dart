import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:scio_security/core/enums/view_state.dart';
import 'package:scio_security/core/models/create_visitor_response.dart';
import 'package:scio_security/core/models/local/localModels.dart';
import 'package:scio_security/core/viewmodels/visitors_log_model.dart';
import 'package:scio_security/core/viewmodels/visitors_onsite_model.dart';
import 'package:scio_security/ui/screens/base_view.dart';
import 'package:scio_security/ui/widgets/onsite_list_item.dart';
import 'package:scio_security/utils/useful.dart';

class VisitorOnSite extends StatefulWidget {
  final VisitorsLogModel model;
  VisitorOnSite(this.model);

  @override
  _VisitorOnSiteState createState() => _VisitorOnSiteState();
}

class _VisitorOnSiteState extends State<VisitorOnSite> {
  final visitors = Hive.box('visitors_in');
  List<Visitor> _visitors = [];

  @override
  Widget build(BuildContext context) {
    return BaseView<VisitorOnSiteModel>(
      onModelReady: (model) async {
        final DateTime maxDate = DateTime.now();
        final DateTime minDate = DateTime.now().subtract(Duration(days: 1));
        final DateFormat formatter = DateFormat('yyyy-MM-dd HH:mm:ss');
        String max = formatter.format(maxDate);
        String min = formatter.format(minDate);
        Map<String, String> data = {
          'datetime_in_min': "$min",
          'datetime_in_max': "$max",
          'site_id': "${getPlanner()!.plannerId}"
        };
        var r = await model.refreshVisitors(data);
        if (r['success']) {
        } else {
          showErrorDialog(r['response'], context, "get visitors");
        }
        if (visitors.isNotEmpty) {
          _visitors.addAll(visitors.values as Iterable<Visitor>);
        }
        _visitors.addAll(model.visitorsIn);
      },
      builder: (context, model, child) => Container(
        margin: EdgeInsets.only(top: 15),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          appBar: AppBar(
            elevation: 0,
            backgroundColor: Colors.transparent,
            title: Text(
              "On Site (${_visitors.length})",
              style: TextStyle(
                  fontWeight: FontWeight.bold, color: fromHex(deepOrange)),
            ),
            centerTitle: true,
            automaticallyImplyLeading: false,
            actions: [
              visitors.values.isNotEmpty
                  ? IconButton(
                      onPressed: () async {
                        bool isConnected = await checkConnection();
                        if (!isConnected) {
                          showToast(
                              'No connection to DB, Check Internet Connection!');
                          return;
                        }
                        sync(model);
                      },
                      icon: Icon(
                        Icons.sync_rounded,
                        color: Colors.white,
                      ))
                  : Container(),
            ],
          ),
          body: Column(
            children: [
              Expanded(
                child: model.state == ViewState.Idle
                    ? ListView.builder(
                        physics: BouncingScrollPhysics(),
                        itemCount: _visitors.length,
                        itemBuilder: (context, index) =>
                            OnsiteListItem(_visitors[index]),
                      )
                    : Center(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                                height: 200,
                                width: 200,
                                child: Lottie.asset('assets/loading.json')),
                            Text(
                              'Please wait \n Loading...',
                              style: TextStyle(color: fromHex(deepOrange)),
                            )
                          ],
                        ),
                      ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void sync(VisitorOnSiteModel model) {
    for (var i = 0; i <= visitors.values.length; i++) {
      if (i <= visitors.values.length - 1) {
        sendDataToDb(visitors.getAt(i), i, model);
      }
    }
    refreshData(model);
  }

  Future<void> sendDataToDb(
      Visitor _visitor, int i, VisitorOnSiteModel model) async {
    Map<String, dynamic> data = {
      'visitor_name': "${_visitor.visitorName}",
      'host_name': "${_visitor.hostName}",
      'department': "${_visitor.department}",
      'designation': "${_visitor.designation}",
      'no_visitors': "${_visitor.noVisitors}",
      'documentNumber': "${_visitor.documentNumber}",
      'document_type': "${_visitor.documentType}",
      'plateNo': "${_visitor.plateNo}",
      'pass_id': "${_visitor.passId}",
      'visit_reason': "${_visitor.visitReason}",
      'datetime_in': "${_visitor.datetimeIn}",
      'datetime_out': null,
      'item': _visitor.items,
      'site_id': "${getPlanner()!.plannerId}",
      'status': "In",
    };
    try {
      var r = await model.createVisitor(data);
      if (r['success']) {
        var d = CreateVisitorResponse.fromJson(r['response']);
        if (!d.error!) {
          visitors.deleteAt(i);
        } else {
          showToast(d.message ?? "Failed");
        }
      } else {
        showErrorDialog(r['response'], context, "check in visitor");
      }
    } catch (e) {
      showToast(e.toString());
    }
  }

  Future<void> refreshData(VisitorOnSiteModel model) async {
    final DateTime maxDate = DateTime.now();
    final DateTime minDate = DateTime.now().subtract(Duration(days: 1));
    final DateFormat formatter = DateFormat('yyyy-MM-dd HH:mm:ss');
    String max = formatter.format(maxDate);
    String min = formatter.format(minDate);
    Map<String, String> data = {
      'datetime_in_min': "$min",
      'datetime_in_max': "$max",
      'site_id': "${getPlanner()!.plannerId}"
    };
    var r = await model.refreshVisitors(data);
    if (r['success']) {
    } else {
      showErrorDialog(r['response'], context, "get visitors");
    }
    _visitors.clear();
    if (visitors.isNotEmpty) {
      _visitors.addAll(visitors.values as Iterable<Visitor>);
    }
    _visitors.addAll(model.visitorsIn);
    setState(() {});
  }
}
