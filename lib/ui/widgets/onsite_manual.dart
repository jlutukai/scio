import 'dart:async';
import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:lottie/lottie.dart';
import 'package:path/path.dart' as path;
import 'package:scio_security/core/enums/view_state.dart';
import 'package:scio_security/core/models/general_response.dart';
import 'package:scio_security/core/models/image_verification_response.dart';
import 'package:scio_security/core/models/local/localModels.dart';
import 'package:scio_security/core/services/api.dart';
import 'package:scio_security/core/viewmodels/guards_manual_onsite_model.dart';
import 'package:scio_security/core/viewmodels/timesheet_manual_model.dart';
import 'package:scio_security/ui/screens/base_view.dart';
import 'package:scio_security/utils/useful.dart';

import '../../core/models/time_sheet_response.dart';
import 'biometric_capture.dart';
import 'guard_info.dart';

class OnSiteManual extends StatefulWidget {
  OnSiteManual();

  @override
  _OnSiteManualState createState() => _OnSiteManualState();
}

class _OnSiteManualState extends State<OnSiteManual> {
  final GlobalKey<ScaffoldState> _scaffoldKey1 = GlobalKey<ScaffoldState>();
  // final _timeSheet = Hive.box('time_sheet_unsaved_manual');
  File? _image;
  String? biometricId;
  final String status = "In";
  String? date;
  String? shiftName;
  Shifts? _shift;

  @override
  void initState() {
    super.initState();
    _shift = getShift();
    date = getShiftDate();
    shiftName =
        "${_shift?.shiftName} ${_shift?.shiftChangeFrom} - ${_shift?.shiftChangeTo}";
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<OnSiteManualModel>(
      onModelReady: (model) async {
        Map<String, String> data = {
          'date': "$date",
          'shift_id': "${_shift?.id ?? 0}",
          'planner_id': "${getPlanner()!.plannerId}",
        };
        bool isConnected = await checkConnection();
        if (isConnected) {
          var r = await model.getTimeSheetRemote(data);
          await getTimeSheetsLocal(model);
          if (r['success']) {
            var d = TimeSheetResponse.fromJson(r['response']);
            if (d.data != null) {
            } else {
              if (d.data!.isEmpty) {
                showToast("");
              }
            }
          } else {
            showErrorDialog(r['response'], context, "get time sheet details");
          }
        }
        await getTimeSheetsLocal(model);
      },
      builder: (context, model, child) => Scaffold(
        backgroundColor: Colors.black,
        key: _scaffoldKey1,
        body: Column(
          children: [
            model.state == ViewState.Idle
                ? Padding(
                    padding: const EdgeInsets.only(left: 80, bottom: 20),
                    child: GestureDetector(
                      onTap: () {
                        showShiftsDialog(getPlanner()!, context, model);
                      },
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Text(
                                    'Date',
                                    style: TextStyle(
                                        color: fromHex(yellow),
                                        fontWeight: FontWeight.normal,
                                        fontSize: 16),
                                  ),
                                  Text(
                                    "  $date",
                                    style: TextStyle(
                                        color: fromHex(deepOrange),
                                        fontWeight: FontWeight.bold,
                                        fontSize: 14),
                                  )
                                ],
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Text(
                                    'Shift',
                                    style: TextStyle(
                                        color: fromHex(yellow),
                                        fontWeight: FontWeight.normal,
                                        fontSize: 16),
                                  ),
                                  Text(
                                    "  $shiftName",
                                    style: TextStyle(
                                        color: fromHex(deepOrange),
                                        fontWeight: FontWeight.bold,
                                        fontSize: 14),
                                  )
                                ],
                              ),
                            ],
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Icon(
                            Icons.calendar_today_outlined,
                            color: fromHex(yellow),
                          )
                        ],
                      ),
                    ),
                  )
                : Container(),
            Expanded(
                child: model.state == ViewState.Idle
                    ? _detailsList(model, context, model.timeSheet)
                    : Center(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Container(
                                height: 200,
                                width: 200,
                                child: Lottie.asset('assets/loading.json')),
                            Text(
                              'Please wait \n Loading...',
                              style: TextStyle(color: fromHex(deepOrange)),
                            )
                          ],
                        ),
                      )),
          ],
        ),
      ),
    );
  }

  Future<void> showShiftsDialog(
      PlannerData p, BuildContext context, OnSiteManualModel model) async {
    final f = new DateFormat('yyyy-MM-dd');

    List<String> _reasons = ['None', 'Yesterday', 'Today', 'Tomorrow'];
    String? reason = 'None';
    String? d;
    Shifts? s;

    List<String?> _shifts = [];
    String? shift = 'None';
    _shifts.add(shift);
    p.shifts!.forEach((element) {
      _shifts.add(
          "${element.shiftName} ( ${element.shiftChangeFrom}-${element.shiftChangeTo} )");
    });
    await showDialog(
      context: _scaffoldKey1.currentContext!,
      builder: (context) => Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        elevation: 0.0,
        backgroundColor: Colors.grey[900],
        child: StatefulBuilder(
          builder: (context, StateSetter setState) {
            return Container(
              color: Colors.transparent,
              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
              child: Container(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 15,
                      ),
                      Row(
                        children: [
                          Container(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 10.0, right: 10.0),
                              child: Text(
                                'Choose Date',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 11,
                                    fontStyle: FontStyle.italic),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Container(
                        decoration: BoxDecoration(
                            color: Colors.white10,
                            borderRadius:
                                BorderRadius.all(Radius.circular(5.0))),
                        child: DropdownButtonHideUnderline(
                          child: Padding(
                            padding: const EdgeInsets.only(
                                top: 8.0, bottom: 8.0, left: 5.0),
                            child: ButtonTheme(
                              alignedDropdown: true,
                              child: DropdownButton(
                                hint: Text('Specify Date'),
                                isExpanded: true,
                                isDense: true,
                                value: reason,
                                dropdownColor: Colors.grey[800],
                                style: TextStyle(
                                  color: fromHex(yellow),
                                ),
                                onChanged: (String? val) {
                                  DateTime now = DateTime.now();

                                  if (val == 'Yesterday') {
                                    setState(() {
                                      DateTime y =
                                          now.subtract(Duration(days: 1));
                                      d = f.format(y);
                                    });
                                  }
                                  if (val == 'Today') {
                                    setState(() {
                                      d = f.format(now);
                                    });
                                  }
                                  if (val == 'Tomorrow') {
                                    setState(() {
                                      DateTime t = now.add(Duration(days: 1));
                                      d = f.format(t);
                                    });
                                  }
                                  print(d);
                                  setState(() {
                                    reason = val;
                                  });
                                },
                                items: _reasons.map<DropdownMenuItem<String>>(
                                    (String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                }).toList(),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      Row(
                        children: [
                          Container(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: 10.0, right: 10.0),
                              child: Text(
                                'Choose Shift',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 11,
                                    fontStyle: FontStyle.italic),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Container(
                        decoration: BoxDecoration(
                            color: Colors.white10,
                            borderRadius:
                                BorderRadius.all(Radius.circular(5.0))),
                        child: DropdownButtonHideUnderline(
                          child: Padding(
                            padding: const EdgeInsets.only(
                                top: 8.0, bottom: 8.0, left: 5.0),
                            child: ButtonTheme(
                              alignedDropdown: true,
                              child: DropdownButton(
                                hint: Text('Specify shift'),
                                isExpanded: true,
                                isDense: true,
                                value: shift,
                                dropdownColor: Colors.grey[800],
                                style: TextStyle(
                                  color: fromHex(yellow),
                                ),
                                onChanged: (String? val) {
                                  setState(() {
                                    shift = val;

                                    p.shifts!.forEach((element) {
                                      if ("${element.shiftName} ( ${element.shiftChangeFrom}-${element.shiftChangeTo} )" ==
                                          val) {
                                        s = element;
                                      }
                                    });
                                  });
                                  print(s!.id);
                                },
                                items: _shifts.map<DropdownMenuItem<String>>(
                                    (String? value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value!),
                                  );
                                }).toList(),
                              ),
                            ),
                          ),
                        ),
                      ),
                      ListTile(
                        onTap: () async {
                          if (d == null) {
                            showToast("Please choose date");
                            return;
                          }
                          if (d == 'None') {
                            showToast("Please choose date");
                            return;
                          }
                          if (s == null) {
                            showToast("Please choose shift");
                            return;
                          }
                          setState(() {
                            _shift = s!;
                            shiftName =
                                "${s!.shiftName} ${s!.shiftChangeFrom} - ${s!.shiftChangeTo}";
                            date = "$d";
                          });

                          Map<String, String> data = {
                            'date': "$d",
                            'shift_id': "${s!.id}",
                            'planner_id': "${getPlanner()!.plannerId}",
                          };
                          setShiftID(s!.id ?? "0");
                          setShift(s!);
                          setShiftDate(d!);
                          Navigator.pop(context);
                          bool isConnected = await checkConnection();
                          if (isConnected) {
                            var r = await model.getTimeSheetRemote(data);
                            await getTimeSheetsLocal(model);
                            if (r['success']) {
                            } else {
                              showErrorDialog(r['response'], context,
                                  "get time sheet details");
                            }
                          } else {
                            await getTimeSheetsLocal(model);
                          }
                        },
                        title: Center(
                          child: Text(
                            'Submit',
                            style: TextStyle(color: Colors.deepOrange),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  _detailsList(OnSiteManualModel model, BuildContext context,
      List<TimeSheetData> timeSheetData) {
    return timeSheetData.isNotEmpty
        ? ListView.builder(
            physics: BouncingScrollPhysics(),
            itemCount: timeSheetData.length,
            itemBuilder: (context, index) => InkWell(
                  onLongPress: () async {
                    File? img =
                        await _getImageProfile(timeSheetData[index].employeeId);
                  },
                  onTap: () {
                    showDialog(
                      context: context,
                      builder: (BuildContext context) =>
                          CustomDialog(timeSheetData[index], true),
                    );
                  },
                  child: Dismissible(
                    key: UniqueKey(),
                    onDismissed: (DismissDirection direction) {
                      if (direction == DismissDirection.startToEnd)
                        _optionsModel(
                            context, timeSheetData[index], 'c', model);
                      if (direction == DismissDirection.endToStart)
                        reportModal(context, timeSheetData[index], 'c', model);
                      setState(() {});
                    },
                    child: Container(
                      padding:
                          EdgeInsets.symmetric(vertical: 3, horizontal: 15),
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.white10.withOpacity(0.07),
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsets.only(left: 0, right: 10),
                              height: 100,
                              width: 10,
                              decoration: BoxDecoration(
                                  color: timeSheetData[index].reason == null ||
                                          timeSheetData[index]
                                              .reason!
                                              .isEmpty ||
                                          timeSheetData[index]
                                                  .reason!
                                                  .toLowerCase() ==
                                              "null"
                                      ? Colors.green[800]
                                      : Colors.deepOrange[900],
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(10),
                                      bottomLeft: Radius.circular(10))),
                            ),
                            CircleAvatar(
                              radius: 35,
                              backgroundImage: CachedNetworkImageProvider(
                                  timeSheetData[index].fileName != null
                                      ? timeSheetData[index]
                                              .fileName!
                                              .isNotEmpty
                                          ? "${Api().getUrl()}/CUSTOM/Employees/profile_pictures/${timeSheetData[index].fileName}"
                                          : noImage
                                      : noImage),
                            ),
                            Expanded(
                              child: Column(
                                children: [
                                  Text(
                                    "${timeSheetData[index].name}",
                                    style: TextStyle(
                                        color: fromHex(yellow),
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15),
                                  ),
                                  SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Column(
                                        children: [
                                          Text(
                                            "${timeSheetData[index].timeIn}",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontWeight: FontWeight.bold,
                                                fontSize: 12),
                                          ),
                                          SizedBox(
                                            height: 3,
                                          ),
                                          Text(
                                            "Time In",
                                            style: TextStyle(
                                                color: fromHex(deepOrange),
                                                fontSize: 11),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                            Icon(
                              Icons.arrow_forward_ios_sharp,
                              color: fromHex(yellow),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ))
        : Center(
            child: Text(
              'No employees checked in yet',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                  fontWeight: FontWeight.bold),
            ),
          );
  }

  // Future<void> _optionsModel(BuildContext context, TimeSheetData timeSheetData,
  //     String s, OnSiteManualModel model) async {
  //   if (getPlanner()!.verificationMethod == "Fingerprint") {
  //     _confirmation(context, timeSheetData, s, model);
  //     //   Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context) => BioMetricCapture()));
  //   }
  //   if (getPlanner()!.verificationMethod == "Picture") {
  //     var i = await _getImage(timeSheetData.id, s);
  //     _image = i;
  //     _confirmCheckOut(timeSheetData, model);
  //   }
  //
  //   if (getPlanner()!.verificationMethod == "No Verification Required") {
  //     _confirmation(context, timeSheetData, s, model);
  //   }
  // }

  Future<void> _optionsModel(BuildContext context, TimeSheetData timeSheetData,
      String s, OnSiteManualModel model) async {
    if (getPlanner()!.verificationMethod == "Fingerprint") {
      // _confirmation(context, timeSheetData, s, model);
      var result = await Navigator.of(context).push(MaterialPageRoute(
          builder: (BuildContext context) =>
              BioMetricCapture(timeSheetData.biometricId ?? "", true)));
      if (result == null) {
        showToast("Error Occurred While Capturing Biometric Data, try Again");
        return;
      }
      final DateTime now = DateTime.now();
      final DateFormat formatter = DateFormat('HH:mm');
      String timeOut = formatter.format(now);
      _checkOutGuard(
          timeSheetData
            ..biometricOut = result
            ..timeOut = timeOut
            ..status = "Awaiting Approval",
          model);
    }
    if (getPlanner()!.verificationMethod == "Picture") {
      var i = await _getImage(timeSheetData.id, s);
      if (i == null) {
        showToast("Error While Capturing Image, Try Again");
        return;
      }
      _image = i;
      final DateTime now = DateTime.now();
      final DateFormat formatter = DateFormat('HH:mm');
      String timeOut = formatter.format(now);
      _checkOutGuard(
          timeSheetData
            ..imageOut = i.path
            ..timeOut = timeOut
            ..status = "Awaiting Approval",
          model);
    }

    if (getPlanner()!.verificationMethod == "No Verification Required") {
      _confirmation(context, timeSheetData, s, model);
    }
  }

  void _checkOutGuard(TimeSheetData t, OnSiteManualModel model) async {
    var _hasConnection = await checkConnection();
    if (_hasConnection) {
      if (_image != null) {
        try {
          var res = await model.uploadImage(_image!);
          if (res['success']) {
            var resd = ImageVerificationResponse.fromJson(res['response']);
            if (resd.error!) {
              showToast(resd.message!);
            }
          } else {
            showErrorDialog(res['response'], context, "upload image");
          }
        } catch (e) {
          showToast(e.toString());
        }
      }

      Map<String, String> data = {
        "timesheet_id": "${t.id}",
        "status": "${t.status}",
        "time_in": "${t.timeIn}",
        "time_out": "${t.timeOut}",
        "comment_in": "${t.commentIn}",
        "comment_out": '${t.commentOut}',
        "reason": "${t.reason}"
      };
      try {
        var r = await model.updateStatus(data);
        if (r['success']) {
          var d = GeneralResponse.fromJson(r['response']);
          if (!d.error!) {
            showToast('success');
            await model.updateTimesheetLocal(t..syncStatus = syncedStatus);
            await getTimeSheetsLocal(model);
          } else {
            showToast(d.erroMessage!);
          }
        } else {
          showErrorDialog(r['response'], context, "update time sheet");
        }
      } catch (e) {
        showToast(e.toString());
      }
    } else {
      await model.updateTimesheetLocal(t..syncStatus = unSyncedStatus);
      await getTimeSheetsLocal(model);
    }
  }

  Future<void> reportModal(BuildContext context, TimeSheetData timeSheetData,
      String s, OnSiteManualModel model) async {
    TextEditingController _comment = TextEditingController();

    List<String> _reasons = ['None', 'Sick', 'Neglect of Duty', 'Other'];
    String? reason = 'None';

    await showDialog(
        context: _scaffoldKey1.currentContext!,
        builder: (context) => Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            elevation: 0.0,
            backgroundColor: Colors.grey[900],
            child: StatefulBuilder(builder: (context, StateSetter setState) {
              return Container(
                color: Colors.transparent,
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                child: Container(
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: 15,
                        ),
                        Row(
                          children: [
                            Container(
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 10.0, right: 10.0),
                                child: Text(
                                  'Specify Reason',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 11,
                                      fontStyle: FontStyle.italic),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Container(
                          decoration: BoxDecoration(
                              color: Colors.white10,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5.0))),
                          child: DropdownButtonHideUnderline(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  top: 8.0, bottom: 8.0, left: 5.0),
                              child: ButtonTheme(
                                alignedDropdown: true,
                                child: DropdownButton(
                                  hint: Text('Specify reason'),
                                  isExpanded: true,
                                  isDense: true,
                                  value: reason,
                                  dropdownColor: Colors.grey[800],
                                  style: TextStyle(
                                    color: fromHex(yellow),
                                  ),
                                  onChanged: (String? val) {
                                    setState(() {
                                      reason = val;
                                    });
                                  },
                                  items: _reasons.map<DropdownMenuItem<String>>(
                                      (String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Container(
                          child: TextFormField(
                            controller: _comment,
                            keyboardType: TextInputType.text,
                            textCapitalization: TextCapitalization.sentences,
                            maxLines: 6,
                            textAlignVertical: TextAlignVertical.top,
                            style: TextStyle(color: Colors.white),
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "Please Enter Comment";
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              filled: true,
                              fillColor: Colors.white10,
                              isDense: true,
                              hintText: "eg feeling unwell",
                              labelText: "Comment",
                              alignLabelWithHint: true,
                              labelStyle: TextStyle(color: fromHex(yellow)),
                              hintStyle: TextStyle(
                                color: Colors.blueGrey[400],
                              ),
                              border: InputBorder.none,
                              focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: fromHex(yellow)),
                              ),
                            ),
                          ),
                        ),
                        ListTile(
                          onTap: () async {
                            if (reason == "None") {
                              showToast('Please Specify Reason');
                              return;
                            }
                            Navigator.pop(context);
                            _checkOutGuard(
                                timeSheetData
                                  ..commentOut = _comment.text
                                  ..reason = reason,
                                model);
                          },
                          title: Center(
                            child: Text(
                              'Submit Report',
                              style: TextStyle(color: Colors.deepOrange),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            })));
  }

  void _confirmation(BuildContext context, TimeSheetData timeSheetData,
      String s, OnSiteManualModel model) {
    showModalBottomSheet(
        backgroundColor: Colors.transparent,
        clipBehavior: Clip.antiAlias,
        context: context,
        builder: (context) {
          return SingleChildScrollView(
            child: Container(
              color: Colors.transparent,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.grey[900],
                  // color: Theme.of(context).canvasColor,
                  borderRadius: BorderRadius.only(
                      topLeft: const Radius.circular(15),
                      topRight: const Radius.circular(15)),
                ),
                child: Padding(
                  padding: const EdgeInsets.only(right: 20.0, left: 20.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      ListTile(
                        title: Center(
                          child: Text(
                            "Do you want to Check-Out ${timeSheetData.name} ?",
                            style: TextStyle(
                              color: Colors.white,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: InkWell(
                              onTap: () {
                                Navigator.pop(context);
                                final DateTime now = DateTime.now();
                                final DateFormat formatter =
                                    DateFormat('HH:mm');
                                String timeOut = formatter.format(now);
                                _checkOutGuard(
                                    timeSheetData
                                      ..timeOut = timeOut
                                      ..status = "Awaiting Approval",
                                    model);
                              },
                              child: Text(
                                'Confirm',
                                style: TextStyle(
                                    color: Colors.green[800],
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                          Expanded(
                            child: InkWell(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: Text(
                                'Dismiss',
                                style: TextStyle(
                                    color: Colors.deepOrange.shade900,
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      )
                    ],
                  ),
                ),
              ),
            ),
          );
        });
  }

  Future<File?> _getImage(String? timeSheetId, String s) async {
    var p = await ImagePicker()
        .pickImage(source: ImageSource.camera, imageQuality: 10);
    if (p != null) {
      File picture = File(p.path);
      String dir = path.dirname(picture.path);
      String newPath = path.join(dir, "out_$timeSheetId.jpg");
      File photo = picture.renameSync(newPath);
      return photo;
    } else {
      return null;
    }
  }

  // void _confirmCheckOut(
  //     TimeSheetData timeSheetData, OnSiteManualModel model) async {
  //   final DateTime now = DateTime.now();
  //   final DateFormat formatter = DateFormat('HH:mm');
  //   String timeOut = formatter.format(now);
  //   TimeSheetData t = timeSheetData
  //     ..timeOut = timeOut
  //     ..status = "Awaiting Approval"
  //     ..imageOut = '';
  //
  //   bool isConnected = await checkConnection();
  //   if (isConnected) {
  //     if (_image != null) {
  //       try {
  //         var res = await model.uploadImage(_image!);
  //         if (res['success']) {
  //           var resd = ImageVerificationResponse.fromJson(res['response']);
  //           if (resd.error!) {
  //             showToast(resd.message!);
  //           }
  //         } else {
  //           showErrorDialog(res['response'], context, "upload image");
  //         }
  //       } catch (e) {
  //         showToast(e.toString());
  //       }
  //     }
  //
  //     Map<String, String> data = {
  //       "timesheet_id": "${t.id}",
  //       "status": "Awaiting Approval",
  //       "time_in": "${t.timeIn}",
  //       "time_out": "$timeOut",
  //       "reason": "${t.reason}",
  //       "comment_in": "${t.commentIn}",
  //       "comment_out": "${t.commentOut}",
  //     };
  //
  //     var r = await model.updateStatus(data);
  //     if (r['success']) {
  //       var d = GeneralResponse.fromJson(r['response']);
  //       if (!d.error!) {
  //         showToast('success');
  //         // for (var i = 0; i <= widget.model.timeSheet.values.length; i++) { //todo
  //         //   if (widget.model.timeSheet.getAt(i).id == timeSheetData.id) {
  //         //     widget.model.timeSheet.putAt(i, t);
  //         //     break;
  //         //   }
  //         // }
  //         widget.refreshData();
  //       } else {
  //         showToast(d.erroMessage!);
  //       }
  //     } else {
  //       showErrorDialog(r['response'], context, "update time sheet");
  //     }
  //   } else {
  //     if (_image != null) {
  //       String img = _image!.path;
  //
  //       // if (_timeSheet.containsKey(t.id)) { //Todo
  //       //   for (var i = 0; i <= _timeSheet.values.length; i++) {
  //       //     if (_timeSheet.getAt(i).id == t.id) {
  //       //       TimeSheetDetails? sheetDetails = _timeSheet.getAt(i)
  //       //         ..timeOut = timeOut
  //       //         ..status = "Awaiting Approval"
  //       //         ..imageOut = img;
  //       //       _timeSheet.putAt(i, sheetDetails);
  //       //       break;
  //       //     }
  //       //   }
  //       // } else {
  //       //   TimeSheetDetails sheetDetails = TimeSheetDetails(
  //       //       timesheetId: t.id,
  //       //       status: "Awaiting Approval",
  //       //       timeOut: timeOut,
  //       //       timeIn: "${t.timeIn}",
  //       //       imageOut: img,
  //       //       reason: "${t.reason}",
  //       //       commentIn: "${t.commentIn}",
  //       //       commentOut: "${t.commentOut}");
  //       //   _timeSheet.put(t.id, sheetDetails);
  //       // }
  //     } else {
  //       // if (_timeSheet.containsKey(t.id)) { //TODO
  //       //   for (var i = 0; i <= _timeSheet.values.length; i++) {
  //       //     if (_timeSheet.getAt(i).id == t.id) {
  //       //       TimeSheetDetails? sheetDetails = _timeSheet.getAt(i)
  //       //         ..timeOut = timeOut
  //       //         ..status = "Awaiting Approval";
  //       //       _timeSheet.putAt(i, sheetDetails);
  //       //       break;
  //       //     }
  //       //   }
  //       // } else {
  //       //   TimeSheetDetails sheetDetails = TimeSheetDetails(
  //       //       timesheetId: t.id,
  //       //       status: "Awaiting Approval",
  //       //       timeOut: timeOut,
  //       //       timeIn: "${t.timeIn}",
  //       //       reason: "${t.reason}",
  //       //       commentIn: "${t.commentIn}",
  //       //       commentOut: "${t.commentOut}");
  //       //   _timeSheet.put(t.id, sheetDetails);
  //       // }
  //     }
  //
  //     // print("@@@@@@ @@@@ @@@  ${widget.model.timeSheet.values.length}");// todo
  //     // for (var i = 0; i <= widget.model.timeSheet.values.length; i++) {
  //     //   if (widget.model.timeSheet.getAt(i).id == timeSheetData.id) {
  //     //     widget.model.timeSheet.putAt(i, t);
  //     //     break;
  //     //   }
  //     // }
  //     showToast('success');
  //     widget.refreshData();
  //   }
  // }

  Future<File?> _getImageProfile(String? employeeId) async {
    var p = await ImagePicker()
        .pickImage(source: ImageSource.camera, imageQuality: 10);
    if (p != null) {
      File croppedFile = await (ImageCropper()
          .cropImage(sourcePath: p.path, aspectRatioPresets: [
        CropAspectRatioPreset.square
      ], uiSettings: [
        AndroidUiSettings(
            toolbarTitle: 'Crop Image',
            toolbarColor: fromHex(yellow),
            toolbarWidgetColor: Colors.black,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: true),
        IOSUiSettings(
          minimumAspectRatio: 1.0,
        )
      ]) as FutureOr<File>);
      File picture = File(croppedFile.path);
      String dir = path.dirname(picture.path);
      String newPath = path.join(dir, "$employeeId.jpg");
      File photo = picture.renameSync(newPath);
      return photo;
    } else {
      return null;
    }
  }

  Future getTimeSheetsLocal(OnSiteManualModel model) async {
    if (_shift != null && date != null) {
      try {
        await model.getTimesheetLocal(status, date!, _shift!);
      } catch (e) {
        print(e.toString());
      }
    }
  }
}
