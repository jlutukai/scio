import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:scio_security/core/enums/view_state.dart';
import 'package:scio_security/core/models/local/localModels.dart';
import 'package:scio_security/core/services/api.dart';
import 'package:scio_security/utils/useful.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../core/models/time_sheet_response.dart';

class CustomDialog extends StatelessWidget {
  final TimeSheetData timeSheetData;
  final bool? isOnSite;
  CustomDialog(this.timeSheetData, [this.isOnSite]);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Consts.padding),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  dialogContent(BuildContext context) {
    bool _isOnSite = isOnSite ?? false;
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
            top: Consts.avatarRadius + Consts.padding,
            bottom: Consts.padding,
            left: Consts.padding,
            right: Consts.padding,
          ),
          margin: EdgeInsets.only(top: Consts.avatarRadius),
          decoration: new BoxDecoration(
            color: Colors.grey[900],
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(Consts.padding),
            boxShadow: [
              BoxShadow(
                color: Colors.black26,
                blurRadius: 10.0,
                offset: const Offset(0.0, 10.0),
              ),
            ],
          ),
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min, // To make the card compact
              children: <Widget>[
                Text(
                  "${timeSheetData.name}",
                  style: TextStyle(
                    fontSize: 18.0,
                    color: fromHex(yellow),
                    fontWeight: FontWeight.w700,
                  ),
                ),
                // Text(
                //   "()",
                //   textAlign: TextAlign.left,
                //   style: TextStyle(
                //       fontSize: 16.0,
                //       fontStyle: FontStyle.italic
                //   ),
                // ),
//              SizedBox(height: 16.0),
//              Text(
//                customerGroup,
//                textAlign: TextAlign.left,
//                style: TextStyle(
//                  fontSize: 16.0,
//                ),
//              ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(height: 16.0),
                        timeSheetData.staffPhone != null
                            ? timeSheetData.staffPhone!.isNotEmpty
                                ? InkWell(
                                    onTap: () {
                                      launch(
                                          "tel://${timeSheetData.staffPhone}");
                                    },
                                    child: Row(
                                      children: <Widget>[
                                        Icon(
                                          Icons.call,
                                          color: fromHex('#F7B733'),
                                        ),
                                        SizedBox(width: 10.0),
                                        Text(
                                          "${timeSheetData.staffPhone}",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              fontSize: 16.0,
                                              color: Colors.white),
                                        ),
                                      ],
                                    ),
                                  )
                                : Container()
                            : Container(),
                        SizedBox(height: 8.0),
                        timeSheetData.reason != null
                            ? timeSheetData.reason!.isNotEmpty
                                ? Row(
                                    children: <Widget>[
                                      Icon(
                                        Icons.warning_amber_rounded,
                                        color: fromHex(yellow),
                                      ),
                                      SizedBox(width: 10.0),
                                      Text(
                                        "${timeSheetData.reason ?? ''}",
                                        textAlign: TextAlign.left,
                                        style: TextStyle(
                                            fontSize: 16.0,
                                            color: Colors.white),
                                      ),
                                    ],
                                  )
                                : Container()
                            : Container(),
                        timeSheetData.timeIn != null
                            ? timeSheetData.timeIn!.isNotEmpty
                                ? Padding(
                                    padding: const EdgeInsets.only(top: 8.0),
                                    child: Row(
                                      children: <Widget>[
                                        Icon(
                                          Icons.access_time_rounded,
                                          color: fromHex(yellow),
                                        ),
                                        SizedBox(width: 10.0),
                                        Text(
                                          "${timeSheetData.timeIn ?? ''}",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              fontSize: 16.0,
                                              color: Colors.white),
                                        ),
                                      ],
                                    ),
                                  )
                                : Container()
                            : Container(),
                        timeSheetData.timeOut != null
                            ? timeSheetData.timeOut!.isNotEmpty
                                ? Padding(
                                    padding: const EdgeInsets.only(top: 8.0),
                                    child: Row(
                                      children: <Widget>[
                                        Icon(
                                          Icons.access_time,
                                          color: fromHex(yellow),
                                        ),
                                        SizedBox(width: 10.0),
                                        Text(
                                          "${timeSheetData.timeOut ?? ''}",
                                          textAlign: TextAlign.left,
                                          style: TextStyle(
                                              fontSize: 16.0,
                                              color: Colors.white),
                                        ),
                                      ],
                                    ),
                                  )
                                : Container()
                            : Container(),
                      ],
                    ),
                  ),
                ),

                timeSheetData.commentIn != null
                    ? timeSheetData.commentIn!.isNotEmpty
                        ? Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(height: 10.0),
                              Row(
                                children: [
                                  Text(
                                    'Comment In',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: fromHex(yellow),
                                        fontSize: 14),
                                  ),
                                ],
                              ),
                              SizedBox(height: 10.0),
                              Text(
                                "${timeSheetData.commentIn ?? ''}",
                                style: TextStyle(
                                    fontSize: 12, color: Colors.grey[500]),
                              )
                            ],
                          )
                        : Container()
                    : Container(),
                timeSheetData.commentOut != null
                    ? timeSheetData.commentOut!.isNotEmpty
                        ? Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(height: 10.0),
                              Row(
                                children: [
                                  Text(
                                    'Comment Out',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: fromHex(yellow),
                                        fontSize: 14),
                                  ),
                                ],
                              ),
                              SizedBox(height: 10.0),
                              Text(
                                "${timeSheetData.commentOut ?? ''}",
                                style: TextStyle(
                                    fontSize: 12, color: Colors.grey[500]),
                              )
                            ],
                          )
                        : Container()
                    : Container(),

                SizedBox(height: 10.0),
                _isOnSite
                    ? Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          ElevatedButton(
                            onPressed: () {},
                            style: ElevatedButton.styleFrom(
                              primary: fromHex(deepOrange),
                              padding: EdgeInsets.symmetric(
                                horizontal: 50,
                              ),
                              textStyle: TextStyle(
                                  fontSize: 30, fontWeight: FontWeight.bold),
                            ),
                            child: Text(
                              "Request Replace Only",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 18),
                            ),
                          ),
                          ElevatedButton(
                            onPressed: () {},
                            style: ElevatedButton.styleFrom(
                              primary: fromHex(deepOrange),
                              padding: EdgeInsets.symmetric(
                                horizontal: 50,
                              ),
                              textStyle: TextStyle(
                                  fontSize: 30, fontWeight: FontWeight.bold),
                            ),
                            child: Text(
                              "Request Replacement and Checkout",
                              style:
                                  TextStyle(color: Colors.black, fontSize: 18),
                            ),
                          ),
                        ],
                      )
                    : Container(),
                SizedBox(height: 10.0),
                Align(
                  alignment: Alignment.bottomRight,
                  child: RaisedButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16.0),
                      side: BorderSide(
                          color: Colors.deepOrange[800]!,
                          style: BorderStyle.solid,
                          width: 1.0),
                    ),
                    color: Colors.grey[900],
                    child: Text(
                      'Dismiss',
                      style: TextStyle(color: Colors.deepOrange[800]),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        Positioned(
          left: Consts.padding,
          right: Consts.padding,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(20.0),
            child: CachedNetworkImage(
              imageUrl: timeSheetData.fileName != null
                  ? timeSheetData.fileName!.isNotEmpty
                      ? "${Api().getUrl()}/CUSTOM/Employees/profile_pictures/${timeSheetData.fileName}"
                      : noImage
                  : noImage,
              height: 100,
              width: 100,
            ),
          ),
        ),
      ],
    );
  }

  Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }
}

class Consts {
  Consts._();

  static const double padding = 16.0;
  static const double avatarRadius = 60.0;
}
