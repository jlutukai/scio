import 'dart:convert';

import 'package:data_tables/data_tables.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:intl/intl.dart';
import 'package:scio_security/core/models/check_out_visitor_response.dart';
import 'package:scio_security/core/models/create_visitor_response.dart';
import 'package:scio_security/core/models/local/localModels.dart';
import 'package:scio_security/core/viewmodels/visitors_log_model.dart';
import 'package:scio_security/utils/useful.dart';

class VisitorLogs extends StatefulWidget {
  final VisitorsLogModel model;
  VisitorLogs(this.model);

  @override
  _VisitorLogsState createState() => _VisitorLogsState();
}

class _VisitorLogsState extends State<VisitorLogs> {
  int _rowsOffset = 0;
  int _rowsPerPage = PaginatedDataTable.defaultRowsPerPage;
  final visitorsIn = Hive.box('visitors_in');
  final visitorsOut = Hive.box('visitors_out');
  List<Visitor> _visitors = [];

  @override
  void initState() {
    _visitors.addAll(widget.model.visitors!);
    if (visitorsIn.isNotEmpty) {
      _visitors.addAll(visitorsIn.values as Iterable<Visitor>);
    }
    if (visitorsOut.isNotEmpty) {
      _visitors.addAll(visitorsOut.values as Iterable<Visitor>);
    }
    if (_visitors.isNotEmpty) {
      _rowsPerPage = _visitors.length;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 15),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          elevation: 0,
          backgroundColor: Colors.transparent,
          title: Text(
            "Visitor's Logs",
            style: TextStyle(
                color: fromHex(deepOrange), fontWeight: FontWeight.bold),
          ),
          centerTitle: true,
          automaticallyImplyLeading: false,
          actions: [
            visitorsOut.values.isNotEmpty
                ? IconButton(
                    onPressed: () async {
                      bool isConnected = await checkConnection();
                      if (!isConnected) {
                        showToast('No connection, Check Internet Connection!');
                        return;
                      }
                      sync();
                    },
                    icon: Icon(
                      Icons.sync_rounded,
                      color: Colors.white,
                    ))
                : Container(),
          ],
        ),
        body: Container(
          child: Column(
            children: [
              Expanded(
                child: _visitors.isNotEmpty
                    ? NativeDataTable.builder(
                        header: Container(),
                        alwaysShowDataTable: true,
                        sortAscending: false,
                        rowsPerPage: _rowsPerPage,
                        firstRowIndex: _rowsOffset,
                        columns: <DataColumn>[
                          DataColumn(
                              label: Text(
                            '#',
                            style: TextStyle(color: fromHex(deepOrange)),
                          )),
                          DataColumn(
                              label: Text(
                            'Name',
                            style: TextStyle(color: fromHex(deepOrange)),
                          )),
                          DataColumn(
                              label: Text(
                            'ID/Passport',
                            style: TextStyle(color: fromHex(deepOrange)),
                          )),
                          DataColumn(
                              label: Text(
                            'Host',
                            style: TextStyle(color: fromHex(deepOrange)),
                          )),
                          DataColumn(
                              label: Text(
                            'Time In',
                            style: TextStyle(color: fromHex(deepOrange)),
                          )),
                          DataColumn(
                              label: Text(
                            'Time Out',
                            style: TextStyle(color: fromHex(deepOrange)),
                          )),
                        ],
                        itemCount: _visitors.length,
                        itemBuilder: (int index) {
                          var visitor = _visitors[index];
                          return DataRow.byIndex(
                              index: index,
                              cells: <DataCell>[
                                DataCell(
                                    Text(
                                      "${index + 1}",
                                      style: TextStyle(color: fromHex(yellow)),
                                    ), onTap: () {
                                  _viewData(visitor, context);
                                }),
                                DataCell(
                                    Text(
                                      "${visitor.visitorName}",
                                      style: TextStyle(color: fromHex(yellow)),
                                    ), onTap: () {
                                  _viewData(visitor, context);
                                }),
                                DataCell(
                                    Text(
                                      "${visitor.documentNumber}",
                                      style: TextStyle(color: fromHex(yellow)),
                                    ), onTap: () {
                                  _viewData(visitor, context);
                                }),
                                DataCell(
                                    Text(
                                      "${visitor.hostName}",
                                      style: TextStyle(color: fromHex(yellow)),
                                    ), onTap: () {
                                  _viewData(visitor, context);
                                }),
                                DataCell(
                                    Text(
                                      "${visitor.datetimeIn}",
                                      style: TextStyle(color: fromHex(yellow)),
                                    ), onTap: () {
                                  _viewData(visitor, context);
                                }),
                                DataCell(
                                    Text(
                                      "${visitor.datetimeOut}",
                                      style: TextStyle(color: fromHex(yellow)),
                                    ), onTap: () {
                                  _viewData(visitor, context);
                                }),
                              ]);
                        },
                        rowCountApproximate: false,
                        handleNext: () async {
                          setState(() {
                            _rowsOffset += _rowsPerPage;
                          });
                        },
                        handlePrevious: () {
                          setState(() {
                            _rowsOffset -= _rowsPerPage;
                          });
                        },
                        mobileIsLoading: CupertinoActivityIndicator(),
                        noItems: Text("No Items Found"),
                        actions: <Widget>[],
                      )
                    : Center(
                        child: Text('No Logs Found !'),
                      ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _viewData(Visitor visitor, BuildContext context) {}

  void sync() {
    for (var i = 0; i <= visitorsOut.values.length; i++) {
      sendDataToDb(visitorsOut.getAt(i), i);
    }
    refreshData();
  }

  Future<void> sendDataToDb(Visitor? _visitor, int i) async {
    bool isConnected = await checkConnection();
    if (isConnected) {
      if (_visitor!.id == null || _visitor.id!.isEmpty) {
        var b = jsonEncode(_visitor.items!.map((e) => e.toJson()).toList());
        Map<String, dynamic> data = {
          'visitor_name': "${_visitor.visitorName}",
          'host_name': "${_visitor.hostName}",
          'department': "${_visitor.department}",
          'designation': "${_visitor.designation}",
          'no_visitors': "${_visitor.noVisitors}",
          'documentNumber': "${_visitor.documentNumber}",
          'document_type': "${_visitor.documentType}",
          'plateNo': "${_visitor.plateNo}",
          'pass_id': "${_visitor.passId}",
          'visit_reason': "${_visitor.visitReason}",
          'datetime_in': "${_visitor.datetimeIn}",
          'datetime_out': "${_visitor.datetimeOut}",
          'item': _visitor.items,
          'site_id': "${getPlanner()!.plannerId}",
          'status': "Out",
        };
        try {
          var r = await widget.model.createVisitor(data);
          if (r['success']) {
            var d = CreateVisitorResponse.fromJson(r['response']);
            if (!d.error!) {
              // showToast("${_visitor.visitorName} has been successfully checked out");
              visitorsOut.deleteAt(i);
              // refreshData();
            } else {
              showToast(d.message ?? "Failed");
            }
          } else {
            showErrorDialog(r['response'], context, "visitor check out");
          }
        } catch (e) {
          showToast(e.toString());
        }
      }
      if (_visitor.id != null || _visitor.id!.isNotEmpty) {
        Map<String, String> data = {
          'visitor_id': "${_visitor.id}",
          'datetime_out': "${_visitor.datetimeOut}",
        };
        try {
          var r = await widget.model.checkOutVisitor(data);
          if (r['success']) {
            var d = CheckOutVisitorResponse.fromJson(r['response']);
            if (!d.error!) {
              // showToast("${_visitor.visitorName} has been successfully checked out");
              visitorsOut.deleteAt(i);
              // refreshData();
            } else {
              showToast(d.message ?? "Failed");
            }
          }
        } catch (e) {
          showToast(e.toString());
        }
      }
    }
  }

  Future<void> refreshData() async {
    final DateTime maxDate = DateTime.now();
    final DateTime minDate = DateTime.now().subtract(Duration(days: 1));
    final DateFormat formatter = DateFormat('yyyy-MM-dd HH:mm:ss');
    String max = formatter.format(maxDate);
    String min = formatter.format(minDate);
    Map<String, String> data = {
      'datetime_in_min': "$min",
      'datetime_in_max': "$max",
      'site_id': "${getPlanner()!.plannerId}"
    };

    var r = await widget.model.refreshVisitors(data);
    if (r['success']) {
    } else {
      showErrorDialog(r['response'], context, "get visitors");
    }
    _visitors.clear();
    _visitors.addAll(widget.model.visitors!);
    if (visitorsIn.isNotEmpty) {
      _visitors.addAll(visitorsIn.values as Iterable<Visitor>);
    }
    if (visitorsOut.isNotEmpty) {
      _visitors.addAll(visitorsOut.values as Iterable<Visitor>);
    }
    setState(() {});
  }
}
