import 'package:external_fp_reader/external_fp_reader.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:lottie/lottie.dart';

import '../../utils/useful.dart';

class BioMetricCapture extends StatefulWidget {
  final String biometricId;
  final bool isRequired;
  BioMetricCapture(this.biometricId, this.isRequired);

  @override
  _BioMetricCaptureState createState() => _BioMetricCaptureState();
}

class _BioMetricCaptureState extends State<BioMetricCapture> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Column(
        children: [
          Expanded(
              child: Center(
            child: Container(
                height: 400, child: Lottie.asset('assets/biometric.json')),
          )),
          Dismissible(
            key: UniqueKey(),
            direction: DismissDirection.startToEnd,
            onDismissed: (DismissDirection direction) async {
              setState(() {});
              var result = await ExternalFpReader.scan();
              if (widget.biometricId != result && widget.isRequired) {
                showToast("Bio metric data does not Match, Try Again");
                return;
              }
              Navigator.pop(context, result);
            },
            child: InkWell(
              onTap: () async {
                var result = await ExternalFpReader.scan();
                if (widget.biometricId != result && widget.isRequired) {
                  showToast("Bio metric data does not Match, Try Again");
                  return;
                }
                Navigator.pop(context, result);
              },
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 5),
                child: Card(
                  color: Colors.white10.withOpacity(0.07),
                  child: Container(
                    // color: fromHex("#c19d3d"),
                    height: 60,
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SvgPicture.asset(
                              "assets/attendance.svg",
                              width: 36,
                              height: 36,
                              color: fromHex(yellow),
                            ),
                            SizedBox(
                              width: 60,
                            ),
                            Text(
                              "Task Manager",
                              style: TextStyle(
                                  color: fromHex(yellow),
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18),
                            )
                          ],
                        ),
                        Icon(
                          Icons.arrow_forward_ios_sharp,
                          color: fromHex(yellow),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
