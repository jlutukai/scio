// import 'package:animated_qr_code_scanner/AnimatedQRViewController.dart';
// import 'package:animated_qr_code_scanner/animated_qr_code_scanner.dart';
// import 'package:flutter/material.dart';
// import 'package:scio_security/utils/useful.dart';
//
// class TestPage extends StatelessWidget {
//   static const tag = 'qr';
//   final AnimatedQRViewController controller = AnimatedQRViewController();
//   final void Function(String passId) getPassId;
//
//   TestPage({this.getPassId});
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Column(
//         children: [
//           Expanded(
//             child: AnimatedQRView(
//               squareColor: fromHex(yellow).withOpacity(0.25),
//               animationDuration: const Duration(milliseconds: 400),
//               onScanBeforeAnimation: (String str) {
//                 print('Callback at the beginning of animation: $str');
//               },
//               onScan: (String str) async {
//                 await showDialog(
//                   context: context,
//                   builder: (context) => AlertDialog(
//                     backgroundColor: Colors.black,
//                     title: Text("Pass ID : $str", style: TextStyle(color: fromHex(yellow)),),
//                     actions: [
//                       FlatButton(
//                         child: Text('OK'),
//                         textColor: Colors.green,
//                         onPressed: () {
//                           print("this is the pass id : $str");
//                           getPassId(str);
//                           Navigator.of(context).pop();
//                           Navigator.pop(context, str);
//                         },
//                       ),
//                       FlatButton(
//                         child: Text('Rescan'),
//                         textColor: Colors.deepOrange,
//                         onPressed: () {
//                           Navigator.of(context).pop();
//                           controller.resume();
//                         },
//                       ),
//                     ],
//                   ),
//                 );
//               },
//               controller: controller,
//             ),
//           ),
//           // Container(
//           //   child: Row(
//           //     mainAxisAlignment: MainAxisAlignment.center,
//           //     children: <Widget>[
//           //       FlatButton(
//           //         color: Colors.blue,
//           //         child: Text('Flash'),
//           //         onPressed: () {
//           //           controller.toggleFlash();
//           //         },
//           //       ),
//           //       const SizedBox(width: 10),
//           //       FlatButton(
//           //         color: Colors.blue,
//           //         child: Text('Flip'),
//           //         onPressed: () {
//           //           controller.flipCamera();
//           //         },
//           //       ),
//           //       const SizedBox(width: 10),
//           //       FlatButton(
//           //         color: Colors.blue,
//           //         child: Text('Resume'),
//           //         onPressed: () {
//           //           controller.resume();
//           //         },
//           //       ),
//           //     ],
//           //   ),
//           // ),
//         ],
//       ),
//     );
//   }
// }